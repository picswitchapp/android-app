package com.picswitch;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.codec.binary.Base64;
import com.squareup.picasso.Picasso;

public class UploadProfileImage extends Activity {

	Context mContext;
	Typeface montSerrat;

	ProgressDialog mDialog;

	ImageView profileImage;

	private static final int PICK_PHOTO_FOR_AVATAR = 3;
	private static final int RESULT_CROP = 0;
	private static final int RESULT_OK = -1;

	private static final String TAG = "UploadProfileImage";

	private boolean externalStorageAvailable;

	public static final String TEMP_PHOTO_FILE_NAME = "temp_photo.jpg";
	protected File mFileTemp;
	protected Uri resultCropUri;

	private ImageView img;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_upload_profile_image);

		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
			window.setStatusBarColor(Color.rgb(32, 99, 99));
		}

		mContext = UploadProfileImage.this;

		montSerrat = Typeface.createFromAsset(mContext.getAssets(),"Montserrat-Regular.ttf");

		mDialog = new ProgressDialog(mContext);
		mDialog.setMessage("Loading image...");
		mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

		//set up views and fonts
		TextView titleText = (TextView) findViewById(R.id.title_text_upload_profile_image);
		titleText.setTypeface(montSerrat);
		TextView buttonLabel = (TextView) findViewById(R.id.upload_new_image_button_text);
		FontsUtils.TypeFace(buttonLabel, getAssets());
		profileImage = (ImageView) findViewById(R.id.profile_image);

		//click listeners
		ImageView leftArrow = (ImageView) findViewById(R.id.left_arrow_upload_profile_image);
		leftArrow.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				UploadProfileImage.this.finish();
			}
		});

		RelativeLayout uploadButton = (RelativeLayout) findViewById(R.id.upload_new_image_button);
		uploadButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
				intent.setType("image/*");
				startActivityForResult(intent, PICK_PHOTO_FOR_AVATAR);
			}
		});

		//set the imageView to the one on the user's table entry
		ParseFile mFile = null;
		try{
			mFile = ParseUser.getCurrentUser().getParseFile(ParseConstants.KEY_PROFILE_PICTURE);
		}catch(Exception e){
			e.printStackTrace();
		}

		if(mFile != null){
			Uri pictureUri = Uri.parse(mFile.getUrl());
			Picasso.with(mContext)
			.load(pictureUri)
			.placeholder(R.drawable.picasso_loading_animation)
			.into(profileImage);
		}else{
			//put default image there
			profileImage.setImageDrawable(getResources().getDrawable(R.drawable.no_profile_image));
		}

		String state = Environment.getExternalStorageState();
		if(state.equals(Environment.MEDIA_MOUNTED)){
			mFileTemp = new File(Environment.getExternalStorageDirectory(), TEMP_PHOTO_FILE_NAME);
			externalStorageAvailable = true;
		}
		else{
			mFileTemp = new File(mContext.getFilesDir(), TEMP_PHOTO_FILE_NAME);
			externalStorageAvailable = false;
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data){
		if(resultCode == RESULT_OK){
			switch(requestCode){
			case PICK_PHOTO_FOR_AVATAR:{
				InputStream inputStream;
				try {
					inputStream = mContext.getContentResolver().openInputStream(data.getData());
					FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
					copyStream(inputStream, fileOutputStream);
					fileOutputStream.close();
					inputStream.close();
					crop();
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			case RESULT_CROP:{
				String path = data.getStringExtra(CropImage.IMAGE_PATH);
				if (path == null) { 
					return;
				}else{					
					//bmp = BitmapFactory.decodeFile(mFileTemp.getPath());
					new BitmapPathLoader(path).execute();
				}
			}
			}
		}
	}

	public static void copyStream(InputStream input, OutputStream output) throws IOException {
		byte[] buffer = new byte[1024];
		int bytesRead;
		while ((bytesRead = input.read(buffer)) != -1) {
			output.write(buffer, 0, bytesRead);
		}
	}

	/**cropping helper*/
	private void crop() {

		ExifInterface exif = null;
		try {
			exif = new ExifInterface(mFileTemp.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}  
		int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);

		// create explicit intent
		Intent intent = new Intent(mContext, CropImage.class);

		// tell CropImage activity to look for image to crop 
		//	    String filePath = photoUri.toString();
		intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());
		intent.putExtra(CropImage.IMAGE_ORIENTATION, orientation);

		// allow CropImage activity to rescale image
		intent.putExtra(CropImage.SCALE, true);

		// aspect ratio is fixed to square
		intent.putExtra(CropImage.ASPECT_X, 1);
		intent.putExtra(CropImage.ASPECT_Y, 1);

		// start activity CropImage with certain request code and listen
		// for result
		startActivityForResult(intent, RESULT_CROP);
		//		resultCropUri = intent.getData();
	}


	public class BitmapPathLoader extends AsyncTask<Void, Void, Bitmap>{

		String mPath;

		public BitmapPathLoader(String path){
			mPath = path;
		}

		@Override
		protected void onPreExecute(){
			mDialog.show();
		}

		@Override
		protected Bitmap doInBackground(Void... params) {
			Bitmap temp = BitmapFactory.decodeFile(mFileTemp.getPath());

			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			temp.compress(Bitmap.CompressFormat.PNG, 100, stream);
			byte[] byteArray = stream.toByteArray();

			return decodeSampledBitmapFromResource(byteArray, 400, 400);
		}

		@Override
		protected void onPostExecute(Bitmap param){

			//compress image		
			Bitmap bmTemp = param;
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			bmTemp.compress(Bitmap.CompressFormat.JPEG, 50, stream);
			final byte[] imageData = stream.toByteArray();

			ParseFile mFile = new ParseFile("profile.jpg", imageData);

			//upload the image to parse
			ParseUser.getCurrentUser().put(ParseConstants.KEY_PROFILE_PICTURE, mFile);
			ParseUser.getCurrentUser().saveInBackground(new SaveCallback() {
				@Override
				public void done(ParseException e) {
					if(mDialog.isShowing()){
						mDialog.cancel();
					}	

					if(e == null){
						//set the imageview on this page to the new image
						profileImage.setImageBitmap(BitmapFactory.decodeByteArray(imageData, 0, imageData.length));
					}else{
						e.printStackTrace();
						System.out.println("error saving user with new profile image");
					}
				}
			});
		}

	}

	public static Bitmap decodeSampledBitmapFromResource(byte[] bitmapData, int reqWidth, int reqHeight) {

		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeByteArray(bitmapData, 0, bitmapData.length, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeByteArray(bitmapData, 0, bitmapData.length, options);
	}

	public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;

		System.out.println("options bitmap values - height: " + height + ", width: " + width);

		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight
					&& (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}

		return inSampleSize;
	}
}
