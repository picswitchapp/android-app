package com.picswitch;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;

public class AdditionalPuzzles extends Fragment{
	
	public List<String> category_titles = new ArrayList<String>();
	public List<String> class_names     = new ArrayList<String>();

	List<ParseObject> category_names;
	List<ParseObject> category_descriptions;
	List<ParseObject> category_images;

	List<ParseObject> objects;
	List<ParseObject> userObjects;

	public Context mContext;
	public String tempSolved;
	public LayoutInflater mInflater;
	
	public ViewGroup additionalContainer;
	SwipeRefreshLayout additionalSwipeContainer;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		mContext = container.getContext();
		mInflater = inflater;

		View additional;
		additional = inflater.inflate(R.layout.additional_puzzles_layout, container, false);

		ImageView right_arrow = (ImageView) additional.findViewById(R.id.right_arrow_additional);
		right_arrow.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				TabActivity.tabViewPager.setCurrentItem(1);
			}
		});
		
		additionalContainer = (ViewGroup) additional.findViewById(R.id.additional_container);
		additionalSwipeContainer = (SwipeRefreshLayout) additional.findViewById(R.id.additional_swipe_container);
		
		Typeface montSerrat = Typeface.createFromAsset(mContext.getAssets(),"Montserrat-Regular.ttf"); 
		((TextView) additional.findViewById(R.id.select_category_label)).setTypeface(montSerrat);
		((TextView) additional.findViewById(R.id.title_text_additional)).setTypeface(montSerrat);
		
		additionalSwipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				new retrieveAdditionalMessagesInBackground().execute();
			}
		});
		
		additionalSwipeContainer.setColorSchemeResources(R.color.refresh_color_1,
				R.color.refresh_color_2,
				R.color.refresh_color_3,
				R.color.refresh_color_4);
		
		return additional;
	}

	public void onActivityCreated(Bundle savedInstanceState){
		super.onActivityCreated(savedInstanceState);

		new retrieveAdditionalMessagesInBackground().execute();
	}

	public String[] concat(String[] a, String[] b) {
		int aLen = a.length;
		int bLen = b.length;
		String[] c= new String[aLen+bLen];
		System.arraycopy(a, 0, c, 0, aLen);
		System.arraycopy(b, 0, c, aLen, bLen);
		return c;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}
	
	@Override
	public void onResume(){
		super.onResume();
		new retrieveCurrentUser().execute();
	}
	
	public class CreateAdditionalViews extends AsyncTask<List<ParseObject>, View, Void> {

		@Override
		protected void onPreExecute(){
			additionalContainer.removeAllViews();
		};
		
		@Override
		protected Void doInBackground(List<ParseObject>... params) {
			List<ParseObject> categoriesToDisplay = params[0];
			
			for(int i = 0; i < categoriesToDisplay.size(); i++){
				
				final View newView;
				final ImageView iconImageView;
				final TextView categoryName;
				final TextView description;
				final ParseFile mFile;
				final Uri mUri;
				final int mPosition = i;
				
				if(i % 2 == 1){
					newView = mInflater.inflate(R.layout.message_item_light, additionalContainer, false);
				}else{
					newView = mInflater.inflate(R.layout.message_item, additionalContainer, false);
				}
				
				iconImageView = (ImageView) newView.findViewById(R.id.messageIcon);
				categoryName  = (TextView)  newView.findViewById(R.id.senderLabel);
				description   = (TextView)  newView.findViewById(R.id.messageLabel);
				
				FontsUtils.TypeFace(categoryName, mContext.getAssets());
				FontsUtils.TypeFace(description, mContext.getAssets());
				
				categoryName.setText(categoriesToDisplay.get(i).getString(ParseConstants.KEY_CATEGORY_NAME));
				description.setText(categoriesToDisplay.get(i).getString(ParseConstants.KEY_CATEGORY_DESCRIPTION));
				
				mFile = categoriesToDisplay.get(i).getParseFile(ParseConstants.KEY_PHOTO);
				mUri = Uri.parse(mFile.getUrl());
				
				((Activity) mContext).runOnUiThread(new Runnable() {

					@Override
					public void run() {
						
						Picasso.with(mContext).load(mUri).placeholder(R.drawable.picasso_loading_animation)
						.into(iconImageView);
						
						//setClickListeners
						newView.setOnClickListener(new View.OnClickListener() {
							
							@Override
							public void onClick(View v) {
								
								String[] titleArray = category_titles.toArray(new String[0]);
								String[] classNamesArray = class_names.toArray(new String[0]);
								
								String[] tempBundleStringArray = concat(titleArray, classNamesArray);

								Bundle bundle = new Bundle();
								bundle.putStringArray(ParseConstants.KEY_ADDITIONAL_DATA, tempBundleStringArray);

								Intent intent = new Intent(mContext, AdditionalPuzzlesOpenImage.class);
								//intent.putExtra(ParseConstants.KEY_PUZZLE_CLASS, category);
								intent.putExtra(ParseConstants.KEY_TEMP_SOLVED, tempSolved);
								intent.putExtra(ParseConstants.KEY_TEMP_SKIPPED, "");
								intent.putExtras(bundle);
								intent.putExtra(ParseConstants.KEY_CURRENT_INT, mPosition);
								//intent.putExtra(ParseConstants.KEY_CATEGORY_TITLE, category_titles[position - 1]);
								startActivity(intent);
							}
						});
					}
					
				});
				
				
				publishProgress(newView);
			}
			
			
			return null;
		}
		
		@Override
		protected void onProgressUpdate(View... params){
			additionalContainer.addView(params[0]);
		}
	}

	public class retrieveAdditionalMessagesInBackground extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {

			ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(ParseConstants.CLASS_ADDITIONAL_PUZZLE_CATEGORIES);
			query.addAscendingOrder(ParseConstants.KEY_CATEGORY_NAME);
			query.findInBackground(new FindCallback<ParseObject>() {
				@Override
				public void done(List<ParseObject> additional, ParseException e) {
					if(e == null){

						objects = additional;
						
						for(int i = 0; i < additional.size(); i++){
							category_titles.add(additional.get(i).getString(ParseConstants.KEY_CATEGORY_NAME));
							class_names.add(additional.get(i).getString(ParseConstants.KEY_PUZZLE_CLASS));
						}
						
						new CreateAdditionalViews().execute(additional);

					}else{
						e.printStackTrace();
					}
				}
			});
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void param){
			additionalSwipeContainer.setRefreshing(false);
		}

	}
	
	/**
	 * 	
	 */
	public void createSolvedString(){
		if(userObjects.size() == 0){
			String tempSolved = "";
			System.out.println("before pass 1: " + tempSolved);
		}else{
			List temp;
			temp = userObjects.get(0).getList(ParseConstants.KEY_PUZZLES_SOLVED);
			String tmp = "";
			if(temp==null){
				tempSolved = "";
			}else{
				tempSolved = temp.toString();
			}
			
			if(tempSolved.length() < 4){
				tempSolved = "";
			}else{
				if(tempSolved.contains("[")){
					tmp = tempSolved.replace("[", "");
					tempSolved = tmp;
					System.out.println("before left brace case");
				}
				if(tempSolved.contains("]")){
					tmp = tempSolved.replace("]", "");
					System.out.println("before right brace case");
					tempSolved = tmp;
				}
			}
			System.out.println("before pass 2: " + tempSolved);
		}
	}
	
	
	public class retrieveCurrentUser extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			
			ParseQuery<ParseObject> currentUser = new ParseQuery<ParseObject>(ParseConstants.CLASS_USER);
			currentUser.whereEqualTo(ParseConstants.KEY_OBJECT_ID, ParseUser.getCurrentUser().getObjectId());
			currentUser.findInBackground(new FindCallback<ParseObject>() {

				@Override
				public void done(List<ParseObject> objects, ParseException e) {
					if(e == null){

						userObjects = objects;
						createSolvedString();

					}else{
						System.out.println("before error");
						e.printStackTrace();
					}
				}
			});
			
			return null;
		}
		
	}


}

