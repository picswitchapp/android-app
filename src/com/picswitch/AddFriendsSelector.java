package com.picswitch;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.parse.ParseUser;

public class AddFriendsSelector extends Activity {

	Typeface montSerrat;
	Context mContext;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_add_friends_selector);

		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
			window.setStatusBarColor(Color.rgb(32, 99, 99));
		}

		mContext = AddFriendsSelector.this;

		montSerrat = Typeface.createFromAsset(mContext.getAssets(),"Montserrat-Regular.ttf");

		TextView selectorTitle = (TextView) findViewById(R.id.title_text_add_friends_selector);
		selectorTitle.setTypeface(montSerrat);

		//labels typeface
		TextView usernameLabel = (TextView) findViewById(R.id.search_username_label);
		usernameLabel.setTypeface(montSerrat);
		TextView phoneLabel = (TextView) findViewById(R.id.by_phone_label);
		phoneLabel.setTypeface(montSerrat);

		//click listeners
		ImageView leftArrow = (ImageView) findViewById(R.id.left_arrow_add_friends_selector);
		leftArrow.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				AddFriendsSelector.this.finish();
			}
		});

		TextView usernameButton = (TextView) findViewById(R.id.search_by_username_button);
		FontsUtils.TypeFace(usernameButton, getAssets());
		usernameButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//open AddFriendsUsername Activity
				Intent intent = new Intent(mContext, AddFriendsUsername.class);
				startActivity(intent);
			}
		});

		TextView contactsButton = (TextView) findViewById(R.id.by_phone_button);
		FontsUtils.TypeFace(contactsButton, getAssets());
		contactsButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//open AddFriendsActivity.  Will add condition for current user having a phone number

				boolean cond = false;
				try{
					cond = (ParseUser.getCurrentUser().getString(ParseConstants.KEY_PHONE_NUMBER) != null) &&
							(ParseUser.getCurrentUser().getString(ParseConstants.KEY_PHONE_NUMBER).length() > 1);
				}catch(Exception e){
					e.printStackTrace();
				}

				if(cond){
					Intent intent = new Intent(mContext, AddFriendsActivity.class);
					startActivity(intent);
				}else{
					AlertDialog.Builder mBuilder = new AlertDialog.Builder(mContext);
					mBuilder.setTitle("Enter phone number");
					mBuilder.setMessage("To find friends using your contacts, you must first enter your phone number on PicSwitch.  You can still add friends directly using their username without entering your phone number.");
					mBuilder.setPositiveButton("ENTER NUMBER", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							Intent intent = new Intent(mContext, SetPhoneNumber.class);
							startActivity(intent);
							dialog.cancel();
							AddFriendsSelector.this.finish();
						}
					});
					mBuilder.setNegativeButton("NOT NOW", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.cancel();
						}
					});
					AlertDialog mDialog = mBuilder.create();
					mDialog.show();
				}
			}
		});
	}
}
