package com.picswitch;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.Display;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseCrashReporting;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

 /**user manipulation of the PicSwitch
 * Allows user to move images around 
 * Detects when the user has solved the PicSwitch*/
public class OpenImage extends ActionBarActivity{
	private ImageView image1, image2, image3,image4,image5,image6, image7,image8,image9;
	private GestureDetector gestureDetector;
	int animationToRun = R.anim.fadeout; //animation to be used when user touches PicSwitch
	public int count=0, switchCounter = 0;
	//images of the PicSwitch
	Bitmap img, img0, img1, img2,img3,img4,img5,img6, img7, img8; 
	ActionBar actionBar;
	RelativeLayout rLayout;
	ArrayList<Integer> images = new ArrayList<Integer>(Arrays.asList(0,1,2,3,4,5,6,7,8));
	ArrayList<Integer> eightStepTmp = new ArrayList<Integer>(Arrays.asList(-1,-1,-1,-1,-1,-1,-1,-1,-1));
	View.OnTouchListener onTouchListener;
	SurfaceHolder s;
	int chunkNumbers =9;
	ImageView placeholder;
	private String firstCaption, secondCaption, messageID;

	ParseRelation<ParseUser> messageRecipients;

	int currentNewIndex;

	Uri imageUri;
	Bitmap imageBitmap;

	private int timeLimit;

	String senderUsername;
	String senderFirstName;
	String senderObjectId;
	String senderLastNameInitial;

	boolean isFeatured;
	boolean isParseObject;
	
	long preciseStartTime;
	long preciseEndTime;
	double timeToDisplay;

	SoundPool sounds;
	ProgressDialog mDialog;

	int switchID;
	int nextID;
	int doneID;
	int outOfTimeID;

	Typeface montserrat;
	Typeface varella;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		getWindow().requestFeature(Window.FEATURE_ACTION_BAR);

		super.onCreate(savedInstanceState);

		getActionBar().hide();
		getWindow().setFormat(PixelFormat.RGBA_8888);

		rLayout =  (RelativeLayout)findViewById(R.layout.activity_open_image);
		setContentView(R.layout.activity_open_image);

		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
			window.setStatusBarColor(Color.rgb(32, 99, 99));
		}
		imageUri = getIntent().getData();
		senderFirstName = getIntent().getExtras().getString(ParseConstants.KEY_SENDER_FIRST_NAME);
		firstCaption = getIntent().getExtras().getString(ParseConstants.CAPTION_FIRST);
		secondCaption = getIntent().getExtras().getString(ParseConstants.CAPTION_SECOND);
		timeLimit = getIntent().getExtras().getInt(ParseConstants.KEY_TIME_LIMIT);
		currentNewIndex = getIntent().getExtras().getInt(ParseConstants.KEY_CURRENT_NEW_INDEX);
		senderLastNameInitial = getIntent().getExtras().getString("senderLastNameInitial");

		isFeatured = getIntent().getExtras().getBoolean("isFeatured");
		isParseObject = getIntent().getExtras().getBoolean("isParseObject");

		messageID = getIntent().getExtras().getString("messageID");
		senderUsername = getIntent().getExtras().getString(ParseConstants.KEY_SENDER);
		senderObjectId = getIntent().getExtras().getString("senderObjectId");

		varella = Typeface.createFromAsset(getAssets(), "VarelaRound.ttf");
		montserrat = Typeface.createFromAsset(getAssets(), "Montserrat-Regular.ttf");

		timerTextView = (TextView) findViewById(R.id.open_image_timer);
		TextView firstCap = (TextView) findViewById(R.id.firstCaption);
		firstCap.setText(firstCaption);
		firstCap.setTypeface(varella);

		//handle case of being the last message, with one deleted before it
		if(!isFeatured){
			if(currentNewIndex == TabActivity.mMessages.size()){
				currentNewIndex--;
			}
		}else{
			if(currentNewIndex == TabActivity.mFeatured.size()){
				currentNewIndex--;
			}
		}

		//create new eight step thing
		//first step, shuffle the stock array
		Collections.shuffle(images);

		for(int i = 0; i < 9; i++){
			eightStepTmp.set(i, (images.get((images.indexOf(i) + 1) % 9)));
		}

		System.out.println("eight step: " + eightStepTmp.toString());

		images = eightStepTmp;

		System.out.println("images step: " + images.toString());

		TextView titleText = (TextView) findViewById(R.id.open_image_title_text);
		if(isFeatured){
			titleText.setText(senderFirstName);
		}else{
			if(senderLastNameInitial.length() > 0){
				titleText.setText(senderFirstName + " " + senderLastNameInitial + ".");
			}else{
				titleText.setText(senderFirstName);
			}
		}
		titleText.setTypeface(montserrat);

		TextView nextText = (TextView) findViewById(R.id.next_picswitch_text);
		nextText.setTypeface(varella);

		makeProgressDialog();

		Picasso.with(this).load(imageUri.toString()).into(target);
		//		actionBar = getActionBar(); //Set up action bar
		//		setupActionBar();
		timerTextView.setText("" + timeLimit);
		buttonsHelper(); //handles buttons
		//		titleBarHelper(savedInstanceState); //handles title bar

		//set up the sounds		
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){

			AudioAttributes mAttributes = new AudioAttributes.Builder()
			.setUsage(AudioAttributes.USAGE_GAME)
			.setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
			.build();
			sounds = new SoundPool.Builder().setAudioAttributes(mAttributes).build();

		}else{
			sounds = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
		}

		switchID    = sounds.load(OpenImage.this, R.raw.switches, 1);
		doneID      = sounds.load(OpenImage.this, R.raw.solved_puzzle, 1);
		nextID      = sounds.load(OpenImage.this, R.raw.next_puzzle, 1);
		outOfTimeID = sounds.load(OpenImage.this, R.raw.out_of_time, 1);
	}

	public void makeProgressDialog(){
		mDialog = new ProgressDialog(OpenImage.this);
		mDialog.setMessage("Loading image...");
		mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		mDialog.setIndeterminate(true);
		mDialog.show();
	}

	@Override
	public void onBackPressed(){
		timerHandler.removeCallbacks(timerRunnable);
		finish();
		super.onBackPressed();
	}

	/**Is a query that updates the easy column when a user completes an easy puzzle*/
	public void statsQuery(){ 
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put(ParseConstants.KEY_STATS_USERNAME, ParseUser.getCurrentUser().getUsername());

		//adding the difficulty to the params
		System.out.println("timeLimit right before stats: " + timeLimit);
		if(timeLimit == 15){
			params.put(ParseConstants.KEY_STATS_PUZZLEDIFFICULTY, ParseConstants.KEY_STATS_VERYHARD);
		}else if(timeLimit == 30){
			params.put(ParseConstants.KEY_STATS_PUZZLEDIFFICULTY, ParseConstants.KEY_STATS_HARD);
		}else if(timeLimit == 45){
			params.put(ParseConstants.KEY_STATS_PUZZLEDIFFICULTY, ParseConstants.KEY_STATS_NORMAL); 
		}else if(timeLimit == 60){
			params.put(ParseConstants.KEY_STATS_PUZZLEDIFFICULTY, ParseConstants.KEY_STATS_EASY);
		}else if(timeLimit == 0){
			params.put(ParseConstants.KEY_STATS_PUZZLEDIFFICULTY, ParseConstants.KEY_STATS_NOLIMIT);
		}

		params.put(ParseConstants.KEY_STATS_SWITCHCOUNTER, switchCounter);
		params.put(ParseConstants.KEY_STATS_TIME, timeToDisplay);
		params.put(ParseConstants.KEY_STATS_CATEGORY, ParseConstants.KEY_STATS_NEW);
		ParseCloud.callFunctionInBackground(ParseConstants.CLOUD_STATISTICS, params, new FunctionCallback<String>() {
			public void done(String success, ParseException e) {
				if (e == null) {
					// StatsQuery completed succesfully
				}
			}
		});
	}


	public void solveQuery(Intent intent){ //to delete user from message's recipients after solving successfully

		//Toast.makeText(OpenImage.this, "Solved", Toast.LENGTH_LONG).show();

		ParseObject message = TabActivity.mMessagesHM.get(messageID);

		messageRecipients = message.getRelation("recipients");
		messageRecipients.remove(ParseUser.getCurrentUser());
		message.saveInBackground(new SaveCallback() {
			@Override
			public void done(ParseException e) {
				if (e != null){
					//Log.e(TAG, e.getMessage());
				}


			}
		});

	}


	Target target = new Target() {

		int timerCount = 0;

		@Override
		public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
			if(mDialog.isShowing()){
				mDialog.cancel();
			}
			imageBitmap = bitmap;
			splitImage(bitmap, chunkNumbers);
			createImageFromBitmap(bitmap, ParseConstants.KEY_NEW_CHALLENGES_TEMP); //save image to disk for viewing in next intent
			imageViewsHelper(); //initializes image views
			imageViewsDimensions(); //sets up the dimensions of the imageviews
			assignImageViewPics(); //add images to imageViews
			setImageViewTouch();

			preciseStartTime = System.nanoTime();

			//start timer;

			if(timerCount==0){
				startTime = System.currentTimeMillis();
				timerHandler.postDelayed(timerRunnable, 0);
				//TextView timer = (TextView) findViewById(R.id.open_image_timer);
				timerTextView.setBackground(getResources().getDrawable(R.drawable.timerblank));
				timerCount++;
			}

		}

		@Override
		public void onBitmapFailed(Drawable arg0) {

		}

		@Override
		public void onPrepareLoad(Drawable arg0) {


		}
	};


	protected void setSolveStatus(ParseObject message){//might need to get rid of this. Network calls reductions
		message.saveInBackground(new SaveCallback() {

			@Override
			public void done(ParseException e) {
				if(e==null){
					//success!
					//					Toast.makeText(OpenImage.this, R.string.success_message, Toast.LENGTH_LONG).show();
				}
				else{
					//there was an error
					AlertDialog.Builder builder = new AlertDialog.Builder(OpenImage.this);
					builder.setMessage(R.string.error_sending_message)
					.setTitle(R.string.error_selecting_file_title)
					.setPositiveButton(android.R.string.ok, null);
					AlertDialog dialog = builder.create();
					dialog.show();
				}
			}
		});
	}

	/**Creates images from the bitmaps*/
	public String createImageFromBitmap(Bitmap bitmap, String name) {
		String fileName = name;//no .png or .jpg needed
		try {
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
			FileOutputStream fo = openFileOutput(fileName, Context.MODE_PRIVATE);
			fo.write(bytes.toByteArray());
			// remember close file output
			fo.close();
		} catch (Exception e) {
			e.printStackTrace();
			fileName = null;
		}
		return fileName;

	}
	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_open_image,
					container, false);
			return rootView;
		}
	}


	/**Sets touch detection for imageViews*/
	public void setImageViewTouch(){
		//set touch for imageviews
		gestureDetector = new GestureDetector(this, new MyGestureDetector());
		onTouchListener = new View.OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				return gestureDetector.onTouchEvent(event);
			}
		};

		image1.setOnTouchListener(onTouchListener);
		image2.setOnTouchListener(onTouchListener);
		image3.setOnTouchListener(onTouchListener);
		image4.setOnTouchListener(onTouchListener);
		image5.setOnTouchListener(onTouchListener);
		image6.setOnTouchListener(onTouchListener);
		image7.setOnTouchListener(onTouchListener);
		image8.setOnTouchListener(onTouchListener);
		image9.setOnTouchListener(onTouchListener);
	}

	/**Assigns bitmaps from the SD to image Views*/
	public void assignImageViewPics(){
		//shuffle images arraylist 
		//		Collections.shuffle(images);

		//		ArrayList<Integer> eightStep = new ArrayList<Integer>();
		//		
		//		//first step, shuffle the array
		//		Collections.shuffle(images);
		//		
		//		for(int i = 0; i < 9; i++){
		//			eightStep.add(i, (images.get(i) + 1) % 9);
		//		}
		//		
		//		System.out.println("eight step: " + eightStep.toString());

		ImageView previewThumbnail = new ImageView(this);
		int[] ids= {R.id.imageView01,R.id.imageView02,R.id.imageView03,R.id.imageView04,R.id.imageView05,R.id.imageView06,R.id.imageView07,R.id.imageView08,R.id.imageView09};
		for(int i=0; i<9;i++){
			previewThumbnail=(ImageView)findViewById(ids[i]);
			Bitmap bitmap ;
			bitmap = chunkedImages.get(images.get(i)); //get image using images array that has been shuffled
			previewThumbnail.setImageBitmap(bitmap);
		}
	}


	/**Initializes the dimensions of the imageviews that hold the chunked 
	 * images*/
	public void imageViewsDimensions(){
		Display  display = getWindowManager().getDefaultDisplay();
		//width of the imageViews
		//minus 6 to allows for space between imageViews
		int swidth = (int) (display.getWidth()/3-6);
		image1.getLayoutParams().width = swidth;
		image1.getLayoutParams().height = swidth;
		image2.getLayoutParams().width = swidth;
		image2.getLayoutParams().height = swidth;
		image3.getLayoutParams().width = swidth;
		image3.getLayoutParams().height = swidth;
		image4.getLayoutParams().width = swidth;
		image4.getLayoutParams().height = swidth;
		image5.getLayoutParams().width = swidth;
		image5.getLayoutParams().height = swidth;
		image6.getLayoutParams().width = swidth;
		image6.getLayoutParams().height = swidth;
		image7.getLayoutParams().width = swidth;
		image7.getLayoutParams().height = swidth;
		image8.getLayoutParams().width = swidth;
		image8.getLayoutParams().height = swidth;
		image9.getLayoutParams().width = swidth;
		image9.getLayoutParams().height = swidth;
	}


	/**Initialize imageViews to hold chucked images*/
	public void imageViewsHelper(){
		image1 = (ImageView) findViewById(R.id.imageView01);
		image2 = (ImageView) findViewById(R.id.imageView02);
		image3 = (ImageView) findViewById(R.id.imageView03);
		image4 = (ImageView) findViewById(R.id.imageView04);
		image5 = (ImageView) findViewById(R.id.imageView05);
		image6 = (ImageView) findViewById(R.id.imageView06);
		image7 = (ImageView) findViewById(R.id.imageView07);
		image8 = (ImageView) findViewById(R.id.imageView08);
		image9 = (ImageView) findViewById(R.id.imageView09);
	}




	/**Helper for buttons*/
	public void buttonsHelper(){
		ImageView backButton = (ImageView) findViewById(R.id.open_image_back_button);
		backButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				timerHandler.removeCallbacks(timerRunnable);
				//				Intent intent = new Intent(getBaseContext(), TabActivity.class);
				//				startActivity(intent);
				finish();
			}
		});

		RelativeLayout nextButton = (RelativeLayout) findViewById(R.id.next_picswitch_button);
		nextButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				//				Toast.makeText(getBaseContext(), "next button click", Toast.LENGTH_SHORT).show();

				timerHandler.removeCallbacks(timerRunnable); // stop current activities timer

				if(TabActivity.SOUND){
					sounds.play(nextID, 1, 1, 0, 0, 1);
				}

				//go on to the next picswitch in new
				//have to figure out which one that is, then recall openImage with that parseObject

				ParseObject nextMessage = null;

				//checks
				System.out.println("next_checks...isFeatured: " + isFeatured);
				System.out.println("next_checks...currentNewIndex: " + currentNewIndex);
				System.out.println("next_checks...TabActivity.mMessages.size(): " + TabActivity.mMessages.size());
				System.out.println("next_checks...TabActivity.mFeatured.size(): " + TabActivity.mFeatured.size());
				System.out.println("next_checks...TabActivity.mMessagesLinked.size(): " + TabActivity.mMessagesLinkedList.size());
				System.out.println("next_checks...TabActivity.mFeaturedLinked.size(): " + TabActivity.mFeaturedLinkedList.size());

				currentNewIndex--;

				if(!isFeatured){
					if(currentNewIndex < 0){
						Toast.makeText(OpenImage.this, "End of new puzzles", Toast.LENGTH_SHORT).show();
						OpenImage.this.finish();
					}else{
						nextMessage = TabActivity.mMessagesLinkedList.get(TabActivity.mMessagesLinkedList.size() - 1 - currentNewIndex);
					}
				}else{
					if(currentNewIndex < 0){
						Toast.makeText(OpenImage.this, "End of featured puzzles", Toast.LENGTH_SHORT).show();
						OpenImage.this.finish();
					}else{
						nextMessage = TabActivity.mFeaturedLinkedList.get(TabActivity.mFeaturedLinkedList.size() - 1 - currentNewIndex);
					}
				}

				if(nextMessage != null){
					Intent intent = new Intent(getBaseContext(), OpenImage.class);

					String firstCaption = nextMessage.getString(ParseConstants.CAPTION_FIRST);
					String secondCaption = nextMessage.getString(ParseConstants.CAPTION_SECOND);
					ParseFile file = nextMessage.getParseFile(ParseConstants.KEY_PHOTO);
					int timeLimit = nextMessage.getInt(ParseConstants.KEY_TIME_LIMIT);
					Uri fileUri = Uri.parse(file.getUrl());

					String firstName="";		
					String lastName="";

					if(!isFeatured){
						try {
							firstName = nextMessage.getParseUser(ParseConstants.KEY_SENDER).fetchIfNeeded().getString(ParseConstants.KEY_FIRST_NAME);
							lastName  = nextMessage.getParseUser(ParseConstants.KEY_SENDER).fetchIfNeeded().getString(ParseConstants.KEY_LAST_NAME);
						} catch (ParseException e) {
							e.printStackTrace();
						}
					}else{
						firstName = nextMessage.getString(ParseConstants.KEY_COMPANY_NAME);
					}

					String lastNameInitial = ""; 
					if(lastName.length() > 0){
						lastNameInitial = lastName.substring(0, 1);
					}

					intent.setData(fileUri);

					intent.putExtra("isFeatured", isFeatured);
					intent.putExtra(ParseConstants.KEY_CURRENT_NEW_INDEX, currentNewIndex);
					intent.putExtra(ParseConstants.CAPTION_FIRST, firstCaption);
					intent.putExtra(ParseConstants.CAPTION_SECOND, secondCaption);
					intent.putExtra(ParseConstants.KEY_TIME_LIMIT, timeLimit);
					intent.putExtra(ParseConstants.KEY_SENDER_FIRST_NAME, firstName);
					intent.putExtra("senderLastNameInitial", lastNameInitial);
					startActivity(intent);
					finish();
				}else{
					//Toast.makeText(OpenImage.this, "nextMessage null", Toast.LENGTH_SHORT).show();
					OpenImage.this.finish();
				}
			}
		});
	}




	/**Set up for the timer*/
	TextView timerTextView;
	long startTime = 0;
	int seconds;
	int minutes;
	//runs without a timer by reposting this handler at the end of the runnable
	Handler timerHandler = new Handler();
	Runnable timerRunnable = new Runnable() {
		@SuppressLint("NewApi")
		@Override
		public void run() {
			long millis = System.currentTimeMillis() - startTime;
			seconds = (int) (millis / 1000);
			minutes = seconds / 60;
			seconds = seconds % 60;
			if(timeLimit > 0){
				//defined timelimit, so count down
				if((timeLimit - seconds - 1) >= 0){
					timerTextView.setText(String.format("%02d", timeLimit - seconds));
					timerTextView.setTextAlignment(6);;
					timerHandler.postDelayed(this, 500);
				}else{
					//handle case where not solved within timelimit using dialogbox
					createFailDialogBox();
				}
			}else{
				timerTextView.setText(String.format("%02d", seconds));
				timerTextView.setTextAlignment(6);;
				timerHandler.postDelayed(this, 500);
			}
		}
	};

	public void createFailDialogBox(){

		timerHandler.removeCallbacks(timerRunnable);

		if(TabActivity.SOUND){
			sounds.play(outOfTimeID, 1, 1, 0, 0, 1);
		}

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(OpenImage.this);
		alertDialogBuilder.setTitle("Out of time!");
		alertDialogBuilder.setMessage("Do you want to try this puzzle again?")
		.setCancelable(false)
		.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				//recall the activity

				Intent intent = new Intent(OpenImage.this, OpenImage.class);
				intent.setData(imageUri);
				intent.putExtra(ParseConstants.KEY_MESSAGE_ID, messageID);
				intent.putExtra(ParseConstants.CAPTION_FIRST, firstCaption);
				intent.putExtra(ParseConstants.CAPTION_SECOND, secondCaption);
				intent.putExtra(ParseConstants.KEY_TIME_LIMIT, timeLimit);
				intent.putExtra(ParseConstants.KEY_FIRST_NAME, senderFirstName);
				intent.putExtra("isFeatured", isFeatured);
				intent.putExtra(ParseConstants.KEY_CURRENT_NEW_INDEX, currentNewIndex);
				intent.putExtra("senderLastNameInitial", senderLastNameInitial);
				startActivity(intent);

				//				Toast.makeText(getBaseContext(), "Yes clicked", Toast.LENGTH_SHORT).show();
				dialog.cancel();
				finish();
			}
		})
		.setNegativeButton("No", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
				finish();
			}
		});

		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();

		int titleId = getResources().getIdentifier( "alertTitle", "id", "android" );
		if (titleId > 0) {
			TextView mTitle = (TextView) alertDialog.findViewById(titleId);
			if (mTitle != null) {
				mTitle.setTypeface(montserrat);
			}
		}

		Button button1 = (Button) alertDialog.findViewById(android.R.id.button1);
		Button button2 = (Button) alertDialog.findViewById(android.R.id.button2);
		FontsUtils.TypeFace(button1, getAssets());
		FontsUtils.TypeFace(button2, getAssets());

		TextView mMessage = (TextView) alertDialog.findViewById(android.R.id.message);
		mMessage.setTextSize(15);
		FontsUtils.TypeFace(mMessage, getAssets());
	}

	/**Detects touch and starts timer*/
	class MyGestureDetector extends SimpleOnGestureListener {
		int timerCount = 0;
		@Override
		public boolean onDown(MotionEvent e) {
			count++;
			onTap(e);
			return false;
		}

	}


	/**Helper to swap images on imageViews*/
	private void swapImages(ImageView initial, ImageView current, int init, int finImage){
		Bitmap bitmap;
		int tmp, tmp2;
		try {
			//swap the images in initial and current imageViews
			//bitmap = BitmapFactory.decodeStream(this.openFileInput("img"+images.get(in)));
			initial.setImageBitmap(BitmapFactory.decodeStream(this.openFileInput("img"+fin)));
			current.setImageBitmap(BitmapFactory.decodeStream(this.openFileInput("img"+in)));
			//swap the numbers in arrayList for in and fin
			tmp = init;
			tmp2 = images.indexOf(finImage);
			images.set(images.indexOf(init), finImage);
			images.set(tmp2, tmp);
			checkFinish();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}


	/**Checks whether the PicSwitch is solved*/
	private void checkFinish(){
		if(images.get(0)==0&&
				images.get(1)==1&&
				images.get(2)==2&&
				images.get(3)==3&&
				images.get(4)==4&&
				images.get(5)==5&&
				images.get(6)==6&&
				images.get(7)==7&&
				images.get(8)==8){
			done();

		}
	}


	/**Handles the case where the user completes the puzzle*/
	public void done() {

		//stop timers
		preciseEndTime = System.nanoTime();
		timerHandler.removeCallbacks(timerRunnable);

		if(TabActivity.SOUND){
			sounds.play(doneID, 1, 1, 0, 0, 1);
		}

		//calculate elapsed times
		long preciseSolveTime = preciseEndTime - preciseStartTime;
		double preciseSolveTimeSeconds = preciseSolveTime / 1000000000.0;

		double featuredTimeToParse = Math.ceil(preciseSolveTimeSeconds * 100000) / 100000.0;
		System.out.println("precise solve time after trunc = " + featuredTimeToParse);

		timeToDisplay = Math.ceil(preciseSolveTimeSeconds * 100) / 100.0;
		System.out.println("precise time to display = " + timeToDisplay);

		Intent intent = new Intent(OpenImage.this, NewChallengesViewImage.class);
		int time =  minutes*60 + seconds;
		firstCaption = firstCaption + ""; //incase there are no captions, to avoid null
		secondCaption = secondCaption + "";

		ParseObject entry = new ParseObject("Solved");
		entry.put("user", ParseUser.getCurrentUser().getObjectId());
		entry.put("puzzleId", "");
		entry.put("timeTaken", "");
		entry.put("solveStatus", true);
		entry.saveInBackground(new SaveCallback() {
			@Override
			public void done(ParseException e) {
				if (e != null){
					//Log.e(TAG, e.getMessage());
				}


			}
		});

		if(isFeatured){
			//calls cloud function for featured statistics
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("puzzleId", messageID);
			params.put("time", featuredTimeToParse);
			params.put("steps", switchCounter);
			ParseCloud.callFunctionInBackground("featuredStatistics", params, new FunctionCallback<String>(){

				@Override
				public void done(String object, ParseException e) {
					if(e == null){

					}else{
						e.printStackTrace();
					}
				}

			});
		}

		String sponsorLink;
		if(isFeatured){
			sponsorLink = TabActivity.mFeaturedHM.get(messageID).getString("sponsorLink");
			intent.putExtra("sponsorLink", sponsorLink);
		}

		Bundle bundle=new Bundle();
		bundle.putStringArray(ParseConstants.KEY_ADDITIONAL_DATA, new String[]
				{firstCaption, secondCaption, Double.toString(timeToDisplay), 
				Integer.toString(switchCounter), senderFirstName, Integer.toString(currentNewIndex), 
				senderUsername, senderObjectId});
		intent.putExtras(bundle);
		intent.putExtra("isFeatured", isFeatured);
		intent.putExtra("isParseObject", isParseObject);
		intent.putExtra("isSolved", false);
		intent.putExtra("senderLastNameInitial", senderLastNameInitial);
		intent.putExtra("messageID", messageID);

		if(isParseObject && !isFeatured){
			solveQuery(getIntent()); //sets the solve status of the current message to solved
			statsQuery();			 //do stats
		}

		//save the message to the solved HashMap in TabActivity
		//only for new messages, not featured or old
		if(!isFeatured && isParseObject){
			TabActivity.saveSolvedMessageToPhone(TabActivity.mMessagesHM.get(messageID), imageBitmap, switchCounter, timeToDisplay, false);			

			TabActivity.mMessagesLinkedList.remove(TabActivity.mMessagesHM.get(messageID));
			TabActivity.mMessages.remove(TabActivity.mMessagesHM.get(messageID));

			TabActivity.allNewMessagesLinkedList.remove(TabActivity.allNewMessages.get(messageID));
		}

		if(isFeatured){
			TabActivity.saveSolvedMessageToPhone(TabActivity.mFeaturedHM.get(messageID), imageBitmap, switchCounter, timeToDisplay, true);

			TabActivity.mFeaturedLinkedList.remove(TabActivity.mFeaturedHM.get(messageID));
			TabActivity.mFeatured.remove(TabActivity.mFeaturedHM.get(messageID));
		}

		startActivity(intent);
		finish();
	}

	ImageView temp = null;
	int in=100, fin=100;
	/**Sets animation when user makes first touch*/
	public void setAnimation(ImageView im, int i){
		Animation animation = AnimationUtils.loadAnimation(this, animationToRun);
		im.startAnimation(animation );
		im.setPadding(8, 8,8,8);
		im.setBackgroundColor(Color.WHITE);
		temp=im;
		in=i;
	}

	public void secondTouch(ImageView imageView, int i){
		count=0;
		if(!(imageView.equals(temp))){
			switchCounter++;
			if(TabActivity.SOUND){
				sounds.play(switchID, 1, 1, 0, 0, 1);
			}
		}
		temp.setPadding(0,0,0,0);
		temp.clearAnimation();
		fin=i;
		swapImages(temp,imageView,in,fin);
	}

	/**Detects touches on imageViews 
	 * Shows the necessary animations
	 * Calls swap images to swap the images*/
	private void onTap(MotionEvent event){
		int firstRow = getLocation(image1)[1]+image1.getHeight(); //lower y coordinate of row 1

		//first column
		if(image1.getLeft()<event.getRawX() && image1.getLeft()+image1.getHeight()>event.getRawX()){
			//image1
			if(image1.getTop()<event.getRawY() && getLocation(image1)[1]+image1.getHeight()>event.getRawY()){
				if(count==1){
					setAnimation(image1, images.get(0));
				}
				else if(count==2){
					secondTouch(image1, images.get(0));
				}
			}
			else if(image4.getTop()<event.getRawY() && getLocation(image4)[1]+image4.getHeight()>event.getRawY()){
				if(count==1){
					setAnimation(image4, images.get(3));
				}
				else if(count==2){
					secondTouch(image4, images.get(3));
				}
			}
			else if(image7.getTop()<event.getRawY() && getLocation(image7)[1]+image7.getHeight()>event.getRawY()){
				if(count==1){
					setAnimation(image7, images.get(6));
				}
				else if(count==2){
					secondTouch(image7, images.get(6));
				}
			}

		}
		//second column
		if(image2.getLeft()<event.getRawX() && image2.getLeft()+image2.getHeight()>event.getRawX()){
			//image1
			if(image2.getTop()<event.getRawY() && firstRow>event.getRawY()){
				if(count==1){
					setAnimation(image2, images.get(1));
				}
				else if(count==2){
					secondTouch(image2, images.get(1));
				}
			}
			else if(image5.getTop()<event.getRawY() && getLocation(image5)[1]+image5.getHeight()>event.getRawY()){
				if(count==1){
					setAnimation(image5, images.get(4));
				}
				else if(count==2){
					secondTouch(image5, images.get(4));
				}
			}
			else if(image8.getTop()<event.getRawY() && getLocation(image8)[1]+image8.getHeight()>event.getRawY()){
				if(count==1){
					setAnimation(image8, images.get(7));
				}
				else if(count==2){
					secondTouch(image8, images.get(7));
				}
			}

		}
		//third column
		if(image3.getLeft()<event.getRawX() && image3.getLeft()+image3.getHeight()>event.getRawX()){
			//image1
			if(image3.getTop()<event.getRawY() && firstRow>event.getRawY()){
				if(count==1){
					setAnimation(image3, images.get(2));
				}
				else if(count==2){
					secondTouch(image3, images.get(2));
				}
			}
			else if(image6.getTop()<event.getRawY() && getLocation(image6)[1]+image6.getHeight()>event.getRawY()){
				if(count==1){
					setAnimation(image6, images.get(5));
				}
				else if(count==2){
					secondTouch(image6, images.get(5));
				}
			}
			else if(image9.getTop()<event.getRawY() && getLocation(image9)[1]+image9.getHeight()>event.getRawY()){
				if(count==1){
					setAnimation(image9, images.get(8));
				}
				else if(count==2){
					secondTouch(image9, images.get(8));
				}
			}

		}
	}

	private int[] getLocation(View mView) {
		Rect mRect = new Rect();
		int[] location = new int[2];

		mView.getLocationOnScreen(location);

		mRect.left = location[0];
		mRect.top = location[1];
		mRect.right = location[0] + mView.getWidth();
		mRect.bottom = location[1] + mView.getHeight();

		return location;
	}

	ArrayList<Bitmap> chunkedImages; //saves the chunked images

	/**method to split the image in imageView*/
	private void splitImage(Bitmap b, int chunkNumbers) {    
		Bitmap bitmap = b;

		//For the number of rows and columns of the grid to be displayed
		int rows,cols;

		//For height and width of the small image chunks 
		int chunkHeight,chunkWidth;

		//To store all the small image chunks in bitmap format in this list 
		chunkedImages = new ArrayList<Bitmap>(chunkNumbers);

		//Getting the scaled bitmap of the source image
		Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), true);

		rows = cols = (int) Math.sqrt(chunkNumbers);
		chunkHeight = bitmap.getHeight()/rows;
		chunkWidth = bitmap.getWidth()/cols;


		//xCoord and yCoord are the pixel positions of the image chunks
		int yCoord = 0;
		for(int x=0; x<rows; x++){
			int xCoord = 0;
			for(int y=0; y<cols; y++){
				chunkedImages.add(Bitmap.createBitmap(scaledBitmap, xCoord, yCoord, chunkWidth, chunkHeight));

				xCoord += chunkWidth;
			}
			yCoord += chunkHeight;
		}
		createImageHelper(chunkedImages);

	}

	/**Saves images to sd and starts next activity*/
	public void createImageHelper(ArrayList<Bitmap> bitmap){
		for(int i=0;i<9;i++){
			createImageFromBitmap(bitmap.get(i), "img"+i);
		}
		try {
			img0= BitmapFactory.decodeStream(this.openFileInput("img0"));
			img1= BitmapFactory.decodeStream(this.openFileInput("img1"));
			img2= BitmapFactory.decodeStream(this.openFileInput("img2"));
			img3= BitmapFactory.decodeStream(this.openFileInput("img3"));
			img4= BitmapFactory.decodeStream(this.openFileInput("img4"));
			img5= BitmapFactory.decodeStream(this.openFileInput("img5"));
			img6= BitmapFactory.decodeStream(this.openFileInput("img6"));
			img7= BitmapFactory.decodeStream(this.openFileInput("img7"));
			img8= BitmapFactory.decodeStream(this.openFileInput("img8"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}