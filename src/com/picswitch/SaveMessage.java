package com.picswitch;

import java.io.ByteArrayOutputStream;
import java.io.File;

import com.parse.ParseFile;

import android.graphics.Bitmap;
import android.net.Uri;

/** 
 * 
 * @author Joey
 *Class which enables local saving of ParseObject type messages.
 *A message will be saved using this class, then added to HashMap.
 */
public class SaveMessage implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String firstCaption;
	private String secondCaption;
	private int timeLimit;
	private String messageUri;
	private String messageID;
	private String senderFirstName;
	private String senderLastName;
	private String senderUsername;
	private String senderObjectId;
	private boolean isFeatured;
	private byte[] imageData;
	private int switches;
	private double time;
	private String sponsorLink;
	private long solveTime;
	
	public SaveMessage(String firstCaption, String secondCaption, String messageUri,
					   int timeLimit, String  messageID, String senderFirstName, 
					   String senderLastName, String senderUsername, String senderObjectId, 
					   boolean isFeatured, Bitmap bitmap, int switches, double time, 
					   String sponsorLink, long solveTime){
		this.firstCaption = firstCaption;
		this.secondCaption = secondCaption;
		this.messageUri = messageUri;
		this.timeLimit = timeLimit;
		this.messageID = messageID;
		this.senderFirstName = senderFirstName;
		this.senderLastName = senderLastName;
		this.senderUsername = senderUsername;
		this.senderObjectId = senderObjectId;
		this.isFeatured = isFeatured;
		this.switches = switches;
		this.time = time;
		this.sponsorLink = sponsorLink;
		this.solveTime = solveTime;
		
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.JPEG, 75, stream);
		imageData = stream.toByteArray();
	}
	
	public long getSolveTime(){
		return solveTime;
	}
	
	public String getSponsorLink(){
		return sponsorLink;
	}
	
	public int getSwitches(){
		return switches;
	}
	
	public double getTime(){
		return time;
	}
	
	public byte[] getImageData(){
		return imageData;
	}
	
	public String getFirstCaption(){
		return firstCaption;
	}
	
	public String getSecondCaption(){
		return secondCaption;
	}
	
	public int getTimeLimit(){
		return timeLimit;
	}
	
	public String getMessageUri(){
		return messageUri;
	}
	
	public String getMessageID(){
		return messageID;
	}	
	
	public String getSenderFirstName(){
		return senderFirstName;
	}
	
	public String getSenderLastName(){
		return senderLastName;
	}
	
	public String getSenderUsername(){
		return senderUsername;
	}
	
	public boolean getIsFeatured(){
		return isFeatured;
	}
	
	public void setIsFeatured(boolean isFeatured){
		this.isFeatured = isFeatured;
	}
	
	public void setSenderObjectId(String objectId){
		this.senderObjectId = objectId;
	}
	
	public String getSenderObjectId(){
		return senderObjectId;
	}
	
}
