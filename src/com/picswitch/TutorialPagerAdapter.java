package com.picswitch;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class TutorialPagerAdapter extends FragmentPagerAdapter {

	public TutorialPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int arg0) {
		switch (arg0){
		case 0:{
			return new TutorialScreen1();
		}
		case 1:{
			return new TutorialScreen2();
		}
		case 2:{
			return new TutorialScreen3();
		}
		case 3:{
			return new TutorialScreen4();
		}
		case 4:{
			return new TutorialScreen5();
		}
		}
		return null;
	}

	@Override
	public int getCount() {
		return 5;
	}

}
