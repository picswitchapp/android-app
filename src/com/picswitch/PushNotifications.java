package com.picswitch;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.R.color;
import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParsePushBroadcastReceiver;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Picasso.LoadedFrom;
import com.squareup.picasso.Target;

public class PushNotifications extends ParsePushBroadcastReceiver{

	public static int numMessages = 0;

	NotificationManager mNotifM;

	Context mContext;

	@Override
	public void onPushReceive(Context context, Intent intent) {
		mNotifM = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		mContext = context;
		System.out.println("THIS 1");
		try {

			JSONObject json = new JSONObject(intent.getExtras().getString("com.parse.Data"));

			if(ParseUser.getCurrentUser()!=null){
				switch (json.getInt("type")) {
				case 0://new message
					generateNotificationMessageImage(context, "PicSwitch",json.getString("alert"), json);
					break;
				case 1://new friend request
					generateNotificationWithButtons(context, "PicSwitch",json.getString("alert"));
					break;
				case 2://friend request accepted
					generateNotification(context, "PicSwitch",json.getString("alert"));
					break;

				default:
					break;
				}
			}



		} catch (JSONException e) {
			Log.d("jsonexc", "JSONException: " + e.getMessage());
		}
	}

	@SuppressLint("NewApi")
	private void generateNotificationMessageImage(Context context, String title, String message, JSONObject json) {
		Intent intent = new Intent(context, TabActivity.class);
		PendingIntent contentIntent = PendingIntent.getActivity(context, 0, intent, 0);

		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
			Notification.Builder mBuilder = new Notification.Builder(context)
			.setDefaults(Notification.DEFAULT_ALL).setSmallIcon(R.drawable.logo_white_border)
			.setContentTitle(title).setContentText(message).setColor(Color.rgb(50, 153, 153));
			mBuilder.setContentIntent(contentIntent);
			new GetMessageImage(mBuilder).execute(json);
		}else{
			NotificationCompat.Builder mmBuilder = new NotificationCompat.Builder(context).setDefaults(Notification.DEFAULT_ALL)
					.setSmallIcon(R.drawable.logo_white_border).setContentTitle(title).setContentText(message);
			mmBuilder.setContentIntent(contentIntent);
			new GetMessageImageSupport(mmBuilder).execute(json);
		}
	}

	@SuppressLint("NewApi")
	private void generateNotificationWithButtons(Context context, String title, String message) {
		Bitmap bitmap = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_launcher);
		System.out.println("VALUE is is is" + bitmap.getByteCount());

		Intent intent = new Intent(context, MenuContactsList.class);
		PendingIntent contentIntent = PendingIntent.getActivity(context, 0, intent, Intent.FLAG_ACTIVITY_NEW_TASK);

		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
			Notification.Builder mBuilder = new Notification.Builder(context)
			.setDefaults(Notification.DEFAULT_ALL).setSmallIcon(R.drawable.logo_white_border)
			.setContentTitle(title).setContentText(message).setLargeIcon(bitmap).setColor(Color.rgb(50, 153, 153));

			mBuilder.setContentIntent(contentIntent);

			mNotifM.notify(ParseConstants.NOTIFICATION_FRIEND, mBuilder.build());
		}else{

			NotificationCompat.Builder mmBuilder = new NotificationCompat.Builder(context)
			.setDefaults(Notification.DEFAULT_ALL).setSmallIcon(R.drawable.logo_white_border)
			.setContentTitle(title).setContentText(message).setLargeIcon(bitmap);

			mmBuilder.setContentIntent(contentIntent);

			mNotifM.notify(ParseConstants.NOTIFICATION_FRIEND, mmBuilder.build());
		}
	}

	@SuppressLint("NewApi")
	private void generateNotification(Context context, String title, String message) {
		Intent intent = new Intent(context, TabActivity.class);
		PendingIntent contentIntent = PendingIntent.getActivity(context, 0, intent, 0);

		NotificationManager mNotifM = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		Bitmap bitmap = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_launcher);
		System.out.println("VALUE is is is" + bitmap.getByteCount());

		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
			Notification.Builder mBuilder = new Notification.Builder(context)
			.setDefaults(Notification.DEFAULT_ALL).setSmallIcon(R.drawable.logo_white_border)
			.setContentTitle(title).setContentText(message).setLargeIcon(bitmap).setColor(Color.rgb(50, 153, 153));

			mBuilder.setContentIntent(contentIntent);
			mNotifM.notify(ParseConstants.NOTIFICATION_TAB, mBuilder.build());
		}else{
			NotificationCompat.Builder mmBuilder = new NotificationCompat.Builder(context)
			.setDefaults(Notification.DEFAULT_ALL).setSmallIcon(R.drawable.logo_white_border)
			.setContentTitle(title).setContentText(message).setLargeIcon(bitmap);

			mmBuilder.setContentIntent(contentIntent);
			mNotifM.notify(ParseConstants.NOTIFICATION_TAB, mmBuilder.build());
		}

	}

	@SuppressLint("NewApi")
	@Override
	protected Notification getNotification(Context context, Intent intent) {
		Notification notification = super.getNotification(context, intent);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			notification.color = context.getResources().getColor(color.holo_red_dark);
		}
		return notification;
	}

	public class GetMessageImage extends AsyncTask<JSONObject, Void, Void>{

		Notification.Builder builder;

		public GetMessageImage(Notification.Builder builder){
			this.builder = builder;
		}

		@Override
		protected Void doInBackground(JSONObject... params) {

			JSONObject pushObject = params[0];
			String messageId = "";
			try {
				messageId = pushObject.getString("messageId");
			} catch (JSONException e) {
				e.printStackTrace();
			}

			if(messageId.length() > 0){
				ParseQuery<ParseObject> mQuery = new ParseQuery<ParseObject>(ParseConstants.CLASS_MESSAGES);
				mQuery.whereEqualTo(ParseConstants.KEY_MESSAGE_ID, messageId);
				mQuery.findInBackground(new FindCallback<ParseObject>() {
					@Override
					public void done(List<ParseObject> objects, ParseException e) {

						if(e == null){
							ParseObject mMessage = objects.get(0);						
							ParseFile mFile = mMessage.getParseFile(ParseConstants.KEY_PHOTO);
							final Uri messageUri = Uri.parse(mFile.getUrl());

							Target target = new Target(){
								@Override
								public void onBitmapFailed(Drawable arg0) {
								}
								@Override
								public void onBitmapLoaded(Bitmap bitmap, LoadedFrom arg1) {
									builder.setLargeIcon(bitmap);
									mNotifM.notify(ParseConstants.NOTIFICATION_TAB, builder.build());
								}
								@Override
								public void onPrepareLoad(Drawable arg0) {
								}
							};

							Picasso.with(mContext)
							.load(messageUri)
							.transform(new BitmapTransform(mMessage.getInt(ParseConstants.KEY_PREVIEW_INDEX)))
							.into(target);
						}
					}
				});
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void param){

		}

	}

	public class GetMessageImageSupport extends AsyncTask<JSONObject, Void, Void>{

		NotificationCompat.Builder builder;

		public GetMessageImageSupport(NotificationCompat.Builder builder){
			this.builder = builder;
		}

		@Override
		protected Void doInBackground(JSONObject... params) {

			JSONObject pushObject = params[0];
			String messageId = "";
			try {
				messageId = pushObject.getString("messageId");
			} catch (JSONException e) {
				e.printStackTrace();
			}

			if(messageId.length() > 0){
				ParseQuery<ParseObject> mQuery = new ParseQuery<ParseObject>(ParseConstants.CLASS_MESSAGES);
				mQuery.whereEqualTo(ParseConstants.KEY_MESSAGE_ID, messageId);
				mQuery.findInBackground(new FindCallback<ParseObject>() {
					@Override
					public void done(List<ParseObject> objects, ParseException e) {

						if(e == null){
							ParseObject mMessage = objects.get(0);						
							ParseFile mFile = mMessage.getParseFile(ParseConstants.KEY_PHOTO);
							final Uri messageUri = Uri.parse(mFile.getUrl());

							Target target = new Target(){
								@Override
								public void onBitmapFailed(Drawable arg0) {
								}
								@Override
								public void onBitmapLoaded(Bitmap bitmap, LoadedFrom arg1) {
									builder.setLargeIcon(bitmap);
									mNotifM.notify(ParseConstants.NOTIFICATION_TAB, builder.build());
								}
								@Override
								public void onPrepareLoad(Drawable arg0) {
								}
							};

							Picasso.with(mContext)
							.load(messageUri)
							.transform(new BitmapTransform(mMessage.getInt(ParseConstants.KEY_PREVIEW_INDEX)))
							.into(target);
						}
					}
				});
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void param){

		}

	}

}




