package com.picswitch;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**Solved tab
 * Specifies what happens in the solved tab of the TabActivity class*/
public class Solved extends Fragment {
  @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container,
              Bundle savedInstanceState) {
          View solved = inflater.inflate(R.layout.solved_tab, container, false);
          return solved;
}}
