package com.picswitch;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**New tab
 * Specifies what happens in the New picswitch tab of the TabActivity class*/
public class New extends Fragment {
  @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container,
              Bundle savedInstanceState) {
          View newTab = inflater.inflate(R.layout.new_tab, container, false);
          return newTab;
}}
