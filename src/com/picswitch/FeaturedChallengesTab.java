package com.picswitch;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.TextView;

import com.parse.ParseObject;


/**Featured tab
 * Specifies what happens in the Featured picswitch tab of the TabActivity class*/
@SuppressLint("NewApi")
public class FeaturedChallengesTab extends Fragment {
	
	int chunkNumbers = 9; //the number of chunks we want the images to be split into
	ArrayList<Bitmap> chunkedImages;

	private static final int SELECT_PICTURE = 1;
	private static final int RESULT_CROP = 0;
	private static final int RESULT_OK = -1;

	protected static final String TAG = null;

	public static final String TEMP_PHOTO_FILE_NAME = "temp_photo.jpg";
	protected File mFileTemp;
	protected Uri resultCropUri;

	private Context mContext;
	private boolean externalStorageAvailable;
	private ImageView img;

	public FeaturedChallengesTab(){
		// Required empty public constructor
	}
	protected List<ParseObject> mMessages;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mContext = container.getContext();

		View featuredTab = inflater.inflate(R.layout.featured_tab, container, false);

		ImageView left_arrow = (ImageView) featuredTab.findViewById(R.id.left_arrow_create);
		left_arrow.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				TabActivity.tabViewPager.setCurrentItem(1);
			}
		});

		TextView new_pic_button = (TextView) featuredTab.findViewById(R.id.new_pic_button_test);
		ImageView open_from_gallery = (ImageView) featuredTab.findViewById(R.id.open_image_from_gallery);

		new_pic_button.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(mContext, NewPic.class);
				startActivity(intent);
			}
		});

		open_from_gallery.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent choosePhotoIntent = new Intent(Intent.ACTION_GET_CONTENT);
				choosePhotoIntent.setType("image/*");
				startActivityForResult(choosePhotoIntent, SELECT_PICTURE);
			}
		});

		String state = Environment.getExternalStorageState();
		if(state.equals(Environment.MEDIA_MOUNTED)){
			mFileTemp = new File(Environment.getExternalStorageDirectory(), TEMP_PHOTO_FILE_NAME);
			externalStorageAvailable = true;
		}
		else{
			mFileTemp = new File(mContext.getFilesDir(), TEMP_PHOTO_FILE_NAME);
			externalStorageAvailable = false;
		}

		/**helper for image view
		 * Sets up the image to be displayed on the imageview*/

		img = (ImageView)featuredTab.findViewById(R.id.messageIcon);
		Display  display = getActivity().getWindowManager().getDefaultDisplay();
		int swidth = display.getWidth();
		LayoutParams params = img.getLayoutParams();
		params.width = LayoutParams.FILL_PARENT;
		params.height = params.width ;
		img.setLayoutParams(params);
		createImageFromBitmap(img.getDrawingCache(), "img");


		return featuredTab;
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data){
		if(resultCode == RESULT_OK){
			switch(requestCode){
			case SELECT_PICTURE:{
				try{
					InputStream inputStream = mContext.getContentResolver().openInputStream(data.getData());
					FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
					copyStream(inputStream, fileOutputStream);
					fileOutputStream.close();
					inputStream.close();

					crop();

				} catch(Exception e) {
					Log.e(TAG, "Error while create temp file", e);
				}
			}
			case RESULT_CROP:{
				String path = data.getStringExtra(CropImage.IMAGE_PATH);
				Bitmap bmp;
				if (path == null) { 
					return;
				}else{
					bmp = BitmapFactory.decodeFile(mFileTemp.getPath());

					saveToSDCard(bmp);
					img.setImageBitmap(bmp);
					splitImage(img, chunkNumbers);
				}
			}
			}

		}
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		getActivity().setProgressBarIndeterminate(true);

//				ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(ParseConstants.CLASS_FEATURED_PUZZLES);
//				query.findInBackground(new FindCallback<ParseObject>() {
//		
//					@Override
//					public void done(List<ParseObject> messages, ParseException e) {
//						getActivity().setProgressBarIndeterminate(false);
//						if(e==null){
//		
//							//Messages are available
//							mMessages = messages;
//							FeaturedChallengesAdapter adapter = new FeaturedChallengesAdapter(getListView().getContext(),
//									mMessages);
//							
//							setListAdapter(adapter);
//						}
//		
//					}
//				});

	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	/**cropping helper*/
	private void crop() {

		ExifInterface exif = null;
		try {
			exif = new ExifInterface(mFileTemp.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}  
		int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);

		// create explicit intent
		Intent intent = new Intent(mContext, CropImage.class);

		// tell CropImage activity to look for image to crop 
		//	    String filePath = photoUri.toString();
		intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());
		intent.putExtra(CropImage.IMAGE_ORIENTATION, orientation);

		// allow CropImage activity to rescale image
		intent.putExtra(CropImage.SCALE, true);

		// if the aspect ratio is fixed to square
		intent.putExtra(CropImage.ASPECT_X, 2);
		intent.putExtra(CropImage.ASPECT_Y, 2);

		// start activity CropImage with certain request code and listen
		// for result
		startActivityForResult(intent, RESULT_CROP);
		//		resultCropUri = intent.getData();
	}

	public void saveToSDCard(Bitmap bitmap) {
		// To be safe check whether external storage is mounted
		//helper method below
		if (externalStorageAvailable){
			File sdCard = Environment.getExternalStorageDirectory();
			File dir = new File(sdCard.getAbsolutePath() + "/Pictures");
			dir.mkdirs();
			File file = new File(dir, "cropped.png");
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
			FileOutputStream f = null;
			try {
				f = new FileOutputStream(file);
				if (f != null) {
					f.write(baos.toByteArray());
					f.flush();
					f.close();
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}


			//Return the image Uri
			resultCropUri = Uri.fromFile(file);
		}

		else{

		}

	}
	
	private void splitImage(ImageView image, int chunkNumbers) {    
		//For the number of rows and columns of the grid to be displayed
		int rows,cols;
		//For height and width of the small image chunks 
		int chunkHeight,chunkWidth;
		//To store all the small image chunks in bitmap format in this list 
		chunkedImages = new ArrayList<Bitmap>(chunkNumbers);
		//Getting the scaled bitmap of the source image
		BitmapDrawable drawable = (BitmapDrawable) image.getDrawable();
		Bitmap bitmap = drawable.getBitmap();
		Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), true);
		rows = cols = (int) Math.sqrt(chunkNumbers);
		chunkHeight = bitmap.getHeight()/rows;
		chunkWidth = bitmap.getWidth()/cols;
		//xCoord and yCoord are the pixel positions of the image chunks
		int yCoord = 0;
		for(int x=0; x<rows; x++){
			int xCoord = 0;
			for(int y=0; y<cols; y++){
				chunkedImages.add(Bitmap.createBitmap(scaledBitmap, xCoord, yCoord, chunkWidth, chunkHeight));
				xCoord += chunkWidth;
			}
			yCoord += chunkHeight;
		}
		createImageHelper(chunkedImages);
	}
	
	/**Saves images to sd and starts next activity*/
	public void createImageHelper(ArrayList<Bitmap> bitmap){
		for(int i=0;i<9;i++){
			createImageFromBitmap(bitmap.get(i), "img"+i);
		}
		Intent typeMessageIntent = new Intent(mContext, TypeMessage.class);
		typeMessageIntent.setData(resultCropUri);
		String fileType = ParseConstants.TYPE_IMAGE;
		typeMessageIntent.putExtra(ParseConstants.KEY_FILE_TYPE, fileType);
		startActivity(typeMessageIntent);


	}
	
	/**Creates images from the bitmaps*/
	public String createImageFromBitmap(Bitmap bitmap, String name) {
		String fileName = name;//no .png or .jpg needed
		try {
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
			FileOutputStream fo = mContext.openFileOutput(fileName, Context.MODE_PRIVATE);
			fo.write(bytes.toByteArray());
			// remember close file output
			fo.close();
		} catch (Exception e) {
			e.printStackTrace();
			fileName = null;
		}
		return fileName;

	}

	public static void copyStream(InputStream input, OutputStream output)
			throws IOException {

		byte[] buffer = new byte[1024];
		int bytesRead;
		while ((bytesRead = input.read(buffer)) != -1) {
			output.write(buffer, 0, bytesRead);
		}
	}



	//	@Override
	//	public void onListItemClick(ListView list, View v, int position, long id) {
	//		super.onListItemClick(list, v, position, id);
	//		ParseObject message = mMessages.get(position);
	//		String messageID = message.getObjectId();
	//		String firstCaption = message.getString(ParseConstants.CAPTION_FIRST);
	//		String secondCaption = message.getString(ParseConstants.CAPTION_SECOND);
	//		ParseFile file = message.getParseFile(ParseConstants.KEY_PHOTO);
	//		Uri fileUri = Uri.parse(file.getUrl());
	//		System.out.println("This is the file Uri " + fileUri);
	//		Intent intent = new Intent(getActivity(), OpenImage.class);
	//		intent.setData(fileUri);
	//		intent.putExtra(ParseConstants.KEY_MESSAGE_ID, messageID);
	//		intent.putExtra(ParseConstants.CAPTION_FIRST, firstCaption);
	//		intent.putExtra(ParseConstants.CAPTION_SECOND, secondCaption);
	//		startActivity(intent);
	//	}


}
