package com.picswitch;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.List;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.parse.ParseFile;
import com.parse.ParseObject;
import com.squareup.picasso.Picasso;

public class SentMessageAdapter extends ArrayAdapter<ParseObject>{
	protected Context mContext;
	protected List<ParseObject> mMessages;

	public SentMessageAdapter(Context context, List<ParseObject> messages){
		super(context, R.layout.sent_message_item, messages);
		mContext = context;
		mMessages = messages;
		
	}

	@SuppressWarnings("deprecation")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if(convertView==null){
			convertView = LayoutInflater.from(mContext).inflate(R.layout.sent_message_item, null);
			holder = new ViewHolder();
			holder.iconImageView = (ImageView)convertView.findViewById(R.id.messageIcon);
			holder.nameLabel = (TextView)convertView.findViewById(R.id.senderLabel);
			holder.monthLabel = (TextView)convertView.findViewById(R.id.monthLabel);
			holder.dateLabel = (TextView)convertView.findViewById(R.id.dateLabel);
			holder.messageLabel = (TextView)convertView.findViewById(R.id.messageLabel);
			holder.solveStatusLabel = (TextView)convertView.findViewById(R.id.solveStatusLabel);
			convertView.setTag(holder);
			
		}
		else{
			holder = (ViewHolder)convertView.getTag();
		}
		ParseObject message = mMessages.get(position);
		ParseFile file = message.getParseFile(ParseConstants.KEY_FILE);
		Uri messageUri = Uri.parse(file.getUrl());	
//		Picasso.with(getContext())
//	    .load(messageUri)
//	    .placeholder(R.drawable.logo)
//	    .transform(new BitmapTransform()) //to get the thumbnail images
//	    .into(holder.iconImageView);
		holder.nameLabel.setText("you said: " );
		holder.solveStatusLabel.setText(message.getString(ParseConstants.KEY_SOLVE_STATUS));
		Format formatMonth = new SimpleDateFormat("MMM");
		Format formatYear = new SimpleDateFormat("yyyy");
		Format formatTime = new SimpleDateFormat("hh:mm aa");
		Format formatDay = new SimpleDateFormat("dd");
		String monthL = formatMonth.format(message.getCreatedAt());
		String dateL = formatDay.format(message.getCreatedAt());
		String year = formatYear.format(message.getCreatedAt());
		String time = formatTime.format(message.getCreatedAt());
		
		holder.dateLabel.setText(dateL);
		holder.monthLabel.setText(monthL + " " + year +", " + time);
		holder.messageLabel.setText(message.getString(ParseConstants.CAPTION_FIRST));
		
		
		

		return convertView;
	}
	private static class ViewHolder{
		ImageView iconImageView;
		TextView nameLabel;
		TextView monthLabel;
		TextView dateLabel;
		TextView messageLabel;
		TextView solveStatusLabel;
		
		

	}




}
