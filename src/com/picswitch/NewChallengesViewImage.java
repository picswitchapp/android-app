package com.picswitch;

import java.io.FileNotFoundException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.Typeface;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseCrashReporting;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;

public class NewChallengesViewImage extends ActionBarActivity{
	ActionBar actionBar;
	TextView timerTextView;  /**Set up for the timer*/
	RelativeLayout rLayout;
	ArrayList<Integer> images = new ArrayList<Integer>(Arrays.asList(0,1,2,3,4,5,6,7,8));
	View.OnTouchListener onTouchListener;
	SurfaceHolder s;
	int switchCounter=0,switches,currentNewIndex;
	double timeTaken;
	ImageView placeholder;

	private String firstCaption, secondCaption;
	String senderFirstName;
	String senderUsername;
	String senderObjectId;
	String sponsorLink;
	String senderLastNameInitial;
	String messageID;

	boolean isSolved;
	boolean isFeatured;
	boolean isParseObject;
	boolean isWelcomeMessage = false;

	SoundPool sounds;
	int nextID;

	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi") @Override
	protected void onCreate(Bundle savedInstanceState) {

		getWindow().requestFeature(Window.FEATURE_ACTION_BAR);

		super.onCreate(savedInstanceState);

		getActionBar().hide();
		getWindow().setFormat(PixelFormat.RGBA_8888);

		setContentView(R.layout.new_challenges_view_image);

		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
			window.setStatusBarColor(Color.rgb(32, 99, 99));
		}
		Bundle b=this.getIntent().getExtras();
		String[] additionalData=b.getStringArray(ParseConstants.KEY_ADDITIONAL_DATA);
		firstCaption = additionalData[0];
		secondCaption = additionalData[1];
		timeTaken = Double.parseDouble(additionalData[2]);
		switches = Integer.parseInt(additionalData[3]);
		senderFirstName = additionalData[4];
		currentNewIndex = Integer.parseInt(additionalData[5]);
		senderUsername = additionalData[6];
		senderObjectId = additionalData[7];

		senderLastNameInitial = getIntent().getExtras().getString("senderLastNameInitial");
		sponsorLink = getIntent().getExtras().getString("sponsorLink");
		isFeatured = getIntent().getExtras().getBoolean("isFeatured");
		isParseObject = getIntent().getExtras().getBoolean("isParseObject");
		isSolved = getIntent().getExtras().getBoolean("isSolved");
		messageID = getIntent().getExtras().getString("messageID");

		System.out.println("ViewImage messageID = " + messageID);

		System.out.println("sponsorLink = " + sponsorLink);

		//check for welcome message
		//try/catch for edge case where messageid is not passed correctly
		try{
			if(messageID.equals("OYY02x2JPS")){
				isWelcomeMessage = true;
			}
		}catch(Exception e){
			e.printStackTrace();
			isWelcomeMessage = false;
		}

		buttonsHelper(); //handles buttons
		setUpCaptions();
		setUpImageView(getIntent());

		//set up the sounds		
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){

			AudioAttributes mAttributes = new AudioAttributes.Builder()
			.setUsage(AudioAttributes.USAGE_GAME)
			.setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
			.build();
			sounds = new SoundPool.Builder().setAudioAttributes(mAttributes).build();

		}else{
			sounds = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
		}
		nextID      = sounds.load(NewChallengesViewImage.this, R.raw.next_puzzle, 1);
	}

	public void setUpImageView(Intent intent){
		ImageView additionalView = (ImageView)findViewById(R.id.additional_view);
		Display  disp = getWindowManager().getDefaultDisplay();
		int width = disp.getWidth();
		additionalView.getLayoutParams().height = width;
		additionalView.getLayoutParams().width = width;
		Bitmap mBitmap = null;
		if(!isSolved){
			try{
				mBitmap = BitmapFactory.decodeStream(NewChallengesViewImage.this.openFileInput(ParseConstants.KEY_NEW_CHALLENGES_TEMP));
				additionalView.setImageBitmap(mBitmap);
			}catch(FileNotFoundException e){
				e.printStackTrace();
			}
		}else{
			//from solved on users phone, so get byte array from intent
			new BitmapWorker(additionalView).execute(intent.getExtras().getByteArray("imageData"));
		}

	}

	//AsyncTask to get entire image and load it into imageView for solved puzzles
	public class BitmapWorker extends AsyncTask<byte[], Void, Bitmap> {

		private final WeakReference<ImageView> imageViewReference;
		private byte[] data;

		public BitmapWorker(ImageView imageView){
			imageViewReference = new WeakReference<ImageView>(imageView);
		}

		@Override
		protected Bitmap doInBackground(byte[]... params) {
			data = params[0];
			return decodeSampledBitmapFromResource(data, 500, 500);
		}

		@Override
		protected void onPostExecute(Bitmap param){
			if(imageViewReference != null && param != null){
				final ImageView imageView = imageViewReference.get();
				if(imageView != null){
					imageView.setImageBitmap(param);
				}
			}
		}
	}

	/**Set up first and second captions*/
	@SuppressLint("NewApi")
	public void setUpCaptions(){

		Typeface varella    = Typeface.createFromAsset(getAssets(), "VarelaRound.ttf");
		Typeface montserrat = Typeface.createFromAsset(getAssets(), "Montserrat-Regular.ttf");

		TextView titleTextView = (TextView) findViewById(R.id.view_image_title_text);

		if(!isFeatured){
			if(senderLastNameInitial.length() > 0){
				titleTextView.setText(senderFirstName + " " + senderLastNameInitial + ".");
			}else{
				titleTextView.setText(senderFirstName);
			}
		}else{
			titleTextView.setText(senderFirstName);
		}

		titleTextView.setTypeface(montserrat);

		TextView firstCap = (TextView) findViewById(R.id.firstCaption);
		firstCap.setText(Html.fromHtml(firstCaption));
		firstCap.setTypeface(varella);

		TextView secondCap = (TextView) findViewById(R.id.secondCaption);
		secondCap.setText(Html.fromHtml(secondCaption));
		secondCap.setTypeface(varella);

		if(isFeatured || isWelcomeMessage){
			TextView switchesView = (TextView) findViewById(R.id.featured_switches);
			TextView timerView = (TextView) findViewById(R.id.featured_time_taken);
			switchesView.setTypeface(varella);
			timerView.setTypeface(varella);

			switchesView.setText(switches + " switches");
			timerView.setText(timeTaken + " seconds");
		}else{
			TextView switchesView = (TextView) findViewById(R.id.reply_switches);
			TextView timerView = (TextView) findViewById(R.id.reply_time_taken);
			switchesView.setTypeface(varella);
			timerView.setTypeface(varella);

			switchesView.setText(switches + " switches");
			timerView.setText(timeTaken + " seconds");
		}
	}

	/**Helper for buttons*/
	@SuppressLint("NewApi")
	public void buttonsHelper(){
		TextView viewImageNext = null;
		if((isFeatured) || (isWelcomeMessage)){
			((LinearLayout) findViewById(R.id.reply_bottom_buttons)).setVisibility(View.GONE);
			((LinearLayout) findViewById(R.id.featured_bottom_buttons)).setVisibility(View.VISIBLE);
			viewImageNext = (TextView) findViewById(R.id.featured_image_next);
		}else{
			((LinearLayout) findViewById(R.id.reply_bottom_buttons)).setVisibility(View.VISIBLE);
			((LinearLayout) findViewById(R.id.featured_bottom_buttons)).setVisibility(View.GONE);
			viewImageNext = (TextView) findViewById(R.id.view_image_next_picswitch);
		}
		viewImageNext.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {

				if(TabActivity.SOUND){
					sounds.play(nextID, 1, 1, 0, 0, 1);
				}

				ParseObject nextMessage = null;
				SaveMessage nextSaveMessage = null;

				System.out.println("next_checks...isFeatured: " + isFeatured + "   isSolved: " + isSolved);
				System.out.println("next_checks...currentNewIndex: " + currentNewIndex);
				System.out.println("next_checks...TabActivity.mMessages.size(): " + TabActivity.mMessages.size());
				System.out.println("next_checks...TabActivity.mFeatured.size(): " + TabActivity.mFeatured.size());
				System.out.println("next_checks...TabActivity.savedFeatured.size(): " + TabActivity.savedFeatured.size());
				System.out.println("next_checks...TabActivity.savedSolved.size(): " + TabActivity.savedSolved.size());

				if((!isFeatured) && (!isSolved)){

					currentNewIndex--;

					//case for new messages
					if(currentNewIndex < 0){
						Toast.makeText(NewChallengesViewImage.this, "End of new puzzles", Toast.LENGTH_SHORT).show();
						NewChallengesViewImage.this.finish();
					}else{
						nextMessage = TabActivity.mMessagesLinkedList.get(TabActivity.mMessagesLinkedList.size() - 1 - currentNewIndex);
						startIntentForParseObject(nextMessage);
					}


				}else if((isFeatured) && (!isSolved)){

					currentNewIndex--;

					//new featured case
					if(currentNewIndex < 0){
						//no more featured left to skip through
						Toast.makeText(NewChallengesViewImage.this, "End of featured puzzles", Toast.LENGTH_SHORT).show();
						NewChallengesViewImage.this.finish();
					}else{
						nextMessage = TabActivity.mFeaturedLinkedList.get(TabActivity.mFeaturedLinkedList.size() - 1 - currentNewIndex);
						startIntentForParseObject(nextMessage);
					}

				}else if(isSolved){

					currentNewIndex++;

					//normal solved case
					if(currentNewIndex > TabActivity.savedSolved.size() - 1){
						Toast.makeText(NewChallengesViewImage.this, "End of solved puzzles", Toast.LENGTH_SHORT).show();
						NewChallengesViewImage.this.finish();
					}else{
						nextSaveMessage = TabActivity.savedSolvedLinkedList.get(currentNewIndex);
						if(nextSaveMessage != null){
							startIntentForSaveMessage(nextSaveMessage);
						}
					}

				}
			}
		});

		if(!isFeatured){
			TextView replyButton = (TextView) findViewById(R.id.view_image_reply);
			replyButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(getBaseContext(), ReplyCamera.class);
					intent.putExtra(ParseConstants.KEY_SENDER_FIRST_NAME, senderFirstName);
					intent.putExtra(ParseConstants.KEY_SENDER, senderUsername);
					intent.putExtra("senderObjectId", senderObjectId);
					startActivity(intent);
					finish();
				}
			});
		}

		ImageView backButton = (ImageView) findViewById(R.id.view_image_back_button);
		backButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});

		ImageView moreInfoButton = (ImageView) findViewById(R.id.featured_more_info_button);
		if(isFeatured && (sponsorLink != null) && (sponsorLink.length() > 3)){
			moreInfoButton.setVisibility(View.VISIBLE);
		}
		moreInfoButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				//Toast.makeText(getBaseContext(), "more info click", Toast.LENGTH_SHORT).show();
				System.out.println("more info link: " + sponsorLink);

				if((sponsorLink != null) && (sponsorLink.length() > 3)){
					Intent i = new Intent(Intent.ACTION_VIEW);
					i.setData(Uri.parse(sponsorLink));
					startActivity(i);
				}
			}
		});
	}

	public void startIntentForParseObject(ParseObject object){
		Intent intent = new Intent(getBaseContext(), OpenImage.class);

		String firstCaption = object.getString(ParseConstants.CAPTION_FIRST);
		String secondCaption = object.getString(ParseConstants.CAPTION_SECOND);
		ParseFile file = object.getParseFile(ParseConstants.KEY_PHOTO);
		int timeLimit = object.getInt(ParseConstants.KEY_TIME_LIMIT);
		Uri fileUri = Uri.parse(file.getUrl());

		String firstName="";	
		String lastName="";

		if(!isFeatured){
			try {
				firstName = object.getParseUser(ParseConstants.KEY_SENDER).fetchIfNeeded().getString(ParseConstants.KEY_FIRST_NAME);
				lastName  = object.getParseUser(ParseConstants.KEY_SENDER).fetchIfNeeded().getString(ParseConstants.KEY_LAST_NAME);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}else{
			firstName = object.getString(ParseConstants.KEY_COMPANY_NAME);
		}

		String lastNameInitial = ""; 
		if(lastName.length() > 0){
			lastNameInitial = lastName.substring(0, 1);
		}

		System.out.println("next_checks...firstName (ParseObject intent): " + firstName);

		intent.setData(fileUri);

		intent.putExtra("messageID", object.getObjectId());
		intent.putExtra("isFeatured", isFeatured);
		intent.putExtra("isParseObject", isParseObject);
		intent.putExtra(ParseConstants.KEY_CURRENT_NEW_INDEX, currentNewIndex);
		intent.putExtra(ParseConstants.CAPTION_FIRST, firstCaption);
		intent.putExtra(ParseConstants.CAPTION_SECOND, secondCaption);
		intent.putExtra(ParseConstants.KEY_TIME_LIMIT, timeLimit);
		intent.putExtra(ParseConstants.KEY_SENDER_FIRST_NAME, firstName);
		intent.putExtra("senderLastNameInitial", lastNameInitial);
		startActivity(intent);
		finish();
	}

	public void startIntentForSaveMessage(SaveMessage mMessage){
		String firstCaption = mMessage.getFirstCaption();
		String secondCaption = mMessage.getSecondCaption();
		String senderUsername = mMessage.getSenderUsername();
		String senderObjectId = mMessage.getSenderObjectId();
		String sponsorLink = mMessage.getSponsorLink();
		String mMessageID = mMessage.getMessageID();
		int switches = mMessage.getSwitches();
		double time = mMessage.getTime();

		boolean featuredStatus = mMessage.getIsFeatured();

		String firstName="";		
		String lastName="";
		String lastNameInitial="";

		if(!featuredStatus){
			lastName = mMessage.getSenderLastName();
			if(lastName.length() > 0){
				lastNameInitial = lastName.substring(0, 1);
			}
		}

		firstName = mMessage.getSenderFirstName();

		System.out.println("next_checks...firstName (SaveMessage intent): " + firstName);

		Intent intent = new Intent(NewChallengesViewImage.this, NewChallengesViewImage.class);

		//Toast.makeText(mContext, "loading size (bytes): " + mMessage.getImageData().length, Toast.LENGTH_SHORT).show();
		//System.out.println("loading size (bytes): " + nextSaveMessage.getImageData().length);

		intent.putExtra("sponsorLink", sponsorLink);
		intent.putExtra("imageData", mMessage.getImageData());
		intent.putExtra("isSolved", isSolved);
		intent.putExtra("isFeatured", featuredStatus);
		intent.putExtra("isParseObject", false);
		intent.putExtra("messageID", mMessageID);

		intent.putExtra("senderLastNameInitial", lastNameInitial);

		Bundle bundle=new Bundle();
		bundle.putStringArray(ParseConstants.KEY_ADDITIONAL_DATA, new String[]
				{firstCaption, secondCaption, Double.toString(time), 
				Integer.toString(switches), firstName, Integer.toString(currentNewIndex), 
				senderUsername, senderObjectId});
		intent.putExtras(bundle);

		startActivity(intent);
		finish();
	}

	public static Bitmap decodeSampledBitmapFromResource(byte[] bitmapData, int reqWidth, int reqHeight) {

		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeByteArray(bitmapData, 0, bitmapData.length, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeByteArray(bitmapData, 0, bitmapData.length, options);
	}

	public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;

		System.out.println("options bitmap values - height: " + height + ", width: " + width);

		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight
					&& (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}

		return inSampleSize;
	}

}
