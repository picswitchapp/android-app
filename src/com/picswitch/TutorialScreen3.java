package com.picswitch;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class TutorialScreen3 extends Fragment {

	public TutorialScreen3() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View newView;
		newView = inflater.inflate(R.layout.tutorial_screen3, container, false);
		TextView giraffeButton = (TextView) newView.findViewById(R.id.giraffe_button);
		giraffeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				TutorialActivity.tutorialPager.setCurrentItem(3);
			}
		});
		return newView;
	}

}
