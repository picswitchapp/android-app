package com.picswitch;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

/***Class for handling the opening of sent images*/
public class SentImageActivity extends ActionBarActivity {
	ActionBar actionBar;
	ImageView imageView;
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sent_image);
		getWindow().setFormat(PixelFormat.RGBA_8888);
		Uri imageUri = getIntent().getData();

		//placeholder = (ImageView)findViewById(R.id.imageView01);
		Picasso.with(this).load(imageUri.toString()).into(target);
		imageView = (ImageView) findViewById(R.id.sentImage);
		actionBar = getActionBar(); //Set up action bar
		setupActionBar();
		buttonsHelper(); //handles buttons
		titleBarHelper(savedInstanceState); //handles title bar
	}

	/**Helper for buttons*/
	public void buttonsHelper(){
		Button reset = (Button) findViewById(R.id.back_to_main_button);
		reset.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent i = new Intent(getBaseContext(), MainActivity.class);

				startActivity(i);
				finish();

			}

		});

	}
	/**title bar set up helper*/
	public void titleBarHelper(Bundle savedInstanceState){
		LinearLayout titleBar = (LinearLayout) findViewById(R.id.sent_screen_title_bar);
		Display  disp = getWindowManager().getDefaultDisplay();
		int width = disp.getWidth();
		LayoutParams paramz = titleBar.getLayoutParams();
		paramz.width = LayoutParams.FILL_PARENT;
		titleBar.setLayoutParams(paramz);
	}

	/**Assigns bitmaps from the SD to image Views*/
	@SuppressLint("NewApi")
	public void assignImageViewPics(Bitmap bitmap){
		Bitmap b = bitmap;
		System.out.println("Bitmap:" +b.getAllocationByteCount());
		if(b.getAllocationByteCount()>0){
			setProgressBarIndeterminateVisibility(false);
			imageView.setImageBitmap(b);
		}
		else{
			while(b==null){
				setProgressBarIndeterminateVisibility(true);
			}

		}
	}


	/**Initializes the dimensions of the imageviews that hold the chunked 
	 * images*/
	public void imageViewsDimensions(){
		Display  display = getWindowManager().getDefaultDisplay();
		//width of the imageViews
		//minus 6 to allows for space between imageViews
		int swidth = (int) (display.getWidth());

		imageView.getLayoutParams().width = swidth;
		imageView.getLayoutParams().height = swidth;

	}
	Target target = new Target() {
		@Override
		public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
			assignImageViewPics(bitmap); //add images to imageViews
			imageViewsDimensions(); //sets up the dimensions of the imageviews

		}

		@Override
		public void onBitmapFailed(Drawable arg0) {
			
		}

		@Override
		public void onPrepareLoad(Drawable arg0) {


		}
	};


	/**Sets up a custom actionbar*/
	@SuppressLint("NewApi")
	private void setupActionBar(){
		final ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(
				R.layout.sent_screen_title_bar,
				null);
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setCustomView(actionBarLayout);

	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.sent_image, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
