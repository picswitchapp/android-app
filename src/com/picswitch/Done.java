package com.picswitch;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

@SuppressLint("NewApi")
/**A Dialog box to handle user dialog**/
//Still unused 
public class Done extends DialogFragment {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("The Carribean!")
               .setPositiveButton("BACK", new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                       // FIRE ZE MISSILES!
                	   dismiss();
                   }
               });
//               .setNegativeButton("RESET", new DialogInterface.OnClickListener() {
//                   public void onClick(DialogInterface dialog, int id) {
//                       // User cancelled the dialog
//                	 // NewPic t= new NewPic();
//                   }
//               });
        // Create the AlertDialog object and return it
        return builder.create();
    }
}
