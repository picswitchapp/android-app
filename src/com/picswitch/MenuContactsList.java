package com.picswitch;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseCrashReporting;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.parse.SaveCallback;

public class MenuContactsList extends Activity {
	public static final String TAG = MenuContactsList.class.getSimpleName();

	private SwipeRefreshLayout swipeContainer;

	protected List<ParseUser>  pendingRequests   = new ArrayList<ParseUser>();
	protected List<ParseUser>  mUsers = new ArrayList<ParseUser>();

	protected HashMap<String, String> friendsUsernames = new HashMap<String, String>();
	protected HashMap<String, String> friendsFirstName = new HashMap<String, String>();

	//for displaying views
	protected List<String>    parseFriends = new ArrayList<String>();
	protected HashMap<String, ParseUser> nameToUser = new HashMap<String, ParseUser>();
	protected List<String>    parseRequests = new ArrayList<String>();
	protected HashMap<String, ParseUser> nameToRequest = new HashMap<String, ParseUser>();

	//used to retrieve the actual parseUser object for blocking
	protected HashMap<String, ParseUser> allParseUsers = new HashMap<String, ParseUser>();

	protected ParseRelation<ParseUser> mFriendsRelation;
	protected ParseRelation<ParseUser> mPendingRequests;
	protected ParseUser mCurrentUser;

	//for blocking
	protected ParseRelation<ParseUser> blockingRelation;
	protected ParseRelation<ParseUser> friendsToModify;

	LayoutInflater mInflater;

	//container labels
	TextView pendingLabel;
	TextView friendsLabel;
	TextView titleText;

	ViewGroup mContactsContainer;
	ViewGroup mPendingContainer;

	//bottom bar layouts
	RelativeLayout bottomBars;
	LinearLayout requestBar;
	LinearLayout friendBar;

	//bottom buttons
	TextView blockRequest;
	TextView denyRequest;
	TextView confirmRequest;
	TextView blockFriend;
	TextView deleteFriend;
	TextView sendToFriend;

	//keeping track of checks
	boolean somethingChecked = false;
	View checkedView;
	String requestCheckedObjectId;
	String friendCheckedObjectId;

	//title typeFace:
	Typeface montSerrat; 

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

		setContentView(R.layout.menu_contacts_list);

		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
			window.setStatusBarColor(Color.rgb(32, 99, 99));
		}
		NotificationManager mNotifM = (NotificationManager) getBaseContext().getSystemService(Context.NOTIFICATION_SERVICE);

		try{
			mNotifM.cancel(ParseConstants.NOTIFICATION_FRIEND);
		}
		catch(Exception e){
			e.printStackTrace();
		}

		montSerrat = Typeface.createFromAsset(MenuContactsList.this.getAssets(),"Montserrat-Regular.ttf");

		blockingRelation = ParseUser.getCurrentUser().getRelation("blocked");
		friendsToModify = ParseUser.getCurrentUser().getRelation("friends");

		swipeContainer = (SwipeRefreshLayout) findViewById(R.id.friends_refresh_container);
		swipeContainer.setOnRefreshListener(new OnRefreshListener() {
			@Override
			public void onRefresh() {
				new findFriendsInBackground().execute();
				new findPendingRequestsInBackground().execute();
			}
		});
		
		swipeContainer.setColorSchemeResources(R.color.refresh_color_1,
				R.color.refresh_color_2,
				R.color.refresh_color_3,
				R.color.refresh_color_4);

		mContactsContainer = (LinearLayout) findViewById(R.id.main_contacts_container);
		mPendingContainer = (LinearLayout) findViewById(R.id.pending_container);
		mInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		//container labels
		titleText    = (TextView) findViewById(R.id.title_text_contacts);
		pendingLabel = (TextView) findViewById(R.id.pending_label);
		friendsLabel = (TextView) findViewById(R.id.friends_label);
		titleText.setTypeface(montSerrat);
		pendingLabel.setTypeface(montSerrat);
		friendsLabel.setTypeface(montSerrat);

		//set fonts of labels

		//bottom bars
		bottomBars = (RelativeLayout) findViewById(R.id.bottom_bars);
		requestBar = (LinearLayout)   findViewById(R.id.block_deny_confirm_bar);
		friendBar  = (LinearLayout)   findViewById(R.id.block_delete_send_to);

		//bottom buttons
		blockRequest   = (TextView) findViewById(R.id.block_request_button);
		denyRequest    = (TextView) findViewById(R.id.deny_request_button);
		confirmRequest = (TextView) findViewById(R.id.confirm_request_button);
		blockFriend    = (TextView) findViewById(R.id.block_friend_button);
		deleteFriend   = (TextView) findViewById(R.id.delete_friend_button);
		sendToFriend   = (TextView) findViewById(R.id.send_to_friend_button);

		//clickListeners for ^^
		blockRequest.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(MenuContactsList.this);
				builder.setTitle("Confirm blocking");
				builder.setMessage("Are you sure you want to block this user?");
				builder.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						//add the user to be blocked
						blockingRelation.add(allParseUsers.get(requestCheckedObjectId));

						//save current user to parse
						ParseUser.getCurrentUser().saveInBackground(new SaveCallback() {
							@Override
							public void done(ParseException e) {
								if(e == null){

								}else{
									e.printStackTrace();
								}
							}
						});

						//deny the request
						HashMap<String, String> params = new HashMap<String, String>();
						params.put("otherUserId", requestCheckedObjectId);
						params.put("action", "1");
						ParseCloud.callFunctionInBackground("FRRequestHandle", params, new FunctionCallback<String>() {
							@Override
							public void done(String object, ParseException e) {
								if(e == null){
									Toast.makeText(MenuContactsList.this, "Successfully denied!", Toast.LENGTH_SHORT).show();
									System.out.println("requestHandle returned: " + object);

									for(ParseUser user : pendingRequests){
										if(user.getObjectId() == requestCheckedObjectId){
											pendingRequests.remove(user);
											requestCheckedObjectId = null;
											break;
										}
									}

									//displayContent(pendingRequests, mPendingContainer);
									//mPendingContainer.removeAllViews();
									//new CreatePendingViews().execute(pendingRequests);
									new findPendingRequestsInBackground().execute();

									//delete from allParseUsers
									allParseUsers.remove(requestCheckedObjectId);

								}else{
									e.printStackTrace();
								}
							}
						});
						bottomBars.setVisibility(View.GONE);
					}
				});
				builder.setNegativeButton("No",
						new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
						removeChecks(checkedView);
						bottomBars.setVisibility(View.GONE);
					}
				});
				AlertDialog blockReqDialog = builder.create();
				blockReqDialog.show();

				int titleId = getResources().getIdentifier( "alertTitle", "id", "android" );
				if (titleId > 0) {
					TextView mTitle = (TextView) blockReqDialog.findViewById(titleId);
					if (mTitle != null) {
						mTitle.setTypeface(montSerrat);
					}
				}
				Button button1 = (Button) blockReqDialog.findViewById(android.R.id.button1);
				Button button2 = (Button) blockReqDialog.findViewById(android.R.id.button2);
				FontsUtils.TypeFace(button1, getAssets());
				FontsUtils.TypeFace(button2, getAssets());

				TextView mMessage = (TextView) blockReqDialog.findViewById(android.R.id.message);
				mMessage.setTextSize(15);
				FontsUtils.TypeFace(mMessage, MenuContactsList.this.getAssets());
			}
		});

		denyRequest.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//calls background function "FRRequestHandle" to deny
				HashMap<String, String> params = new HashMap<String, String>();
				params.put("otherUserId", requestCheckedObjectId);
				params.put("action", "1");
				ParseCloud.callFunctionInBackground("FRRequestHandle", params, new FunctionCallback<String>() {
					@Override
					public void done(String object, ParseException e) {
						if(e == null){
							Toast.makeText(MenuContactsList.this, "Successfully denied!", Toast.LENGTH_SHORT).show();
							System.out.println("requestHandle returned: " + object);

							for(ParseUser user : pendingRequests){
								if(user.getObjectId() == requestCheckedObjectId){
									pendingRequests.remove(user);
									requestCheckedObjectId = null;
									break;
								}
							}

							//displayContent(pendingRequests, mPendingContainer);
							//mPendingContainer.removeAllViews();
							//new CreatePendingViews().execute(pendingRequests);
							new findPendingRequestsInBackground().execute();

						}else{
							e.printStackTrace();
						}
					}
				});
				bottomBars.setVisibility(View.GONE);
			}
		});

		confirmRequest.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//calls background function "FRRequestHandle" to confirm
				HashMap<String, String> params = new HashMap<String, String>();
				params.put("otherUserId", requestCheckedObjectId);
				params.put("action", "0");
				ParseCloud.callFunctionInBackground("FRRequestHandle", params, new FunctionCallback<String>() {
					@Override
					public void done(String object, ParseException e) {
						if(e == null){
							Toast.makeText(MenuContactsList.this, "Added successfully!", Toast.LENGTH_SHORT).show();
							System.out.println("requestHandle returned: " + object);
							//change location of the View

							for(ParseUser user : pendingRequests){
								if(user.getObjectId() == requestCheckedObjectId){
									pendingRequests.remove(user);
									mUsers.add(user);
									requestCheckedObjectId = null;
									break;
								}
							}

							new findFriendsInBackground().execute();
							new findPendingRequestsInBackground().execute();

						}else{
							e.printStackTrace();
						}
					}
				});
				bottomBars.setVisibility(View.GONE);
			}
		});

		blockFriend.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(MenuContactsList.this);
				builder.setTitle("Confirm blocking");
				builder.setMessage("Are you sure you want to block this friend?");	
				builder.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						//add the user to be blocked
						blockingRelation.add(allParseUsers.get(friendCheckedObjectId));
						//remove the blocked user from the CurrentUsers friends
						friendsToModify.remove(allParseUsers.get(friendCheckedObjectId));
						//save current users to parse
						ParseUser.getCurrentUser().saveInBackground(new SaveCallback() {
							@Override
							public void done(ParseException e) {
								if(e == null){

								}else{
									e.printStackTrace();
								}
							}
						});

						//delete from allParseUsers
						allParseUsers.remove(friendCheckedObjectId);

						//remove that request from the screen
						for(ParseUser user : mUsers){
							if(user.getObjectId() == friendCheckedObjectId){
								mUsers.remove(user);
								friendCheckedObjectId = null;
								break;
							}
						}


						new findFriendsInBackground().execute();

						if(mUsers.size() == 0){
							friendsLabel.setVisibility(View.GONE);
						}

						bottomBars.setVisibility(View.GONE);
					}
				});
				builder.setNegativeButton("No",
						new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
						removeChecks(checkedView);
						bottomBars.setVisibility(View.GONE);
					}
				});

				AlertDialog blockFriDialog = builder.create();
				blockFriDialog.show();

				int titleId = getResources().getIdentifier( "alertTitle", "id", "android" );
				if (titleId > 0) {
					TextView mTitle = (TextView) blockFriDialog.findViewById(titleId);
					if (mTitle != null) {
						mTitle.setTypeface(montSerrat);
					}
				}

				Button button1 = (Button) blockFriDialog.findViewById(android.R.id.button1);
				Button button2 = (Button) blockFriDialog.findViewById(android.R.id.button2);
				FontsUtils.TypeFace(button1, getAssets());
				FontsUtils.TypeFace(button2, getAssets());

				TextView mMessage = (TextView) blockFriDialog.findViewById(android.R.id.message);
				mMessage.setTextSize(15);
				FontsUtils.TypeFace(mMessage, getAssets());
			}
		});

		deleteFriend.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(MenuContactsList.this);
				builder.setTitle("Delete friend");
				builder.setMessage("Are you sure you want to delete this friend?");
				builder.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						//calls "FRRemoveFriend" to remove the selected friend
						HashMap<String, String> params = new HashMap<String, String>();
						if(friendCheckedObjectId != null){
							params.put("otherUserId", friendCheckedObjectId);
						}
						ParseCloud.callFunctionInBackground("FRRemoveFriend", params, new FunctionCallback<String>(){
							@Override
							public void done(String object, ParseException e) {
								if(e == null){
									Toast.makeText(MenuContactsList.this, "Deleted successfully!", Toast.LENGTH_SHORT).show();

									//delete from allParseUsers
									allParseUsers.remove(friendCheckedObjectId);

									//remove the view
									for(ParseUser user : mUsers){
										if(user.getObjectId() == friendCheckedObjectId){
											mUsers.remove(user);
											friendCheckedObjectId = null;
											break;
										}
									}

									//displayContent(mUsers, mContactsContainer);
									//mContactsContainer.removeAllViews();
									//new CreateFriendViews().execute(mUsers);
									new findFriendsInBackground().execute();

									if(mUsers.size() == 0){
										friendsLabel.setVisibility(View.GONE);
									}

								}else{
									e.printStackTrace();
								}
							}

						});
						bottomBars.setVisibility(View.GONE);
					}
				});
				builder.setNegativeButton("No", 
						new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
						removeChecks(checkedView);
						bottomBars.setVisibility(View.GONE);
					}
				});

				AlertDialog deleteDialog = builder.create();
				deleteDialog.show();

				int titleId = getResources().getIdentifier( "alertTitle", "id", "android" );
				if (titleId > 0) {
					TextView mTitle = (TextView) deleteDialog.findViewById(titleId);
					if (mTitle != null) {
						mTitle.setTypeface(montSerrat);
					}
				}

				Button button1 = (Button) deleteDialog.findViewById(android.R.id.button1);
				Button button2 = (Button) deleteDialog.findViewById(android.R.id.button2);
				FontsUtils.TypeFace(button1, getAssets());
				FontsUtils.TypeFace(button2, getAssets());

				TextView mMessage = (TextView) deleteDialog.findViewById(android.R.id.message);
				mMessage.setTextSize(15);
				FontsUtils.TypeFace(mMessage, getAssets());
			}
		});

		sendToFriend.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent sendIntent = new Intent(MenuContactsList.this, ReplyCamera.class);
				sendIntent.putExtra("senderObjectId", friendCheckedObjectId);
				sendIntent.putExtra(ParseConstants.KEY_SENDER, friendsUsernames.get(friendCheckedObjectId));
				sendIntent.putExtra(ParseConstants.KEY_SENDER_FIRST_NAME, friendsFirstName.get(friendCheckedObjectId));
				startActivity(sendIntent);
				MenuContactsList.this.finish();
				bottomBars.setVisibility(View.GONE);
			}
		});

		mCurrentUser = ParseUser.getCurrentUser();
		mFriendsRelation = mCurrentUser.getRelation("friends");
		mPendingRequests = mCurrentUser.getRelation("friendRequests");

		ImageView addFriendsButton = (ImageView) findViewById(R.id.add_friends_button);
		addFriendsButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getBaseContext(), AddFriendsSelector.class);
				startActivity(intent);
			}
		});

		ImageView backButton = (ImageView) findViewById(R.id.left_arrow_contacts);
		backButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				MenuContactsList.this.finish();
			}
		});

		//get content
		new findFriendsInBackground().execute();
		new findPendingRequestsInBackground().execute();
	}

	@Override
	public void onResume(){
		super.onResume();
	}

	public void removeChecks(View view){
		CheckBox cb = (CheckBox) view.findViewById(R.id.check_box);
		cb.setChecked(false);
	}

	public class findFriendsInBackground extends AsyncTask<Void,Void,Void> {

		@Override
		protected Void doInBackground(Void... params) {

			ParseQuery<ParseUser> friendsQuery = mFriendsRelation.getQuery();
			friendsQuery.findInBackground(new FindCallback<ParseUser>() {

				@Override
				public void done(List<ParseUser> objects, ParseException e) {
					if(e == null){
						mUsers = objects;						
						//displayContent(mUsers, mContactsContainer);
						mPendingContainer.removeAllViews();
						new CreateFriendViews().execute(objects);
					}else{
						e.printStackTrace();
					}

				}

			});

			return null;
		}

		@Override
		protected void onPostExecute(Void param){
			if(swipeContainer.isRefreshing()){
				swipeContainer.setRefreshing(false);
			}
		}

	}

	public class CreateFriendViews extends AsyncTask<List<ParseUser>, Wrapper, Void>{

		String currentLetter = null;
		ViewGroup mLetterContainer;

		@Override
		protected void onPreExecute(){
			mContactsContainer.removeAllViews();
		}

		@Override
		protected Void doInBackground(List<ParseUser>... params) {
			List<ParseUser> friendsToDisplay = params[0];

			nameToUser.clear();
			parseFriends.clear();

			boolean moreNamesToAdd = true;

			//check again for the zero friends case
			if(friendsToDisplay.size() == 0){
				return null;
			}

			for(ParseUser user : friendsToDisplay){
				String personsName = user.getString(ParseConstants.KEY_FIRST_NAME) + " " + 
						user.getString(ParseConstants.KEY_LAST_NAME);

				parseFriends.add(personsName);
				nameToUser.put(personsName, user);

				friendsFirstName.put(user.getObjectId(), user.getString(ParseConstants.KEY_FIRST_NAME));
				friendsUsernames.put(user.getObjectId(), user.getUsername());

				if(!(allParseUsers.containsKey(user.getObjectId()))){
					allParseUsers.put(user.getObjectId(), user);
				}
			}

			int letterCount = 0;
			int contactCount = 0;
			int totalContactCount = 0;

			String firstLetter;

			Collections.sort(parseFriends, String.CASE_INSENSITIVE_ORDER);

			do{
				//make a new letter
				if(parseFriends.get(totalContactCount) != null){
					firstLetter = parseFriends.get(totalContactCount).substring(0,1);
				}else{
					System.out.println("firstLetter is null");
					break;
				}

				currentLetter = firstLetter;
				View newLetter;
				if(letterCount % 2 == 0){
					newLetter = mInflater.inflate(R.layout.menu_contacts_list_item, mContactsContainer, false);
				}else{
					newLetter = mInflater.inflate(R.layout.menu_contacts_list_item_dark, mContactsContainer, false);
				}
				TextView letter = (TextView) newLetter.findViewById(R.id.contacts_menu_letter);
				letter.setText(firstLetter);
				FontsUtils.TypeFace(letter, getAssets());

				mLetterContainer = (LinearLayout) newLetter.findViewById(R.id.contacts_container);

				publishProgress(new Wrapper(mContactsContainer, newLetter, mLetterContainer, null));

				letterCount++;
				contactCount = 0;

				do{
					//creating and adding views to new letter container
					final String person = parseFriends.get(totalContactCount);

					//add person to the current letter
					final View newContact;
					if(contactCount % 2 == 0){
						newContact = mInflater.inflate(R.layout.contact_light, mLetterContainer, false);
					}else{
						newContact = mInflater.inflate(R.layout.contact_dark, mLetterContainer, false);
					}
					TextView contact = (TextView) newContact.findViewById(R.id.contact_text);
					contact.setText(person);
					FontsUtils.TypeFace(contact, getAssets());

					final CheckBox checkBox = (CheckBox) newContact.findViewById(R.id.check_box);

					runOnUiThread(new Runnable(){
						@Override
						public void run() {
							newContact.setOnClickListener(new View.OnClickListener() {
								@Override
								public void onClick(View v) {
									if(!(checkBox.isChecked())){
										checkBox.setChecked(true);
									}else{
										checkBox.setChecked(false);
									}
								}
							});

							checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
								@Override
								public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
									if(isChecked){
										if(somethingChecked == false){
											bottomBars.setVisibility(View.VISIBLE);
											somethingChecked = true;
											checkedView = newContact;
											friendCheckedObjectId = nameToUser.get(person).getObjectId();

										}else{
											bottomBars.setVisibility(View.GONE);
											if(checkedView != null){
												removeChecks(checkedView);
												checkedView = null;
											}
											checkBox.setChecked(false);
										}
									}else{
										somethingChecked = false;
										checkedView = null;
										bottomBars.setVisibility(View.GONE);
									}
									requestBar.setVisibility(View.GONE);
									friendBar.setVisibility(View.VISIBLE);
								}
							});
						}
					});

					publishProgress(new Wrapper(null, null, mLetterContainer, newContact));

					totalContactCount++;
					contactCount++;

					//exits the loop when reached the end of contacts to add
					if(totalContactCount == parseFriends.size()){
						moreNamesToAdd = false;
						break;
					}


				}while(parseFriends.get(totalContactCount).substring(0,1).equals(currentLetter));

			}while(moreNamesToAdd);

			return null;
		}

		ViewGroup letterAddTo;

		@Override 
		protected void onProgressUpdate(Wrapper... params){
			Wrapper param = params[0];
			ViewGroup addToUiGroup = param.mAddToUi;
			View letterView = param.letterView;
			ViewGroup currLetterViewGroup = param.letterViewGroup;
			View contact = param.newContact;

			if(addToUiGroup == null){
				//adding to the same letter
				letterAddTo.addView(contact);
			}else{
				//adding a new letter
				if(contact == null){
					//add the empty new letter to the Ui
					addToUiGroup.addView(letterView);
					letterAddTo = currLetterViewGroup;
				}
			}
		}

		@Override
		protected void onPostExecute(Void param){
			if(mContactsContainer.getChildCount() > 0){
				friendsLabel.setVisibility(View.VISIBLE);
			}else{
				friendsLabel.setVisibility(View.GONE);
			}
		}

	}

	public class findPendingRequestsInBackground extends AsyncTask<Void,Void,Void> {

		@Override
		protected Void doInBackground(Void... params) {

			ParseQuery<ParseUser> pendingQuery = mPendingRequests.getQuery();
			pendingQuery.findInBackground(new FindCallback<ParseUser>(){

				@Override
				public void done(List<ParseUser> objects, ParseException e) {
					if(e == null){
						System.out.println("requests.size: " + objects.size());
						pendingRequests = objects;
						//displayContent(pendingRequests, mPendingContainer);
						mPendingContainer.removeAllViews();
						new CreatePendingViews().execute(objects);
					}else{
						e.printStackTrace();
					}
				}
			});

			return null;
		}

		@Override
		protected void onPostExecute(Void param){
			if(swipeContainer.isRefreshing()){
				swipeContainer.setRefreshing(false);
			}
		}

	}

	public class CreatePendingViews extends AsyncTask<List<ParseUser>, Wrapper, Void>{

		String currentLetter = null;
		ViewGroup mLetterContainer;

		@Override
		protected void onPreExecute(){
			mPendingContainer.removeAllViews();
		}

		@Override
		protected Void doInBackground(List<ParseUser>... params) {
			List<ParseUser> requestsToDisplay = params[0];

			parseRequests.clear();
			nameToRequest.clear();

			boolean moreNamesToAdd = true;

			//check again for the zero friends case
			if(requestsToDisplay.size() == 0){
				return null;
			}

			for(ParseUser user : requestsToDisplay){
				String personsName = user.getString(ParseConstants.KEY_FIRST_NAME) + " " + 
						user.getString(ParseConstants.KEY_LAST_NAME);

				parseRequests.add(personsName);
				nameToRequest.put(personsName, user);

				if(!(allParseUsers.containsKey(user.getObjectId()))){
					allParseUsers.put(user.getObjectId(), user);
				}
			}

			int letterCount = 0;
			int contactCount = 0;
			int totalContactCount = 0;

			String firstLetter;
			Collections.sort(parseRequests, String.CASE_INSENSITIVE_ORDER);

			do{
				//make a new letter
				if(parseRequests.get(totalContactCount) != null){
					firstLetter = parseRequests.get(totalContactCount).substring(0,1);
				}else{
					System.out.println("firstLetter is null");
					break;
				}

				currentLetter = firstLetter;
				View newLetter;
				if(letterCount % 2 == 0){
					newLetter = mInflater.inflate(R.layout.menu_contacts_list_item, mPendingContainer, false);
				}else{
					newLetter = mInflater.inflate(R.layout.menu_contacts_list_item_dark, mPendingContainer, false);
				}
				TextView letter = (TextView) newLetter.findViewById(R.id.contacts_menu_letter);
				letter.setText(firstLetter);
				FontsUtils.TypeFace(letter, getAssets());

				mLetterContainer = (LinearLayout) newLetter.findViewById(R.id.contacts_container);

				publishProgress(new Wrapper(mPendingContainer, newLetter, mLetterContainer, null));

				letterCount++;
				contactCount = 0;

				do{
					//creating and adding views to new letter container
					final String person = parseRequests.get(totalContactCount);

					//add person to the current letter
					final View newContact;
					if(contactCount % 2 == 0){
						newContact = mInflater.inflate(R.layout.contact_light, mLetterContainer, false);
					}else{
						newContact = mInflater.inflate(R.layout.contact_dark, mLetterContainer, false);
					}
					TextView contact = (TextView) newContact.findViewById(R.id.contact_text);
					contact.setText(person);
					FontsUtils.TypeFace(contact, getAssets());

					final CheckBox checkBox = (CheckBox) newContact.findViewById(R.id.check_box);

					runOnUiThread(new Runnable(){
						@Override
						public void run() {
							newContact.setOnClickListener(new View.OnClickListener() {
								@Override
								public void onClick(View v) {
									if(!(checkBox.isChecked())){
										checkBox.setChecked(true);
									}else{
										checkBox.setChecked(false);
									}
								}
							});

							checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
								@Override
								public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
									if(isChecked){
										if(somethingChecked == false){
											bottomBars.setVisibility(View.VISIBLE);
											somethingChecked = true;
											checkedView = newContact;
											requestCheckedObjectId = nameToRequest.get(person).getObjectId();

										}else{
											bottomBars.setVisibility(View.GONE);
											if(checkedView != null){
												removeChecks(checkedView);
												checkedView = null;
											}
											checkBox.setChecked(false);
										}
									}else{
										somethingChecked = false;
										checkedView = null;
										bottomBars.setVisibility(View.GONE);
									}
									requestBar.setVisibility(View.VISIBLE);
									friendBar.setVisibility(View.GONE);

								}
							});
						}
					});

					publishProgress(new Wrapper(null, null, mLetterContainer, newContact));

					totalContactCount++;
					contactCount++;

					//exits the loop when reached the end of contacts to add
					if(totalContactCount == parseRequests.size()){
						moreNamesToAdd = false;
						break;
					}


				}while(parseRequests.get(totalContactCount).substring(0,1).equals(currentLetter));

			}while(moreNamesToAdd);

			return null;
		}

		ViewGroup letterAddTo;

		@Override 
		protected void onProgressUpdate(Wrapper... params){
			Wrapper param = params[0];
			ViewGroup addToUiGroup = param.mAddToUi;
			View letterView = param.letterView;
			ViewGroup currLetterViewGroup = param.letterViewGroup;
			View contact = param.newContact;

			if(addToUiGroup == null){
				//adding to the same letter
				letterAddTo.addView(contact);
			}else{
				//adding a new letter
				if(contact == null){
					//add the empty new letter to the Ui
					addToUiGroup.addView(letterView);
					letterAddTo = currLetterViewGroup;
				}
			}
		}

		@Override
		protected void onPostExecute(Void param){
			pendingLabel.setText("requests(" + pendingRequests.size() + ")");
		}

	}

	public class Wrapper{
		public final ViewGroup mAddToUi;
		public final View letterView;
		public final ViewGroup letterViewGroup;
		public final View newContact;

		public Wrapper(ViewGroup addToUi, View letterToAdd, ViewGroup lettersViewGroup, View contact){
			mAddToUi = addToUi;
			letterView = letterToAdd;
			letterViewGroup = lettersViewGroup;
			newContact = contact;
		}
	}
}



