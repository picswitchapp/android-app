package com.picswitch;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;

public class ContactsAdapter extends BaseAdapter {
	
	private List<String> names;
	private List<String> phnos;

	public ContactsAdapter(List<String> names, List<String> phnos){
		this.names = names;
		this.phnos = phnos;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		View view = convertView;
		
		if(view == null){
			LayoutInflater mInflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = (RelativeLayout) mInflater.inflate(R.layout.menu_contacts_list_item, parent, false);
		}
		
		return null;
	}

}
