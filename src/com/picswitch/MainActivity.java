package com.picswitch;

import java.util.HashMap;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AppEventsLogger;
import com.parse.FunctionCallback;
import com.parse.LogInCallback;
import com.parse.ParseAnalytics;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.ui.ParseLoginBuilder;

//import com.parse.ui.ParseLoginBuilder;
/**Shows the user profile. The user has to be logged in.
 */
@SuppressLint("NewApi")
public class MainActivity extends Activity {
	private static final int LOGIN_REQUEST = 0;
	private TextView titleTextView;
	private TextView emailTextView;
	private TextView nameTextView;
	private Button loginButton;
	private Button signUpButton;
	private ParseUser currentUser;
	Typeface varella;
	Typeface montserrat;
	ActionBar actionBar;
	EditText emailField, passwordField;
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);

		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
			window.setStatusBarColor(Color.rgb(51, 51, 51));
		}

		ParseAnalytics.trackAppOpened(getIntent());
		if (android.os.Build.VERSION.SDK_INT >= 11){
			actionBar = getActionBar(); //initialize the actionbar
		}
		if (android.os.Build.VERSION.SDK_INT < 11){
			//actionBar = getSupportActionBar(); //initialize the actionbar
		}
		varella = Typeface.createFromAsset(getBaseContext().getAssets(), "VarelaRound.ttf");
		montserrat = Typeface.createFromAsset(getBaseContext().getAssets(), "Montserrat-Regular.ttf");
		emailField = (EditText)findViewById(R.id.login_email_input);
		passwordField = (EditText)findViewById(R.id.login_password_input);
		emailField.setTypeface(varella);
		passwordField.setTypeface(varella);

		passwordField.setOnKeyListener(new View.OnKeyListener() {	
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (keyCode == EditorInfo.IME_ACTION_DONE)) {
					logIn();
				} 
				return false;
			}
		});

		//inflate a custom actionBar
		final ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(
				R.layout.title_bar,
				null);
		// Set up your ActionBar
		//setUpActionBar();
		titleTextView = (TextView) findViewById(R.id.profile_title);
		emailTextView = (TextView) findViewById(R.id.profile_email);
		nameTextView = (TextView) findViewById(R.id.profile_name);
		loginButton = (Button) findViewById(R.id.login_button);
		signUpButton = (Button) findViewById(R.id.sign_up_button);

		TextView welcomeMessage = (TextView) findViewById(R.id.main_welcome_message);
		FontsUtils.TypeFace(welcomeMessage, getAssets());
		FontsUtils.TypeFace(loginButton, getAssets());
		FontsUtils.TypeFace(signUpButton, getAssets());

		titleTextView.setText(R.string.profile_title_logged_in);

		SpannableString ss = new SpannableString(Html.fromHtml("<i>By using picswitch, you acknowledge that you have read and agree to our <u>terms of use</u> and <u>privacy policy</u>.</i>"));
		ClickableSpan tosClickableSpan = new ClickableSpan(){
			@Override
			public void onClick(View widget) {
				//open tos in intent
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse("http://picswitchapp.com/terms/"));
				startActivity(intent);
			}
		};
		ClickableSpan ppClickableSpan = new ClickableSpan(){
			@Override
			public void onClick(View widget) {
				//open pp in intent
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse("http://picswitchapp.com/privacy/"));
				startActivity(intent);
			}
		};
		ss.setSpan(tosClickableSpan, 72, 84, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		ss.setSpan(ppClickableSpan , 89, 103, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

		TextView legalText = (TextView) findViewById(R.id.legal_disclaimer);
		legalText.setText(ss);
		legalText.setMovementMethod(LinkMovementMethod.getInstance());

		//add listener to loginOrLogoutButton
		loginOrlogOut();

	}

	@Override
	public void onBackPressed(){
		moveTaskToBack(true);
	}
	
	public void logIn(){
		ParseUser.logInInBackground(emailField.getText().toString().trim(), passwordField.getText().toString().trim(), new LogInCallback() {
			@Override
			public void done(ParseUser user, ParseException e) {
				if (e == null && user != null) {
					currentUser = ParseUser.getCurrentUser();
					try{
						List<String> check = currentUser.getList("puzzlesSolved");
						System.out.println("check: " + check.toString());
						if(check.size() < 1){
							HashMap<String, String> params = new HashMap<String, String>();
							ParseCloud.callFunctionInBackground("tutorialMessage", params, new FunctionCallback<String>() {
								@Override
								public void done(String object, ParseException e3) {

									if(e3 == null){
										//logged in, so return											
									}else{
										e3.printStackTrace();
										//Toast.makeText(MainActivity.this, "Error logging in, please try again", Toast.LENGTH_LONG).show();
									}
									startTabActivity();
								}
							});
						}
						startTabActivity();
					}catch(Exception e1){
						e1.printStackTrace();
						HashMap<String, String> params = new HashMap<String, String>();
						ParseCloud.callFunctionInBackground("tutorialMessage", params, new FunctionCallback<String>() {
							@Override
							public void done(String object, ParseException e2) {

								if(e2 == null){
									//logged in, so return											
									startTabActivity();
								}else{
									e2.printStackTrace();
									Toast.makeText(MainActivity.this, "Error logging in, please try again", Toast.LENGTH_LONG).show();
								}

							}
						});
					}
				} else if (user == null) {
					//email or password invalid
					Toast.makeText(getApplicationContext(), "Email or password invalid", Toast.LENGTH_SHORT).show();
				} else {
					//something else went wrong
				}
			}
		});
	}


	/**on click listener for loginOrLogoutButton helper*/
	public void loginOrlogOut(){
		loginButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				logIn();
			}
		});
		signUpButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				ParseLoginBuilder loginBuilder = new ParseLoginBuilder(MainActivity.this);
				startActivityForResult(loginBuilder.build(), LOGIN_REQUEST);
			}
		});
	}


	@SuppressLint("NewApi")
	/**Actionbar setUp helper*/
	public void setUpActionBar(){
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayShowCustomEnabled(true);
	}

	@Override
	protected void onStart() {
		super.onStart();
		currentUser = ParseUser.getCurrentUser();
		if (currentUser != null) {
			startTabActivity();
		} else {
			showProfileLoggedOut();
		}
	}


	/**
	 * Start TabActivity activity
	 * Means the user has successfully logged in.
	 * Start either tutorial or TabActivity depending on sharedPrefs. */
	private void startTabActivity() {

		//get shared preferences to see if already seen tutorial
		SharedPreferences prefs = MainActivity.this.getSharedPreferences("com.picswitch", Context.MODE_PRIVATE);
		boolean tutorialSeen = prefs.getBoolean("tutorialSeen", false);

		if(tutorialSeen){
			Intent i = new Intent(getApplicationContext(), TabActivity.class);
			startActivity(i);
		}else{
			Intent i = new Intent(getApplicationContext(), TutorialActivity.class);
			startActivity(i);
		}

		ParseInstallation installation = ParseInstallation.getCurrentInstallation();
		installation.put("username", currentUser.getUsername());
		installation.saveInBackground();
		finish();
	}


	/**
	 * Show a message asking the user to log in, toggle login/logout button text.
	 */
	private void showProfileLoggedOut() {
		titleTextView.setText(R.string.profile_title_logged_out);
		emailTextView.setText("");
		nameTextView.setText("");
		//loginOrLogoutButton.setText(R.string.profile_login_button_label);
	}


	@Override
	protected void onResume() {
		super.onResume();

		// Logs 'install' and 'app activate' App Events.
		AppEventsLogger.activateApp(this);
	}



	@Override
	protected void onPause() {
		super.onPause();

		// Logs 'app deactivate' App Event.
		AppEventsLogger.deactivateApp(this);
	}
}