package com.picswitch;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.parse.ParseUser;

public class Settings extends Activity {

	public boolean isLoggedOut = false;
	Context mContext;
	private static final int PICK_PHOTO_FOR_AVATAR = 3;
	
	ProgressDialog mDialog;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
		super.onCreate(savedInstanceState);

		getActionBar().hide();

		setContentView(R.layout.activity_settings);

		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
			window.setStatusBarColor(Color.rgb(32, 99, 99));
		}
		
		mContext = Settings.this;
		
		mDialog = new ProgressDialog(mContext);
		mDialog.setMessage("Loading image...");
		mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		
		//clickListeners
		ImageView left_arrow = (ImageView) findViewById(R.id.left_arrow_settings);
		left_arrow.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Settings.this.finish();
			}
		});

		final Typeface montserrat = Typeface.createFromAsset(getAssets(), "Montserrat-Regular.ttf");

		//setting fonts for the labels and title
		TextView settingsTitle = (TextView) findViewById(R.id.title_text_settings);
		settingsTitle.setTypeface(montserrat);
		TextView accountLabel = (TextView) findViewById(R.id.account_label);
		accountLabel.setTypeface(montserrat);
		TextView preferencesLabel = (TextView) findViewById(R.id.preferences_label);
		preferencesLabel.setTypeface(montserrat);
		TextView othersLabel = (TextView) findViewById(R.id.others_label);
		othersLabel.setTypeface(montserrat);

		//change name
		TextView editProfile = (TextView) findViewById(R.id.edit_profile);
		FontsUtils.TypeFace(editProfile, getAssets());
		editProfile.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent editIntent = new Intent(Settings.this, EditProfile.class);
				startActivity(editIntent);
			}
		});

		//change password
		TextView changePass = (TextView) findViewById(R.id.change_password);
		FontsUtils.TypeFace(changePass, getAssets());
		changePass.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent changeIntent = new Intent(Settings.this, ChangePassword.class);
				startActivity(changeIntent);
			}
		});

		//enter phone number
		TextView enterPhone = (TextView) findViewById(R.id.enter_phone_number);
		FontsUtils.TypeFace(enterPhone, getAssets());
		enterPhone.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent phoneIntent = new Intent(Settings.this, SetPhoneNumber.class);
				startActivity(phoneIntent);
			}
		});
		
		//change username
		TextView changeUsername = (TextView) findViewById(R.id.set_username);
		FontsUtils.TypeFace(changeUsername, getAssets());
		changeUsername.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Settings.this, SetUsername.class);
				startActivity(intent);
			}
		});
		
		//upload image
		TextView upload = (TextView) findViewById(R.id.upload_profile_image);
		FontsUtils.TypeFace(upload, getAssets());
		upload.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Settings.this, UploadProfileImage.class);
				startActivity(intent);
			}
		});
		
		//sound
		TextView soundText = (TextView) findViewById(R.id.sound_text);
		final CheckBox soundCB = (CheckBox) findViewById(R.id.sound_check_box);
		soundCB.setChecked(TabActivity.SOUND);
		FontsUtils.TypeFace(soundText, getAssets());
		RelativeLayout soundLayout = (RelativeLayout) findViewById(R.id.sound_setting);
		soundLayout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(soundCB.isChecked()){
					soundCB.setChecked(false);
				}else{
					soundCB.setChecked(true);
				}
			}
		});
		soundCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked){
					TabActivity.SOUND = true;
				}else{
					TabActivity.SOUND = false;
				}
			}
		});

		//save photos
		TextView savePhotosText = (TextView) findViewById(R.id.save_photos_text);
		final CheckBox savePhotosCB = (CheckBox) findViewById(R.id.save_photos_check_box);
		savePhotosCB.setChecked(TabActivity.SAVE_PHOTOS);
		FontsUtils.TypeFace(savePhotosText, getAssets());
		RelativeLayout savePhotosLayout = (RelativeLayout) findViewById(R.id.photo_save_setting);
		savePhotosLayout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(savePhotosCB.isChecked()){
					savePhotosCB.setChecked(false);
				}else{
					savePhotosCB.setChecked(true);
				}
			}
		});
		savePhotosCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked){
					TabActivity.SAVE_PHOTOS = true;
				}else{
					TabActivity.SAVE_PHOTOS = false;
				}
			}
		});

		//faq
		TextView faq = (TextView) findViewById(R.id.settings_FAQs);
		FontsUtils.TypeFace(faq, getAssets());
		faq.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse("http://www.picswitchapp.com/faqs/"));
				startActivity(intent);
			}
		});
		
		//terms of service
		TextView tos = (TextView) findViewById(R.id.settings_terms_of_service);
		FontsUtils.TypeFace(tos, getAssets());
		tos.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse("http://www.picswitchapp.com/terms/"));
				startActivity(intent);
			}
		});
		
		//privacy policy
		TextView pp = (TextView) findViewById(R.id.settings_privacy_policy);
		FontsUtils.TypeFace(pp, getAssets());
		pp.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse("http://www.picswitchapp.com/privacy/"));
				startActivity(intent);
			}
		});
		
		//contestRules
		TextView cr = (TextView) findViewById(R.id.settings_contest_rules);
		FontsUtils.TypeFace(cr, getAssets());
		cr.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse("http://www.picswitchapp.com/contest-rules/"));
				startActivity(intent);
			}
		});
		
		//logout
		TextView logout = (TextView) findViewById(R.id.log_out);
		FontsUtils.TypeFace(logout, getAssets());
		logout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(Settings.this);
				builder.setTitle("Logout");
				builder.setMessage("Are you sure you want to logout?");
				builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						saveSettingsToPhone();
						
						//clear shared prefs for addfriends enter ph number dialog
						SharedPreferences prefs = Settings.this.getSharedPreferences("com.picswitch", Context.MODE_PRIVATE);
						prefs.edit().putBoolean("noBefore", false).apply();
						
						ParseUser.logOut();
						Intent mainActivityIntent = new Intent(getBaseContext(), MainActivity.class);
						startActivity(mainActivityIntent);
						isLoggedOut = true;
						finish();
					}
				});
				builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
				AlertDialog logoutDialog = builder.create();
				logoutDialog.show();
				
				int titleId = getResources().getIdentifier( "alertTitle", "id", "android" );
				if (titleId > 0) {
					TextView mTitle = (TextView) logoutDialog.findViewById(titleId);
					if (mTitle != null) {
						mTitle.setTypeface(montserrat);
					}
				}
				
				Button button1 = (Button) logoutDialog.findViewById(android.R.id.button1);
				Button button2 = (Button) logoutDialog.findViewById(android.R.id.button2);
				FontsUtils.TypeFace(button1, getAssets());
				FontsUtils.TypeFace(button2, getAssets());

				TextView mMessage = (TextView) logoutDialog.findViewById(android.R.id.message);
				mMessage.setTextSize(15);
				FontsUtils.TypeFace(mMessage, getAssets());
			}
		});
	}

	public void saveSettingsToPhone(){
		//save settings to the phone
		if(!isLoggedOut){
			HashMap<String, SettingsWrapper> saveSettings = new HashMap<String, SettingsWrapper>();
			saveSettings.put(ParseUser.getCurrentUser().getObjectId(), 
					new SettingsWrapper(TabActivity.SOUND, TabActivity.SAVE_PHOTOS));

			try{

				FileOutputStream fos = new FileOutputStream(TabActivity.settingsFile);
				ObjectOutputStream oos = new ObjectOutputStream(fos);

				oos.writeObject(saveSettings);
				oos.flush();
				oos.close();
				fos.close();

			}catch(Exception e){
				e.printStackTrace();
				System.out.println("saving settings failed");
			}
		}
	}

	@Override
	public void onResume(){
		super.onResume();
	}

	@Override
	public void onPause(){
		System.out.println("inside Settings.onPause");
		super.onPause();

		saveSettingsToPhone();

	}
	

}
