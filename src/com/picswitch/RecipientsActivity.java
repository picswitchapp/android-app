package com.picswitch;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseCrashReporting;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.parse.codec.binary.Base64;

public class RecipientsActivity extends Activity {
	public static final String TAG = RecipientsActivity.class.getSimpleName();
	protected List<ParseUser> mUsers;
	protected List<String>    parseFriends = new ArrayList<String>();
	protected ParseRelation<ParseUser> mFriendsRelation;
	protected ParseUser mCurrentUser;
	protected MenuItem mSendMenuItem;
	protected Uri mMediaUri;
	protected String mFileType;
	protected String firstCaption;
	protected String secondCaption;
	protected int timeLimit;
	protected int previewIndex;

	protected ArrayList<String> recipientNames = new ArrayList<String>();
	protected ParseObject message;

	protected HashMap<String, ParseUser> nameToUser = new HashMap<String, ParseUser>();

	LayoutInflater mInflater;
	String currentLetter = null;
	ViewGroup mContactsContainer;
	ViewGroup mLetterContainer;

	Button sendButton;

	CheckBox sendToSelf;

	int letterCount = 0;
	int contactCount = 0;
	int positionInContacts = 0;

	ProgressDialog mDialog;

	boolean loadingFromParse = true;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_recipients);

		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
			window.setStatusBarColor(Color.rgb(32, 99, 99));
		}
		mMediaUri = getIntent().getData();
		mFileType = getIntent().getExtras().getString(ParseConstants.KEY_FILE_TYPE);
		firstCaption = getIntent().getExtras().getString(ParseConstants.CAPTION_FIRST);
		secondCaption = getIntent().getExtras().getString(ParseConstants.CAPTION_SECOND);
		timeLimit = getIntent().getExtras().getInt(ParseConstants.KEY_TIME_LIMIT);
		previewIndex = getIntent().getExtras().getInt(ParseConstants.KEY_PREVIEW_INDEX);

		mContactsContainer = (LinearLayout) findViewById(R.id.recipients_container);

		mDialog = new ProgressDialog(this);
		mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		mDialog.setMessage("Sending message...");

		ImageView left_button = (ImageView) findViewById(R.id.left_arrow_recipients_activity);
		left_button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent returnIntent = new Intent();
				setResult(RESULT_CANCELED, returnIntent);
				finish();
			}
		});

		sendButton = (Button) findViewById(R.id.send_button);
		sendButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if(!loadingFromParse){
					mDialog.show();
					send(message);		
				}else{
					//do nothing
				}
			}
		});

		sendToSelf = (CheckBox) findViewById(R.id.send_to_yourself_checkbox);
		TextView sendToSelfText = (TextView) findViewById(R.id.send_to_yourself_text);
		FontsUtils.TypeFace(sendToSelfText, getAssets());

		RelativeLayout sendToSelfLayout = (RelativeLayout) findViewById(R.id.send_to_yourself_layout);
		sendToSelfLayout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(sendToSelf.isChecked()){
					sendToSelf.setChecked(false);
				}else{
					sendToSelf.setChecked(true);
				}
			}
		});

		sendToSelf.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked){
					sendButton.setVisibility(View.VISIBLE);
				}else{
					if(recipientNames.size() == 0){
						sendButton.setVisibility(View.GONE);
					}
				}
			}
		});

		//fonts
		Typeface montSerrat = Typeface.createFromAsset(getAssets(), "Montserrat-Regular.ttf");

		TextView titleText = (TextView) findViewById(R.id.title_text_recipients_activity);
		TextView chooseLabel = (TextView) findViewById(R.id.choose_friends_label);
		titleText.setTypeface(montSerrat);
		chooseLabel.setTypeface(montSerrat);
	}

	@Override
	protected void onResume() {
		super.onResume();
		mCurrentUser = ParseUser.getCurrentUser();
		mFriendsRelation = mCurrentUser.getRelation("friends");

		setProgressBarIndeterminateVisibility(true);

		//new findFriendsInBackGround task
		mInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		new FindFriendsInBackground().execute();

	}

	protected void send(ParseObject message){

		List<String> recipientObjIds = new ArrayList<String>();
		for(String name : recipientNames){
			recipientObjIds.add(nameToUser.get(name).getObjectId());
		}
		if(sendToSelf.isChecked()){
			recipientObjIds.add(ParseUser.getCurrentUser().getObjectId());
		}

		byte[] fileBytes = FileHelper.getByteArrayFromFile(this, mMediaUri);

		//compress the image beforesending
		Bitmap bmTemp = BitmapFactory.decodeByteArray(fileBytes, 0, fileBytes.length);
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bmTemp.compress(Bitmap.CompressFormat.JPEG, 50, stream);
		byte[] imageData = stream.toByteArray();

		String base64String = Base64.encodeBase64String(imageData);

		mDialog.show();

		HashMap<String, Object> params = new HashMap<String,Object>();
		params.put("recipientIds", recipientObjIds);
		params.put("photo", base64String);
		params.put("caption1", firstCaption);
		params.put("caption2", secondCaption);
		params.put("previewIndex", previewIndex);
		params.put("puzzleSize", 3);
		params.put("timeLimit", timeLimit);
		ParseCloud.callFunctionInBackground("MSSend", params, new FunctionCallback<String>(){

			@Override
			public void done(String object, ParseException e) {
				if(e == null){
					Toast.makeText(RecipientsActivity.this, R.string.success_message, Toast.LENGTH_LONG).show();
					mDialog.cancel();
					Intent returnIntent = new Intent();
					returnIntent.putExtra("result", RESULT_OK);
					setResult(RESULT_OK, returnIntent);
					finish();
				}else{
					mDialog.cancel();
					Toast.makeText(RecipientsActivity.this, "Sending failed", Toast.LENGTH_SHORT).show();
					Intent returnIntent = new Intent();
					setResult(RESULT_CANCELED, returnIntent);
					finish();
				}
			}

		});

	}

	public class FindFriendsInBackground extends AsyncTask<Void, Void, Void>{

		@Override
		protected Void doInBackground(Void... params) {

			ParseQuery<ParseUser> query = mFriendsRelation.getQuery();
			query.orderByAscending("firstName");
			query.setLimit(1000);
			query.findInBackground(new FindCallback<ParseUser>() {
				@Override
				public void done(List<ParseUser> objects, ParseException e) {
					if(e == null){
						if(objects.size() > 0){
							mUsers = objects;
							new CreateFriendViews().execute(objects);
						}else{
							//case of no friends (lol)
						}
					}else{
						e.printStackTrace();
					}
				}
			});

			return null;
		}

		@Override
		protected void onPostExecute(Void param){
			loadingFromParse = false;
		}
	}

	public class CreateFriendViews extends AsyncTask <List<ParseUser>, Wrapper, Void>{

		@Override
		protected Void doInBackground(List<ParseUser>... params) {

			List<ParseUser> friendsToDisplay = params[0];

			boolean moreNamesToAdd = true;

			//check again for the zero friends case
			if(friendsToDisplay.size() == 0){
				return null;
			}

			for(ParseUser user : friendsToDisplay){
				String personsName = user.getString(ParseConstants.KEY_FIRST_NAME) + " " + 
						user.getString(ParseConstants.KEY_LAST_NAME);

				parseFriends.add(personsName);
				nameToUser.put(personsName, user);
			}

			int letterCount = 0;
			int contactCount = 0;
			int totalContactCount = 0;

			String firstLetter;

			do{
				//make a new letter
				if(parseFriends.get(totalContactCount) != null){
					firstLetter = parseFriends.get(totalContactCount).substring(0,1);
				}else{
					System.out.println("firstLetter is null");
					break;
				}

				currentLetter = firstLetter;
				View newLetter;
				if(letterCount % 2 == 0){
					newLetter = mInflater.inflate(R.layout.menu_contacts_list_item, mContactsContainer, false);
				}else{
					newLetter = mInflater.inflate(R.layout.menu_contacts_list_item_dark, mContactsContainer, false);
				}
				TextView letter = (TextView) newLetter.findViewById(R.id.contacts_menu_letter);
				letter.setText(firstLetter);
				FontsUtils.TypeFace(letter, getAssets());

				mLetterContainer = (LinearLayout) newLetter.findViewById(R.id.contacts_container);

				publishProgress(new Wrapper(mContactsContainer, newLetter, mLetterContainer, null));

				letterCount++;
				contactCount = 0;

				do{
					//creating and adding views to new letter container
					final String person = parseFriends.get(totalContactCount);

					//add person to the current letter
					final View newContact;
					if(contactCount % 2 == 0){
						newContact = mInflater.inflate(R.layout.contact_light, mLetterContainer, false);
					}else{
						newContact = mInflater.inflate(R.layout.contact_dark, mLetterContainer, false);
					}
					TextView contact = (TextView) newContact.findViewById(R.id.contact_text);
					contact.setText(person);
					FontsUtils.TypeFace(contact, getAssets());

					final CheckBox checkBox = (CheckBox) newContact.findViewById(R.id.check_box);

					runOnUiThread(new Runnable(){
						@Override
						public void run() {
							newContact.setOnClickListener(new View.OnClickListener() {
								@Override
								public void onClick(View v) {
									if(!(checkBox.isChecked())){
										checkBox.setChecked(true);
									}else{
										checkBox.setChecked(false);
									}
								}
							});

							checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

								@Override
								public void onCheckedChanged(CompoundButton buttonView,	boolean isChecked) {
									if(isChecked){
										if(!(recipientNames.contains(person))){
											recipientNames.add(person);
										}
									}else{
										if(recipientNames.contains(person)){
											recipientNames.remove(person);
										}
									}

									//handle showing / hiding the sendbutton
									if((recipientNames.size() == 0) && (!sendToSelf.isChecked())){
										sendButton.setVisibility(View.GONE);
									}else{
										sendButton.setVisibility(View.VISIBLE);
									}
								}

							});
						}
					});

					publishProgress(new Wrapper(null, null, mLetterContainer, newContact));

					totalContactCount++;
					contactCount++;

					//exits the loop when reached the end of contacts to add
					if(totalContactCount == parseFriends.size()){
						moreNamesToAdd = false;
						break;
					}


				}while(parseFriends.get(totalContactCount).substring(0,1).equals(currentLetter));

			}while(moreNamesToAdd);

			return null;
		}

		ViewGroup letterAddTo;

		@Override
		protected void onProgressUpdate(Wrapper... params){
			Wrapper param = params[0];
			ViewGroup addToUiGroup = param.mAddToUi;
			View letterView = param.letterView;
			ViewGroup currLetterViewGroup = param.letterViewGroup;
			View contact = param.newContact;

			if(addToUiGroup == null){
				//adding to the same letter
				letterAddTo.addView(contact);
			}else{
				//adding a new letter
				if(contact == null){
					//add the empty new letter to the Ui
					addToUiGroup.addView(letterView);
					letterAddTo = currLetterViewGroup;
				}
			}
		}

	}

	public class Wrapper{
		public final ViewGroup mAddToUi;
		public final View letterView;
		public final ViewGroup letterViewGroup;
		public final View newContact;

		public Wrapper(ViewGroup addToUi, View letterToAdd, ViewGroup lettersViewGroup, View contact){
			mAddToUi = addToUi;
			letterView = letterToAdd;
			letterViewGroup = lettersViewGroup;
			newContact = contact;
		}
	}

}