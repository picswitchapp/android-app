package com.picswitch;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.Display;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.ParseCloud;
import com.parse.ParseCrashReporting;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.codec.binary.Base64;

/**Class that handles user manipulation of the PicSwitch
 * Allows user to move images around 
 * Detects when the user has solved the PicSwitch*/
public class ReplyTypeMessage extends ActionBarActivity{
	private ImageView image1, image2, image3,image4,image5,image6, image7,image8,image9;
	private GestureDetector gestureDetector;
	int animationToRun = R.anim.fadeout; //animation to be used whem user touches PicSwitch
	public int count=0;
	//images of the PicSwitch
	Bitmap img, img0, img1, img2,img3,img4,img5,img6, img7, img8; 
	ActionBar actionBar;
	RelativeLayout rLayout;
	ArrayList<Integer> images = new ArrayList<Integer>(Arrays.asList(0,1,2,3,4,5,6,7,8));
	View.OnTouchListener onTouchListener;
	SurfaceHolder s;
	int chunkNumbers =9;
	ImageView placeholder;
	Uri resultCropUri;

	ViewGroup timeDropDownContainer;

	LayoutInflater mInflater;

	String senderUsername;
	String senderFirstName;
	String senderObjectId;

	String[] timeOptions = new String[]{"15","30","45","60","none"};

	TextView firstCaption;
	TextView secondCaption;
	TextView timerTextView;

	String firstCaptionText;
	String secondCaptionText;

	ProgressDialog mDialog;

	public enum TimeText{
		FIFTEEN, THIRTY, FORTYFIVE, SIXTY, NONE, NOVALUE;

		public static TimeText toMenuTitles(String str){
			try{
				return valueOf(str);
			}catch(Exception ex) {
				return NOVALUE;
			}
		}
	}

	int selectedTimeLimit;
	int previewIndexSelected;

	Typeface montserrat;

	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi") @Override
	protected void onCreate(Bundle savedInstanceState) {

		getWindow().requestFeature(Window.FEATURE_ACTION_BAR);

		super.onCreate(savedInstanceState);
		getWindow().setFormat(PixelFormat.RGBA_8888);
		getActionBar().hide();

		rLayout =  (RelativeLayout)findViewById(R.layout.activity_type_message);
		setContentView(R.layout.activity_reply_type_message);

		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
			window.setStatusBarColor(Color.rgb(32, 99, 99));
		}
		mInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		montserrat = Typeface.createFromAsset(getAssets(), "Montserrat-Regular.ttf");

		resultCropUri = getIntent().getData();

		senderUsername = getIntent().getExtras().getString(ParseConstants.KEY_SENDER);
		senderFirstName = getIntent().getExtras().getString(ParseConstants.KEY_SENDER_FIRST_NAME);
		senderObjectId = getIntent().getExtras().getString("senderObjectId");

		actionBar = getActionBar(); //Set up action bar

		timeDropDownContainer = (ViewGroup) findViewById(R.id.reply_time_drop_down_container);

		TextView replyTitleText = (TextView) findViewById(R.id.reply_type_message_title);
		replyTitleText.setTypeface(montserrat);
		replyTitleText.setText("send to " + senderFirstName);

		firstCaption = (EditText) findViewById(R.id.firstCaption);
		secondCaption = (EditText) findViewById(R.id.secondCaption);
		FontsUtils.TypeFace(firstCaption, getAssets());
		FontsUtils.TypeFace(secondCaption, getAssets());

		imageViewsHelper(); //initializes image views
		imageViewsDimensions(); //sets up the dimensions of the imageviews
		assignImageViewPics(); //add images to imageViews
		setImageViewTouch();
		//setupActionBar();
		buttonsHelper(); //handles buttons
		timerTextView = (TextView) findViewById(R.id.reply_empty_timer);
		//titleBarHelper(savedInstanceState); //handles title bar
		selectedTimeLimit = 45;

		mDialog = new ProgressDialog(ReplyTypeMessage.this);
		mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		mDialog.setMessage("Sending message...");
	}



	/**title bar set up helper*/
	public void titleBarHelper(Bundle savedInstanceState){
		LinearLayout titleBar = (LinearLayout) findViewById(R.id.solve_screen_layout);
		Display  disp = getWindowManager().getDefaultDisplay();
		int width = disp.getWidth();
		LayoutParams paramz = titleBar.getLayoutParams();
		paramz.width = LayoutParams.FILL_PARENT;
		titleBar.setLayoutParams(paramz);
		//Timer code
		timerTextView = (TextView) findViewById(R.id.reply_empty_timer);
		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
			.add(R.id.container, new PlaceholderFragment()).commit();
		}
	}





	/**Sets up a custom actionbar*/
	@SuppressLint("NewApi")
	private void setupActionBar(){
		final ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(
				R.layout.solve_screen,
				null);
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setCustomView(actionBarLayout);

	}
	/**Creates images from the bitmaps*/
	public String createImageFromBitmap(Bitmap bitmap, String name) {
		String fileName = name;//no .png or .jpg needed
		try {
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
			FileOutputStream fo = openFileOutput(fileName, Context.MODE_PRIVATE);
			fo.write(bytes.toByteArray());
			// remember close file output
			fo.close();
		} catch (Exception e) {
			e.printStackTrace();
			fileName = null;
		}
		return fileName;

	}



	/**Sets touch detection for imageViews*/
	public void setImageViewTouch(){
		//set touch for imageviews
		gestureDetector = new GestureDetector(this, new MyGestureDetector());
		onTouchListener = new View.OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				return gestureDetector.onTouchEvent(event);
			}
		};

		image1.setOnTouchListener(onTouchListener);
		image2.setOnTouchListener(onTouchListener);
		image3.setOnTouchListener(onTouchListener);
		image4.setOnTouchListener(onTouchListener);
		image5.setOnTouchListener(onTouchListener);
		image6.setOnTouchListener(onTouchListener);
		image7.setOnTouchListener(onTouchListener);
		image8.setOnTouchListener(onTouchListener);
		image9.setOnTouchListener(onTouchListener);
	}

	/**Assigns bitmaps from the SD to image Views*/
	public void assignImageViewPics(){
		//shuffle images arraylist 
		ImageView previewThumbnail = new ImageView(this);
		int[] ids= {R.id.imageView01,R.id.imageView02,R.id.imageView03,R.id.imageView04,R.id.imageView05,R.id.imageView06,R.id.imageView07,R.id.imageView08,R.id.imageView09};
		for(int i=0; i<9;i++){

			previewThumbnail=(ImageView)findViewById(ids[i]);
			Bitmap bitmap ;
			try {
				bitmap = BitmapFactory.decodeStream(this.openFileInput("img"+images.get(i)));
				//bitmap = images.get(i);
				previewThumbnail.setImageBitmap(bitmap);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
	}


	/**Initializes the dimensions of the imageviews that hold the chunked 
	 * images*/
	public void imageViewsDimensions(){
		Display  display = getWindowManager().getDefaultDisplay();
		//width of the imageViews
		//minus 6 to allows for space between imageViews
		int swidth = (int) (display.getWidth()/3-6);
		image1.getLayoutParams().width = swidth;
		image1.getLayoutParams().height = swidth;
		image2.getLayoutParams().width = swidth;
		image2.getLayoutParams().height = swidth;
		image3.getLayoutParams().width = swidth;
		image3.getLayoutParams().height = swidth;
		image4.getLayoutParams().width = swidth;
		image4.getLayoutParams().height = swidth;
		image5.getLayoutParams().width = swidth;
		image5.getLayoutParams().height = swidth;
		image6.getLayoutParams().width = swidth;
		image6.getLayoutParams().height = swidth;
		image7.getLayoutParams().width = swidth;
		image7.getLayoutParams().height = swidth;
		image8.getLayoutParams().width = swidth;
		image8.getLayoutParams().height = swidth;
		image9.getLayoutParams().width = swidth;
		image9.getLayoutParams().height = swidth;
	}


	/**Initialize imageViews to hold chucked images*/
	public void imageViewsHelper(){
		image1 = (ImageView) findViewById(R.id.imageView01);
		image2 = (ImageView) findViewById(R.id.imageView02);
		image3 = (ImageView) findViewById(R.id.imageView03);
		image4 = (ImageView) findViewById(R.id.imageView04);
		image5 = (ImageView) findViewById(R.id.imageView05);
		image6 = (ImageView) findViewById(R.id.imageView06);
		image7 = (ImageView) findViewById(R.id.imageView07);
		image8 = (ImageView) findViewById(R.id.imageView08);
		image9 = (ImageView) findViewById(R.id.imageView09);

		//set up original previewIndex
		previewIndexSelected = 4;
		Animation animation = AnimationUtils.loadAnimation(this, animationToRun);
		image5.startAnimation(animation );
		image5.setPadding(8,8,8,8);
		image5.setBackgroundColor(Color.WHITE);
		temp=image5;
	}




	/**Helper for buttons*/
	public void buttonsHelper(){
		ImageView reset = (ImageView) findViewById(R.id.reply_type_message_back_button);
		reset.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent i = new Intent(getBaseContext(), TabActivity.class);
				startActivity(i);
				finish();

			}

		});

		firstCaption.setOnTouchListener(new OnTouchListener() {
			final int DRAWABLE_LEFT = 0;
			final int DRAWABLE_TOP = 1;
			final int DRAWABLE_RIGHT = 2;
			final int DRAWABLE_BOTTOM = 3;
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_UP) {
					int leftEdgeOfRightDrawable = firstCaption.getRight() 
							- firstCaption.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width();
					// when EditBox has padding, adjust leftEdge like
					// leftEdgeOfRightDrawable -= getResources().getDimension(R.dimen.edittext_padding_left_right);
					if (event.getRawX() >= leftEdgeOfRightDrawable) {
						// clicked on clear icon
						firstCaption.setText("");
						return true;
					}
				}
				return false;
			}
		});
		secondCaption.setOnTouchListener(new OnTouchListener() {
			final int DRAWABLE_LEFT = 0;
			final int DRAWABLE_TOP = 1;
			final int DRAWABLE_RIGHT = 2;
			final int DRAWABLE_BOTTOM = 3;
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_UP) {
					int leftEdgeOfRightDrawable = secondCaption.getRight() 
							- secondCaption.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width();
					// when EditBox has padding, adjust leftEdge like
					// leftEdgeOfRightDrawable -= getResources().getDimension(R.dimen.edittext_padding_left_right);
					if (event.getRawX() >= leftEdgeOfRightDrawable) {
						// clicked on clear icon
						secondCaption.setText("");
						return true;
					}
				}
				return false;
			}
		});

		RelativeLayout main = (RelativeLayout) findViewById(R.id.reply_main_content_layout);
		main.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if(timeDropDownContainer.getChildCount() != 0){
					timeDropDownContainer.removeAllViews();
				}
			}
		});

		RelativeLayout sendButton = (RelativeLayout) findViewById(R.id.reply_send_button_layout);
		sendButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if(timeDropDownContainer.getChildCount() == 0){
					//do the sending to "sender"
					firstCaptionText = firstCaption.getText().toString();
					secondCaptionText = secondCaption.getText().toString();
					send();
				}else{
					if(timeDropDownContainer.getChildCount() != 0){
						timeDropDownContainer.removeAllViews();
					}
				}
			}
		});

		TextView timer = (TextView) findViewById(R.id.reply_empty_timer);
		timer.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(timeDropDownContainer.getChildCount() == 0){
					addViewsToTimeDropDown();
					timeDropDownContainer.bringToFront();
				}else{
					timeDropDownContainer.removeAllViews();
				}
			}
		});

		TextView sendText = (TextView) findViewById(R.id.send_to_text);
		FontsUtils.TypeFace(sendText, getAssets());
		String senderFirstNameUpper = senderFirstName.toUpperCase();
		sendText.setText("SEND TO " + senderFirstNameUpper);
	}

	public void addViewsToTimeDropDown(){

		for(int i = 0; i < 5; i++){

			final ViewGroup newView = (ViewGroup) mInflater.inflate(R.layout.drop_down_time_menu_item, timeDropDownContainer, false);
			((TextView) newView.findViewById(R.id.time)).setText(timeOptions[i]);

			newView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {

					String itemValue = (String) ((TextView) newView.findViewById(R.id.time)).getText();

					String tmp;

					if(itemValue == "15"){
						tmp = "FIFTEEN";
					}else if(itemValue == "30"){
						tmp = "THIRTY";
					}else if(itemValue == "45"){
						tmp = "FORTYFIVE";
					}else if(itemValue == "60"){
						tmp = "SIXTY";
					}else if(itemValue == "none"){
						tmp = "NONE";
					}else{
						tmp = "NOVALUE";
					}

					switch(TimeText.toMenuTitles(tmp)){
					case FIFTEEN:{
						timerTextView.setText("15");
						selectedTimeLimit = 15;
						timeDropDownContainer.removeAllViews();
						break;
					}
					case THIRTY:{
						timerTextView.setText("30");
						selectedTimeLimit = 30;
						timeDropDownContainer.removeAllViews();
						break;
					}
					case FORTYFIVE:{
						timerTextView.setText("45");
						selectedTimeLimit = 45;
						timeDropDownContainer.removeAllViews();
						break;
					}
					case SIXTY: {
						timerTextView.setText("60");
						selectedTimeLimit = 60;
						timeDropDownContainer.removeAllViews();
						break;
					}
					case NONE:{
						timerTextView.setText("00");
						selectedTimeLimit = 00;
						timeDropDownContainer.removeAllViews();
						break;
					}
					default:{
						break;
					}
					}

				}
			});

			timeDropDownContainer.addView(newView);
			count = 0;
		}
	}


	/**Detects touch and starts timer*/
	class MyGestureDetector extends SimpleOnGestureListener {
		int timerCount = 0;
		@SuppressLint("NewApi")
		@Override
		public boolean onDown(MotionEvent e) {
			count++;

			if(timeDropDownContainer.getChildCount() != 0){
				timeDropDownContainer.removeAllViews();
				count = 0;
			}
			onTap(e);
			return false;
		}

	}

	ImageView temp = null;
	int in=100, fin=100;
	/**Sets animation when user makes first touch*/
	public void setAnimation(ImageView im, int i){
		Animation animation = AnimationUtils.loadAnimation(this, animationToRun);
		im.startAnimation(animation );
		im.setPadding(8, 8,8,8);
		im.setBackgroundColor(Color.WHITE);
		temp=im;
		in=i;
	}

	public void secondTouch(ImageView imageView, int i){
		count=0;
		temp.setPadding(0,0,0,0);
		temp.clearAnimation();
		fin=i;
	}
	/**Detects touches on imageViews 
	 * Shows the necessary animations
	 * Calls swap images to swap the images*/

	private void onTap(MotionEvent event){
		int firstRow = getLocation(image1)[1]+image1.getHeight(); //lower y coordinate of row 1

		//first column
		if(image1.getLeft()<event.getRawX() && image1.getLeft()+image1.getHeight()>event.getRawX()){
			//image1
			if(image1.getTop()<event.getRawY() && getLocation(image1)[1]+image1.getHeight()>event.getRawY()){
				if(temp != null){
					//count=0;
					temp.setPadding(0,0,0,0);
					temp.clearAnimation();
					fin=images.get(0);
					//swapImages(temp,image1,in,fin);
					//System.out.println("image1, 2");
				}
				Animation animation = AnimationUtils.loadAnimation(this, animationToRun);
				image1.startAnimation(animation );
				image1.setPadding(8,8,8,8);
				image1.setBackgroundColor(Color.WHITE);
				temp=image1;
				in=images.get(0);
				previewIndexSelected = 0;
				//System.out.println("image1");				
			}
			else if(image4.getTop()<event.getRawY() && getLocation(image4)[1]+image4.getHeight()>event.getRawY()){
				if(temp != null){
					//count=0;
					temp.setPadding(0,0,0,0);
					temp.clearAnimation();
					fin=images.get(3);
					//swapImages(temp,image1,in,fin);
					//System.out.println("image1, 2");
				}
				Animation animation = AnimationUtils.loadAnimation(this, animationToRun);
				image4.startAnimation(animation );
				image4.setPadding(8,8,8,8);
				image4.setBackgroundColor(Color.WHITE);
				temp=image4;
				in=images.get(3);
				previewIndexSelected = 3;
				//System.out.println("image4");
			}
			else if(image7.getTop()<event.getRawY() && getLocation(image7)[1]+image7.getHeight()>event.getRawY()){
				if(temp != null){
					//count=0;
					temp.setPadding(0,0,0,0);
					temp.clearAnimation();
					fin=images.get(6);
					//swapImages(temp,image1,in,fin);
					//System.out.println("image1, 2");
				}
				Animation animation = AnimationUtils.loadAnimation(this, animationToRun);
				image7.startAnimation(animation );
				image7.setPadding(8,8,8,8);
				image7.setBackgroundColor(Color.WHITE);
				temp=image7;
				in=images.get(6);
				previewIndexSelected = 6;
				//System.out.println("image1");
			}

		}
		//second column
		if(image2.getLeft()<event.getRawX() && image2.getLeft()+image2.getHeight()>event.getRawX()){
			//image1
			if(image2.getTop()<event.getRawY() && firstRow>event.getRawY()){
				if(temp != null){
					//count=0;
					temp.setPadding(0,0,0,0);
					temp.clearAnimation();
					fin=images.get(1);
					//swapImages(temp,image1,in,fin);
					//System.out.println("image2, 2");
				}
				Animation animation = AnimationUtils.loadAnimation(this, animationToRun);
				image2.startAnimation(animation );
				image2.setPadding(8,8,8,8);
				image2.setBackgroundColor(Color.WHITE);
				temp=image2;
				in=images.get(1);
				previewIndexSelected = 1;
				//System.out.println("image1");
			}
			else if(image5.getTop()<event.getRawY() && getLocation(image5)[1]+image5.getHeight()>event.getRawY()){
				if(temp != null){
					//count=0;
					temp.setPadding(0,0,0,0);
					temp.clearAnimation();
					fin=images.get(4);
					//swapImages(temp,image1,in,fin);
					//System.out.println("image1, 2");
				}
				Animation animation = AnimationUtils.loadAnimation(this, animationToRun);
				image5.startAnimation(animation );
				image5.setPadding(8,8,8,8);
				image5.setBackgroundColor(Color.WHITE);
				temp=image5;
				in=images.get(4);
				previewIndexSelected = 4;
				//System.out.println("image1");
			}
			else if(image8.getTop()<event.getRawY() && getLocation(image8)[1]+image8.getHeight()>event.getRawY()){
				if(temp != null){
					//count=0;
					temp.setPadding(0,0,0,0);
					temp.clearAnimation();
					fin=images.get(7);
					//swapImages(temp,image1,in,fin);
					//System.out.println("image1, 2");
				}
				Animation animation = AnimationUtils.loadAnimation(this, animationToRun);
				image8.startAnimation(animation );
				image8.setPadding(8,8,8,8);
				image8.setBackgroundColor(Color.WHITE);
				temp=image8;
				in=images.get(7);
				previewIndexSelected = 7;
				//System.out.println("image1");
			}

		}
		//third column
		if(image3.getLeft()<event.getRawX() && image3.getLeft()+image3.getHeight()>event.getRawX()){
			//image1
			if(image3.getTop()<event.getRawY() && firstRow>event.getRawY()){
				if(temp != null){
					//count=0;
					temp.setPadding(0,0,0,0);
					temp.clearAnimation();
					fin=images.get(2);
					//swapImages(temp,image1,in,fin);
					//System.out.println("image1, 2");
				}
				Animation animation = AnimationUtils.loadAnimation(this, animationToRun);
				image3.startAnimation(animation );
				image3.setPadding(8,8,8,8);
				image3.setBackgroundColor(Color.WHITE);
				temp=image3;
				in=images.get(2);
				previewIndexSelected = 2;
				//System.out.println("image1");
			}
			else if(image6.getTop()<event.getRawY() && getLocation(image6)[1]+image6.getHeight()>event.getRawY()){
				if(temp != null){
					//count=0;
					temp.setPadding(0,0,0,0);
					temp.clearAnimation();
					fin=images.get(5);
					//swapImages(temp,image1,in,fin);
					//System.out.println("image1, 2");
				}
				Animation animation = AnimationUtils.loadAnimation(this, animationToRun);
				image6.startAnimation(animation );
				image6.setPadding(8,8,8,8);
				image6.setBackgroundColor(Color.WHITE);
				temp=image6;
				in=images.get(5);
				previewIndexSelected = 5;
				//System.out.println("image1");
			}
			else if(image9.getTop()<event.getRawY() && getLocation(image9)[1]+image9.getHeight()>event.getRawY()){
				if(temp != null){
					//count=0;
					temp.setPadding(0,0,0,0);
					temp.clearAnimation();
					fin=images.get(0);
					//swapImages(temp,image1,in,fin);
					//System.out.println("image1, 2");
				}
				Animation animation = AnimationUtils.loadAnimation(this, animationToRun);
				image9.startAnimation(animation );
				image9.setPadding(8,8,8,8);
				image9.setBackgroundColor(Color.WHITE);
				temp=image9;
				in=images.get(8);
				previewIndexSelected = 8;
				//System.out.println("image1");
			}

		}
	}




	private int[] getLocation(View mView) {
		Rect mRect = new Rect();
		int[] location = new int[2];

		mView.getLocationOnScreen(location);

		mRect.left = location[0];
		mRect.top = location[1];
		mRect.right = location[0] + mView.getWidth();
		mRect.bottom = location[1] + mView.getHeight();

		return location;
	}

	public void send(){
		//final ParseObject mMessage = createMessage();

		mDialog.show();

		ParseQuery<ParseUser> replyQuery = ParseUser.getQuery();
		replyQuery.whereEqualTo("objectId", senderObjectId);
		replyQuery.getFirstInBackground(new GetCallback<ParseUser>() {

			@Override
			public void done(ParseUser user, ParseException e) {
				if(e == null){

					byte[] fileBytes = FileHelper.getByteArrayFromFile(ReplyTypeMessage.this, resultCropUri);

					//compress the image beforesending
					Bitmap bmTemp = BitmapFactory.decodeByteArray(fileBytes, 0, fileBytes.length);
					ByteArrayOutputStream stream = new ByteArrayOutputStream();
					bmTemp.compress(Bitmap.CompressFormat.JPEG, 50, stream);
					byte[] imageData = stream.toByteArray();
					String base64String = Base64.encodeBase64String(imageData);

					List<String> recipientIds = new ArrayList<String>();
					recipientIds.add(user.getObjectId());

					HashMap<String, Object> params = new HashMap<String, Object>();
					params.put("recipientIds", recipientIds);
					params.put("photo", base64String);
					params.put("caption1", firstCaptionText);
					params.put("caption2", secondCaptionText);
					params.put("previewIndex", previewIndexSelected);
					params.put("puzzleSize", 3);
					params.put("timeLimit", selectedTimeLimit);
					ParseCloud.callFunctionInBackground("MSSend", params, new FunctionCallback<String>(){
						@Override
						public void done(String object, ParseException e) {

							if(mDialog.isShowing()){
								mDialog.cancel();
							}

							if(e == null){
								Toast.makeText(ReplyTypeMessage.this, "Message sent!", Toast.LENGTH_SHORT).show();
							}else{
								Toast.makeText(ReplyTypeMessage.this, "Sending failed", Toast.LENGTH_SHORT).show();
							}
							//							Intent intent = new Intent(ReplyTypeMessage.this, TabActivity.class);
							//							startActivity(intent);
							ReplyTypeMessage.this.finish();
						}
					});
				}else{
					e.printStackTrace();
				}
			}

		});
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_open_image,
					container, false);
			return rootView;
		}
	}

}
