package com.picswitch;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;

import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.RefreshCallback;
import com.viewpagerindicator.CirclePageIndicator;
import com.viewpagerindicator.TabPageIndicator;
/**Class that loads the tabbed view of the app
 * */
public class TabActivity extends FragmentActivity {
	//Tab variables
	ViewPager Tab;
	public static ViewPager tabViewPager;
	Window mWindow;

	static Calendar timeTaken = Calendar.getInstance(TimeZone.getTimeZone("GMT"));

	public static List<ParseObject> mMessages = new ArrayList<ParseObject>();
	public static List<ParseObject> mFeatured = new ArrayList<ParseObject>();

	public static List<SaveMessage> savedSolved   = new ArrayList<SaveMessage>();
	public static List<SaveMessage> savedFeatured = new ArrayList<SaveMessage>();

	public static HashMap<String, ParseObject> mMessagesHM = new HashMap<String, ParseObject>();
	public static HashMap<String, ParseObject> mFeaturedHM = new HashMap<String, ParseObject>();

	public static HashMap<String, HashMap<String, SaveMessage>> savedMessages = new HashMap<String, HashMap<String, SaveMessage>>();

	public static LinkedList<ParseObject> mMessagesLinkedList = new LinkedList<ParseObject>();
	public static LinkedList<ParseObject> mFeaturedLinkedList = new LinkedList<ParseObject>();
	public static LinkedList<SaveMessage> savedSolvedLinkedList = new LinkedList<SaveMessage>();
	public static LinkedList<SaveMessage> savedFeatLinkedList = new LinkedList<SaveMessage>();

	public static LinkedList<View> allNewMessagesLinkedList = new LinkedList<View>();
	public static HashMap<String, View> allNewMessages = new HashMap<String, View>();

	TabPagerAdapter TabAdapter; 

	static Context mContext;

	private static File saveFile;
	public static File settingsFile;

	//app settings
	public static boolean SOUND = false;
	public static boolean SAVE_PHOTOS = false;

	@SuppressLint({ "NewApi", "ClickableViewAccessibility" }) @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
		getActionBar().hide();

		setContentView(R.layout.main);

		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
			window.setStatusBarColor(Color.rgb(32, 99, 99));
		}
		
		mContext = getApplication();
		NotificationManager mNotifM = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
		
		try{
			mNotifM.cancel(ParseConstants.NOTIFICATION_TAB);
		}
		catch(Exception e){
			e.printStackTrace();
		}

		if(ParseUser.getCurrentUser() != null){
			ParseUser.getCurrentUser().refreshInBackground(new RefreshCallback() {
				@Override
				public void done(ParseObject object, ParseException e) {
					if(e == null){
						//Toast.makeText(TabActivity.this, "user refreshed", Toast.LENGTH_SHORT).show();	
						//successfully refreshed
					}else{
						e.printStackTrace();
					}
				}
			});
		}

		setUpTabs();

		tabViewPager = (ViewPager)findViewById(R.id.pager);
		
		tabViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener(){

			@Override
			public void onPageScrollStateChanged(int arg0) {}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {}

			@Override 
			public void onPageSelected(int arg0) {
				tabViewPager.setCurrentItem(arg0);				
			}
		});

		tabViewPager.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				tabViewPager.onTouchEvent(event);
				return false;
			}
		});
		
		TabPageIndicator tabIndicator = (TabPageIndicator)findViewById(R.id.tab_activity_indicator);
		tabIndicator.setViewPager(tabViewPager);
		
		tabViewPager.setCurrentItem(1);
		tabViewPager.setOffscreenPageLimit(2);

		String[] files = fileList();
		for(String file : files){
			if(file.equals("picswitch_messages")){
				//do nothing
			}else{
				saveFile = new File(mContext.getFilesDir(), "picswitch_messages");
			}

			if(file.equals("picswitch_settings")){
				//do nothing
			}else{
				settingsFile = new File(mContext.getFilesDir(), "picswitch_settings");
			}
		}

		if(!(savedMessages.keySet().contains(ParseUser.getCurrentUser().getObjectId()))){
			savedMessages.put(ParseUser.getCurrentUser().getObjectId(), new HashMap<String, SaveMessage>());
		}

	}
	
	@Override
	public void onBackPressed(){
		moveTaskToBack(true);
	}

	/**Initializes the tab activity, setting up the format*/
	public void setUpTabs(){
		TabAdapter = new TabPagerAdapter(getSupportFragmentManager());
		Tab = (ViewPager)findViewById(R.id.pager);
		Tab.setOnPageChangeListener(
				new ViewPager.SimpleOnPageChangeListener() {
					@SuppressLint("NewApi") @Override
					public void onPageSelected(int position) {

					}
				});
		Tab.setAdapter(TabAdapter);
	}
	
	@Override
	public void onStart(){
		super.onStart();
	}

	@Override
	public void onResume(){
		super.onResume();
		
		//load settings
		HashMap<String, SettingsWrapper> mapInSettingsFile = new HashMap<String, SettingsWrapper>();
		try{
			//File toRead = new File(mContext.getFilesDir(), "picswitch_messages");
			FileInputStream fis = new FileInputStream(settingsFile);
			ObjectInputStream ois = new ObjectInputStream(fis);

			mapInSettingsFile = (HashMap<String, SettingsWrapper>)ois.readObject();

			ois.close();
			fis.close();

			if(mapInSettingsFile.isEmpty()){
				System.out.println("fetching settings returned nothing");
			}

			//set the global values depending on current user
			try{
				SettingsWrapper settings = mapInSettingsFile.get(ParseUser.getCurrentUser().getObjectId());
				SOUND = settings.getSound();
				SAVE_PHOTOS = settings.getSavePhotos();
			}catch(Exception e){
				e.printStackTrace();
				SOUND = false;
				SAVE_PHOTOS = false;
			}

		}catch(Exception e){
			e.printStackTrace();
			System.out.println("loading settings failed");
		}
	}

	@Override
	public void onPause(){
		super.onPause();
	}

	static Bitmap imageToSave;
	static ParseObject messageToSave;

	public static void saveSolvedMessageToPhone(ParseObject message, Bitmap bitmap, final int switches, final double time, final boolean isFeatured){
		//Toast.makeText(mContext, "number of messages to save to phone: " + mMessages.size(), Toast.LENGTH_LONG).show();
		System.out.println("Saving solved message to phone");

		messageToSave = message;
		imageToSave = bitmap;

		new AsyncTask<Void,Void,Void>() {

			@Override
			protected Void doInBackground(Void... params) {
				ParseFile file = messageToSave.getParseFile(ParseConstants.KEY_PHOTO);
				Uri messageUri = Uri.parse(file.getUrl());

				System.out.println("messageUri: " + messageUri.toString());

				SaveMessage currentMessage = null;
				if(!isFeatured){
					try {
						currentMessage = new SaveMessage(
								messageToSave.getString(ParseConstants.CAPTION_FIRST),
								messageToSave.getString(ParseConstants.CAPTION_SECOND),
								messageUri.toString(),
								messageToSave.getInt(ParseConstants.KEY_TIME_LIMIT),
								messageToSave.getObjectId(),
								messageToSave.getParseUser(ParseConstants.KEY_SENDER).fetchIfNeeded().getString(ParseConstants.KEY_FIRST_NAME),
								messageToSave.getParseUser(ParseConstants.KEY_SENDER).fetchIfNeeded().getString(ParseConstants.KEY_LAST_NAME),
								messageToSave.getParseUser(ParseConstants.KEY_SENDER).getUsername(),
								messageToSave.getParseUser(ParseConstants.KEY_SENDER).getObjectId(),
								false,
								imageToSave,
								switches,
								time,
								null,
								timeTaken.getTimeInMillis());
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}else{
					currentMessage = new SaveMessage(
							messageToSave.getString(ParseConstants.CAPTION_FIRST),
							messageToSave.getString(ParseConstants.CAPTION_SECOND),
							messageUri.toString(),
							0,
							messageToSave.getObjectId(),
							messageToSave.getString(ParseConstants.KEY_COMPANY_NAME),
							"",
							null,
							null,
							true,
							imageToSave,
							switches,
							time,
							messageToSave.getString("sponsorLink"),
							timeTaken.getTimeInMillis());
				}

				if(savedMessages.get(ParseUser.getCurrentUser().getObjectId()) != null){
					savedMessages.get(ParseUser.getCurrentUser().getObjectId()).put(messageToSave.getObjectId(), currentMessage);
				}else{
					savedMessages.put(ParseUser.getCurrentUser().getObjectId(), new HashMap<String, SaveMessage>());
					savedMessages.get(ParseUser.getCurrentUser().getObjectId()).put(messageToSave.getObjectId(), currentMessage);
				}

				//actually do the saving
				try {

					//File saveFile = new File(mContext.getFilesDir(), "picswitch_messages");
					FileOutputStream fos   = new FileOutputStream(saveFile);
					ObjectOutputStream oos = new ObjectOutputStream(fos);

					oos.writeObject(savedMessages);
					oos.flush();
					oos.close();
					fos.close();

				} catch (Exception e) {
					System.out.println("saving new hashmap failed");
					e.printStackTrace();
				}
				return null;
			}

			@Override
			protected void onPostExecute(Void param){
				//Toast.makeText(mContext, "new message saved", Toast.LENGTH_SHORT).show();
			}

		}.execute();
	}

	public static void saveMapToFile(HashMap<String, SaveMessage> map){
		System.out.println("saving entire map to phone");


		try {

			File saveFile = new File(mContext.getFilesDir(), "picswitch_messages");
			FileOutputStream fos   = new FileOutputStream(saveFile);
			ObjectOutputStream oos = new ObjectOutputStream(fos);

			HashMap<String, HashMap<String,SaveMessage>> userMap = new HashMap<String, HashMap<String,SaveMessage>>();
			userMap.put(ParseUser.getCurrentUser().getObjectId(), map);

			oos.writeObject(userMap);
			oos.flush();
			oos.close();
			fos.close();

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("saving hashmap failed");
		}

	}

	public static HashMap<String, SaveMessage> getSavedMessagesFromPhone(){

		HashMap<String, HashMap<String, SaveMessage>> mapInFile = new HashMap<String, HashMap<String, SaveMessage>>();
		try{
			//File toRead = new File(mContext.getFilesDir(), "picswitch_messages");
			FileInputStream fis = new FileInputStream(saveFile);
			ObjectInputStream ois = new ObjectInputStream(fis);

			mapInFile = (HashMap<String, HashMap<String, SaveMessage>>)ois.readObject();

			ois.close();
			fis.close();

			//check the size of data received
			//Toast.makeText(mContext, "size of loaded map: " + mapInFile.size(), Toast.LENGTH_LONG).show();

			//check ids of messages saved in the hashmap
			//System.out.println("mapInFile ids: " + mapInFile.keySet().toString());

			if(mapInFile.isEmpty()){
				System.out.println("fetching returned false");
			}

			savedMessages = mapInFile;

			savedSolved.clear();
			for(SaveMessage message : mapInFile.get(ParseUser.getCurrentUser().getObjectId()).values()){
				savedSolved.add(message);
			}

		}catch(Exception e){
			e.printStackTrace();
			System.out.println("loading failed");
		}

		return mapInFile.get(ParseUser.getCurrentUser().getObjectId());
	}

}

