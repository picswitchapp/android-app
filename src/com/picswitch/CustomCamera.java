package com.picswitch;
//package com.gibsonmulonga.ru_pics;
//
//import java.io.FileOutputStream;
//import java.io.IOException;
//
//import android.app.Activity;
//import android.content.Context;
//import android.content.pm.PackageManager;
//import android.graphics.Bitmap;
//import android.hardware.Camera;
//import android.hardware.Camera.Parameters;
//import android.os.Bundle;
//import android.util.Log;
//import android.view.SurfaceHolder;
//import android.view.SurfaceView;
//import android.widget.Toast;
//
/**Our Custom Camera that is square in shape
 * Has lines divding it into either 3x3 or 4x4 depending on user preferences*/	
//public class CustomCamera extends SurfaceView implements SurfaceHolder.Callback {
//		private static final String TAG = "Tag";
//
//		// a variable to store a reference to the Image View at the main.xml file
//		// private ImageView iv_image;
//		// a variable to store a reference to the Surface View at the main.xml file
//		private SurfaceView sv;
//
//		// a bitmap to display the captured image
//		private Bitmap bmp;
//		FileOutputStream fo;
//
//		// Camera variables
//		// a surface holder
//		private SurfaceHolder sHolder;
//		// a variable to control the camera
//		private Camera mCamera;
//		// the camera parameters
//		private Parameters parameters;
//		private String FLASH_MODE ;
//		private boolean isFrontCamRequest = false;
//		
//		public CustomCamera(Context context, Camera camera) {
//	        super(context);
//	        mCamera = camera;
//
//	        // Install a SurfaceHolder.Callback so we get notified when the
//	        // underlying surface is created and destroyed.
//	        sHolder = getHolder();
//	        sHolder.addCallback(this);
//	        // deprecated setting, but required on Android versions prior to 3.0
//	        sHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
//	    }
//
//		
//		
//		
//		
//		
//		
//		/** Check if this device has a camera */
//		private boolean checkCameraHardware(Context context) {
//		    if (context.getPackageManager().hasSystemFeature(
//		            PackageManager.FEATURE_CAMERA)) {
//		        // this device has a camera
//		        return true;
//		    } else {
//		        // no camera on this device
//		        return false;
//		    }
//		}
//
//		public static Camera getCameraInstance() {
//		    Camera c = null;
//		    try {
//		        c = Camera.open(); // attempt to get a Camera instance
//		    } catch (Exception e) {
//		        // Camera is not available (in use or does not exist)
//		    }
//		    return c; // returns null if camera is unavailable
//		}
//
//		@Override
//		public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
//		    // get camera parameters
////		    parameters =  mCamera.getParameters();
////		    if (FLASH_MODE == null || FLASH_MODE.isEmpty())
////		    {
////		        FLASH_MODE = "auto";
////		    }
////		    parameters.setFlashMode(FLASH_MODE);
////
////		    // set camera parameters
////		    mCamera.setParameters(parameters);
////		    mCamera.startPreview();
////
////		    // sets what code should be executed after the picture is taken
////		    Camera.PictureCallback mCall = new Camera.PictureCallback() {
////		        @Override
////		        public void onPictureTaken(byte[] data, Camera camera) {
////		            // decode the data obtained by the camera into a Bitmap
////		            Log.d("ImageTakin", "Done");
////
////		            mCamera.stopPreview();
////		            // release the camera
////		            mCamera.release();
////		            Toast.makeText(getApplicationContext(),
////		                    "Your Picture has been taken !", Toast.LENGTH_LONG)
////		                    .show();
////
////		            finish();
////
////		        }
////		    };
////
////		    mCamera.takePicture(null, null, mCall);
//			// If your preview can change or rotate, take care of those events here.
//	        // Make sure to stop the preview before resizing or reformatting it.
//
//	        if (sHolder.getSurface() == null){
//	          // preview surface does not exist
//	          return;
//	        }
//
//	        // stop preview before making changes
//	        try {
//	            mCamera.stopPreview();
//	        } catch (Exception e){
//	          // ignore: tried to stop a non-existent preview
//	        }
//
//	        // set preview size and make any resize, rotate or
//	        // reformatting changes here
//
//	        // start preview with new settings
//	        try {
//	            mCamera.setPreviewDisplay(sHolder);
//	            mCamera.startPreview();
//
//	        } catch (Exception e){
//	            Log.d(TAG, "Error starting camera preview: " + e.getMessage());
//	        }
//	    
//
//		}
//		
//		public void surfaceCreated(SurfaceHolder holder) {
//	        // The Surface has been created, now tell the camera where to draw the preview.
//	        try {
//	            mCamera.setPreviewDisplay(holder);
//	            mCamera.startPreview();
//	        } catch (IOException e) {
//	            Log.d(TAG, "Error setting camera preview: " + e.getMessage());
//	        }
//	    }
//
//
//		
//
//		@Override
//		public void surfaceDestroyed(SurfaceHolder holder) {
//
//		}
//		
//
//}
