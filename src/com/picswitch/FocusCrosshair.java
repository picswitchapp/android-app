package com.picswitch;

import android.content.Context;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.View;


public class FocusCrosshair extends View{

	private Point mLocationPoint;

	public FocusCrosshair(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	private void setDrawable(int resid) {
		this.setBackgroundResource(resid);
	}

	public void showStart() {
		setDrawable(R.drawable.tap_focus_shape);
	}

	public void clear() {
		setBackground(null);
	}

}
