package com.picswitch;
//package com.gibsonmulonga.picswitch;
//
////package com.androidzeitgeist.mustache.activity;
//
//import java.io.File;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.List;
//
////import com.example.caaamera.R;
//import com.parse.ParseObject;
//
//import android.app.Activity;
//import android.content.Context;
//import android.content.Intent;
//import android.graphics.Bitmap;
//import android.graphics.Bitmap.CompressFormat;
//import android.media.MediaScannerConnection;
//import android.net.Uri;
//import android.os.Bundle;
//import android.os.Environment;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentManager;
//import android.support.v4.app.FragmentTransaction;
//import android.support.v4.view.ViewPager;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.FrameLayout;
//import android.widget.ImageView;
//import android.widget.Toast;
//
////	import com.androidzeitgeist.mustache.R;
////import com.androidzeitgeist.mustache.fragment.CameraFragment;
////import com.androidzeitgeist.mustache.listener.CameraFragmentListener;
//
///**
// * Activity displaying the camera and mustache preview.
// *
// * @author Sebastian Kaspari <sebastian@androidzeitgeist.com>
// */
//public class NewPicCamera extends Fragment implements CameraFragmentListener {
//	public static final String TAG = "Mustache/CameraActivity";
//	private Context mContext;
//	private static final int PICTURE_QUALITY = 90;
//	
////	private CameraFragment mCameraFragment;
////	private ViewPager mViewPager;
//	
//	public NewPicCamera(){
//		// Required empty public constructor
//	}
//	
//	protected List<ParseObject> mMessages;
//
//	/**
//	 * On activity getting created.
//	 *
//	 * @param savedInstanceState
//	 */
//	@Override
//
//	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//		mContext = container.getContext();
//		
//		View cameraTab = inflater.inflate(R.layout.activity_camera, container, false);
//		//mViewPager = (ViewPager)cameraTab.findViewById(R.id.camera_fragment);
//		
//		
//		ImageView left_arrow = (ImageView) cameraTab.findViewById(R.id.left_arrow_create);
//		left_arrow.setOnClickListener(new View.OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				TabActivity.tabViewPager.setCurrentItem(1);
//			}
//		});
//		
////		Fragment videoFragment = new CameraFragment();
////		// we get the 'childFragmentManager' for our transaction
////		FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
////		// make the back button return to the main screen
////		// and supply the tag 'left' to the backstack
////		transaction.addToBackStack("left");
////		// add our new nested fragment
////		transaction.add(getId(), videoFragment, "left");
////		// commit the transaction
////		transaction.commit();
//		
//		return cameraTab;
//	}
//	
//	public void onActivityCreated(Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//		getActivity().setProgressBarIndeterminate(true);
//		
//	}
//	
//	public void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		//mCameraFragment = new CameraFragment();
//	}
//	/**
//	 * On fragment notifying about a non-recoverable problem with the camera.
//	 */
//	public void onCameraError() {
//		//add toast or something
//	}
//
//	/**
//	 * The user wants to take a picture.
//	 *
//	 * @param view
//	 */
//	public void takePicture(View view) {
//		view.setEnabled(false);
//		//getFragmentManager().beginTransaction().add(R.id.camera_fragment, new CameraFragment()).commit();
//		//		final FragmentTransaction ft = getFragmentManager().beginTransaction(); 
////		ft.replace(R.id.camera_fragment, new CameraFragment()); 
////		ft.commit();
////		ft.addToBackStack(null);
//	
//		
////		CameraFragment duedateFrag = new CameraFragment();
////		FragmentTransaction ft  = getChildFragmentManager().beginTransaction();
////		ft.replace(R.id.camera_fragment, duedateFrag);
////		ft.addToBackStack(null);
////		ft.commit();
//		
//		
//		//		FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
////        preview.addView(mPreview);
////		NewPicCamera fragment = new NewPicCamera();
////		FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
////		transaction.add(R.id.camera_fragment, fragment).commit();
////		CameraFragment fragment = (CameraFragment) getFragmentManager().findFragmentById(R.id.camera_fragment);
////		CameraFragment fragment = (CameraFragment) getChildFragmentManager().findFragmentById(R.id.camera_fragment);
////		fragment.takePicture();
////	    FragmentManager fm = getFragmentManager();
////	    FragmentTransaction ft = fm.beginTransaction();
////	    CameraFragment llf = new CameraFragment();
////	    ft.replace(R.id.new_pic_layout, llf);
////	    ft.commit();
////	    llf.takePicture();
////		Fragment1 fragment2 = new Fragment2();
////		FragmentManager fragmentManager = getFragmentManager();
////		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
////		fragmentTransaction.replace(android.R.id.content, fragment2);
////		fragmentTransaction.commit();
////		CameraFragment secFrag = new CameraFragment();
////		FragmentTransaction fragTransaction = getFragmentManager().beginTransaction();
////		                    fragTransaction.replace(R.id.camera_fragment,secFrag );
////		                    fragTransaction.addToBackStack(null);
////		                    fragTransaction.commit();
//		
//	}
//
//	/**
//	 * A picture has been taken.
//	 */
//	public void onPictureTaken(Bitmap bitmap) {
//		File mediaStorageDir = new File(
//				Environment.getExternalStoragePublicDirectory(
//						Environment.DIRECTORY_PICTURES
//						),
//						getString(R.string.app_name)
//				);
//
//		if (!mediaStorageDir.exists()) {
//			if (!mediaStorageDir.mkdirs()) {
//				showSavingPictureErrorToast();
//				return;
//			}
//		}
//
//		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
//		File mediaFile = new File(
//				mediaStorageDir.getPath() + File.separator + "MUSTACHE_"+ timeStamp + ".jpg"
//				);
//
//		try {
//			FileOutputStream stream = new FileOutputStream(mediaFile);
//			bitmap.compress(CompressFormat.JPEG, PICTURE_QUALITY, stream);
//		} catch (IOException exception) {
//			showSavingPictureErrorToast();
//
//			Log.w(TAG, "IOException during saving bitmap", exception);
//			return;
//		}
//
//		MediaScannerConnection.scanFile(
//				mContext,
//				new String[] { mediaFile.toString() },
//				new String[] { "image/jpeg" },
//				null
//				);
//
//		// Original Mustache: send photo to photoactivity screen from camera
//		// Intent intent = new Intent(this, PhotoActivity.class);
//		// intent.setData(Uri.fromFile(mediaFile));
//		// startActivity(intent);
//
//		//finish();
//	}
//
//	private void showSavingPictureErrorToast() {
//		//	        Toast.makeText(this, getText(R.string.toast_error_save_picture), Toast.LENGTH_SHORT).show();
//	}
//}
//
