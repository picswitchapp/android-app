package com.picswitch;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.viewpagerindicator.IconPagerAdapter;

public class TabPagerAdapter extends FragmentPagerAdapter implements IconPagerAdapter {
	
	//private static final String[] CONTENT = new String[] { "Additional Puzzles", "Home", "Camera" };
	private static final int[] ICONS = new int[] {
		R.drawable.perm_additional_puzzles,
		R.drawable.perm_home,
		R.drawable.perm_create_puzzle
	};
	
	public TabPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int i) {
		switch (i) {
		case 0:
			return new AdditionalPuzzles();
		case 1:
			return new NewChallengesTab();
		case 2:
			return new CameraFragment();
		}
		return null;
	}
	
	@Override
	public int getCount() {
		return 3;
	}

	@Override
	public int getIconResId(int index) {
		return ICONS[index];
	}

    @Override
    public CharSequence getPageTitle(int position) {
        return null;
    }





}