package com.picswitch;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.parse.FindCallback;
import com.parse.ParseCrashReporting;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.parse.SaveCallback;

@SuppressLint("NewApi")
/**Activity for adding friends
 * Provides a list of all friends available in the parse database*/
//friend search to be implemented
public class EditFriendsActivity extends ListActivity {
	public static final String TAG = EditFriendsActivity.class.getSimpleName();
	protected List<ParseUser> mUsers;
	protected ParseRelation<ParseUser> mFriendsRelation;
	protected ParseUser mCurrentUser;

	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.fragment_friends);
		//show up button n action bar
		setupActionBar();
		getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
	}

	@Override
	protected void onResume() {
		super.onResume();

		mCurrentUser = ParseUser.getCurrentUser();
		mFriendsRelation = mCurrentUser.getRelation("friends");

		setProgressBarIndeterminateVisibility(true);
		
		
		
		
		
		//
		ParseQuery<ParseUser> query = ParseUser.getQuery();
		query.orderByAscending("username");
		query.setLimit(1000);
		query.findInBackground(new FindCallback<ParseUser>() {

			@Override
			public void done(List<ParseUser> users, ParseException e) {
				setProgressBarIndeterminateVisibility(false);
				if (e == null){
					//success
					mUsers = users;
					String[] usernames = new String[mUsers.size()];
					int i = 0;
					for(ParseUser user : mUsers){
						//if(user!=mCurrentUser){
							usernames[i] = user.getUsername();
							i++;
						//}
					}
					ArrayAdapter<String> adapter = new ArrayAdapter<String>(
							EditFriendsActivity.this,
							android.R.layout.simple_list_item_checked,
							usernames);
					setListAdapter(adapter);

					addFriendCheckMarks();
				}
				else {
					Log.e(TAG, e.getMessage());
					AlertDialog.Builder builder = new AlertDialog.Builder(EditFriendsActivity.this);
					builder.setMessage(e.getMessage())
					.setTitle("No friends found")
					.setPositiveButton(android.R.string.ok, null);

					AlertDialog dialog = builder.create();
					dialog.show();
				}
			}
		});

	}
	//finish
	
	/**
	 * set up the {@link android.app.ActionBar}
	 */
	@SuppressLint("NewApi")
	private void setupActionBar(){
		getActionBar().setDisplayHomeAsUpEnabled(true);
	}

	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}

		return super.onOptionsItemSelected(item);
	}
	ArrayList<String> friends_list;
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);

		if (getListView().isItemChecked(position)){
			//add friend
			mFriendsRelation.add(mUsers.get(position));
			//friends_list.add(mUsers.get(position).getUsername());

		}

		else {
			//remove friend
			mFriendsRelation.remove(mUsers.get(position));
		}
		mCurrentUser.saveInBackground(new SaveCallback() {
			@Override
			public void done(ParseException e) {
				if (e != null){
					Log.e(TAG, e.getMessage());
				}


			}
		});
		Bundle b=new Bundle();
		b.putStringArrayList("friends",
				friends_list);
//		Intent i=new Intent(getBaseContext(), NewPic.class);
//		i.putExtras(b);
	}

	
	/**Adds check marks to friend list*/
	private void addFriendCheckMarks(){
		mFriendsRelation.getQuery().findInBackground(new FindCallback<ParseUser>() {
			@Override
			public void done(List<ParseUser> friends, ParseException e) {
				if (e == null){
					//list returned - look for match
					for (int i = 0; i < mUsers.size(); i++){
						ParseUser user = mUsers.get(i);
						for (ParseUser friend : friends){
							if (friend.getObjectId().equals(user.getObjectId())){
								getListView().setItemChecked(i, true);
							}
						}
					}
				}
				else {
					Log.e(TAG, e.getMessage());
				}

			}
		});
	}
}
