package com.picswitch;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.parse.ParseFile;
import com.parse.ParseObject;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

public class MessageAdapter extends ArrayAdapter<ParseObject>{
	protected Context mContext;
	protected List<ParseObject> mMessages;
	protected ArrayList<Bitmap> chunkedImages; //saves the chunked images
	public MessageAdapter(Context context, List<ParseObject> messages){
		super(context, R.layout.message_item, messages);
		mContext = context;
		mMessages = messages;

	}

	@SuppressWarnings("deprecation")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if(convertView==null){
			convertView = LayoutInflater.from(mContext).inflate(R.layout.message_item, null);
			holder = new ViewHolder();
			holder.iconImageView = (ImageView)convertView.findViewById(R.id.messageIcon);
			holder.nameLabel = (TextView)convertView.findViewById(R.id.senderLabel);
			holder.monthLabel = (TextView)convertView.findViewById(R.id.monthLabel);
			holder.dateLabel = (TextView)convertView.findViewById(R.id.dateLabel);
			holder.messageLabel = (TextView)convertView.findViewById(R.id.messageLabel);			
			convertView.setTag(holder);

		}

		else{
			holder = (ViewHolder)convertView.getTag();
		}
		ParseObject message = mMessages.get(position);
		ParseFile file = message.getParseFile(ParseConstants.KEY_FILE);
		Uri messageUri = Uri.parse(file.getUrl());			
//		Picasso.with(getContext())
//	    .load(messageUri)
//	    .placeholder(R.drawable.logo)
//	    .transform(new BitmapTransform())
//	    .into(holder.iconImageView);
		holder.nameLabel.setText(message.getString(ParseConstants.KEY_SENDER_NAME) + " says:");
		Format formatMonth = new SimpleDateFormat("MMM");
		Format formatYear = new SimpleDateFormat("yyyy");
		Format formatTime = new SimpleDateFormat("hh:mm aa");
		Format formatDay = new SimpleDateFormat("dd");
		String monthL = formatMonth.format(message.getCreatedAt());
		String dateL = formatDay.format(message.getCreatedAt());
		String year = formatYear.format(message.getCreatedAt());
		String time = formatTime.format(message.getCreatedAt());
		holder.dateLabel.setText(dateL);
		holder.monthLabel.setText(monthL + " " + year +", " + time);
		holder.messageLabel.setText(message.getString(ParseConstants.CAPTION_FIRST));

		if(position % 2 == 1){
			convertView.findViewById(R.id.senderLabel).setBackgroundColor(Color.rgb(51,51,51));
			convertView.findViewById(R.id.monthLabel).setBackgroundColor(Color.rgb(51,51,51));
			convertView.findViewById(R.id.messageLabel).setBackgroundColor(Color.rgb(51,51,51));
			convertView.findViewById(R.id.dateLabel).setBackgroundColor(Color.rgb(50,153,153));
		}
		
		return convertView;
	}
	Target target = new Target() {
		@Override
		public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
			splitImage(bitmap, 9);

		}

		@Override
		public void onBitmapFailed(Drawable arg0) {

		}

		@Override
		public void onPrepareLoad(Drawable arg0) {


		}
	};

	public static class ViewHolder{
		ImageView iconImageView;
		TextView nameLabel;
		TextView monthLabel;
		TextView dateLabel;
		TextView messageLabel;



	}


	/**method to split the image in imageView*/
	private void splitImage(Bitmap b, int chunkNumbers) {    
		Bitmap bitmap = b;

		//For the number of rows and columns of the grid to be displayed
		int rows,cols;

		//For height and width of the small image chunks 
		int chunkHeight,chunkWidth;

		//To store all the small image chunks in bitmap format in this list 
		chunkedImages = new ArrayList<Bitmap>(chunkNumbers);

		//Getting the scaled bitmap of the source image
		Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), true);

		rows = cols = (int) Math.sqrt(chunkNumbers);
		chunkHeight = bitmap.getHeight()/rows;
		chunkWidth = bitmap.getWidth()/cols;

		chunkedImages.add(Bitmap.createBitmap(scaledBitmap, chunkWidth, chunkHeight, chunkWidth, chunkHeight));
	}

}
