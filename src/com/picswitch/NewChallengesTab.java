package com.picswitch;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.squareup.picasso.Picasso;


/**New tab
 * Specifies what happens in the main picswitch tab of the TabActivity class*/
public class NewChallengesTab extends Fragment{
	private SwipeRefreshLayout swipeContainer;
	Calendar timeTaken = Calendar.getInstance(TimeZone.getTimeZone("GMT"));

	private Context mContext;
	private ViewGroup mContainerView;
	protected List<ParseObject> mMessages = new ArrayList<ParseObject>();
	protected List<ParseObject> mFeaturedPuzzles = new ArrayList<ParseObject>();

	protected HashMap<String, View> allOldMessages = new HashMap<String, View>();
	protected HashMap<String, View> allFeaturedMessages = new HashMap<String, View>();
	protected HashMap<String, View> allFeaturedOldMessages = new HashMap<String, View>();

	protected HashMap<String, SaveMessage> solvedMessages = new HashMap<String, SaveMessage>();
	protected HashMap<String, SaveMessage> featuredSolvedMessages = new HashMap<String, SaveMessage>();

	protected List<SaveMessage> mOldMessages = new ArrayList<SaveMessage>();
	protected List<SaveMessage> mFeaturedOldMessages = new ArrayList<SaveMessage>();

	protected LayoutInflater mInflater;
	String addFriends = "Add Friends";
	String friendsRequests = "Friends & Requests";
	public String solvedFeaturedFromParse;

	String[] menuText = new String[]{
			addFriends,
			friendsRequests,
			"Settings",
			"Statistics"
	};

	Integer[] menuImages = new Integer[]{
			R.drawable.add_friends_menu_icon,
			R.drawable.contacts,
			R.drawable.settings,
			R.drawable.statistics
	};

	public enum MenuTitles{
		
		AddFriends, FriendsRequests, Settings, Statistics, NOVALUE;

		public static MenuTitles toMenuTitles(String str){
			try{
				return valueOf(str);
			}catch(Exception ex) {
				return NOVALUE;
			}
		}
	}

	public boolean creatingActivity = true;

	public TextView upTriangle;
	public TextView downTriangle;
	public TextView bottom_bar;

	public TextView newChallengesLabel;
	public TextView featuredChallengesLabel;
	public TextView solvedChallengesLabel;

	public ScrollView mContent;
	public RelativeLayout mContentLayout;

	public Button newShowMore;

	public ViewGroup newChallengesContainer;
	public ViewGroup featuredContainer;
	public ViewGroup solvedFeaturedContainer;
	public ViewGroup solvedChallengesContainer;

	//settings progressDialog
	public static ProgressDialog mStatsDialog;

	Typeface montSerrat;

	@Override
	public View onCreateView(final LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mContext = container.getContext();
		mInflater = inflater;

		mStatsDialog = new ProgressDialog(mContext);
		mStatsDialog.setMessage("Loading statistics...");
		mStatsDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		mStatsDialog.setIndeterminate(true);

		View newTab = inflater.inflate(R.layout.new_tab, container, false);

		mContainerView = (ViewGroup) newTab.findViewById(R.id.menu_container);

		newChallengesContainer = (ViewGroup) newTab.findViewById(R.id.new_challenges_container);
		featuredContainer = (ViewGroup) newTab.findViewById(R.id.featured_challenges_container);
		solvedChallengesContainer = (ViewGroup) newTab.findViewById(R.id.solved_challenges_container);
		solvedFeaturedContainer = (ViewGroup) newTab.findViewById(R.id.featured_solved_container);

		final LinearLayout menuContainer = (LinearLayout)newTab.findViewById(R.id.menu_container);

		upTriangle =   (TextView) newTab.findViewById(R.id.up_triangle);
		downTriangle = (TextView) newTab.findViewById(R.id.down_triangle);

		montSerrat = Typeface.createFromAsset(mContext.getAssets(), "Montserrat-Regular.ttf");
		TextView title = (TextView) newTab.findViewById(R.id.title_text);
		title.setTypeface(montSerrat);

		ImageView menuButton = (ImageView) newTab.findViewById(R.id.title_bar_menu_button);
		ImageView addFriendsButton = (ImageView) newTab.findViewById(R.id.title_bar_add_friends_button);
		bottom_bar = (TextView) newTab.findViewById(R.id.bottom_bar);
		newShowMore = (Button) newTab.findViewById(R.id.new_show_more_button);
		mContent = (ScrollView) newTab.findViewById(R.id.messages_scroll_view);
		mContentLayout = (RelativeLayout) newTab.findViewById(R.id.main_content_layout);


		newChallengesLabel = (TextView) newTab.findViewById(R.id.new_challenges_label);
		featuredChallengesLabel = (TextView) newTab.findViewById(R.id.featured_challenges_label);
		solvedChallengesLabel = (TextView) newTab.findViewById(R.id.solved_challenges_label);

		newChallengesLabel.setTypeface(montSerrat);
		featuredChallengesLabel.setTypeface(montSerrat);
		solvedChallengesLabel.setTypeface(montSerrat);

		featuredChallengesLabel.setText(Html.fromHtml("featured <sup><small>(?)</small></sup>"));

		featuredChallengesLabel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				AlertDialog.Builder mBuilder = new AlertDialog.Builder(mContext);
				mBuilder.setTitle("Contest Rules");
				mBuilder.setMessage(mContext.getString(R.string.contestRules1) + "\n\n" + mContext.getString(R.string.contestRules2)
						+ "\n\n" + mContext.getString(R.string.contestRules3) + "\n\n" + mContext.getString(R.string.contestRules4)
						+ "\n\n" + mContext.getString(R.string.contestRules5));
				mBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
				AlertDialog mDialog = mBuilder.create();

				mDialog.show();

				int titleId = getResources().getIdentifier( "alertTitle", "id", "android" );
				if (titleId > 0) {
					TextView mTitle = (TextView) mDialog.findViewById(titleId);
					if (mTitle != null) {
						mTitle.setTypeface(montSerrat);
					}
				}
				Button button1 = (Button) mDialog.findViewById(android.R.id.button1);
				FontsUtils.TypeFace(button1, mContext.getAssets());

				TextView mMessage = (TextView) mDialog.findViewById(android.R.id.message);
				mMessage.setTextSize(15);
				FontsUtils.TypeFace(mMessage, mContext.getAssets());
			}
		});

		newShowMore.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				
				//mMessages contains all of the user's current puzzle parseobjects
				List<View> correctViews = new ArrayList<View>();
				for(ParseObject obj : mMessages){
					String currentObjId = obj.getObjectId();
					View checkView = TabActivity.allNewMessages.get(currentObjId); //view which should be displayed
					correctViews.add(checkView);
				}
				//loop through the views to be displayed, if it is not in correct views, toss it out
				List<View> viewsToRemove = new ArrayList<View>();
				for(View view : TabActivity.allNewMessagesLinkedList){
					if(!correctViews.contains(view)){
						viewsToRemove.add(view);
					}
				}
				//actually do the removing
				for(View removeView : viewsToRemove){
					TabActivity.allNewMessagesLinkedList.remove(removeView);
				}
				
				System.out.println("newShowMore click");
				System.out.println("allNewMessagesLinkedList.size() = " + TabActivity.allNewMessagesLinkedList.size());
				System.out.println("allNewMessages HM size = " + TabActivity.allNewMessages.size());
				if(newChallengesContainer.getChildCount() <= TabActivity.allNewMessages.size()){
					int posToAddAt = newChallengesContainer.getChildCount();
					for(int i = TabActivity.allNewMessagesLinkedList.size() - 1; i >=  1; i--){
						System.out.println("adding view #" + i + " from the linked list");
						newChallengesContainer.removeView(TabActivity.allNewMessagesLinkedList.get(i));
						try{
							newChallengesContainer.removeView(TabActivity.allNewMessagesLinkedList.get(i));
							newChallengesContainer.addView(TabActivity.allNewMessagesLinkedList.get(i), posToAddAt);
						}catch(Exception e){
							e.printStackTrace();
							newShowMore.setVisibility(View.GONE);
							TabActivity.allNewMessages.clear();
							newChallengesContainer.removeAllViews();
							TabActivity.allNewMessagesLinkedList.clear();
							TabActivity.mMessages.clear();
							new retrieveNewMessagesInBackground().execute();
							break;
						}
					}
				}
				newShowMore.setVisibility(View.GONE);
			}
		});

		mContent.getViewTreeObserver().addOnScrollChangedListener(new OnScrollChangedListener(){

			@Override
			public void onScrollChanged() {
				if(mContainerView.getChildCount() != 0){
					upTriangle.setVisibility(View.GONE);
					downTriangle.setVisibility(View.VISIBLE);
					mContainerView.removeAllViews();
				}
			}	
		});

		mContentLayout.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if(mContainerView.getChildCount() != 0){
					upTriangle.setVisibility(View.GONE);
					downTriangle.setVisibility(View.VISIBLE);
					mContainerView.removeAllViews();
				}
			}
		});

		title.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if(mContainerView.getChildCount() == 0){
					upTriangle.setVisibility(View.VISIBLE);
					downTriangle.setVisibility(View.GONE);
					addMenuItemsToContainer(inflater);
					menuContainer.bringToFront();
				}else{
					upTriangle.setVisibility(View.GONE);
					downTriangle.setVisibility(View.VISIBLE);
					mContainerView.removeAllViews();
				}
			}
		});

		menuButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if(mContainerView.getChildCount() == 0){
					upTriangle.setVisibility(View.VISIBLE);
					downTriangle.setVisibility(View.GONE);
					addMenuItemsToContainer(inflater);
					menuContainer.bringToFront();
				}else{
					upTriangle.setVisibility(View.GONE);
					downTriangle.setVisibility(View.VISIBLE);
					mContainerView.removeAllViews();
				}
			}
		});

		addFriendsButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent addFriendsIntent = new Intent(mContext, AddFriendsSelector.class);
				startActivity(addFriendsIntent);
			}
		});

		swipeContainer = (SwipeRefreshLayout) newTab.findViewById(R.id.swipeContainer);
		//        // Setup refresh listener which triggers new data loading
		swipeContainer.setOnRefreshListener(new OnRefreshListener() {
			@Override
			public void onRefresh() {
				// Your code to refresh the list here.
				// Make sure you call swipeContainer.setRefreshing(false)
				// once the network request has completed successfully.
				if(mContainerView.getChildCount() == 0){
					if( (newChallengesContainer.getChildCount() == 0) && (TabActivity.allNewMessages.size() > 0) ){
						TabActivity.allNewMessages.clear();
						TabActivity.allNewMessagesLinkedList.clear();
						new retrieveNewMessagesInBackground().execute();
					}else{
						new retrieveNewMessagesInBackground().execute();
					}
					new displayOldMessagesInBackground(true).execute();
					new retrieveFeaturedMessagesInBackground().execute();
				}else{
					upTriangle.setVisibility(View.GONE);
					downTriangle.setVisibility(View.VISIBLE);
					mContainerView.removeAllViews();
				}
			} 
		});

		//#676767
		//#999999
		//#FFFFFF
		//#333333

		swipeContainer.setColorSchemeResources(R.color.refresh_color_1,
				R.color.refresh_color_2,
				R.color.refresh_color_3,
				R.color.refresh_color_4);

		return newTab;
	}

	public void addMenuItemsToContainer(LayoutInflater inflater){
		for(int i = 0; i < 4; i++){
			final ViewGroup newView = (ViewGroup) inflater.inflate(R.layout.menu_list_item, mContainerView, false);

			TextView menu_text = (TextView) newView.findViewById(R.id.menu_text);
			ImageView menu_image = (ImageView) newView.findViewById(R.id.menu_image);

			menu_text.setText(menuText[i]);
			menu_image.setImageResource(menuImages[i]);

			FontsUtils.TypeFace(menu_text,  mContext.getAssets());

			if(i == 3){
				((TextView) newView.findViewById(R.id.green_bar)).setVisibility(View.GONE);
			}

			newView.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {

					String itemText = (String) ((TextView) newView.findViewById(R.id.menu_text)).getText();
					if(itemText.equals(addFriends)){
						itemText = "AddFriends";
					}
					if(itemText.equals(friendsRequests)){
						itemText = "FriendsRequests";
					}

					switch (MenuTitles.toMenuTitles(itemText)){
					case AddFriends: {
						Intent intent = new Intent(mContext, AddFriendsSelector.class);
						startActivity(intent);
						break;
					}
					case FriendsRequests: {
						Intent intent = new Intent(mContext, MenuContactsList.class);
						startActivity(intent);
						break;
					}
					case Statistics: {
						Intent intent = new Intent(mContext, Statistics.class);
						startActivity(intent);
						mStatsDialog.show();
						break;
					}
					case Settings: {
						Intent intent = new Intent(mContext, Settings.class);
						startActivity(intent);
						break;
					}
					default: {
						break;
					}
					}
				}
			});

			mContainerView.addView(newView);
		}
	}

	@Override
	public void onPause(){
		super.onPause();
	}

	@Override
	public void onResume() {
		super.onResume();

		if(mStatsDialog.isShowing()){
			mStatsDialog.cancel();
		}

		solvedMessages = TabActivity.getSavedMessagesFromPhone();
		//featuredSolvedMessages = TabActivity.getFeaturedSavedMessagesFromPhone();

		if(!creatingActivity){

			new retrieveNewMessagesInBackground().execute();
			new displayOldMessagesInBackground(true).execute();
			new retrieveFeaturedMessagesInBackground().execute();

			upTriangle.setVisibility(View.GONE);
			downTriangle.setVisibility(View.VISIBLE);
			mContainerView.removeAllViews();

			timeTaken = Calendar.getInstance(TimeZone.getTimeZone("GMT"));

			//updating time code
			long currentTime = timeTaken.getTimeInMillis();
			int day = 1000*60*60*24, hour = 1000*60*60, minute = 1000*60;

			//update time for new messages
			for(ParseObject obj : mMessages){
				View mView = TabActivity.allNewMessages.get(obj.getObjectId());
				TextView timeTakenLabel = (TextView) mView.findViewById(R.id.timeTaken);

				long solvedTime = obj.getCreatedAt().getTime();
				long elapsedTimeSinceSolve = Math.abs(currentTime - solvedTime);

				//time stamp code				
				if(elapsedTimeSinceSolve>(2*day)){
					timeTakenLabel.setText(elapsedTimeSinceSolve/day + " days ago");
				}else if(elapsedTimeSinceSolve<(2*day) && elapsedTimeSinceSolve>day){
					timeTakenLabel.setText(elapsedTimeSinceSolve/day + " day ago");
				}else if(elapsedTimeSinceSolve<day){
					if(elapsedTimeSinceSolve>(2*hour)){
						timeTakenLabel.setText(elapsedTimeSinceSolve/hour + " hours ago");
					}else if(elapsedTimeSinceSolve<(2*hour) && elapsedTimeSinceSolve>hour){
						timeTakenLabel.setText(elapsedTimeSinceSolve/hour + " hour ago");
					}else if(elapsedTimeSinceSolve<hour && elapsedTimeSinceSolve>(2*minute)){
						timeTakenLabel.setText(elapsedTimeSinceSolve/minute + " minutes ago");
					}else if(elapsedTimeSinceSolve<hour && elapsedTimeSinceSolve>minute){
						timeTakenLabel.setText(elapsedTimeSinceSolve/minute + " minute ago");
					}else{
						if(elapsedTimeSinceSolve<minute && elapsedTimeSinceSolve>0){
							timeTakenLabel.setText("a few seconds ago");
						}
					}
				}
				//end of time code
			}

			//update time displayed for all old views
			for(String v : allOldMessages.keySet()){
				SaveMessage mMessage = solvedMessages.get(v);
				View mView = allOldMessages.get(v);
				TextView timeTakenLabel = (TextView) mView.findViewById(R.id.timeTaken);

				long solvedTime = mMessage.getSolveTime();
				long elapsedTimeSinceSolve = Math.abs(currentTime - solvedTime);

				//time stamp code				
				if(elapsedTimeSinceSolve>(2*day)){
					timeTakenLabel.setText(elapsedTimeSinceSolve/day + " days ago");
				}else if(elapsedTimeSinceSolve<(2*day) && elapsedTimeSinceSolve>day){
					timeTakenLabel.setText(elapsedTimeSinceSolve/day + " day ago");
				}else if(elapsedTimeSinceSolve<day){
					if(elapsedTimeSinceSolve>(2*hour)){
						timeTakenLabel.setText(elapsedTimeSinceSolve/hour + " hours ago");
					}else if(elapsedTimeSinceSolve<(2*hour) && elapsedTimeSinceSolve>hour){
						timeTakenLabel.setText(elapsedTimeSinceSolve/hour + " hour ago");
					}else if(elapsedTimeSinceSolve<hour && elapsedTimeSinceSolve>(2*minute)){
						timeTakenLabel.setText(elapsedTimeSinceSolve/minute + " minutes ago");
					}else if(elapsedTimeSinceSolve<hour && elapsedTimeSinceSolve>minute){
						timeTakenLabel.setText(elapsedTimeSinceSolve/minute + " minute ago");
					}else{
						if(elapsedTimeSinceSolve<minute && elapsedTimeSinceSolve>0){
							timeTakenLabel.setText("a few seconds ago");
						}
					}
				}
				//end of time code
				System.out.println("time updated to : " + timeTakenLabel.getText().toString());
			}

		}

		creatingActivity = false;

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		getActivity().setProgressBarIndeterminate(true);

		TabActivity.allNewMessages = new HashMap<String, View>();

		TabActivity.mMessages = new ArrayList<ParseObject>();
		TabActivity.mFeatured = new ArrayList<ParseObject>();

		newChallengesContainer.removeAllViews();

		solvedMessages = TabActivity.getSavedMessagesFromPhone();
		//featuredSolvedMessages = TabActivity.getFeaturedSavedMessagesFromPhone();

		new retrieveNewMessagesInBackground().execute();
		new retrieveFeaturedMessagesInBackground().execute();

		if(solvedMessages != null){
			for(SaveMessage solvedMessage : solvedMessages.values()){
				mOldMessages.add(solvedMessage);
			}
		}

		if(featuredSolvedMessages != null){
			for(SaveMessage oldFeatured : featuredSolvedMessages.values()){
				mFeaturedOldMessages.add(oldFeatured);
			}
		}

		if(mOldMessages.size() > 0){
			allOldMessages = new HashMap<String, View>();
			solvedChallengesContainer.removeAllViews();
			new displayOldMessagesInBackground(false).execute();
		}
		System.out.println("adding to solvedContainer");

		if(mFeaturedOldMessages.size() > 0){
			allFeaturedOldMessages = new HashMap<String, View>();
			solvedFeaturedContainer.removeAllViews();
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//					Multi-threading of ParseQuery loading, creating views to display
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public class CreateNewViews extends AsyncTask<List<ParseObject>,View,Void> {

		boolean startWithZero = true;

		@Override
		protected void onPreExecute(){
			if(newChallengesContainer.getChildCount() != 0){
				startWithZero = false;
			}
		}

		@Override
		protected Void doInBackground(List<ParseObject>... params) {
			List<ParseObject> newMessagesToDisplay = params[0];

			if(newMessagesToDisplay.size() > 0){
				Collections.sort(newMessagesToDisplay, new Comparator<ParseObject>() {
					@Override
					public int compare(ParseObject lhs, ParseObject rhs) {
						return lhs.getCreatedAt().compareTo(rhs.getCreatedAt());
					}
				});
			}

			for(int i = 0; i < newMessagesToDisplay.size(); i++){

				final ParseObject message = newMessagesToDisplay.get(i);

				//TabActivity.mMessages.add(i, message);

				final View newView;

				final ImageView iconImageView;
				TextView nameLabel;
				TextView messageLabel;
				TextView timeTakenLabel;
				int day = 1000*60*60*24; //day in milliseconds
				int hour = 1000*60*60;
				int minute = 1000*60;

				long elapsedTimeSinceSolve = Math.abs(message.getCreatedAt().getTime() - timeTaken.getTimeInMillis());
				//System.out.println("elapsedTime: " + elapsedTimeSinceSolve);

				if(i % 2 == 1){
					newView = (View) mInflater.inflate(R.layout.message_item_light, newChallengesContainer, false);
				}else{
					newView = (View) mInflater.inflate(R.layout.message_item, newChallengesContainer, false);
				}
				iconImageView = (ImageView)newView.findViewById(R.id.messageIcon);
				nameLabel = (TextView)newView.findViewById(R.id.senderLabel);
				messageLabel = (TextView)newView.findViewById(R.id.messageLabel);
				timeTakenLabel = (TextView)newView.findViewById(R.id.timeTaken);

				FontsUtils.TypeFace(nameLabel, mContext.getAssets());
				FontsUtils.TypeFace(messageLabel, mContext.getAssets());
				FontsUtils.TypeFace(timeTakenLabel, mContext.getAssets());

				ParseFile file = message.getParseFile(ParseConstants.KEY_PHOTO);
				final Uri messageUri = Uri.parse(file.getUrl());			

				if(message.getString(ParseConstants.CAPTION_FIRST).length()==0||message.getString(ParseConstants.CAPTION_FIRST)==null){
					try {
						nameLabel.setText(message.getParseUser(ParseConstants.KEY_SENDER).fetchIfNeeded().getString(ParseConstants.KEY_FIRST_NAME) + " "
								+ message.getParseUser(ParseConstants.KEY_SENDER).fetchIfNeeded().getString(ParseConstants.KEY_LAST_NAME));
					} catch (ParseException e1) {
						e1.printStackTrace();
					}
				}
				else{
					try {
						nameLabel.setText(message.getParseUser(ParseConstants.KEY_SENDER).fetchIfNeeded().getString(ParseConstants.KEY_FIRST_NAME) + " "
								+ message.getParseUser(ParseConstants.KEY_SENDER).fetchIfNeeded().getString(ParseConstants.KEY_LAST_NAME));
					} catch (ParseException e2) {
						e2.printStackTrace();
					}
					messageLabel.setText(message.getString(ParseConstants.CAPTION_FIRST));
				}

				//time stamp code				
				if(elapsedTimeSinceSolve>(2*day)){
					timeTakenLabel.setText(elapsedTimeSinceSolve/day + " days ago");
				}else if(elapsedTimeSinceSolve<(2*day) && elapsedTimeSinceSolve>day){
					timeTakenLabel.setText(elapsedTimeSinceSolve/day + " day ago");
				}else if(elapsedTimeSinceSolve<day){
					if(elapsedTimeSinceSolve>(2*hour)){
						timeTakenLabel.setText(elapsedTimeSinceSolve/hour + " hours ago");
					}else if(elapsedTimeSinceSolve<(2*hour) && elapsedTimeSinceSolve>hour){
						timeTakenLabel.setText(elapsedTimeSinceSolve/hour + " hour ago");
					}else if(elapsedTimeSinceSolve<hour && elapsedTimeSinceSolve>(2*minute)){
						timeTakenLabel.setText(elapsedTimeSinceSolve/minute + " minutes ago");
					}else if(elapsedTimeSinceSolve<hour && elapsedTimeSinceSolve>minute){
						timeTakenLabel.setText(elapsedTimeSinceSolve/minute + " minute ago");
					}else{
						if(elapsedTimeSinceSolve<minute && elapsedTimeSinceSolve>0){
							timeTakenLabel.setText("a few seconds ago");
						}
					}
				}
				//end of time code

				//set clickListeners
				((Activity) mContext).runOnUiThread(new Runnable() {

					@Override
					public void run() {

						loadIntoImageView(iconImageView, messageUri, message.getInt(ParseConstants.KEY_PREVIEW_INDEX));

						newView.setOnTouchListener(new OnTouchListener() {
							private GestureDetector newGestureDetector = new GestureDetector(mContext, new GestureDetector.SimpleOnGestureListener() {

								@Override
								public boolean onDown(MotionEvent e){
									return false;
								}

								@Override
								public boolean onSingleTapConfirmed(MotionEvent e){
									if(mContainerView.getChildCount() == 0){
										String messageID = message.getObjectId();
										String firstCaption = message.getString(ParseConstants.CAPTION_FIRST);
										String secondCaption = message.getString(ParseConstants.CAPTION_SECOND);
										String senderObjectId = message.getParseUser(ParseConstants.KEY_SENDER).getObjectId();
										String senderUsername = message.getParseUser(ParseConstants.KEY_SENDER).getUsername();

										ParseFile file = message.getParseFile(ParseConstants.KEY_PHOTO);
										int timeLimit = message.getInt(ParseConstants.KEY_TIME_LIMIT);

										String firstName="";
										String lastName="";

										try {
											firstName = message.getParseUser(ParseConstants.KEY_SENDER).fetchIfNeeded().getString(ParseConstants.KEY_FIRST_NAME);
											lastName  = message.getParseUser(ParseConstants.KEY_SENDER).fetchIfNeeded().getString(ParseConstants.KEY_LAST_NAME);
										} catch (ParseException e3) {
											e3.printStackTrace();
										}

										String lastNameInitial = ""; 
										if(lastName.length() > 0){
											lastNameInitial = lastName.substring(0, 1);
										}

										Uri fileUri = Uri.parse(file.getUrl());
										Intent intent = new Intent(getActivity(), OpenImage.class);
										intent.setData(fileUri);

										intent.putExtra("isParseObject", true);
										intent.putExtra("isFeatured", false);
										intent.putExtra(ParseConstants.KEY_CURRENT_NEW_INDEX, TabActivity.mMessages.indexOf(message));
										intent.putExtra(ParseConstants.CAPTION_FIRST, firstCaption);
										intent.putExtra(ParseConstants.CAPTION_SECOND, secondCaption);
										intent.putExtra(ParseConstants.KEY_TIME_LIMIT, timeLimit);
										intent.putExtra(ParseConstants.KEY_SENDER_FIRST_NAME, firstName);
										intent.putExtra("senderLastNameInitial", lastNameInitial);

										intent.putExtra(ParseConstants.KEY_SENDER, senderUsername);
										intent.putExtra("senderObjectId", senderObjectId);
										intent.putExtra("messageID", messageID);

										startActivity(intent);
									}else{
										upTriangle.setVisibility(View.GONE);
										downTriangle.setVisibility(View.VISIBLE);
										mContainerView.removeAllViews();
									}

									return false;
								}

								@Override
								public void onLongPress(MotionEvent e){
									//delete current user from the messages recipients, then save message back to parse

									AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
									builder.setTitle("Delete");
									builder.setMessage("Do you want to delete this puzzle?");
									builder.setPositiveButton("Yes", 
											new DialogInterface.OnClickListener() {

										@Override
										public void onClick(DialogInterface dialog, int which) {
											//deleting

											//immediatley remove the view

											newChallengesContainer.removeView(TabActivity.allNewMessages.get(message.getObjectId()));

											TabActivity.mMessages.remove(message);
											TabActivity.allNewMessages.remove(message.getObjectId());
											TabActivity.allNewMessagesLinkedList.remove(message);

											ParseRelation<ParseUser> messageRecipients = message.getRelation("recipients");
											messageRecipients.remove(ParseUser.getCurrentUser());
											message.saveInBackground(new SaveCallback() {
												@Override
												public void done(ParseException e) {
													if(e == null){
														new retrieveNewMessagesInBackground().execute();
													}else{
														e.printStackTrace();
													}
												}
											});

										}
									});
									builder.setNegativeButton("No", 
											new DialogInterface.OnClickListener() {

										@Override
										public void onClick(DialogInterface dialog, int which) {
											dialog.cancel();
										}
									});

									AlertDialog mDialog = builder.create();
									mDialog.show();

									int titleId = getResources().getIdentifier( "alertTitle", "id", "android" );
									if (titleId > 0) {
										TextView mTitle = (TextView) mDialog.findViewById(titleId);
										if (mTitle != null) {
											mTitle.setTypeface(montSerrat);
										}
									}

									Button button1 = (Button) mDialog.findViewById(android.R.id.button1);
									Button button2 = (Button) mDialog.findViewById(android.R.id.button2);
									FontsUtils.TypeFace(button1, mContext.getAssets());
									FontsUtils.TypeFace(button2, mContext.getAssets());

									TextView mMessage = (TextView) mDialog.findViewById(android.R.id.message);
									mMessage.setTextSize(15);
									FontsUtils.TypeFace(mMessage, mContext.getAssets());

								}

							});

							@Override
							public boolean onTouch(View v, MotionEvent event) {
								newGestureDetector.onTouchEvent(event);
								return true;
							}

						});		
					}

				});

				if(!(TabActivity.allNewMessages.containsKey(message.getObjectId()))){
					TabActivity.allNewMessages.put(message.getObjectId(), newView);
					TabActivity.mMessages.add(message);
				}

				//display the oldest 3 first, the rest will come on the button click
				if(startWithZero){
					if(newMessagesToDisplay.size() > 3){
						if( (i == (newMessagesToDisplay.size() - 3)) || ( i == (newMessagesToDisplay.size() - 2)) || ( i == (newMessagesToDisplay.size() - 1)) ){
							publishProgress(newView);
						}
					}else{
						publishProgress(newView);
					}
				}else{
					publishProgress(newView);
				}

				if(!TabActivity.allNewMessagesLinkedList.contains(newView)){
					TabActivity.allNewMessagesLinkedList.addFirst(newView);
				}
				if(!TabActivity.mMessagesLinkedList.contains(message)){
					TabActivity.mMessagesLinkedList.addFirst(message);
				}
				
			}

			return null;
		}

		@Override
		protected void onProgressUpdate(View... v){
			View newView = v[0];
			newChallengesContainer.addView(newView, 0);
		}

		@Override
		protected void onPostExecute(Void param){
			if(TabActivity.allNewMessages.size() > 3){
				newShowMore.setVisibility(View.VISIBLE);
			}else{
				newShowMore.setVisibility(View.GONE);
			}
			System.out.println("allNewMessages.size() = " + TabActivity.allNewMessages.size());
		}
	}

	public class CreateFeaturedViews extends AsyncTask<List<ParseObject>,View,Void> {

		@Override
		protected Void doInBackground(List<ParseObject>... params) {
			List<ParseObject> featuredMessagesToDisplay = params[0];

			if(featuredMessagesToDisplay.size() > 0){
				Collections.sort(featuredMessagesToDisplay, new Comparator<ParseObject>() {
					@Override
					public int compare(ParseObject lhs, ParseObject rhs) {
						return lhs.getCreatedAt().compareTo(rhs.getCreatedAt());
					}
				});
			}

			for(int i = 0; i < featuredMessagesToDisplay.size(); i++){

				final View newView;

				final ImageView iconImageView;
				TextView nameLabel;
				TextView messageLabel;
				TextView timeTakenLabel;
				int day = 1000*60*60*24, hour = 1000*60*60, minute = 1000*60;


				final ParseObject message = mFeaturedPuzzles.get(i);
				final int currentIndex = i;

				//TabActivity.mFeatured.add(i, message);

				if(i % 2 == 1){
					newView = (View) mInflater.inflate(R.layout.message_item_light, featuredContainer, false);
				}else{
					newView = (View) mInflater.inflate(R.layout.message_item, featuredContainer, false);
				}
				iconImageView = (ImageView)newView.findViewById(R.id.messageIcon);
				nameLabel = (TextView)newView.findViewById(R.id.senderLabel);
				messageLabel = (TextView)newView.findViewById(R.id.messageLabel);
				timeTakenLabel = (TextView)newView.findViewById(R.id.timeTaken);


				FontsUtils.TypeFace(nameLabel, mContext.getAssets());
				FontsUtils.TypeFace(messageLabel, mContext.getAssets());
				FontsUtils.TypeFace(timeTakenLabel, mContext.getAssets());

				ParseFile file = message.getParseFile(ParseConstants.KEY_PHOTO);
				final Uri messageUri = Uri.parse(file.getUrl());		

				//loadIntoImageView(iconImageView, messageUri);
				long elapsedTimeSinceSolve = (message.getDate("endDate").getTime()-timeTaken.getTimeInMillis());
				nameLabel.setText(message.getString(ParseConstants.KEY_COMPANY_NAME));
				messageLabel.setText(message.getString(ParseConstants.CAPTION_FIRST));

				//time stamp code				
				if(elapsedTimeSinceSolve>(2*day)){
					timeTakenLabel.setText(elapsedTimeSinceSolve/day + " days left");
				}else if(elapsedTimeSinceSolve<(2*day) && elapsedTimeSinceSolve>day){
					timeTakenLabel.setText(elapsedTimeSinceSolve/day + " day left");
				}else if(elapsedTimeSinceSolve<day){
					if(elapsedTimeSinceSolve>(2*hour)){
						timeTakenLabel.setText(elapsedTimeSinceSolve/hour + " hours left");
					}else if(elapsedTimeSinceSolve<(2*hour) && elapsedTimeSinceSolve>hour){
						timeTakenLabel.setText(elapsedTimeSinceSolve/hour + " hour left");
					}else if(elapsedTimeSinceSolve<hour && elapsedTimeSinceSolve>(2*minute)){
						timeTakenLabel.setText(elapsedTimeSinceSolve/minute + " minutes left");
					}else if(elapsedTimeSinceSolve<hour && elapsedTimeSinceSolve>minute){
						timeTakenLabel.setText(elapsedTimeSinceSolve/minute + " minute left");
					}else{
						if(elapsedTimeSinceSolve<minute && elapsedTimeSinceSolve>0){
							timeTakenLabel.setText("a few seconds left");
						}
					}
				}
				//end of time code

				((Activity) mContext).runOnUiThread(new Runnable() {

					@Override
					public void run() {

						loadIntoImageView(iconImageView, messageUri, message.getInt(ParseConstants.KEY_PREVIEW_INDEX));

						newView.setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View v){
								if(mContainerView.getChildCount() == 0){
									String firstCaption = message.getString(ParseConstants.CAPTION_FIRST);
									String secondCaption = message.getString(ParseConstants.CAPTION_SECOND);

									ParseFile file = message.getParseFile(ParseConstants.KEY_PHOTO);
									int timeLimit = message.getInt(ParseConstants.KEY_TIME_LIMIT);

									String firstName="";						
									firstName = message.getString(ParseConstants.KEY_COMPANY_NAME);

									Uri fileUri = Uri.parse(file.getUrl());
									Intent intent = new Intent(getActivity(), OpenImage.class);
									intent.setData(fileUri);

									intent.putExtra("messageID", message.getObjectId());
									intent.putExtra("isParseObject", true);
									intent.putExtra("isFeatured", true);
									intent.putExtra(ParseConstants.KEY_CURRENT_NEW_INDEX, currentIndex);
									intent.putExtra(ParseConstants.CAPTION_FIRST, firstCaption);
									intent.putExtra(ParseConstants.CAPTION_SECOND, secondCaption);
									intent.putExtra(ParseConstants.KEY_TIME_LIMIT, timeLimit);
									intent.putExtra(ParseConstants.KEY_SENDER_FIRST_NAME, firstName);

									startActivity(intent);
								}else{
									upTriangle.setVisibility(View.GONE);
									downTriangle.setVisibility(View.VISIBLE);
									mContainerView.removeAllViews();
								}
							}
						});
					}

				});

				publishProgress(newView);

				allFeaturedMessages.put(message.getObjectId(), newView);
				TabActivity.mFeatured.add(message);
				TabActivity.mFeaturedLinkedList.addFirst(message);
			}
			return null;
		}

		@Override
		protected void onProgressUpdate(View... v){
			View newView = v[0];
			featuredContainer.addView(newView, 0);
		}

		@Override
		protected void onPostExecute(Void param){

		}

	}

	public class retrieveNewMessagesInBackground extends AsyncTask<Void, View, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			//query for new messages
			ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(ParseConstants.CLASS_MESSAGES);
			query.whereEqualTo(ParseConstants.KEY_RECIPIENTS, ParseUser.getCurrentUser());
			query.addDescendingOrder(ParseConstants.KEY_CREATED_AT);
			query.findInBackground(new FindCallback<ParseObject>() {
				@Override
				public void done(final List<ParseObject> objects, ParseException e) {
					swipeContainer.setRefreshing(false);
					if(e==null){
						//new messages are available to update
						//objects contains all of the users new messages
						mMessages = objects;

						//TabActivity.mMessages = objects;

						for(int i = 0; i < objects.size(); i++){
							if(!(TabActivity.mMessagesHM.keySet().contains(objects.get(i).getObjectId()))){	//if the HM doesnt contain the current object
								TabActivity.mMessagesHM.put(objects.get(i).getObjectId(), objects.get(i));
							}
						}


						//creates a HashMap, objects ids as keys, the full parseobject as the mapping
						final HashMap<String, ParseObject> objectsIds = new HashMap<String, ParseObject>();
						for(int i = 0; i < objects.size(); i++){
							objectsIds.put(objects.get(i).getObjectId(), objects.get(i));
						}

						//handles the case for removing a view from the newcontainer
						List<String> objectIdsToRemove = new ArrayList<String>();
						if(TabActivity.allNewMessages.size() > 0){
							for(final String objId : TabActivity.allNewMessages.keySet()){
								if(!(objectsIds.keySet().contains(objId))){

									((Activity) mContext).runOnUiThread(new Runnable() {

										@Override
										public void run() {

											View currentView = TabActivity.allNewMessages.get(objId);


											TabActivity.allNewMessagesLinkedList.remove(currentView);


											TabActivity.mMessages.remove(objectsIds.get(objId));
											TabActivity.mMessagesHM.remove(objId);

											newChallengesContainer.removeView(TabActivity.allNewMessages.get(objId));
										}

									});

									objectIdsToRemove.add(objId);
								}
							}
						}else{
							//dont do anything, as there arent any in the hashmap to deal with
						}

						for(String str : objectIdsToRemove){
							TabActivity.allNewMessages.remove(str);
							TabActivity.mMessages.remove(TabActivity.mMessagesHM.get(str));
						}

						//handles the case of adding a view to the newcontainer
						LinkedList<ParseObject> newObjects = new LinkedList<ParseObject>(); //will contain the new parseobjects that need to be displayed
						for(String objId : objectsIds.keySet()){
							if(!(TabActivity.allNewMessages.keySet().contains(objId))){	//if HM doesnt contain that object ID
								//new view to be added to the container
								newObjects.addFirst(objectsIds.get(objId));
							}
						}

						((Activity) mContext).runOnUiThread(new Runnable() {

							@Override
							public void run() {
								if(objects.size() == 0){
									newChallengesLabel.setVisibility(View.GONE);
								}else{
									newChallengesLabel.setVisibility(View.VISIBLE);
									newChallengesLabel.setText("new " + "(" + objects.size() + ")");
								}
							}
						});

						if(newObjects.size() > 0){
							new CreateNewViews().execute(newObjects);
						}

						if(newObjects.size() > 3){
							newShowMore.setVisibility(View.VISIBLE);
						}

					}else{
						e.printStackTrace();
					}


				}
			});

			return null;
		}

		@Override
		protected void onPostExecute(Void param){
			if(newChallengesContainer.getChildCount() == 0){
				newShowMore.setVisibility(View.GONE);
			}
		}
	}

	public void createSolvedString(List<ParseObject> userObjects){
		if(userObjects.size() == 0){
			String tempSolved = "";
			System.out.println("before pass 1: " + tempSolved);
		}else{
			List temp;
			temp = userObjects.get(0).getList(ParseConstants.KEY_FEATURED_SOLVED);
			String tmp = "";
			if(temp==null){
				solvedFeaturedFromParse = "";
			}else{
				solvedFeaturedFromParse = temp.toString();
			}

			if(solvedFeaturedFromParse.length() < 4){
				solvedFeaturedFromParse = "";
			}else{
				if(solvedFeaturedFromParse.contains("[")){
					tmp = solvedFeaturedFromParse.replace("[", "");
					solvedFeaturedFromParse = tmp;
					System.out.println("before left brace case");
				}
				if(solvedFeaturedFromParse.contains("]")){
					tmp = solvedFeaturedFromParse.replace("]", "");
					System.out.println("before right brace case");
					solvedFeaturedFromParse = tmp;
				}
			}
			System.out.println("before pass 2: " + solvedFeaturedFromParse);
		}
	}

	public class retrieveFeaturedMessagesInBackground extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {


			//first, query for user's already-solved featured puzzle array
			ParseQuery<ParseObject> currentUser = new ParseQuery<ParseObject>(ParseConstants.CLASS_USER);
			currentUser.whereEqualTo(ParseConstants.KEY_OBJECT_ID, ParseUser.getCurrentUser().getObjectId());
			currentUser.findInBackground(new FindCallback<ParseObject>() {

				@Override
				public void done(List<ParseObject> objects, ParseException e) {
					if(e == null){

						createSolvedString(objects);

						//do the other query in here, eh?

						String t = solvedFeaturedFromParse;
						if(t.contains(" ")){
							t = t.replaceAll(" ", "");
						}

						List<String> tmpFeatured = Arrays.asList(t.split(","));

						//Date currentDate = new Date();
						Date currentDate = timeTaken.getTime();
						String region = getUserCountry(getActivity());
						System.out.println("REGION: " + region);
						if(region==null){
							region = "PICSWITCH";
						}
						
						
						//query for featured messages
						ParseQuery<ParseObject> featuredQuery = new ParseQuery<ParseObject>(ParseConstants.CLASS_FEATURED_PUZZLES);
						featuredQuery.whereGreaterThanOrEqualTo("endDate", currentDate);
						featuredQuery.whereLessThanOrEqualTo("startDate", currentDate);
						featuredQuery.whereNotContainedIn(ParseConstants.KEY_OBJECT_ID, TabActivity.mFeaturedHM.keySet());
						featuredQuery.whereNotContainedIn(ParseConstants.KEY_OBJECT_ID, tmpFeatured);
						featuredQuery.whereContains("region", region);//whereContainedIn("region", regionColl);
						featuredQuery.findInBackground(new FindCallback<ParseObject>() {
							
							@Override
							public void done(final List<ParseObject> messages, ParseException e) {
								getActivity().setProgressBarIndeterminate(false);
								if(e==null){
									//Featured puzzles are available
									mFeaturedPuzzles = messages;

									for(int i = 0; i < messages.size(); i++){
										if(!(TabActivity.mFeaturedHM.keySet().contains(messages.get(i).getObjectId()))){	//if the HM doesnt contain the current object
											TabActivity.mFeaturedHM.put(messages.get(i).getObjectId(), messages.get(i));
										}
									}

									//creates a local HashMap, objects ids as keys, the full parseobject as the mapping
									final HashMap<String, ParseObject> objectsIds = new HashMap<String, ParseObject>();
									for(int i = 0; i < messages.size(); i++){
										objectsIds.put(messages.get(i).getObjectId(), messages.get(i));
									}

									//handles the case for removing a view from the featuredcontainer
									List<String> objectIdsToRemove = new ArrayList<String>();
									if(allFeaturedMessages.size() > 0){
										for(final String objId : allFeaturedMessages.keySet()){
											if(!(objectsIds.keySet().contains(objId))){
												((Activity) mContext).runOnUiThread(new Runnable(){

													@Override
													public void run() {

														TabActivity.mFeatured.remove(objectsIds.get(objId));
														TabActivity.mFeaturedHM.remove(objId);

														featuredContainer.removeView(allFeaturedMessages.get(objId));
													}

												});
												objectIdsToRemove.add(objId);
											}
										}
									}else{
										//do nothing
									}

									for(String str : objectIdsToRemove){
										allFeaturedMessages.remove(str);
										TabActivity.mFeatured.remove(TabActivity.mFeaturedHM.get(str));
									}

									//handles the case of adding a view to the newcontainer
									List<ParseObject> objectsToAdd = new ArrayList<ParseObject>(); //will contain the new parseobjects that need to be displayed
									for(String objId : objectsIds.keySet()){
										if(featuredSolvedMessages != null){
											if( (!(allFeaturedMessages.keySet().contains(objId))) && (!(featuredSolvedMessages.keySet().contains(objId))) ){	//if HM doesnt contain that object ID
												//new view to be added to the container
												objectsToAdd.add(objectsIds.get(objId));
											}
										}else{
											if((!(allFeaturedMessages.keySet().contains(objId)))){
												objectsToAdd.add(objectsIds.get(objId));
											}
										}
									}

									((Activity) mContext).runOnUiThread(new Runnable(){
										@Override
										public void run() {
											//always set the label to visible
											featuredChallengesLabel.setVisibility(View.VISIBLE);
											View noFeaturedView = null;
											if(messages.size() == 0){
												//featuredChallengesLabel.setVisibility(View.GONE);
												if(featuredContainer.getChildCount() == 0){
													noFeaturedView = mInflater.inflate(R.layout.no_featured_view, featuredContainer, false);
													TextView checkBack = (TextView) noFeaturedView.findViewById(R.id.senderLabel);
													TextView swipeTo   = (TextView) noFeaturedView.findViewById(R.id.messageLabel);
													FontsUtils.TypeFace(swipeTo, mContext.getAssets());
													FontsUtils.TypeFace(checkBack, mContext.getAssets());
													allFeaturedMessages.put("noFeaturedView", noFeaturedView);
													featuredContainer.addView(noFeaturedView);
												}
											}else{
												if(allFeaturedMessages.keySet().contains("noFeaturedView")){
													featuredContainer.removeView(noFeaturedView);
												}
											}
										}
									});

									if(objectsToAdd.size() > 0){
										new CreateFeaturedViews().execute(objectsToAdd);
									}

								}else{
									e.printStackTrace();
								}
							}
						});

					}else{
						System.out.println("before error");
						e.printStackTrace();
					}
				}
			});



			return null;
		}

		@Override
		protected void onPostExecute(Void param){
			if(featuredContainer.getChildCount() == 0){
				featuredChallengesLabel.setVisibility(View.GONE);
			}
		}
	}

	/**
	 * Helper function for loading images with Picasso into an imageView using UI thread
	 */
	public void loadIntoImageView(final ImageView target, final Uri mUri, int previewIndex){
		//Toast.makeText(mContext, "loading with Picasso", Toast.LENGTH_SHORT).show();
		Picasso.with(mContext)
		.load(mUri)
		.placeholder(R.drawable.picasso_loading_animation)
		.transform(new BitmapTransform(previewIndex))
		.into(target);
	}

	public class displayOldMessagesInBackground extends AsyncTask<Void,View,Void> {

		List<SaveMessage> mOldMessagesBack = new ArrayList<SaveMessage>();
		boolean addFirst;

		public displayOldMessagesInBackground(boolean first) {
			addFirst = first;
		}

		@Override
		protected void onPreExecute(){
			HashMap<String, SaveMessage> loadedFromPhone = new HashMap<String,SaveMessage>();

			if(solvedMessages != null){
				for(String str : solvedMessages.keySet()){
					loadedFromPhone.put(str, solvedMessages.get(str));
				}
			}

			if(loadedFromPhone.size() == 0){
				solvedChallengesLabel.setVisibility(View.GONE);
			}else{
				solvedChallengesLabel.setVisibility(View.VISIBLE);
				//solvedChallengesContainer.removeAllViews();
			}

			//handle the case of a savemessage not displayed, but in loadedfromphone
			for(String str : loadedFromPhone.keySet()){
				if(!(allOldMessages.keySet().contains(str))){
					//key not in allOldMessages
					mOldMessagesBack.add(loadedFromPhone.get(str));
				}
			}

			//handle the case of removing a view if its no longer saved on phone
			List<String> objectIdsToRemove = new ArrayList<String>();
			if(allOldMessages.size() > 0){
				for(final String str : allOldMessages.keySet()){
					if(!(loadedFromPhone.containsKey(str))){

						solvedChallengesContainer.removeView(allOldMessages.get(str));

						objectIdsToRemove.add(str);
					}
				}
				for(String str : objectIdsToRemove){
					allOldMessages.remove(str);
				}
			}
		}

		@Override
		protected Void doInBackground(Void... params) {

			TextView nameLabel;
			TextView messageLabel;
			TextView timeTakenLabel;
			long currentTime = timeTaken.getTimeInMillis();
			int day = 1000*60*60*24, hour = 1000*60*60, minute = 1000*60;

			Collections.sort(mOldMessagesBack, new Comparator<SaveMessage>(){
				@Override
				public int compare(SaveMessage lhs, SaveMessage rhs) {
					return Long.valueOf(rhs.getSolveTime()).compareTo(Long.valueOf(lhs.getSolveTime()));
				}
			});


			for(int i = 0; i < mOldMessagesBack.size(); i++){

				final ImageView iconImageView;

				final SaveMessage mMessage = mOldMessagesBack.get(i);
				final View newView;
				final int currentIndex = i;

				long solvedTime = mMessage.getSolveTime();
				long elapsedTimeSinceSolve = Math.abs(currentTime - solvedTime);

				if(i % 2 == 1){
					newView = (View) mInflater.inflate(R.layout.message_item_light, solvedChallengesContainer, false);
				}else{
					newView = (View) mInflater.inflate(R.layout.message_item, solvedChallengesContainer, false);
				}

				iconImageView = (ImageView)newView.findViewById(R.id.messageIcon);
				nameLabel = (TextView)newView.findViewById(R.id.senderLabel);
				messageLabel = (TextView)newView.findViewById(R.id.messageLabel);
				timeTakenLabel = (TextView)newView.findViewById(R.id.timeTaken);

				FontsUtils.TypeFace(nameLabel, mContext.getAssets());
				FontsUtils.TypeFace(messageLabel, mContext.getAssets());
				FontsUtils.TypeFace(timeTakenLabel, mContext.getAssets());

				nameLabel.setText(mMessage.getSenderFirstName() + " " + mMessage.getSenderLastName());
				messageLabel.setText(mMessage.getFirstCaption());

				//time stamp code				
				if(elapsedTimeSinceSolve>(2*day)){
					timeTakenLabel.setText(elapsedTimeSinceSolve/day + " days ago");
				}else if(elapsedTimeSinceSolve<(2*day) && elapsedTimeSinceSolve>day){
					timeTakenLabel.setText(elapsedTimeSinceSolve/day + " day ago");
				}else if(elapsedTimeSinceSolve<day){
					if(elapsedTimeSinceSolve>(2*hour)){
						timeTakenLabel.setText(elapsedTimeSinceSolve/hour + " hours ago");
					}else if(elapsedTimeSinceSolve<(2*hour) && elapsedTimeSinceSolve>hour){
						timeTakenLabel.setText(elapsedTimeSinceSolve/hour + " hour ago");
					}else if(elapsedTimeSinceSolve<hour && elapsedTimeSinceSolve>(2*minute)){
						timeTakenLabel.setText(elapsedTimeSinceSolve/minute + " minutes ago");
					}else if(elapsedTimeSinceSolve<hour && elapsedTimeSinceSolve>minute){
						timeTakenLabel.setText(elapsedTimeSinceSolve/minute + " minute ago");
					}else{
						if(elapsedTimeSinceSolve<minute && elapsedTimeSinceSolve>0){
							timeTakenLabel.setText("a few seconds ago");
						}
					}
				}
				//end of time code

				((Activity) mContext).runOnUiThread(new Runnable(){

					@Override
					public void run() {

						iconImageView.setBackgroundResource(R.drawable.custom_loading1);
						Animation mAnimation = new AnimationUtils().loadAnimation(mContext, R.anim.rotate_image);
						mAnimation.setRepeatMode(Animation.INFINITE);
						iconImageView.startAnimation(mAnimation);

						//set the bitmap
						BitmapWorkerOldMessages task = new BitmapWorkerOldMessages(iconImageView);
						task.execute(mMessage.getImageData());

						final String mID = mMessage.getMessageID();
						newView.setOnTouchListener(new OnTouchListener() {
							private GestureDetector gestureDetector = new GestureDetector(mContext, new GestureDetector.SimpleOnGestureListener() {

								@Override
								public boolean onDown(MotionEvent e){
									return true;
								}

								@Override
								public boolean onSingleTapConfirmed(MotionEvent e){
									if(mContainerView.getChildCount() == 0){
										String messageID = mMessage.getMessageID();
										String firstCaption = mMessage.getFirstCaption();
										String secondCaption = mMessage.getSecondCaption();
										String senderUsername = mMessage.getSenderUsername();
										String senderObjectId = mMessage.getSenderObjectId();
										boolean isFeatured = mMessage.getIsFeatured();
										int switches = mMessage.getSwitches();
										double time = mMessage.getTime();

										Uri fileUri = Uri.parse(mMessage.getMessageUri());

										String firstName="";	

										String lastName="";
										String lastNameInitial="";

										if(!isFeatured){
											lastName = mMessage.getSenderLastName();
											if(lastName.length() > 0){
												lastNameInitial = lastName.substring(0, 1);
											}
										}

										firstName = mMessage.getSenderFirstName();

										Intent intent = new Intent(getActivity(), NewChallengesViewImage.class);

										//Toast.makeText(mContext, "loading size (bytes): " + mMessage.getImageData().length, Toast.LENGTH_SHORT).show();
										System.out.println("loading size (bytes): " + mMessage.getImageData().length);

										String sponsorLink = "";
										if(isFeatured){
											sponsorLink = mMessage.getSponsorLink();
											intent.putExtra("sponsorLink", sponsorLink);
										}

										intent.putExtra("imageData", mMessage.getImageData());
										intent.putExtra("isSolved", true);
										intent.putExtra("isFeatured", isFeatured);
										intent.putExtra("isParseObject", false);

										intent.putExtra("messageID", messageID);

										intent.putExtra("senderLastNameInitial", lastNameInitial);

										String currentIndexString = null;
										if(addFirst){
											currentIndexString = Integer.toString(mOldMessagesBack.size() - 1 - currentIndex);
										}else{
											currentIndexString = Integer.toString(currentIndex);
										}

										Bundle bundle=new Bundle();
										bundle.putStringArray(ParseConstants.KEY_ADDITIONAL_DATA, new String[]
												{firstCaption, secondCaption, Double.toString(time), 
												Integer.toString(switches), firstName, currentIndexString, 
												senderUsername, senderObjectId});
										intent.putExtras(bundle);

										startActivity(intent);
									}else{
										upTriangle.setVisibility(View.GONE);
										downTriangle.setVisibility(View.VISIBLE);
										mContainerView.removeAllViews();
									}
									return false;
								}

								@Override
								public void onLongPress(MotionEvent e){
									//Toast.makeText(mContext, "LongPress", Toast.LENGTH_SHORT).show();
									//remove the solved message from the user's phone, and the ViewGroup
									//make a dialog box

									//createDeleteDialog(mID);
									AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);

									alertDialogBuilder.setTitle("Delete");
									alertDialogBuilder.setMessage("Do you want to delete this puzzle?")
									.setCancelable(false)
									.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

										@Override
										public void onClick(DialogInterface dialog, int which) {
											//remove all views

											//addTo.removeAllViews();
											solvedMessages.remove(mID);

											TabActivity.saveMapToFile(solvedMessages);
											//				Intent intent = new Intent(mContext, TabActivity.class);
											//
											//				startActivity(intent);

											if(mContainerView.getChildCount() == 0){
												new retrieveNewMessagesInBackground().execute();
												new displayOldMessagesInBackground(false).execute();
											}else{
												upTriangle.setVisibility(View.GONE);
												downTriangle.setVisibility(View.VISIBLE);
												mContainerView.removeAllViews();
											}

											dialog.cancel();
										}
									})
									.setNegativeButton("No", new DialogInterface.OnClickListener() {

										@Override
										public void onClick(DialogInterface dialog, int which) {
											//close the dialog, do nothing
											dialog.cancel();
										}
									});

									AlertDialog mDialog = alertDialogBuilder.create();
									mDialog.show();

									int titleId = getResources().getIdentifier( "alertTitle", "id", "android" );
									if (titleId > 0) {
										TextView mTitle = (TextView) mDialog.findViewById(titleId);
										if (mTitle != null) {
											mTitle.setTypeface(montSerrat);
										}
									}

									Button button1 = (Button) mDialog.findViewById(android.R.id.button1);
									Button button2 = (Button) mDialog.findViewById(android.R.id.button2);
									FontsUtils.TypeFace(button1, mContext.getAssets());
									FontsUtils.TypeFace(button2, mContext.getAssets());

									TextView mMessage = (TextView) mDialog.findViewById(android.R.id.message);
									mMessage.setTextSize(15);
									FontsUtils.TypeFace(mMessage, mContext.getAssets());
								}

							});

							@Override
							public boolean onTouch(View v, MotionEvent event) {
								gestureDetector.onTouchEvent(event);
								return true;
							}

						});

					}

				});

				allOldMessages.put(mMessage.getMessageID(), newView);

				publishProgress(newView);
				if(!addFirst){
					TabActivity.savedSolvedLinkedList.addLast(mMessage);
				}else{
					TabActivity.savedSolvedLinkedList.addFirst(mMessage);
				}

			}

			return null;
		}

		@Override
		protected void onProgressUpdate(View... v){
			View oldView = v[0];
			if(!addFirst){
				solvedChallengesContainer.addView(oldView);
			}else{
				solvedChallengesContainer.addView(oldView, 0);
			}
		}

		@Override
		protected void onPostExecute(Void param){

		}

	}

	public class BitmapWorkerOldMessages extends AsyncTask<byte[], Void, Bitmap> {

		private final WeakReference<ImageView> imageViewReference;
		private byte[] data;

		public BitmapWorkerOldMessages(ImageView imageView){
			imageViewReference = new WeakReference<ImageView>(imageView);
		}

		@Override
		protected Bitmap doInBackground(byte[]... params) {
			data = params[0];

			return decodeSampledBitmapFromResource(data, 100, 100);
		}

		@Override
		protected void onPostExecute(Bitmap param){
			if(imageViewReference != null && param != null){
				final ImageView imageView = imageViewReference.get();
				if(imageView != null){
					imageView.setAnimation(null);
					imageView.setImageBitmap(param);
				}
			}
		}
	}

	public static Bitmap decodeSampledBitmapFromResource(byte[] bitmapData, int reqWidth, int reqHeight) {

		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeByteArray(bitmapData, 0, bitmapData.length, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeByteArray(bitmapData, 0, bitmapData.length, options);
	}

	public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;

		//System.out.println("options bitmap values - height: " + height + ", width: " + width);

		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight
					&& (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}

		return inSampleSize;
	}
	
	
	public static String getUserCountry(Context context) {
	    try {
	        final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
	        final String simCountry = tm.getSimCountryIso();
	        if (simCountry != null && simCountry.length() == 2) { // SIM country code is available
	            return simCountry.toUpperCase(Locale.US);
	        }
	        else if (tm.getPhoneType() != TelephonyManager.PHONE_TYPE_CDMA) { // device is not 3G (would be unreliable)
	            String networkCountry = tm.getNetworkCountryIso();
	            if (networkCountry != null && networkCountry.length() == 2) { // network country code is available
	                return networkCountry.toUpperCase(Locale.US);
	            }
	        }
	    }
	    catch (Exception e) { }
	    return null;
	}
}

