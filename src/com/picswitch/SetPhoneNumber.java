package com.picswitch;

import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;

public class SetPhoneNumber extends Activity {

	EditText etPhone1;
	EditText etPhone2;
	String newPhoneNumber;

	Button selectCountry;
	Typeface montserrat;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_set_phone_number);

		getActionBar().hide();

		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
			window.setStatusBarColor(Color.rgb(32, 99, 99));
		}

		montserrat = Typeface.createFromAsset(getAssets(), "Montserrat-Regular.ttf");

		TextView titleText = (TextView) findViewById(R.id.title_text_pn);
		titleText.setTypeface(montserrat);

		//back
		ImageView leftArrow = (ImageView) findViewById(R.id.left_arrow_pn);
		leftArrow.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				SetPhoneNumber.this.finish();
			}
		});

		//setting fonts for things
		TextView pnLabel = (TextView) findViewById(R.id.pn_label);
		pnLabel.setTypeface(montserrat);
		etPhone1 = (EditText) findViewById(R.id.user_entered_pn1);
		etPhone2 = (EditText) findViewById(R.id.user_entered_pn2);
		FontsUtils.TypeFace(etPhone1, getAssets());
		FontsUtils.TypeFace(etPhone2, getAssets());
		etPhone1.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
		etPhone2.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
		TextView confirmLabel = (TextView) findViewById(R.id.confirm_pn_button);
		FontsUtils.TypeFace(confirmLabel, getAssets());


		/**Country list*/
		selectCountry = (Button) findViewById(R.id.pn_select_country);
		FontsUtils.TypeFace(selectCountry, getAssets());
		selectCountry.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				selectCountry();

			}
		});


		//confirm button listener
		RelativeLayout confirmButton = (RelativeLayout) findViewById(R.id.pn_confirm_button);
		confirmButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				TextView countryCodeTextView = (TextView) findViewById(R.id.pn_country_code1);
				String countryCode = countryCodeTextView.getText().toString();
				String newPhoneNumberTemp1 = etPhone1.getText().toString().trim();
				String newPhoneNumberTemp2 = etPhone2.getText().toString().trim();

				if( (newPhoneNumberTemp1 != null && newPhoneNumberTemp1.length() == 0) || (newPhoneNumberTemp1 == null)){
					Toast.makeText(SetPhoneNumber.this, "Please enter your phone number", Toast.LENGTH_SHORT).show();
				}else if( (newPhoneNumberTemp2 != null && newPhoneNumberTemp2.length() == 0) || (newPhoneNumberTemp2 == null)){
					Toast.makeText(SetPhoneNumber.this, "Please confirm your phone number", Toast.LENGTH_SHORT).show();
				}else if(newPhoneNumberTemp1.length() <= 6){
					Toast.makeText(SetPhoneNumber.this, "Please enter your phone number", Toast.LENGTH_SHORT).show();
				}else if(!newPhoneNumberTemp1.equals(newPhoneNumberTemp2)){
					Toast.makeText(SetPhoneNumber.this, "Phone numbers don't match. Please try again", Toast.LENGTH_SHORT).show();
				}else{
					newPhoneNumber = countryCode + newPhoneNumberTemp1;

					/**International formatting*/
					PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
					PhoneNumber formattedPhoneNumber = null;


					try {
						formattedPhoneNumber = phoneUtil.parse(newPhoneNumber, "");
					} catch (NumberParseException e) {

						System.err.println("NumberParseException was thrown: " + e.toString());
					}

					newPhoneNumber = phoneUtil.format(formattedPhoneNumber, PhoneNumberFormat.INTERNATIONAL);
					/**End of international phone number formatting*/

					int changeCount;
					try{
						changeCount = ParseUser.getCurrentUser().getInt("phoneNumberChangeCount");
					}catch(Exception e){
						e.printStackTrace();
						changeCount = -1;
					}

					if(phoneUtil.isValidNumber(formattedPhoneNumber)){

						if(changeCount == -1){

							AlertDialog.Builder mBuilder = new AlertDialog.Builder(SetPhoneNumber.this);
							mBuilder.setTitle("Confirm");
							mBuilder.setMessage("Are you sure you want to enter " + newPhoneNumber + " as your phone number on picswitch?");

							mBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									ParseUser.getCurrentUser().put("phoneNumber", newPhoneNumber);
									ParseUser.getCurrentUser().put("phoneNumberChangeCount", 0);
									ParseUser.getCurrentUser().saveInBackground();
									dialog.cancel();
								}
							});
							mBuilder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									dialog.cancel();
								}
							});
							AlertDialog mDialog = mBuilder.create();
							mDialog.show();
							int titleId = getResources().getIdentifier( "alertTitle", "id", "android" );
							if (titleId > 0) {
								TextView mTitle = (TextView) mDialog.findViewById(titleId);
								if (mTitle != null) {
									mTitle.setTypeface(montserrat);
								}
							}

							Button button1 = (Button) mDialog.findViewById(android.R.id.button1);
							Button button2 = (Button) mDialog.findViewById(android.R.id.button2);
							FontsUtils.TypeFace(button1, getAssets());
							FontsUtils.TypeFace(button2, getAssets());

							TextView mMessage = (TextView) mDialog.findViewById(android.R.id.message);
							mMessage.setTextSize(15);
							FontsUtils.TypeFace(mMessage, getAssets());

						}else if(changeCount == 0){

							AlertDialog.Builder mBuilder = new AlertDialog.Builder(SetPhoneNumber.this);
							mBuilder.setTitle("Confirm");
							mBuilder.setMessage("Are you sure you want to enter " + newPhoneNumber + " as your phone number on picswitch?");
							mBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									newPhoneNumber = newPhoneNumber.replaceAll("\\D+","");
									ParseUser.getCurrentUser().put("phoneNumber", newPhoneNumber);
									ParseUser.getCurrentUser().put("phoneNumberChangeCount", 1);
									ParseUser.getCurrentUser().saveInBackground(new SaveCallback() {

										@Override
										public void done(ParseException arg0) {
											finish();

										}
									});

									dialog.cancel();
								}
							});
							mBuilder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									dialog.cancel();
								}
							});
							AlertDialog mDialog = mBuilder.create();
							mDialog.show();
							int titleId = getResources().getIdentifier( "alertTitle", "id", "android" );
							if (titleId > 0) {
								TextView mTitle = (TextView) mDialog.findViewById(titleId);
								if (mTitle != null) {
									mTitle.setTypeface(montserrat);
								}
							}

							Button button1 = (Button) mDialog.findViewById(android.R.id.button1);
							Button button2 = (Button) mDialog.findViewById(android.R.id.button2);
							FontsUtils.TypeFace(button1, getAssets());
							FontsUtils.TypeFace(button2, getAssets());

							TextView mMessage = (TextView) mDialog.findViewById(android.R.id.message);
							mMessage.setTextSize(15);
							FontsUtils.TypeFace(mMessage, getAssets());

						}else if(changeCount == 1){

							AlertDialog.Builder mBuilder = new AlertDialog.Builder(SetPhoneNumber.this);
							mBuilder.setTitle("Error");
							mBuilder.setMessage("You have already used this form to change your phone number on picswitch.  Please contact numbers@picswitchapp.com if you need to change your number again.");
							mBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									dialog.cancel();
								}
							});
							AlertDialog mDialog = mBuilder.create();
							mDialog.show();
							int titleId = getResources().getIdentifier( "alertTitle", "id", "android" );
							if (titleId > 0) {
								TextView mTitle = (TextView) mDialog.findViewById(titleId);
								if (mTitle != null) {
									mTitle.setTypeface(montserrat);
								}
							}

							Button button1 = (Button) mDialog.findViewById(android.R.id.button1);
							FontsUtils.TypeFace(button1, getAssets());

							TextView mMessage = (TextView) mDialog.findViewById(android.R.id.message);
							mMessage.setTextSize(15);
							FontsUtils.TypeFace(mMessage, getAssets());
						}
					}else{
						Toast.makeText(SetPhoneNumber.this, "Please enter a valid phone number", Toast.LENGTH_LONG).show();
					}
				}

			}
		});
	}

	public void selectCountry(){
		String[] recourseList=SetPhoneNumber.this.getResources().getStringArray(R.array.CountryCodes);
		final TextView countryCodeTextView1 = (TextView) findViewById(R.id.pn_country_code1);
		final TextView countryCodeTextView2 = (TextView) findViewById(R.id.pn_country_code2);
		FontsUtils.TypeFace(countryCodeTextView1, getAssets());
		FontsUtils.TypeFace(countryCodeTextView2, getAssets());
		/**alert dialog to prompt user to enter phone number if they haven't yet*/
		Context context = getBaseContext();
		LinearLayout layout = new LinearLayout(context);
		layout.setOrientation(LinearLayout.VERTICAL);


		AlertDialog.Builder dialog = new AlertDialog.Builder(this);
		dialog.setTitle("Select your country");
		ListView countryList = new ListView(this);
		countryList.setAdapter(new CountryListAdapter(this, recourseList));

		layout.addView(countryList);
		dialog.setView(layout);

		final AlertDialog mDialog = dialog.create();
		mDialog.show();

		int titleId = getResources().getIdentifier( "alertTitle", "id", "android" );
		if (titleId > 0) {
			TextView mTitle = (TextView) mDialog.findViewById(titleId);
			if (mTitle != null) {
				mTitle.setTypeface(montserrat);
				mTitle.setGravity(View.TEXT_ALIGNMENT_CENTER);
			}
		}

		countryList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				String c =  (String) parent.getItemAtPosition(position);
				String countryCode = c.substring(0, c.indexOf(",")).replace("", "");
				String isoCode = c.substring(c.indexOf(",")+1);
				System.out.println("this: " + parent.getItemIdAtPosition(position));
				countryCodeTextView1.setText("+" + countryCode);
				countryCodeTextView2.setText("+" + countryCode);
				/**Get country name*/
				Locale loc = new Locale("",isoCode);
				selectCountry.setText(loc.getDisplayCountry());
				mDialog.dismiss();

			}
		});
		/**End of phone number alert dialog*/
	}
}
