package com.picswitch;

import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.widget.TextView;

public class FontsUtils {
	
	public static Typeface fontsStyle;
	
	public static void TypeFace(TextView tv, AssetManager asm){
		fontsStyle = Typeface.createFromAsset(asm, "VarelaRound.ttf");
		tv.setTypeface(fontsStyle);
	}
}
