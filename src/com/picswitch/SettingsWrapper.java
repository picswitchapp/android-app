package com.picswitch;

public class SettingsWrapper implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	private boolean sound;
	private boolean savePhotos;

	public SettingsWrapper(boolean sound, boolean savePhotos){
		this.sound = sound;
		this.savePhotos = savePhotos;
	}

	public boolean getSound(){
		return sound;
	}

	public boolean getSavePhotos(){
		return savePhotos;
	}
}