package com.picswitch;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.Typeface;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseCrashReporting;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.picswitch.AdditionalPuzzlesOpenImage.PlaceholderFragment;

public class AdditionalPuzzlesViewImage extends ActionBarActivity{
	ActionBar actionBar;
	TextView timerTextView;  /**Set up for the timer*/
	RelativeLayout rLayout;
	ArrayList<Integer> images = new ArrayList<Integer>(Arrays.asList(0,1,2,3,4,5,6,7,8));
	View.OnTouchListener onTouchListener;
	SurfaceHolder s;
	int chunkNumbers = 9, switches;
	double timeTaken;
	ImageView placeholder;
	private String firstCaption, secondCaption, puzzleClass = "";
	protected List<ParseObject> additionalPuzzles;
	public int puzzleCounter; //puzzleCounter keeps track of the puzzle the user is to solve

	int currentInt;
	String tempSolved;
	String tempSkipped;
	String currCategoryTitle;
	String[] tempData;

	SoundPool sounds;
	int nextID;

	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi") @Override
	protected void onCreate(Bundle savedInstanceState) {

		getWindow().requestFeature(Window.FEATURE_ACTION_BAR);

		super.onCreate(savedInstanceState);
		getActionBar().hide();
		getWindow().setFormat(PixelFormat.RGBA_8888);

		setContentView(R.layout.additional_puzzles_view_image);

		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
			window.setStatusBarColor(Color.rgb(32, 99, 99));
		}
		Bundle b=this.getIntent().getExtras();
		String[] additionalData=b.getStringArray(ParseConstants.KEY_ADDITIONAL_DATA);
		firstCaption = additionalData[0];
		secondCaption = additionalData[1];
		timeTaken = Double.parseDouble(additionalData[2]);
		switches = Integer.parseInt(additionalData[3]);
		puzzleClass = additionalData[4];
		puzzleCounter = Integer.parseInt(additionalData[5]);
		tempSkipped = additionalData[6];
		currCategoryTitle = additionalData[7];

		TextView firstCap = (TextView) findViewById(R.id.firstCaption);
		TextView secondCap = (TextView) findViewById(R.id.secondCaption);
		FontsUtils.TypeFace(firstCap, getAssets());
		FontsUtils.TypeFace(secondCap, getAssets());
		
		System.out.println("after call: " + firstCaption + " ..... " + secondCaption);

		if(!(firstCaption == null) && firstCaption.length() > 0){
			firstCap.setText(Html.fromHtml(firstCaption));
		}

		if(!(firstCaption == null) && secondCaption.length() > 0){
			secondCap.setText(Html.fromHtml(secondCaption));
		}

		tempData = getIntent().getStringArrayExtra(ParseConstants.KEY_TEMP_DATA);
		currentInt = getIntent().getExtras().getInt(ParseConstants.KEY_CURRENT_INT);

		setUpCaptions();
		actionBar = getActionBar(); //Set up action bar
		//setupActionBar();
		buttonsHelper(); //handles buttons
		//titleBarHelper(savedInstanceState); //handles title bar
		setUpImageView();

		//set up sounds
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){

			AudioAttributes mAttributes = new AudioAttributes.Builder()
			.setUsage(AudioAttributes.USAGE_GAME)
			.setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
			.build();
			sounds = new SoundPool.Builder().setAudioAttributes(mAttributes).build();

		}else{
			sounds = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
		}

		nextID      = sounds.load(AdditionalPuzzlesViewImage.this, R.raw.next_puzzle, 1);
	}

	public void setUpImageView(){
		ImageView additionalView = (ImageView)findViewById(R.id.additional_view);
		Display  disp = getWindowManager().getDefaultDisplay();
		int width = disp.getWidth();
		additionalView.getLayoutParams().height = width;
		additionalView.getLayoutParams().width = width;

		Bitmap mBitmap = null;
		try {
			mBitmap = BitmapFactory.decodeStream(AdditionalPuzzlesViewImage.this.openFileInput(ParseConstants.KEY_ADDITIONAL_PUZZLE_TEMP));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		additionalView.setImageBitmap(mBitmap);
	}

	/**Set up first and second captions*/
	@SuppressLint("NewApi")
	public void setUpCaptions(){
		TextView switchesView = (TextView) findViewById(R.id.switches);
		switchesView.setText(switches + " switches");
		TextView timerView = (TextView) findViewById(R.id.time_taken);
		timerView.setText(timeTaken + " seconds");
		Typeface montSerrat = Typeface.createFromAsset(getAssets(),"Montserrat-Regular.ttf");
		TextView title = (TextView) findViewById(R.id.add_view_image_title_text);
		title.setText(currCategoryTitle);
		title.setTypeface(montSerrat);
	}

	/**Sets up a custom actionbar*/
	@SuppressLint("NewApi")
	private void setupActionBar(){
		final ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(
				R.layout.solve_screen,
				null);
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setCustomView(actionBarLayout);
	}


	/**Helper for buttons*/
	@SuppressLint("NewApi")
	public void buttonsHelper(){
		TextView nextPicswitchButton = (TextView) findViewById(R.id.next_picswitch);
		nextPicswitchButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {

				if(TabActivity.SOUND){
					sounds.play(nextID, 1, 1, 0, 0, 1);
				}

				Intent intent = new Intent(getBaseContext(), AdditionalPuzzlesOpenImage.class);
				//intent.putExtra(ParseConstants.KEY_PUZZLE_CLASS, puzzleClass);
				Bundle bundle = new Bundle();
				bundle.putStringArray(ParseConstants.KEY_ADDITIONAL_DATA, tempData);

				intent.putExtra(ParseConstants.KEY_CURRENT_INT, currentInt);
				intent.putExtra(ParseConstants.KEY_PUZZLE_COUNTER, puzzleCounter);
				intent.putExtra(ParseConstants.KEY_TEMP_SOLVED, tempSkipped);
				//intent.putExtra(ParseConstants.KEY_CATEGORY_TITLE, currCategoryTitle);
				intent.putExtras(bundle);
				startActivity(intent);
				finish();
			}
		});
		ImageView backButton = (ImageView) findViewById(R.id.add_view_image_back_button);
		backButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
	}

	/**title bar set up helper*/
	public void titleBarHelper(Bundle savedInstanceState){
		LinearLayout titleBar = (LinearLayout) findViewById(R.id.solve_screen_layout);
		Display  disp = getWindowManager().getDefaultDisplay();
		int width = disp.getWidth();
		LayoutParams paramz = titleBar.getLayoutParams();
		paramz.width = LayoutParams.FILL_PARENT;
		titleBar.setLayoutParams(paramz);
		//Timer code
		timerTextView = (TextView) findViewById(R.id.timer);
		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
			.add(R.id.container, new PlaceholderFragment()).commit();
		}
	}

	/***Selects images from the class specified in the parameters
	 * queries to see the images the user hasn't solved yet
	 * starts the next intent based on the query*/
	public void AdditionalPuzzlesSelector(String pClass){
		ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(pClass);
		query.whereNotEqualTo(ParseConstants.KEY_COMPLETED_USERS, 
				ParseUser.getCurrentUser().getObjectId());
		query.addAscendingOrder(ParseConstants.KEY_CREATED_AT);
		query.findInBackground(new FindCallback<ParseObject>(){
			@Override
			public void done(List<ParseObject> messages, ParseException e) {
				if(e==null){
					//Messages are available
					additionalPuzzles = messages;
					ParseObject message = additionalPuzzles.get(puzzleCounter);
					String messageID = message.getObjectId();
					String firstCaption = message.getString(ParseConstants.CAPTION_FIRST);
					String secondCaption = message.getString(ParseConstants.CAPTION_SECOND);

					ParseFile file = message.getParseFile(ParseConstants.KEY_PHOTO);
					Uri fileUri = Uri.parse(file.getUrl());
					Intent intent = new Intent(getBaseContext(), AdditionalPuzzlesOpenImage.class);
					intent.setData(fileUri);
					intent.putExtra(ParseConstants.KEY_PUZZLE_COUNTER, puzzleCounter);
					intent.putExtra(ParseConstants.KEY_MESSAGE_ID, messageID);
					intent.putExtra(ParseConstants.CAPTION_FIRST, firstCaption);
					intent.putExtra(ParseConstants.CAPTION_SECOND, secondCaption);
					intent.putExtra(ParseConstants.KEY_PUZZLE_CLASS, puzzleClass);
					startActivity(intent);
					finish();
				}

			}
		});
	}

}
