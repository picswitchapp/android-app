package com.picswitch;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.parse.ParseCrashReporting;
import com.viewpagerindicator.CirclePageIndicator;

public class TutorialActivity extends FragmentActivity {
	
	public static ViewPager tutorialPager;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
		getActionBar().hide();
		setContentView(R.layout.activity_tutorial);
		
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
			window.setStatusBarColor(Color.rgb(32, 99, 99));
		}
		final Typeface montserrat = Typeface.createFromAsset(getAssets(), "Montserrat-Regular.ttf");
		
		TextView skipButton = (TextView) findViewById(R.id.skip_tutorial_button);
		FontsUtils.TypeFace(skipButton, getAssets());
		skipButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				AlertDialog.Builder mBuilder = new AlertDialog.Builder(TutorialActivity.this);
				mBuilder.setTitle("Confirm");
				mBuilder.setMessage("Are you sure you want to skip the tutorial?");
				mBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
						SharedPreferences prefs = getSharedPreferences("com.picswitch", Context.MODE_PRIVATE);
						prefs.edit().putBoolean("tutorialSeen", true).apply();
						Intent tabIntent = new Intent(TutorialActivity.this, TabActivity.class);
						startActivity(tabIntent);
						finish();
					}
				});
				mBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
				AlertDialog mDialog = mBuilder.create();
				mDialog.show();
				int titleId = getResources().getIdentifier( "alertTitle", "id", "android" );
				if (titleId > 0) {
					TextView mTitle = (TextView) mDialog.findViewById(titleId);
					if (mTitle != null) {
						mTitle.setTypeface(montserrat);
					}
				}
				
				Button button1 = (Button) mDialog.findViewById(android.R.id.button1);
				Button button2 = (Button) mDialog.findViewById(android.R.id.button2);
				FontsUtils.TypeFace(button1, getAssets());
				FontsUtils.TypeFace(button2, getAssets());

				TextView mMessage = (TextView) mDialog.findViewById(android.R.id.message);
				mMessage.setTextSize(15);
				FontsUtils.TypeFace(mMessage, getAssets());
			}
		});
		
		tutorialPager = (ViewPager) findViewById(R.id.tutorial_pager);
		tutorialPager.setAdapter(new TutorialPagerAdapter(getSupportFragmentManager()));
		
		CirclePageIndicator titleIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
		titleIndicator.setViewPager(tutorialPager);
	}
}
