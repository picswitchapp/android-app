package com.picswitch;

import java.text.DecimalFormat;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseCrashReporting;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;


public class Statistics extends Activity {
	ActionBar actionBar;

	@SuppressLint("NewApi")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getActionBar().hide();
		setContentView(R.layout.activity_statistics);

		actionBar = getActionBar(); //initialize the actionbar

		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
			window.setStatusBarColor(Color.rgb(32, 99, 99));
		}
		setUpActionBar();

		new FriendsQueryInBackground().execute();

		ImageView backButton = (ImageView) findViewById(R.id.stats_back_button);
		backButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();

			}
		});

		Typeface montSerrat = Typeface.createFromAsset(getAssets(), "Montserrat-Regular.ttf");

		TextView titleText = (TextView) findViewById(R.id.stats_title_text);
		titleText.setTypeface(montSerrat);
		TextView howDoYouLabel = (TextView) findViewById(R.id.stack_up);
		howDoYouLabel.setTypeface(montSerrat);
		TextView numbersDontLabel = (TextView) findViewById(R.id.numbers_dont_lie);
		numbersDontLabel.setTypeface(montSerrat);

		TextView weeklyLabel = (TextView) findViewById(R.id.weekly_ranking);
		FontsUtils.TypeFace(weeklyLabel, getAssets());
		TextView friendChallenges = (TextView) findViewById(R.id.puzzles_solved);
		FontsUtils.TypeFace(friendChallenges, getAssets());
		TextView featuredPuzzlesLabel = (TextView) findViewById(R.id.featured_puzzles);
		FontsUtils.TypeFace(featuredPuzzlesLabel, getAssets());
		TextView addPuzzlesLabel = (TextView) findViewById(R.id.additional_puzzles);
		FontsUtils.TypeFace(addPuzzlesLabel, getAssets());
		TextView myNumber = (TextView) findViewById(R.id.my_number);
		FontsUtils.TypeFace(myNumber, getAssets());

	}

	@Override
	public void onPause(){
		super.onPause();
		//Statistics.this.finish();
	}

	@Override
	public void onResume(){
		super.onResume();
	}

	/**Actionbar setUp helper*/
	public void setUpActionBar(){
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayShowCustomEnabled(true);

	}

	public void getTopFriends(List<ParseObject> friends){
		int one=0, two=0, three=0;
		String first="", second="", third=""; //names for the first three friends
		int points = 0;
		for(int i=0; i<friends.size(); i++){
			ParseObject stats = (ParseObject) friends.get(i).get("stats");
			try{
				points = (Integer) stats.get("points");
			}catch(Exception e){
				e.printStackTrace();
				//System.out.println("friend error is with obj id: " + friends.get(i).getObjectId());
				points = 0;
			}

			System.out.println("THis is the size: " + friends.size() + " and " + stats.get("points"));

			if(friends.size()>0){ //user has friends
				/**Ranking the best three friends*/
				if(points>one){ //user has more points than friend at position 1
					three = two;
					third = second;
					two = one;
					second = first;
					one = points;
					first = friends.get(i).get("firstName") + " " + friends.get(i).get("lastName");
				}
				else if(points>two && friends.size()>1){
					three = two;
					third = second;
					two = points;
					second = friends.get(i).get("firstName") + " " + friends.get(i).get("lastName");
				}
				else if(points>three && friends.size()>2){
					three = points;
					third = friends.get(i).get("firstName") + " " + friends.get(i).get("lastName");
				}
			}

		}

		setRanking(first, one, second, two, third, three, friends.size());

	}


	public void setRanking(String first, int one, String second, int two,
			String third, int three, int numberOfFriends){

		TextView friendOne = (TextView) findViewById(R.id.friend_one);
		TextView friendTwo = (TextView) findViewById(R.id.friend_two);
		TextView friendThree = (TextView) findViewById(R.id.friend_three);

		FontsUtils.TypeFace(friendOne, getAssets());
		FontsUtils.TypeFace(friendTwo, getAssets());
		FontsUtils.TypeFace(friendThree, getAssets());


		/*Set the points for the current user's friends depending on how many friends the user has */
		if(numberOfFriends>0){
			friendOne.setText(first + " - " + one + " pts");
		}
		if(numberOfFriends>1){
			friendTwo.setText(second + " - " + two+ " pts");
		}
		if(numberOfFriends>2){
			friendThree.setText(third + " - " + three+ " pts");
		}
	}

	public void setCurrentUserDetails(int points, String name){
		TextView myPosition = (TextView) findViewById(R.id.my_position);
		FontsUtils.TypeFace(myPosition, getAssets());
		myPosition.setText(points + " pts"); //set points for current user
	}


	public void setUpGraphs(ParseObject stats){
		int noLimit;
		try{
			noLimit = (Integer) stats.getInt("noLimit");
		}catch(Exception e){
			noLimit = 0;
			e.printStackTrace();
		}

		int easy;
		try{
			easy = (Integer) stats.getInt("easy");
		}catch(Exception e){
			easy = 0;
			e.printStackTrace();
		}

		int normal;
		try{
			normal = (Integer) stats.getInt("normal");
		}catch(Exception e){
			normal = 0;
			e.printStackTrace();
		}

		int hard;
		try{
			hard = (Integer) stats.getInt("hard");
		}catch(Exception e){
			hard = 0;
			e.printStackTrace();
		}

		int veryHard;
		try{
			veryHard = (Integer) stats.getInt("veryHard");
		}catch(Exception e){
			veryHard = 0;
			e.printStackTrace();
		}

		TextView noLimitGraph = (TextView) findViewById(R.id.no_limit_graph);
		TextView easyGraph = (TextView) findViewById(R.id.easy_graph);
		TextView normalGraph = (TextView) findViewById(R.id.normal_graph);
		TextView hardGraph = (TextView) findViewById(R.id.hard_graph);
		TextView veryHardGraph = (TextView) findViewById(R.id.very_hard_graph);

		FontsUtils.TypeFace(noLimitGraph, getAssets());
		FontsUtils.TypeFace(hardGraph, getAssets());
		FontsUtils.TypeFace(veryHardGraph, getAssets());
		FontsUtils.TypeFace(normalGraph, getAssets());
		FontsUtils.TypeFace(easyGraph, getAssets());

		int maxPoints = Math.max( noLimit, Math.max( easy, 
				Math.max( normal, Math.max( hard,veryHard ) ) ) );
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		float widthOfGraphs = (float) (size.x/2.0); //size.x is the width
		noLimitGraph.setWidth((int) ((noLimit*widthOfGraphs/maxPoints)));
		easyGraph.setWidth((int) ((easy*widthOfGraphs/maxPoints)));
		normalGraph.setWidth((int) ((normal*widthOfGraphs/maxPoints)));
		hardGraph.setWidth((int) ((hard*widthOfGraphs/maxPoints)));
		veryHardGraph.setWidth((int) ((veryHard*widthOfGraphs/maxPoints)));

		//set up graph numbers
		TextView noLimitNumber = (TextView) findViewById(R.id.no_limit_number);
		TextView easyNumber = (TextView) findViewById(R.id.easy_number);
		TextView normalNumber = (TextView) findViewById(R.id.normal_number);
		TextView hardNumber = (TextView) findViewById(R.id.hard_number);
		TextView veryHardNumber = (TextView) findViewById(R.id.very_hard_number);
		TextView totalpointsFriendChallenges = (TextView) findViewById(R.id.total_points);

		FontsUtils.TypeFace(noLimitNumber, getAssets());
		FontsUtils.TypeFace(easyNumber, getAssets());
		FontsUtils.TypeFace(normalNumber, getAssets());
		FontsUtils.TypeFace(hardNumber, getAssets());
		FontsUtils.TypeFace(veryHardNumber, getAssets());
		FontsUtils.TypeFace(totalpointsFriendChallenges, getAssets());

		totalpointsFriendChallenges.setText(noLimit+easy*2+normal*3+hard*4+veryHard*5 + " pts");



		noLimitNumber.setText(""+noLimit);
		easyNumber.setText(""+easy);
		normalNumber.setText(""+normal);
		hardNumber.setText(""+ hard);
		veryHardNumber.setText(""+veryHard);

	}


	public void setUpButtons(){
		ImageView backButton = (ImageView) findViewById(R.id.stats_back_button);
		backButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();

			}
		});
	}

	public void setUpAdditional(ParseObject stats){



		//get the type doubles from the table
		int completed;
		try{
			completed = (Integer) stats.get("numberOfAdditional");
		}catch(Exception e){
			completed = 0;
		}

		Object switches = 0, time = 0;

		try{
			switches = (Double) stats.get("averageSteps");
		}catch(Exception e){
			e.printStackTrace();
			try{
				switches = (Integer) stats.get("averageSteps");
			}catch(Exception e1){
				e1.printStackTrace();
				switches = 0;
			}	
		}

		try{
			time = (Double) stats.get("averageTime");
		}catch(Exception e){
			e.printStackTrace();
			try{
				time = (Integer) stats.get("averageTime");
			}catch(Exception e1){
				e1.printStackTrace();
				time = 0;
			}
		}

		DecimalFormat twoDecimalPlaces = new DecimalFormat("#.##");

		TextView numberCompleted = (TextView) findViewById(R.id.number_completed_number);
		TextView averageSwitches = (TextView) findViewById(R.id.average_switches_number);
		TextView averageTime = (TextView) findViewById(R.id.average_time_number);
		TextView additionalPoints = (TextView) findViewById(R.id.additional_total_number);

		FontsUtils.TypeFace(numberCompleted, getAssets());
		FontsUtils.TypeFace(averageSwitches, getAssets());
		FontsUtils.TypeFace(averageTime, getAssets());
		FontsUtils.TypeFace(additionalPoints, getAssets());

		additionalPoints.setText(completed + " pts.");
		numberCompleted.setText(""+completed);
		averageSwitches.setText(""+twoDecimalPlaces.format(switches));
		averageTime.setText(""+twoDecimalPlaces.format(time));

	}

	public void setUpFeatured(ParseObject stats){
		DecimalFormat twoDecimalPlaces = new DecimalFormat("#.00");

		Object switches = null;

		int completed = (Integer) stats.get("numberOfFeatured");
		try{
			switches = (Double) stats.get("featuredAverageSteps");
		}catch(Exception e){
			try{
				switches = (Integer) stats.get("featuredAverageSteps");
			}catch(Exception e1){
				e1.printStackTrace();
				switches = 0;
			}
		}

		Object time = null;
		try{
			time = (Double) stats.get("featuredAverageTime");
		}catch(Exception e){
			try{
				time = (Integer) stats.get("featuredAverageTime");
			}catch(Exception e1){
				time = 0;
			}
		}

		TextView numberCompleted = (TextView) findViewById(R.id.featured_number_completed_number);
		TextView averageSwitches = (TextView) findViewById(R.id.featured_average_switches_number);
		TextView averageTime = (TextView) findViewById(R.id.featured_average_time_number);
		TextView featuredPoints = (TextView) findViewById(R.id.featured_total_number);

		FontsUtils.TypeFace(numberCompleted, getAssets());
		FontsUtils.TypeFace(averageSwitches, getAssets());
		FontsUtils.TypeFace(averageTime, getAssets());
		FontsUtils.TypeFace(featuredPoints, getAssets());

		featuredPoints.setText(completed*5 + " pts.");
		numberCompleted.setText(""+completed);
		averageSwitches.setText(""+twoDecimalPlaces.format(switches));
		averageTime.setText(""+twoDecimalPlaces.format(time));
	}

	public class FriendsQueryInBackground extends AsyncTask<Void, Void, Void>{

		private boolean noFriends = false;

		@Override
		protected void onPreExecute(){
			//Toast.makeText(Statistics.this, "starting stats friend query", Toast.LENGTH_SHORT).show();
			System.out.println("first query starting stats");
		}

		@Override
		protected Void doInBackground(Void... params) {
			ParseObject currentUser = ParseUser.getCurrentUser();
			ParseRelation<ParseObject> relation = currentUser.getRelation("friends");

			ParseQuery<ParseObject> friendsQuery = relation.getQuery();
			friendsQuery.include("stats");
			friendsQuery.findInBackground(new FindCallback<ParseObject>() {

				@Override
				public void done(List<ParseObject> friends, ParseException e) {
					System.out.println("first query done stats");
					if(e==null){
						new FindCurrentUserDetails().execute(friends);
						if(friends.size()>0){
							//queryCurrentUserDetails(friends);
							getTopFriends(friends);
						}
						else{
							noFriends = true;
						}
					}else{
						e.printStackTrace();
					}
				}
			});

			return null;
		}

		@Override
		protected void onPostExecute(Void param){
			if(noFriends){
				Statistics.this.setContentView(R.layout.activity_statistics_new_user);
				setUpButtons();
			}
			System.out.println("first query done stats");
		}

	}

	public class FindCurrentUserDetails extends AsyncTask<List<ParseObject>, Void, Void>{

		@Override
		protected void onPreExecute(){
			System.out.println("second query begin stats");
		}

		@Override
		protected Void doInBackground(List<ParseObject>... params) {
			ParseQuery<ParseObject> query = ParseQuery.getQuery("Statistics");
			query.whereEqualTo("userPointer", ParseUser.getCurrentUser());
			query.include("userPointer");
			query.getFirstInBackground(new GetCallback<ParseObject>() {
				@Override
				public void done(ParseObject stats, ParseException e) {
					System.out.println("second query done stats");
					if(e==null){
						ParseObject currentUser = (ParseObject) ParseUser.getCurrentUser();
						int points;
						try{
							points = (Integer) stats.getInt("points");
						}catch(Exception e1){
							e1.printStackTrace();
							points = 0;
						}

						String name = currentUser.getString("firstName") + " " + currentUser.getString("lastName");

						System.out.println("points: " + points);

						setCurrentUserDetails(points, name);

						setUpGraphs(stats);
						setUpAdditional(stats);
						setUpFeatured(stats);
					}else{
						e.printStackTrace();
					}
				}
			});

			return null;
		}

		@Override
		protected void onPostExecute(Void param){
			System.out.println("second query done stats");
			if(NewChallengesTab.mStatsDialog != null){
				if(NewChallengesTab.mStatsDialog.isShowing()){
					NewChallengesTab.mStatsDialog.cancel();
				}
			}
		}

	}

}
