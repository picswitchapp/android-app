package com.picswitch;

import java.util.ArrayList;
import java.util.List;

import com.parse.ParseObject;

import android.os.Parcel;
import android.os.Parcelable;


public class AdditionalHelper implements Parcelable {
    String name;
    int yearsOfExperience;
    List<Puzzle> skillSet;
    float favoriteFloat;
 
    AdditionalHelper(Parcel in) {
        this.skillSet = new ArrayList<Puzzle>();
        in.readTypedList(skillSet, Puzzle.CREATOR);
    }
 
   public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeInt(yearsOfExperience);
        dest.writeTypedList(skillSet);
        dest.writeFloat(favoriteFloat);
    }
 
   public int describeContents() {
        return 0;
    }
 
 
    static final Parcelable.Creator<AdditionalHelper> CREATOR
            = new Parcelable.Creator<AdditionalHelper>() {
 
    	public AdditionalHelper createFromParcel(Parcel in) {
            return new AdditionalHelper(in);
        }
 
    	public AdditionalHelper[] newArray(int size) {
            return new AdditionalHelper[size];
        }
    };
 
    static class Puzzle extends ParseObject implements Parcelable {
        String name;
        boolean programmingRelated;
        ParseObject message;
        private static ClassLoader mClassLoader;
 
        Puzzle(Parcel in) {
            message = (ParseObject) in.readValue(Puzzle.mClassLoader);
        }
 
        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(message);
        }
 
        static final Parcelable.Creator<Puzzle> CREATOR
            = new Parcelable.Creator<Puzzle>() {
 
        	public  Puzzle createFromParcel(Parcel in) {
                return new Puzzle(in);
            }
 
        	public  Puzzle[] newArray(int size) {
                return new Puzzle[size];
            }
        };
 
        @Override
        public  int describeContents() {
            return 0;
        }
    }
}