package com.picswitch;

import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

public class AddFriendsUsername extends Activity {

	Typeface montSerrat;
	Context mContext;

	EditText usernameET;
	String enteredUsername;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_add_friends_username);

		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
			window.setStatusBarColor(Color.rgb(32, 99, 99));
		}

		mContext = AddFriendsUsername.this;

		montSerrat = Typeface.createFromAsset(mContext.getAssets(),"Montserrat-Regular.ttf");

		usernameET = (EditText) findViewById(R.id.enter_search_username);

		FontsUtils.TypeFace(usernameET, getAssets());

		//setting montserrat
		TextView usernameTitle = (TextView) findViewById(R.id.title_text_add_friends_username);
		usernameTitle.setTypeface(montSerrat);
		TextView usernameLabel = (TextView) findViewById(R.id.enter_search_username_label);
		usernameLabel.setTypeface(montSerrat);
		TextView searchButtonLabel = (TextView) findViewById(R.id.search_button_text);
		FontsUtils.TypeFace(searchButtonLabel, getAssets());

		//click listeners
		ImageView leftArrow = (ImageView) findViewById(R.id.left_arrow_add_friends_username);
		leftArrow.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				AddFriendsUsername.this.finish();
			}
		});

		RelativeLayout searchButton = (RelativeLayout) findViewById(R.id.search_username_button);
		searchButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(usernameET.getText().toString() != null){
					enteredUsername = usernameET.getText().toString().trim();
					//query for matches
					if(enteredUsername.length() > 0){
						new SearchUsernamesInBackground(enteredUsername).execute();
					}else{
						Toast.makeText(mContext, "Please enter a username to search", Toast.LENGTH_SHORT).show();
					}
				}else{
					Toast.makeText(mContext, "Please enter a username to search", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}

	public class SearchUsernamesInBackground extends AsyncTask<Void, Boolean, Void> {

		String searchTerm;
		ProgressDialog mDialog;
		ParseUser mUser;

		public SearchUsernamesInBackground(String searchFor) {
			searchTerm = searchFor;
			mDialog = new ProgressDialog(AddFriendsUsername.this);
			mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			mDialog.setMessage("Searching...");
		}

		@Override
		protected void onPreExecute(){
			mDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {

			ParseQuery<ParseUser> mQuery = new ParseQuery<ParseUser>(ParseConstants.CLASS_USER);
			mQuery.whereEqualTo("picswitchUsername", searchTerm);
			mQuery.findInBackground(new FindCallback<ParseUser>() {
				@Override
				public void done(List<ParseUser> users, ParseException e) {
					if(e == null){
						if(users.size() == 1){
							mUser = users.get(0);
							publishProgress(true);
						}else{
							publishProgress(false);
						}
					}else{
						publishProgress(false);
						e.printStackTrace();
					}
				}
			});

			return null;
		}

		@Override
		protected void onProgressUpdate(Boolean... returnStatus) {

			if(mDialog.isShowing()){
				mDialog.cancel();
			}

			if(returnStatus[0]){
				//user found, go on to profile type activity thing
				//Toast.makeText(AddFriendsUsername.this, "user found: " + searchTerm, Toast.LENGTH_LONG).show();

				Intent intent = new Intent(mContext, ProfilePageAdd.class);
				intent.putExtra(ParseConstants.KEY_OBJECT_ID, mUser.getObjectId());
				startActivity(intent);

			}else{
				//no user found
				Toast.makeText(AddFriendsUsername.this, "No user found with username " + searchTerm, Toast.LENGTH_SHORT).show();
			}
		};		
	}
}
