package com.picswitch;

import android.graphics.Bitmap;

import com.squareup.picasso.Transformation;

public class FeaturedBitmapTransform implements Transformation{

	@Override
	public String key() {
		return "MessageThumbnails";
	}

	@Override
	public Bitmap transform(Bitmap source) {
	    int x = (int) ((source.getWidth()) /2);
	    int y = (int) ((source.getHeight() ) / 2);
	    int width = (int)((source.getHeight())*0.8);
	    Bitmap result = Bitmap.createBitmap(source, x, y, x, y);
	    result = Bitmap.createScaledBitmap(result,width, width , true);
	    if (result != source) {
	      source.recycle();
	    }
	    return result;
	  }
}
