package com.picswitch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;

public class ProfilePageAdd extends Activity {

	String userObjectID;
	Typeface montSerrat;
	Context mContext;

	ImageView profileImage;
	TextView profileName;
	TextView profileUsername;

	ParseUser mUser;
	String name = null;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_profile_page_add);

		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
			window.setStatusBarColor(Color.rgb(32, 99, 99));
		}

		mContext = ProfilePageAdd.this;

		userObjectID = getIntent().getExtras().getString(ParseConstants.KEY_OBJECT_ID);

		montSerrat = Typeface.createFromAsset(mContext.getAssets(),"Montserrat-Regular.ttf");

		//set up views and fonts
		TextView titleText = (TextView) findViewById(R.id.title_text_add_profile);
		titleText.setTypeface(montSerrat);
		TextView profileLabel = (TextView) findViewById(R.id.add_profile_label);
		profileLabel.setTypeface(montSerrat);
		profileImage = (ImageView) findViewById(R.id.add_profile_image);
		profileName = (TextView) findViewById(R.id.add_profile_name);
		FontsUtils.TypeFace(profileName, getAssets());
		profileUsername = (TextView) findViewById(R.id.add_profile_username);
		FontsUtils.TypeFace(profileUsername, getAssets());
		TextView buttonText = (TextView) findViewById(R.id.send_request_add_profile_button_text);
		FontsUtils.TypeFace(buttonText, getAssets());

		//click listeners
		ImageView leftArrow = (ImageView) findViewById(R.id.left_arrow_add_profile);
		leftArrow.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				ProfilePageAdd.this.finish();
			}
		});

		RelativeLayout sendRequestButton = (RelativeLayout) findViewById(R.id.send_request_add_profile);
		sendRequestButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				createSendRequestAlert();
			}
		});

		RelativeLayout profileLayout = (RelativeLayout) findViewById(R.id.add_profile_layout);
		profileLayout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				createSendRequestAlert();
			}
		});

		//get user details
		new FindUserDetailsInBackground(userObjectID).execute();
	}
	
	@Override
	public void onBackPressed(){
		ProfilePageAdd.this.finish();
	}

	public void createSendRequestAlert(){

		AlertDialog.Builder mBuilder = new AlertDialog.Builder(mContext);
		mBuilder.setTitle("Confirm");
		if(name == null){
			mBuilder.setMessage("Send friend request to this user?");
		}else{
			mBuilder.setMessage("Send friend request to " + name + "?");
		}
		mBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(final DialogInterface dialog, int which) {
				//send friend request
				String objectIdSend = mUser.getObjectId();
				if(!objectIdSend.equals(ParseUser.getCurrentUser().getObjectId())){
					List<String> sendRequestToList = new ArrayList<String>();
					sendRequestToList.add(objectIdSend);
					HashMap<String, List<String>> params = new HashMap<String, List<String>>();
					params.put("recipients", sendRequestToList);
					ParseCloud.callFunctionInBackground("FRRequestSend", params, new FunctionCallback<String>(){
						@Override
						public void done(String str, ParseException e) {
							if(e == null){
								Toast.makeText(getBaseContext(), "Request Sent!", Toast.LENGTH_SHORT).show();
								dialog.cancel();
								finish();
							}else{
								e.printStackTrace();
							}
						}
					});
				}else{
					Toast.makeText(mContext, "Cannot send friend request to yourself", Toast.LENGTH_SHORT).show();
					dialog.cancel();
				}
			}
		});
		mBuilder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
		AlertDialog mDialog = mBuilder.create();
		mDialog.show();

	}

	public class FindUserDetailsInBackground extends AsyncTask<Void, Boolean, Void>{

		String userID;
		ProgressDialog mDialog;

		public FindUserDetailsInBackground(String id){
			userID = id;
			mDialog = new ProgressDialog(mContext);
			mDialog.setMessage("Loading profile...");
			mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		}

		@Override
		protected void onPreExecute(){
			mDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {

			ParseQuery<ParseUser> mQuery = new ParseQuery<ParseUser>(ParseConstants.CLASS_USER);
			mQuery.whereEqualTo(ParseConstants.KEY_OBJECT_ID, userID);
			mQuery.findInBackground(new FindCallback<ParseUser>() {
				@Override
				public void done(List<ParseUser> users, ParseException e) {
					if(e == null){
						if(users.size() == 1){
							mUser = users.get(0);
							publishProgress(true);
						}else{
							publishProgress(false);
						}
					}else{
						publishProgress(false);
						e.printStackTrace();
					}
				}
			});

			return null;
		}

		@Override
		protected void onProgressUpdate(Boolean... bools){

			if(mDialog.isShowing()){
				mDialog.cancel();
			}

			if(bools[0]){
				//update the ui with the user details

				//get details
				ParseFile mFile = null;
				try{
					mFile = mUser.getParseFile(ParseConstants.KEY_PROFILE_PICTURE);
				}catch(Exception e){
					//no photo
					e.printStackTrace();
				}

				String userFirstName = null;
				String userLastName = null;

				try{
					userFirstName = mUser.getString(ParseConstants.KEY_FIRST_NAME);
				}catch(Exception e){
					e.printStackTrace();
				}
				try{
					userLastName = mUser.getString(ParseConstants.KEY_LAST_NAME);
				}catch(Exception e){
					e.printStackTrace();
				}

				String picswitchUsername = null;

				try{
					picswitchUsername = mUser.getString(ParseConstants.KEY_PICSWITCH_USERNAME);
				}catch(Exception e){
					e.printStackTrace();
					System.out.println("error getting picswitch username");
				}

				//set text details
				if(userFirstName != null && userLastName != null){
					name = userFirstName + " " + userLastName;
					profileName.setText(name);
				}

				if(picswitchUsername != null){
					profileUsername.setText("Username: " + picswitchUsername);
				}

				//image
				if(mFile != null){
					Uri pictureUri = Uri.parse(mFile.getUrl());
					Picasso.with(mContext)
					.load(pictureUri)
					.placeholder(R.drawable.picasso_loading_animation)
					.into(profileImage);
				}else{
					profileImage.setImageDrawable(getResources().getDrawable(R.drawable.no_profile_image));
				}

			}else{
				//error of some sort
				Toast.makeText(mContext, "Error loading profile details", Toast.LENGTH_SHORT).show();
				ProfilePageAdd.this.finish();
			}
		}

	}
}
