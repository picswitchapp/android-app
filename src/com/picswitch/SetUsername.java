package com.picswitch;

import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

public class SetUsername extends Activity {

	Typeface montSerrat;
	Context mContext;

	EditText usernameField;
	String enteredUsername;

	ProgressDialog mDialog;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_set_username);

		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
			window.setStatusBarColor(Color.rgb(32, 99, 99));
		}

		mContext = SetUsername.this;
		montSerrat = Typeface.createFromAsset(mContext.getAssets(),"Montserrat-Regular.ttf");

		mDialog = new ProgressDialog(mContext);
		mDialog.setMessage("Checking availability...");
		mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

		//set up views and fonts
		TextView titleText = (TextView) findViewById(R.id.title_text_set_username);
		titleText.setTypeface(montSerrat);
		TextView usernameLabel = (TextView) findViewById(R.id.set_username_label);
		usernameLabel.setTypeface(montSerrat);
		usernameField = (EditText) findViewById(R.id.set_username_field);
		FontsUtils.TypeFace(usernameField, getAssets());
		TextView buttonText = (TextView) findViewById(R.id.set_username_button_text);
		FontsUtils.TypeFace(buttonText, getAssets());

		//click listeners
		ImageView leftArrow = (ImageView) findViewById(R.id.left_arrow_set_username);
		leftArrow.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				SetUsername.this.finish();
			}
		});

		//set username button
		RelativeLayout confirmButton = (RelativeLayout) findViewById(R.id.set_username_confirm_button);
		confirmButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				enteredUsername = usernameField.getText().toString().trim();
				System.out.println("entered username : '" + enteredUsername + "'");
				if(enteredUsername.length() >= 1){
					if(!enteredUsername.contains(" ")){
						//valid username
						mDialog.show();

						ParseQuery<ParseUser> mQuery = new ParseQuery<ParseUser>(ParseConstants.CLASS_USER);
						mQuery.whereEqualTo(ParseConstants.KEY_PICSWITCH_USERNAME, enteredUsername);
						mQuery.findInBackground(new FindCallback<ParseUser>() {
							@Override
							public void done(List<ParseUser> users, ParseException e) {
								if(e == null){
									if(users.size() == 0){
										//good username
										ParseUser.getCurrentUser().put(ParseConstants.KEY_PICSWITCH_USERNAME, enteredUsername);
										ParseUser.getCurrentUser().saveInBackground(new SaveCallback() {
											@Override
											public void done(ParseException e1) {
												if(mDialog.isShowing()){
													mDialog.cancel();
												}
												if(e1 == null){

													Toast.makeText(mContext, "Username successfully updated!", Toast.LENGTH_SHORT).show();
													SetUsername.this.finish();
												}else{
													e1.printStackTrace();
												}
											}
										});
									}else{
										if(mDialog.isShowing()){
											mDialog.cancel();
										}
										//username taken already
										Toast.makeText(mContext, "Username already taken", Toast.LENGTH_SHORT).show();
									}
								}else{
									if(mDialog.isShowing()){
										mDialog.cancel();
									}
									e.printStackTrace();
								}
							}
						});

					}else{
						//invalid username
						Toast.makeText(mContext, "Username cannot contain spaces", Toast.LENGTH_SHORT).show();
					}
				}else{
					Toast.makeText(mContext, "Please enter a username", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
}


