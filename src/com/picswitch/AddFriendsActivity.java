package com.picswitch;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.telephony.TelephonyManager;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseCrashReporting;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.SaveCallback;

public class AddFriendsActivity extends Activity {

	public static final String TAG = AddFriendsActivity.class.getSimpleName();

	List<String> allContactNames = new ArrayList<String>();
	List<String> allPhoneNumbers = new ArrayList<String>();

	HashMap<String, String> requestContacts = new HashMap<String, String>();
	HashMap<String, String> contacts = new HashMap<String, String>();

	Map<String, String> friends               = new HashMap<String, String>();
	Map<String, String> onPicswitchNotFriends = new HashMap<String, String>();

	LayoutInflater mInflater;

	Typeface montSerrat;
	TextView titleText;
	TextView requestLabel;
	TextView inviteLabel;

	ViewGroup onPicswitchContainer;
	ViewGroup notOnPicswitchContainer;

	//bottom bars:
	RelativeLayout bottomBars;
	LinearLayout sendRequestBar;
	LinearLayout inviteBar;

	//buttons
	TextView requestButton;
	TextView inviteButton;

	//check counters for bottom bar switching
	int requestCount = 0;
	int inviteCount = 0;

	//View to be forced unchecked
	List<View> requestChecked = new ArrayList<View>();
	List<View> inviteChecked  = new ArrayList<View>();

	//Objects to track users that are checked in each category
	//onPicswitch needs a List<String> of objectIds for users to send friend requests to
	List<String> sendRequestTo = new ArrayList<String>();
	List<String> invitePhoneNumbers = new ArrayList<String>();

	ViewGroup mLetterContainer;
	String currentLetter = null;

	Context mContext;

	ProgressDialog dialog;
	List<View> requestViews;
	List<View> inviteViews;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Typeface varella = Typeface.createFromAsset(getBaseContext().getAssets(), "VarelaRound.ttf");
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

		setContentView(R.layout.activity_add_friends);

		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
			window.setStatusBarColor(Color.rgb(32, 99, 99));
		}
		mContext = AddFriendsActivity.this;

		final SharedPreferences prefs = mContext.getSharedPreferences("com.picswitch", Context.MODE_PRIVATE);

		montSerrat = Typeface.createFromAsset(mContext.getAssets(),"Montserrat-Regular.ttf");
		//System.out.println("THIS THIS" + ParseUser.getCurrentUser().get("phoneNumber"));
		Context context = getBaseContext();
		LinearLayout layout = new LinearLayout(context);
		layout.setOrientation(LinearLayout.VERTICAL);


		//		final EditText phoneNumberFirstEntry = new EditText(context);
		//		phoneNumberFirstEntry.setTypeface(varella);
		//		phoneNumberFirstEntry.setHint("Phone Number");
		//		phoneNumberFirstEntry.setTextColor(getResources().getColorStateList(R.color.charcoal));
		//		phoneNumberFirstEntry.setHintTextColor(getResources().getColorStateList(R.color.com_parse_ui_parse_login_text_hint));
		//		phoneNumberFirstEntry.setInputType(InputType.TYPE_CLASS_PHONE);
		//		layout.addView(phoneNumberFirstEntry);
		//
		//		final EditText phoneNumberSecondEntry = new EditText(context);
		//		phoneNumberSecondEntry.setHint("Re-Enter Number");
		//		phoneNumberSecondEntry.setTypeface(varella);
		//		phoneNumberSecondEntry.setTextColor(getResources().getColorStateList(R.color.charcoal));
		//		phoneNumberSecondEntry.setHintTextColor(getResources().getColorStateList(R.color.com_parse_ui_parse_login_text_hint));
		//		phoneNumberSecondEntry.setInputType(InputType.TYPE_CLASS_PHONE);
		//		layout.addView(phoneNumberSecondEntry);

		String phoneNumOnParse;
		try{
			phoneNumOnParse = ParseUser.getCurrentUser().getString("phoneNumber");
		}catch(Exception e){
			phoneNumOnParse = null;
		}

		if(phoneNumOnParse != null){
			prefs.edit().putBoolean("noBefore", true).apply();
		}

		boolean cond = false;
		if(phoneNumOnParse == null){
			if(!prefs.getBoolean("noBefore", false)){
				cond = true;
			}else{
				cond = false;
			}

		}else if(phoneNumOnParse.equals("")){
			cond = true;
		}

		if(cond){

			/**alert dialog to prompt user to enter phone number if they haven't yet*/

			AlertDialog.Builder dialog = new AlertDialog.Builder(this);
			dialog.setTitle("Connect");
			dialog.setMessage("Enter phone number so that your friends can find you");
			dialog.setPositiveButton("CONTINUE", new OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					prefs.edit().putBoolean("noBefore", true).apply();

					Intent intent = new Intent(AddFriendsActivity.this, SetPhoneNumber.class);
					startActivity(intent);
					dialog.dismiss();
				}
			});
			dialog.setNegativeButton("NOT NOW", new OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {

					prefs.edit().putBoolean("noBefore", true).apply();

					Toast.makeText(AddFriendsActivity.this, "You can add a phone number anytime in settings", Toast.LENGTH_LONG).show();

					dialog.dismiss();

				}
			});

			AlertDialog mDialog = dialog.create();
			mDialog.show();
			int titleId = getResources().getIdentifier( "alertTitle", "id", "android" );
			if (titleId > 0) {
				TextView mTitle = (TextView) mDialog.findViewById(titleId);
				if (mTitle != null) {
					mTitle.setTypeface(montSerrat);
					mTitle.setGravity(View.TEXT_ALIGNMENT_CENTER);
				}
			}

			/**End of phone number alert dialog*/

		}




		titleText    = (TextView) findViewById(R.id.title_text_add_friends);
		requestLabel = (TextView) findViewById(R.id.on_picswitch_label);
		inviteLabel  = (TextView) findViewById(R.id.not_on_picswitch_label);
		titleText.setTypeface(montSerrat);
		requestLabel.setTypeface(montSerrat);
		inviteLabel.setTypeface(montSerrat);

		onPicswitchContainer = (ViewGroup) findViewById(R.id.on_picswitch_container);
		notOnPicswitchContainer = (ViewGroup) findViewById(R.id.not_on_picswitch_container);

		bottomBars = (RelativeLayout) findViewById(R.id.bottom_bars);
		sendRequestBar = (LinearLayout) findViewById(R.id.send_request_bar);
		inviteBar = (LinearLayout) findViewById(R.id.invite_to_picswitch_bar);

		requestButton = (TextView) findViewById(R.id.request_button);
		inviteButton = (TextView) findViewById(R.id.invite_button);

		mInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		ImageView left_arrow = (ImageView) findViewById(R.id.left_arrow_add_friends);
		left_arrow.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});

		requestButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//send request to a user
				List<String> sendRequestToObjIds = new ArrayList<String>();
				for(String number : sendRequestTo){
					sendRequestToObjIds.add(onPicswitchNotFriends.get(number));
					System.out.println("just added id: " + onPicswitchNotFriends.get(number));
				}
				HashMap<String, List<String>> objectIds = new HashMap<String, List<String>>();
				objectIds.put("recipients", sendRequestToObjIds);
				ParseCloud.callFunctionInBackground("FRRequestSend", objectIds, new FunctionCallback<String>() {
					@Override
					public void done(String object, ParseException e) {
						if(e == null){
							Toast.makeText(getBaseContext(), "Request Sent!", Toast.LENGTH_SHORT).show();
							finish();
						}else{
							e.printStackTrace();
						}
					}
				});
			}
		});

		inviteButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//invite people via text

				String[] numbersArray = new String[invitePhoneNumbers.size()];
				invitePhoneNumbers.toArray(numbersArray);

				StringBuilder destNumbers = new StringBuilder("sms:");
				for(int i = 0; i < numbersArray.length; i++){
					destNumbers.append(numbersArray[i]);
					if(!(i == numbersArray.length - 1)){
						destNumbers.append(", ");
					}
				}

				Intent smsIntent = new Intent(Intent.ACTION_VIEW);
				smsIntent.setType("vnd.android-dir/mms-sms");
				smsIntent.setData(Uri.parse(destNumbers.toString()));
				smsIntent.putExtra("sms_body", "Switch it up with the most competitive and fun way to share moments yet!  Download picswitch at http://smarturl.it/picswitch");
				startActivity(smsIntent);
			}
		});

		//set up textview for current user's number, with formatting
		TextView userPnLabel = (TextView) findViewById(R.id.user_pn_label);
		userPnLabel.setTypeface(montSerrat);

		TextView userPnDisplay = (TextView) findViewById(R.id.user_pn_display);
		FontsUtils.TypeFace(userPnDisplay, getAssets());

		String userPn = null;
		try{
			userPn = ParseUser.getCurrentUser().getString(ParseConstants.KEY_PHONE_NUMBER); //this will need formatting
		}catch(Exception e){
			e.printStackTrace();
		}

		System.out.println("userPn: " + userPn);
		if(userPn != null && userPn.length() > 1){
			userPnDisplay.setText(userPn);
		}else{
			userPnDisplay.setText("Click the gear to add your phone number");
		}

		TextView userAddPnIcon = (TextView) findViewById(R.id.user_add_pn_icon);
		userAddPnIcon.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(AddFriendsActivity.this, SetPhoneNumber.class);
				startActivity(intent);
			}
		});

		getAllContacts(this.getContentResolver());

		makeProgressDialog();
		new sortContactsInBack().execute();	
		
		
		//testing for bs number bs		
		PhoneNumberUtil util = PhoneNumberUtil.getInstance();
		PhoneNumber num = new PhoneNumber();
		String number = "6072801044";
		try {
			num = util.parse("+" + util.getCountryCodeForRegion(getUserCountry(AddFriendsActivity.this)) + number, "");
		} catch (NumberParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int mCountryCode = util.getCountryCodeForRegion(getUserCountry(AddFriendsActivity.this));
		System.out.println("country code test: " + mCountryCode);
		System.out.println("number parse test: " + util.format(num, PhoneNumberFormat.INTERNATIONAL).replaceAll("\\D+",""));
	}
	
	public static String getUserCountry(Context context) {
	    try {
	        final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
	        final String simCountry = tm.getSimCountryIso();
	        if (simCountry != null && simCountry.length() == 2) { // SIM country code is available
	            return simCountry.toUpperCase(Locale.US);
	        }
	        else if (tm.getPhoneType() != TelephonyManager.PHONE_TYPE_CDMA) { // device is not 3G (would be unreliable)
	            String networkCountry = tm.getNetworkCountryIso();
	            if (networkCountry != null && networkCountry.length() == 2) { // network country code is available
	                return networkCountry.toUpperCase(Locale.US);
	            }
	        }
	    }
	    catch (Exception e) { }
	    return null;
	}

	@Override
	public void onPause(){
		super.onPause();
		if(dialog.isShowing()){
			dialog.dismiss();
		}
	}

	/** Helper function to get all contacts from user's phone */
	public void getAllContacts(ContentResolver cr){
		Cursor phones = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null,null, null);
		while (phones.moveToNext())
		{
			int phoneType = phones.getInt(phones.getColumnIndex(Phone.TYPE));

			if(phoneType == Phone.TYPE_MOBILE){
				String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
				String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
						.replace("(", "").replace(")", "").replace("-", "").replace("+", "").trim();

				//				if(phoneNumber.length()==10){
				//					phoneNumber = "1" + phoneNumber;
				//				}
				/**International formatting*/
				PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
				PhoneNumber formattedPhoneNumber = null;
				/**checks for numbers that have been saved with their country codes*/
				try { 
					formattedPhoneNumber = phoneUtil.parse("+" + phoneNumber, "");


				} catch (NumberParseException e) {
					System.out.println("GAAS: " + phoneNumber);
					/**checks for numbers that have been saved without country code. 
					 *Appends the current user's country code to these numnbers*/
					try{
						formattedPhoneNumber = phoneUtil.parse("+" + phoneUtil.getCountryCodeForRegion(getUserCountry(AddFriendsActivity.this)) + phoneNumber, "");
					}catch(NumberParseException f){
						continue;
					}
					//System.err.println("NumberParseException was thrown: " + e.toString());

				}
				try{//format the phone number using international format
					//System.out.println("PHONENUMBER:" + phoneUtil.format(formattedPhoneNumber, PhoneNumberFormat.INTERNATIONAL));
					
					//System.out.println("PHONENUMBER2: " + phoneNumber);
					if(phoneUtil.isValidNumber(formattedPhoneNumber)){
						phoneNumber = phoneUtil.format(formattedPhoneNumber, PhoneNumberFormat.INTERNATIONAL);
						phoneNumber = phoneNumber.replaceAll("\\D+","");
					}else{
						formattedPhoneNumber = phoneUtil.parse("+" + phoneUtil.getCountryCodeForRegion(getUserCountry(AddFriendsActivity.this)) + phoneNumber, "");
						phoneNumber = phoneUtil.format(formattedPhoneNumber, PhoneNumberFormat.INTERNATIONAL);
						phoneNumber = phoneNumber.replaceAll("\\D+","");
					}
						
						
				}catch(Exception e){

					e.printStackTrace();
				}

				/**End of international formatting*/

				//contact has name number and phone number does not exists in list
				if ( (phoneNumber != null) && (name != null) && !(allContactNames.contains(name))){ 
					allContactNames.add(name.substring(0,1).toUpperCase() + name.substring(1));
					/*add code to handle number parsing here*/
					System.out.println("PHONENUMBER2: " + phoneNumber);
					allPhoneNumbers.add(phoneNumber);
					contacts.put(phoneNumber, name.substring(0,1).toUpperCase() + name.substring(1));
				}
			}
		}
		Collections.sort(allContactNames, String.CASE_INSENSITIVE_ORDER);
		phones.close();
		//System.out.println("contacts just after getAllContacts(): " + contacts.keySet().toString());
	}

	public int getCurrentUserCode(){
		PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
		try {
			// phone must begin with '+'
			PhoneNumber numberProto = phoneUtil.parse("+" + ParseUser.getCurrentUser().getString("phoneNumber"), "");
			int countryCode = numberProto.getCountryCode();
			System.out.println("country code: " + countryCode);
			return countryCode;
		} catch (NumberParseException e) {
			System.err.println("Current user country code error: " + e.toString());
		}
		return 0;
	}
	public void removeChecks(List<View> views){
		for(View view : views){
			CheckBox cb = (CheckBox) view.findViewById(R.id.check_box);
			cb.setChecked(false);
		}
	}

	public void makeProgressDialog(){
		dialog=new ProgressDialog(mContext);
		dialog.setMessage("Loading...");
		dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		dialog.setIndeterminate(true);
		dialog.show();
	}

	/** Background function to compare the phone numbers of contacts on user's
	 *  phone with all users on parse and sort the contacts into the correct 
	 *  list. 
	 *  Calls a cloud function to complete that task. */
	public class sortContactsInBack extends AsyncTask<Void,Void,Void> {

		@Override
		protected Void doInBackground(Void... params) {
			String[] check = new String[allPhoneNumbers.size()];
			check = allPhoneNumbers.toArray(check);
			HashMap<String, List<String>> numbersToParse = new HashMap<String, List<String>>();
			numbersToParse.put("numbers", allPhoneNumbers);
			ParseCloud.callFunctionInBackground("FRCheckContacts", numbersToParse, new FunctionCallback< Map<String, Map<String, String>> >() {
				public void done(Map< String, Map<String, String> > mapObject, ParseException e) {
					if (e == null){   
						friends = mapObject.get("friends");
						onPicswitchNotFriends = mapObject.get("others");

						//save the usually deleted ones

						for(String number : onPicswitchNotFriends.keySet()){
							requestContacts.put(number, contacts.get(number));
						}

						HashMap<String,String> notOnPicswitch = contacts;
						Set<String> restToAdd = notOnPicswitch.keySet();

						restToAdd.removeAll(onPicswitchNotFriends.keySet());
						restToAdd.removeAll(friends.keySet());

						PhoneNumberHolder holder = new PhoneNumberHolder(onPicswitchNotFriends.keySet(), restToAdd);
						new CreateViews().execute(holder);

					}else{
						Toast.makeText(getBaseContext(), "parse exception", Toast.LENGTH_LONG).show();
						e.printStackTrace();
					}
				}
			});
			return null;
		}

	}

	public class CreateViews extends AsyncTask<PhoneNumberHolder, Wrapper, Void> {

		@Override
		protected Void doInBackground(PhoneNumberHolder... params) {
			PhoneNumberHolder numbers = params[0];
			Set<String> inviteNumbers = numbers.inviteNumbers;
			Set<String> requestNumbers = numbers.requestNumbers;

			//System.out.println("size of request numbers =  " + requestNumbers.size());
			//System.out.println("size of invite numbers = " + inviteNumbers.size());

			//create the list of views
			List<ViewGroup> groups = new ArrayList<ViewGroup>();
			groups.add(onPicswitchContainer);
			groups.add(notOnPicswitchContainer);

			for(final ViewGroup mViewGroup : groups){

				HashMap<String, String> peopleToDisplay = new HashMap<String, String>();

				//System.out.println("contacts HashMap numbers inside CreateViews task: " + contacts.keySet().toString());

				if(mViewGroup == notOnPicswitchContainer){
					for(String phNumber : inviteNumbers){
						peopleToDisplay.put(contacts.get(phNumber), phNumber);
					}
					//System.out.println("HashMap size for invites: "+ peopleToDisplay.size());
				}else if(mViewGroup == onPicswitchContainer){
					//System.out.println("request for loop size: " + requestNumbers.size());
					for(String phNumber : requestNumbers){
						if(requestContacts.get(phNumber) != null){
							peopleToDisplay.put(requestContacts.get(phNumber), phNumber);
						}else{
							//System.out.println("request phone number not in contacts: " + phNumber);
						}
					}

					//System.out.println("request for loop ran: " + i + " times.");
				}

				//				if(mViewGroup == onPicswitchContainer){
				//					System.out.println("HashMap size for requests: "+ peopleToDisplay.size());
				//				}

				//System.out.println("number of people to display in current group: " + peopleToDisplay.size());

				List<String> sortedNamesToAdd = new ArrayList<String>(peopleToDisplay.keySet());
				Collections.sort(sortedNamesToAdd, String.CASE_INSENSITIVE_ORDER);

				//				if(mViewGroup == onPicswitchContainer){
				//					System.out.println("sortedNamesToAdd size for requests: "+ sortedNamesToAdd.size());
				//				}

				int letterCount = 0;
				int contactCount = 0;
				int totalContactCount = 0;

				boolean moreNamesToAdd = false;

				//handle the zero case
				if(sortedNamesToAdd.size() > 0){
					moreNamesToAdd = true;
				}

				String firstLetter;

				do{
					//make new letter container

					if(sortedNamesToAdd.size() > 0){
						if(sortedNamesToAdd.get(totalContactCount) != null){
							firstLetter = sortedNamesToAdd.get(totalContactCount).substring(0,1);
						}else{
							System.out.println("firstLetter is null: " + mViewGroup.toString());
							break;
						}
					}else{
						break;
					}

					if(mViewGroup == onPicswitchContainer){
						System.out.println("requests firstLetter: " + firstLetter);
					}

					currentLetter = firstLetter;
					View newLetter;
					if(letterCount % 2 == 0){
						newLetter = mInflater.inflate(R.layout.menu_contacts_list_item, mViewGroup, false);
					}else{
						newLetter = mInflater.inflate(R.layout.menu_contacts_list_item_dark, mViewGroup, false);
					}
					TextView letter = (TextView) newLetter.findViewById(R.id.contacts_menu_letter);
					letter.setText(firstLetter);
					FontsUtils.TypeFace(letter, getAssets());

					mLetterContainer = (LinearLayout) newLetter.findViewById(R.id.contacts_container);

					publishProgress(new Wrapper(mViewGroup, newLetter, mLetterContainer, null));

					letterCount++;
					contactCount = 0;

					do{
						//creating and adding views to new letter container
						String person = sortedNamesToAdd.get(totalContactCount);
						final String personNumber = peopleToDisplay.get(person);

						//add person to the current letter						
						final View newContact;
						if(contactCount % 2 == 0){
							newContact = mInflater.inflate(R.layout.contact_light, mLetterContainer, false);
						}else{
							newContact = mInflater.inflate(R.layout.contact_dark,  mLetterContainer, false);
						}
						TextView contact = (TextView) newContact.findViewById(R.id.contact_text);
						contact.setText(person);
						FontsUtils.TypeFace(contact, getAssets());

						final CheckBox checkBox = (CheckBox) newContact.findViewById(R.id.check_box);

						runOnUiThread(new Runnable(){

							@Override
							public void run() {

								newContact.setOnClickListener(new View.OnClickListener() {
									@Override
									public void onClick(View v) {
										if(!(checkBox.isChecked())){
											checkBox.setChecked(true);
										}else{
											checkBox.setChecked(false);
										}
									}
								});

								checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
									@Override
									public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

										if(isChecked){
											bottomBars.setVisibility(View.VISIBLE);
										}

										if(mViewGroup == onPicswitchContainer){
											//send request
											if(isChecked){
												if(!(inviteChecked.size()==0)){
													checkBox.setChecked(false);		//case where there are checks in the invite section 
													removeChecks(inviteChecked);	//so remove those checks, empty the list of views for invite
													invitePhoneNumbers.clear();
													inviteChecked.clear();
													inviteCount = 0;
												}else{
													requestCount++;					//normal case, add to the lists
													sendRequestTo.add(personNumber);
													requestChecked.add(newContact);
												}
											}else{
												requestCount--;
												if(sendRequestTo.contains(personNumber)){
													sendRequestTo.remove(personNumber);
												}
											}

											if(sendRequestBar.getVisibility() != View.VISIBLE){
												bottomBars.setVisibility(View.VISIBLE);
												sendRequestBar.setVisibility(View.VISIBLE);
												inviteBar.setVisibility(View.GONE);
											}
										}else{
											//invite
											if(isChecked){
												if(!(requestChecked.size() == 0)){
													checkBox.setChecked(false);
													removeChecks(requestChecked);
													sendRequestTo.clear();
													requestChecked.clear();
													requestCount = 0;
												}else{
													inviteCount++;
													invitePhoneNumbers.add(personNumber);
													inviteChecked.add(newContact);
												}
											}else{
												inviteCount--;
												if(invitePhoneNumbers.contains(personNumber)){
													invitePhoneNumbers.remove(personNumber);
												}
											}

											if(inviteBar.getVisibility() != View.VISIBLE){
												bottomBars.setVisibility(View.VISIBLE);
												sendRequestBar.setVisibility(View.GONE);
												inviteBar.setVisibility(View.VISIBLE);
											}
										}
										//if none, remove the bars all together
										if((requestCount == 0) && (inviteCount == 0)){
											bottomBars.setVisibility(View.GONE);
										}
										System.out.println("sendRequestTo: " + sendRequestTo.toString());
									}
								});

							}

						});


						//add contact to the letter viewgroup
						//mLetterContainer.addView(newContact);
						publishProgress(new Wrapper(null, null, mLetterContainer, newContact));



						totalContactCount++;
						contactCount++;

						//exits the loop when reached the end of contacts to add
						if(totalContactCount == sortedNamesToAdd.size()){
							moreNamesToAdd = false;
							break;
						}

						//						System.out.println("nextLetter = " + sortedNamesToAdd.get(totalContactCount).substring(0,1));
						//						boolean checkLetters = (sortedNamesToAdd.get(totalContactCount).substring(0,1) == currentLetter);
						//						System.out.println("currentLetter: " + currentLetter);
						//						System.out.println("checkLetters = " + checkLetters);

					}while(sortedNamesToAdd.get(totalContactCount).substring(0,1).equals(currentLetter));//next letter == current letter

					//done with current letter, so publish the progress (add the entire letter to the ui)
					//publishProgress(new Wrapper(mViewGroup, newLetter));
				}while(moreNamesToAdd);//there are more letters to add
			}

			return null;
		}

		ViewGroup letterAddTo;

		@Override
		protected void onProgressUpdate(Wrapper... param){
			if(dialog.isShowing()){
				dialog.dismiss();
			}
			Wrapper params = param[0];
			ViewGroup addToUiGroup = params.mAddToUi;
			View letterView = params.letterView;
			ViewGroup currLetterViewGroup = params.letterViewGroup;
			View contact = params.newContact;

			if(addToUiGroup == null){
				//adding to the same letter
				letterAddTo.addView(contact);
			}else{
				//adding a new letter
				if(contact == null){
					//add the empty new letter to the Ui
					addToUiGroup.addView(letterView);
					letterAddTo = currLetterViewGroup;
				}
			}
		}

		@Override
		protected void onPostExecute(Void param){

		}

	}

	public class Wrapper{
		public final ViewGroup mAddToUi;
		public final View letterView;
		public final ViewGroup letterViewGroup;
		public final View newContact;

		public Wrapper(ViewGroup addToUi, View letterToAdd, ViewGroup lettersViewGroup, View contact){
			mAddToUi = addToUi;
			letterView = letterToAdd;
			letterViewGroup = lettersViewGroup;
			newContact = contact;
		}
	}

	public class SetViewGroup{
		public final Set<String> set;
		public final ViewGroup vg;

		public SetViewGroup(Set<String> strSet, ViewGroup vg){
			set = strSet;
			this.vg = vg;
		}
	}

	public class PhoneNumberHolder{
		public final Set<String> requestNumbers;
		public final Set<String> inviteNumbers;

		public PhoneNumberHolder(Set<String> phoneNumbersRequest, Set<String> phoneNumbersInvite){
			requestNumbers = phoneNumbersRequest;
			inviteNumbers = phoneNumbersInvite;
		}
	}

}



