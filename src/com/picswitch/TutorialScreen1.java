package com.picswitch;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class TutorialScreen1 extends Fragment {

	public TutorialScreen1() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		View newView;
		newView = inflater.inflate(R.layout.tutorial_screen1, container, false);
		
		TextView titleButton = (TextView) newView.findViewById(R.id.tutorial_button_title);
		titleButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				TutorialActivity.tutorialPager.setCurrentItem(1);
			}
		});
		
		return newView;
	}

}
