package com.picswitch;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class TutorialScreen4 extends Fragment {

	public TutorialScreen4() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		View newView;
		newView = inflater.inflate(R.layout.tutorial_screen4, container, false); 
		TextView giraffeButton2 = (TextView) newView.findViewById(R.id.giraffe_button_2);
		giraffeButton2.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				TutorialActivity.tutorialPager.setCurrentItem(4);
			}
		});
		return newView;
	}

}
