package com.picswitch;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.ParseQuery;


/**Activity where the user gets to choose a photo to send to their friends
 * User has options of choosing a pic from their gallery or taking a photo*/
public class NewPic extends ActionBarActivity {
	ActionBar actionBar;
	int chunkNumbers = 9; //the number of chunks we want the images to be split into
	ArrayList<Bitmap> chunkedImages; //saves the chunked images
	private static final int SELECT_PICTURE = 1;
	protected static final int CAMERA_REQUEST = 0;
	private static final int RESULT_CROP = 2;
	private static final int MEDIA_TYPE_IMAGE = 3;
	protected static final String TAG = null;
	private String selectedImagePath;
	private ImageView img;
	private String filemanagerstring;
	protected Uri mMediaUri;
	protected Uri resultCropUri;

	private boolean externalStorageAvailable;

	public static final String TEMP_PHOTO_FILE_NAME = "temp_photo.jpg";
	protected File mFileTemp;


	@SuppressLint("NewApi") public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		actionBar = getActionBar(); //initialize actionbar
		setupActionBar();// Set up ActionBar
		titleBarSetUp();
		//to set the imageview to a square
		imageViewHelper();
		buttonsHelper();

		/**Checks whether the phone has external storage available*/

		String state = Environment.getExternalStorageState();
		if(state.equals(Environment.MEDIA_MOUNTED)){
			mFileTemp = new File(Environment.getExternalStorageDirectory(), TEMP_PHOTO_FILE_NAME);
			externalStorageAvailable = true;
		}
		else{
			mFileTemp = new File(getFilesDir(), TEMP_PHOTO_FILE_NAME);
			externalStorageAvailable = false;
		}

	}


	/**helper for buttons*/
	public void buttonsHelper(){
		Button sendPicswitch = (Button) findViewById(R.id.timer);
		sendPicswitch.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				ParseQuery query = ParseInstallation.getQuery(); 
				query.whereEqualTo("username", "gibson");    
				ParsePush push = new ParsePush();
				push.setQuery(query);
				push.setMessage("Welcome to PicS");
				push.sendInBackground();


				System.out.println("Pushed");
			}
		});

		Button friends = (Button) findViewById(R.id.button3);
		friends.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(getBaseContext(), EditFriendsActivity.class);
				startActivity(i); 
			}
		});

		Button buttonLoadImage = (Button) findViewById(R.id.button1);
		buttonLoadImage.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent choosePhotoIntent = new Intent(Intent.ACTION_GET_CONTENT);
				choosePhotoIntent.setType("image/*");
				startActivityForResult(choosePhotoIntent, SELECT_PICTURE);
			}
		});
		
		//camera button
		Button buttonCamera = (Button) findViewById(R.id.button2);
		buttonCamera.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
//				Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE); 
//				mMediaUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
//				if(mMediaUri==null){
//					//display an error
//					Toast.makeText(NewPic.this, R.string.error_external_storage, Toast.LENGTH_LONG).show();
//				}
//				cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, mMediaUri);
//				startActivityForResult(cameraIntent, CAMERA_REQUEST); 
				
				takePicture();
			}


			/**Creates Uri for the photos taken*/
			private Uri getOutputMediaFileUri(int mediaTypeImage) {
				// To be safe check whether external storage is mounted
				//helper method below
				if (externalStorageAvailable){
					//Get the Uri
					//1. First get the external storage directory
					String appName = NewPic.this.getString(R.string.app_name);
					File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory
							(Environment.DIRECTORY_PICTURES), appName);
					//2. create PicSwitch subdirectory
					if(!mediaStorageDir.exists()){
						if(!mediaStorageDir.mkdirs()){
							Log.e(TAG, "Failed to create directory");
							return null;
						}
					}
					//3. Create the file 
					File mediaFile;
					Date now = new Date();
					String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(now);
					String path = mediaStorageDir.getPath() + File.separator;

					mediaFile = new File(path+"PicSwitch_" + timestamp + ".jpg");

					//Return the image Uri
					return Uri.fromFile(mediaFile);
				}
				else{
					return null;
				}
			}

			/**Checks whether the phone has external storage available*/
			//			private boolean isExternalStorageAvailable(){
			//				String state = Environment.getExternalStorageState();
			//				if(state.equals(Environment.MEDIA_MOUNTED)){
			//					mFileTemp = new File(Environment.getExternalStorageDirectory(), TEMP_PHOTO_FILE_NAME);
			//					return true;
			//				}
			//				else{
			//					mFileTemp = new File(getFilesDir(), TEMP_PHOTO_FILE_NAME);
			//					return false;
			//				}
			//			}
		});
		//reset button
		Button reset = (Button) findViewById(R.id.back_button);
		reset.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent i = new Intent(getBaseContext(), MainActivity.class);
				startActivity(i);
				finish();
			}
		});
	}

	
    private void takePicture() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        try {
        	Uri mImageCaptureUri = null;
        	String state = Environment.getExternalStorageState();
        	if (Environment.MEDIA_MOUNTED.equals(state)) {
        		mImageCaptureUri = Uri.fromFile(mFileTemp);
        	}
        	else {
	        	/*
	        	 * The solution is taken from here: http://stackoverflow.com/questions/10042695/how-to-get-camera-result-as-a-uri-in-data-folder
	        	 */
	        	mImageCaptureUri = InternalStorageContentProvider.CONTENT_URI;
        	}	
            intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, CAMERA_REQUEST);
        } catch (ActivityNotFoundException e) {

            Log.d(TAG, "cannot take picture", e);
        }
    }

	/**helper for image view
	 * Sets up the image to be displayed on the imageview*/
	public void imageViewHelper(){
		img = (ImageView)findViewById(R.id.messageIcon);
		Display  display = getWindowManager().getDefaultDisplay();
		int swidth = display.getWidth();
		LayoutParams params = img.getLayoutParams();
		params.width = LayoutParams.FILL_PARENT;
		params.height = params.width ;
		img.setLayoutParams(params);
		createImageFromBitmap(img.getDrawingCache(), "img");
	}


	/**helper for titleBar*/
	public void titleBarSetUp(){
		LinearLayout titleBar = (LinearLayout) findViewById(R.id.new_pic_layout);
		Display  disp = getWindowManager().getDefaultDisplay();
		int width = disp.getWidth();
		LayoutParams paramz = titleBar.getLayoutParams();
		paramz.width = LayoutParams.FILL_PARENT;
		titleBar.setLayoutParams(paramz);
	}


	/**set up for action bar*/
	@SuppressLint("NewApi")
	private void setupActionBar(){
		final ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(
				R.layout.new_pic,
				null);
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setCustomView(actionBarLayout);

	}


	/**cropping helper*/
	private void crop() {
		//		Intent intent = new Intent("com.android.camera.action.CROP");
		//		intent.setData(photoUri);
		//		intent.putExtra("outputX", 300);
		//		intent.putExtra("outputY", 300);
		//		intent.putExtra("aspectX", 1);
		//		intent.putExtra("aspectY", 1);
		//		intent.putExtra("scale", true);
		//		intent.putExtra("return-data", true);
		
		ExifInterface exif = null;
		try {
			exif = new ExifInterface(mFileTemp.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}  
		int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);

		// create explicit intent
		Intent intent = new Intent(this, CropImage.class);

		// tell CropImage activity to look for image to crop 
		//	    String filePath = photoUri.toString();
		intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());
		intent.putExtra(CropImage.IMAGE_ORIENTATION, orientation);

		// allow CropImage activity to rescale image
		intent.putExtra(CropImage.SCALE, true);

		// if the aspect ratio is fixed to square
		intent.putExtra(CropImage.ASPECT_X, 2);
		intent.putExtra(CropImage.ASPECT_Y, 2);

		// start activity CropImage with certain request code and listen
		// for result
		startActivityForResult(intent, RESULT_CROP);
		//		resultCropUri = intent.getData();
	}


	/**activity result handler*/
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {
			if (requestCode == SELECT_PICTURE) {
				try{
					InputStream inputStream = getContentResolver().openInputStream(data.getData());
					FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
					copyStream(inputStream, fileOutputStream);
					fileOutputStream.close();
					inputStream.close();

					crop();

				} catch(Exception e) {
					Log.e(TAG, "Error while create temp file", e);
				}

			}
			//CAMERA
			else if(requestCode== CAMERA_REQUEST){
//				Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
//				mediaScanIntent.setData(mMediaUri);
//				sendBroadcast(mediaScanIntent);
				
				
				
				crop();
			}
			else if(requestCode==RESULT_CROP){
				String path = data.getStringExtra(CropImage.IMAGE_PATH);
				Bitmap bmp;
				if (path == null) { 
					return;
				}else{
					bmp = BitmapFactory.decodeFile(mFileTemp.getPath());

					saveToSDCard(bmp);
					img.setImageBitmap(bmp);
					splitImage(img, chunkNumbers);
				}
			}
		}
		else if(resultCode!=RESULT_CANCELED){
			Toast.makeText(this, R.string.general_error, Toast.LENGTH_LONG).show();
		}
	}


	public void saveToSDCard(Bitmap bitmap) {
		// To be safe check whether external storage is mounted
		//helper method below
		if (externalStorageAvailable){
			File sdCard = Environment.getExternalStorageDirectory();
			File dir = new File(sdCard.getAbsolutePath() + "/Pictures");
			dir.mkdirs();
			File file = new File(dir, "cropped.png");
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
			FileOutputStream f = null;
			try {
				f = new FileOutputStream(file);
				if (f != null) {
					f.write(baos.toByteArray());
					f.flush();
					f.close();
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}


			//Return the image Uri
			resultCropUri = Uri.fromFile(file);
		}



		else{

		}

	}








	//	/**Checks whether the phone has external storage available*/
	//	private boolean isExternalStorageAvailable(){
	//		String state = Environment.getExternalStorageState();
	//		if(state.equals(Environment.MEDIA_MOUNTED)){
	//			mFileTemp = new File(Environment.getExternalStorageDirectory(), TEMP_PHOTO_FILE_NAME);
	//			return true;
	//		}
	//		else{
	//			mFileTemp = new File(getFilesDir(), TEMP_PHOTO_FILE_NAME);
	//			return false;
	//		}
	//	}




	public Uri getUri(){
		String root = Environment.getExternalStorageDirectory().toString();
		File myDir = new File(root + "/PicSwitch");    
		myDir.mkdirs();
		Random generator = new Random();
		int n = 10000;
		n = generator.nextInt(n);
		String fname = "PicSwitch_"+ n +".jpg";
		File file = new File (myDir, fname);
		Uri imageUri = Uri.fromFile(file);
		return imageUri;
	}


	/***To get the Uri of the bitmap*/
	public Uri getImageUri(Context inContext, Bitmap inImage) {
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
		String path = Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
		return Uri.parse(path);
	}
	/**helper to get the path uri*/
	public String getPath(Uri uri) {
		String selectedImagePath;
		//1:MEDIA GALLERY --- query from MediaStore.Images.Media.DATA
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = managedQuery(uri, projection, null, null, null);
		if(cursor != null){
			int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			selectedImagePath = cursor.getString(column_index);
		}else{
			selectedImagePath = null;
		}

		if(selectedImagePath == null){
			selectedImagePath = uri.getPath();
		}
		return selectedImagePath;
	}


	/**method to split the image in imageView*/
	private void splitImage(ImageView image, int chunkNumbers) {    
		//For the number of rows and columns of the grid to be displayed
		int rows,cols;
		//For height and width of the small image chunks 
		int chunkHeight,chunkWidth;
		//To store all the small image chunks in bitmap format in this list 
		chunkedImages = new ArrayList<Bitmap>(chunkNumbers);
		//Getting the scaled bitmap of the source image
		BitmapDrawable drawable = (BitmapDrawable) image.getDrawable();
		Bitmap bitmap = drawable.getBitmap();
		Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), true);
		rows = cols = (int) Math.sqrt(chunkNumbers);
		chunkHeight = bitmap.getHeight()/rows;
		chunkWidth = bitmap.getWidth()/cols;
		//xCoord and yCoord are the pixel positions of the image chunks
		int yCoord = 0;
		for(int x=0; x<rows; x++){
			int xCoord = 0;
			for(int y=0; y<cols; y++){
				chunkedImages.add(Bitmap.createBitmap(scaledBitmap, xCoord, yCoord, chunkWidth, chunkHeight));
				xCoord += chunkWidth;
			}
			yCoord += chunkHeight;
		}
		createImageHelper(chunkedImages);
	}


	/**Saves images to sd and starts next activity*/
	public void createImageHelper(ArrayList<Bitmap> bitmap){
		for(int i=0;i<9;i++){
			createImageFromBitmap(bitmap.get(i), "img"+i);
		}
		Intent typeMessageIntent = new Intent(this, TypeMessage.class);
		typeMessageIntent.setData(resultCropUri);
		String fileType = ParseConstants.TYPE_IMAGE;
		typeMessageIntent.putExtra(ParseConstants.KEY_FILE_TYPE, fileType);
		startActivity(typeMessageIntent);


	}


	/**reset helper
	 * reshuffles the PicSwitch*/
	public void reset(){
		Intent i = new Intent(this, OpenImage.class);
		startActivity(i);
	}


	/**Creates images from the bitmaps*/
	public String createImageFromBitmap(Bitmap bitmap, String name) {
		String fileName = name;//no .png or .jpg needed
		try {
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
			FileOutputStream fo = openFileOutput(fileName, Context.MODE_PRIVATE);
			fo.write(bytes.toByteArray());
			// remember close file output
			fo.close();
		} catch (Exception e) {
			e.printStackTrace();
			fileName = null;
		}
		return fileName;

	}

	public static void copyStream(InputStream input, OutputStream output)
			throws IOException {

		byte[] buffer = new byte[1024];
		int bytesRead;
		while ((bytesRead = input.read(buffer)) != -1) {
			output.write(buffer, 0, bytesRead);
		}
	}

}
