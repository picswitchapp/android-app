package com.picswitch;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseCrashReporting;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;

public class ChangePassword extends Activity {

	Typeface montserrat;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_change_password);
		getActionBar().hide();

		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
			window.setStatusBarColor(Color.rgb(32, 99, 99));
		}
		ImageView leftArrow = (ImageView) findViewById(R.id.left_arrow_change_pass);
		leftArrow.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				ChangePassword.this.finish();
			}
		});

		montserrat = Typeface.createFromAsset(getAssets(), "Montserrat-Regular.ttf");
		Typeface varella = Typeface.createFromAsset(getAssets(), "VarelaRound.ttf");

		//labels
		TextView changePassTitle = (TextView) findViewById(R.id.title_text_change_pass);
		changePassTitle.setTypeface(montserrat);
		TextView changePassLabel = (TextView) findViewById(R.id.change_pass_label);
		changePassLabel.setTypeface(montserrat);
		TextView confirmChangeText = (TextView) findViewById(R.id.confirm_change_pass_button);
		confirmChangeText.setTypeface(varella);

		//edit texts
		final EditText oldPassword = (EditText) findViewById(R.id.old_password);
		oldPassword.setTypeface(varella);

		final EditText newPassword = (EditText) findViewById(R.id.new_password);
		newPassword.setTypeface(varella);

		final EditText confirmNewPassword = (EditText) findViewById(R.id.confirm_new_password);
		confirmNewPassword.setTypeface(varella);

		//handle changing of passwords
		RelativeLayout changePassConfirmButton = (RelativeLayout) findViewById(R.id.confirm_pass_button);
		changePassConfirmButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String currentPass = oldPassword.getText().toString();
				final String newPass = newPassword.getText().toString();
				final String confNewPass = confirmNewPassword.getText().toString();

				if((currentPass.length() > 0) && (newPass.length() > 0) && (confNewPass.length() > 0)){

					ParseUser.logInInBackground(ParseUser.getCurrentUser().getUsername(), currentPass, new LogInCallback() {
						@Override
						public void done(ParseUser user, ParseException e) {
							if(e ==null){
								if(user != null){								
									//password is correct
									if(newPass.equals(confNewPass)){
										//passwords match
										if(newPass.length() > 5){
											//new password is valid
											ParseUser mUser = ParseUser.getCurrentUser();
											mUser.setPassword(newPass);
											mUser.saveInBackground(new SaveCallback() {
												@Override
												public void done(ParseException e) {
													if(e == null){
														Toast.makeText(ChangePassword.this, "Successfully changed password", Toast.LENGTH_SHORT).show();
													}else{
														e.printStackTrace();
														Toast.makeText(ChangePassword.this, "Changing password failed", Toast.LENGTH_SHORT).show();
													}
													ChangePassword.this.finish();
												}
											});
										}else{
											//new passwords are not valid
											AlertDialog.Builder mBuilder = new AlertDialog.Builder(ChangePassword.this);
											mBuilder.setTitle("New password is not valid");
											mBuilder.setMessage("Passwords must be at least 6 characters in length.");
											mBuilder.setPositiveButton("Try again", new DialogInterface.OnClickListener() {
												@Override
												public void onClick(DialogInterface dialog, int which) {
													Intent intent = new Intent(ChangePassword.this, ChangePassword.class);
													startActivity(intent);
													dialog.cancel();
													finish();
												}
											});
											mBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
												@Override
												public void onClick(DialogInterface dialog, int which) {
													dialog.cancel();
													ChangePassword.this.finish();
												}
											});
											AlertDialog mDialog = mBuilder.create();
											mDialog.show();

											int titleId = getResources().getIdentifier( "alertTitle", "id", "android" );
											if (titleId > 0) {
												TextView mTitle = (TextView) mDialog.findViewById(titleId);
												if (mTitle != null) {
													mTitle.setTypeface(montserrat);
												}
											}
											
											Button button1 = (Button) mDialog.findViewById(android.R.id.button1);
											Button button2 = (Button) mDialog.findViewById(android.R.id.button2);
											FontsUtils.TypeFace(button1, getAssets());
											FontsUtils.TypeFace(button2, getAssets());

											TextView mMessage = (TextView) mDialog.findViewById(android.R.id.message);
											mMessage.setTextSize(15);
											FontsUtils.TypeFace(mMessage, getAssets());
										}

									}else{
										//new passwords dont match
										AlertDialog.Builder mBuilder = new AlertDialog.Builder(ChangePassword.this);
										mBuilder.setTitle("New passwords don't match");
										mBuilder.setPositiveButton("Try again", new DialogInterface.OnClickListener() {
											@Override
											public void onClick(DialogInterface dialog, int which) {
												Intent intent = new Intent(ChangePassword.this, ChangePassword.class);
												startActivity(intent);
												dialog.cancel();
												finish();
											}
										});
										mBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
											@Override
											public void onClick(DialogInterface dialog, int which) {
												dialog.cancel();
												ChangePassword.this.finish();
											}
										});
										AlertDialog mDialog = mBuilder.create();
										mDialog.show();

										int titleId = getResources().getIdentifier( "alertTitle", "id", "android" );
										if (titleId > 0) {
											TextView mTitle = (TextView) mDialog.findViewById(titleId);
											if (mTitle != null) {
												mTitle.setTypeface(montserrat);
											}
										}
										
										Button button1 = (Button) mDialog.findViewById(android.R.id.button1);
										Button button2 = (Button) mDialog.findViewById(android.R.id.button2);
										FontsUtils.TypeFace(button1, getAssets());
										FontsUtils.TypeFace(button2, getAssets());

										TextView mMessage = (TextView) mDialog.findViewById(android.R.id.message);
										mMessage.setTextSize(15);
										FontsUtils.TypeFace(mMessage, getAssets());
									}
								}else{
									//password is incorrect
									AlertDialog.Builder mBuilder = new AlertDialog.Builder(ChangePassword.this);
									mBuilder.setTitle("Current password is incorrect");
									mBuilder.setPositiveButton("Try again", new DialogInterface.OnClickListener() {
										@Override
										public void onClick(DialogInterface dialog, int which) {
											Intent intent = new Intent(ChangePassword.this, ChangePassword.class);
											startActivity(intent);
											dialog.cancel();
											finish();
										}
									});
									mBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
										@Override
										public void onClick(DialogInterface dialog, int which) {
											dialog.cancel();
											ChangePassword.this.finish();
										}
									});
									AlertDialog mDialog = mBuilder.create();
									mDialog.show();

									int titleId = getResources().getIdentifier( "alertTitle", "id", "android" );
									if (titleId > 0) {
										TextView mTitle = (TextView) mDialog.findViewById(titleId);
										if (mTitle != null) {
											mTitle.setTypeface(montserrat);
										}
									}
									
									Button button1 = (Button) mDialog.findViewById(android.R.id.button1);
									Button button2 = (Button) mDialog.findViewById(android.R.id.button2);
									FontsUtils.TypeFace(button1, getAssets());
									FontsUtils.TypeFace(button2, getAssets());

									TextView mMessage = (TextView) mDialog.findViewById(android.R.id.message);
									mMessage.setTextSize(15);
									FontsUtils.TypeFace(mMessage, getAssets());
								}
							}else{
								e.printStackTrace();
								
								//password is incorrect
								AlertDialog.Builder mBuilder = new AlertDialog.Builder(ChangePassword.this);
								mBuilder.setTitle("Current password is incorrect");
								mBuilder.setPositiveButton("Try again", new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										Intent intent = new Intent(ChangePassword.this, ChangePassword.class);
										startActivity(intent);
										dialog.cancel();
										finish();
									}
								});
								mBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										dialog.cancel();
										ChangePassword.this.finish();
									}
								});
								AlertDialog mDialog = mBuilder.create();
								mDialog.show();
								
								Button button1 = (Button) mDialog.findViewById(android.R.id.button1);
								Button button2 = (Button) mDialog.findViewById(android.R.id.button2);
								FontsUtils.TypeFace(button1, getAssets());
								FontsUtils.TypeFace(button2, getAssets());

								int titleId = getResources().getIdentifier( "alertTitle", "id", "android" );
								if (titleId > 0) {
									TextView mTitle = (TextView) mDialog.findViewById(titleId);
									if (mTitle != null) {
										mTitle.setTypeface(montserrat);
									}
								}
							}
						}
					});
				}else{
					//handle case of empty entries here
					AlertDialog.Builder mBuilder = new AlertDialog.Builder(ChangePassword.this);
					mBuilder.setTitle("Invalid Passwords");
					mBuilder.setMessage("Enter your current and desired passwords");
					mBuilder.setPositiveButton("Try again", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							Intent intent = new Intent(ChangePassword.this, ChangePassword.class);
							startActivity(intent);
							dialog.cancel();
							finish();
						}
					});
					mBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.cancel();
							ChangePassword.this.finish();
						}
					});
					AlertDialog mDialog = mBuilder.create();
					mDialog.show();

					int titleId = getResources().getIdentifier( "alertTitle", "id", "android" );
					if (titleId > 0) {
						TextView mTitle = (TextView) mDialog.findViewById(titleId);
						if (mTitle != null) {
							mTitle.setTypeface(montserrat);
						}
					}
					
					Button button1 = (Button) mDialog.findViewById(android.R.id.button1);
					Button button2 = (Button) mDialog.findViewById(android.R.id.button2);
					FontsUtils.TypeFace(button1, getAssets());
					FontsUtils.TypeFace(button2, getAssets());

					TextView mMessage = (TextView) mDialog.findViewById(android.R.id.message);
					mMessage.setTextSize(15);
					FontsUtils.TypeFace(mMessage, getAssets());
				}

			}
		});

	}
}
