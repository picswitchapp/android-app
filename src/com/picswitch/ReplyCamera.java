package com.picswitch;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.parse.ParseCrashReporting;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.Size;
import android.media.ExifInterface;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ReplyCamera extends ActionBarActivity {

	int chunkNumbers = 9; //the number of chunks we want the images to be split into
	ArrayList<Bitmap> chunkedImages;

	public static final int MEDIA_TYPE_IMAGE = 1;

	private static final String TAG = "MyActivity";
	private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
	private static final int SELECT_PICTURE = 1;
	private static final int RESULT_CROP = 0;
	private static final int RESULT_OK = -1;

	private static final int FOCUS_AREA_SIZE = 300;

	private ImageView img;
	private boolean externalStorageAvailable;

	public static final String TEMP_PHOTO_FILE_NAME = "temp_photo.jpg";
	protected File mFileTemp;
	protected Uri resultCropUri;

	private Uri fileUri;
	Context mContext;

	String senderFirstName;
	String senderUsername;
	String senderObjectId;

	Camera mCamera;
	private ReplyCameraPreview mPreview;
	int mNumberOfCameras;
	int mCurrentCamera;  // Camera ID currently chosen
	int mCameraCurrentlyLocked;  // Camera ID that's actually acquired

	FrameLayout mFrame;

	// The first rear facing camera
	int mDefaultCameraId;

	ProgressDialog mDialog;
	FocusCrosshair tapFocusIcon;

	//will hold the screen size
	Point displaySize = new Point();

	LinearLayout cameraFrameView;
	int pixelsTopScreenToCameraCutOut;

	Typeface montSerrat;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.activity_reply_camera);

		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
			window.setStatusBarColor(Color.rgb(32, 99, 99));
		}
		mContext = getApplicationContext();

		mPreview = new ReplyCameraPreview(this);

		montSerrat = Typeface.createFromAsset(getAssets(), "Montserrat-Regular.ttf");
		img =(ImageView) findViewById(R.id.messageIcon);
		img.setVisibility(View.GONE);
		tapFocusIcon = (FocusCrosshair) findViewById(R.id.reply_focus_crosshair);

		Display display = getWindowManager().getDefaultDisplay();
		display.getSize(displaySize);

		// Find the total number of cameras available
		mNumberOfCameras = Camera.getNumberOfCameras();

		// Find the ID of the rear-facing ("default") camera
		CameraInfo cameraInfo = new CameraInfo();
		for (int i = 0; i < mNumberOfCameras; i++) {
			Camera.getCameraInfo(i, cameraInfo);
			if (cameraInfo.facing == CameraInfo.CAMERA_FACING_BACK) {
				mCurrentCamera = mDefaultCameraId = i;
			}
		}

		mFrame = (FrameLayout) findViewById(R.id.camera_preview);

		mFrame.removeAllViews();
		mFrame.addView(mPreview);

		//touchlistener for the preview window
		mPreview.setOnTouchListener(new OnTouchListener() {
			private GestureDetector gestureDetector = new GestureDetector(mContext, new GestureDetector.SimpleOnGestureListener() {
				@Override
				public boolean onDoubleTapEvent(MotionEvent e) {
					//code for switching cameras on double tap, omit for now
					//					if(mNumberOfCameras > 1){
					//						if (mCamera != null) {
					//							mCamera.stopPreview();
					//							mPreview.setCamera(null);
					//							mCamera.release();
					//							mCamera = null;
					//						}
					//
					//						// Acquire the next camera and request Preview to reconfigure
					//						// parameters.
					//						mCurrentCamera = (mCameraCurrentlyLocked + 1) % mNumberOfCameras;
					//						mCamera = Camera.open(mCurrentCamera);
					//						mCameraCurrentlyLocked = mCurrentCamera;
					//						mCamera.setDisplayOrientation(90);
					//						mPreview.switchCamera(mCamera);
					//
					//						// Start the preview
					//						mCamera.startPreview();
					//					}
					return super.onDoubleTap(e);
				}

				@Override
				public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY){
					return false;
				}

				@Override
				public boolean onSingleTapConfirmed(MotionEvent e){

					//make visible the focus shape
					int boxSize = displaySize.x / 6;
					LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(boxSize,boxSize);
					params.leftMargin = Math.round(e.getRawX() - (boxSize / 2));
					params.topMargin = Math.round(e.getRawY() - (boxSize / 2) - pixelsTopScreenToCameraCutOut);
					tapFocusIcon.showStart();
					cameraFrameView.removeView(tapFocusIcon);
					cameraFrameView.addView(tapFocusIcon, params);

					Animation mFocusAnimationIn  = AnimationUtils.loadAnimation(mContext, R.anim.focus_animation_in);
					final Animation mFocusAnimationOut = AnimationUtils.loadAnimation(mContext, R.anim.focus_animation_out);
					tapFocusIcon.startAnimation(mFocusAnimationIn);
					mFocusAnimationIn.setAnimationListener(new AnimationListener() {
						@Override
						public void onAnimationStart(Animation animation) {}
						@Override
						public void onAnimationEnd(Animation animation) {
							tapFocusIcon.setAnimation(null);
							tapFocusIcon.setAnimation(mFocusAnimationOut);
						}
						@Override
						public void onAnimationRepeat(Animation animation) {}
					});
					mFocusAnimationOut.setAnimationListener(new AnimationListener() {
						@Override
						public void onAnimationStart(Animation animation) {}
						@Override
						public void onAnimationEnd(Animation animation) {
							tapFocusIcon.clear();
						}
						@Override
						public void onAnimationRepeat(Animation animation) {}
					});


					focusOnTouch(e);

					System.out.println("touch at: " + e.getRawX() + ", " + e.getRawY());
					System.out.println("parentView starts at " + pixelsTopScreenToCameraCutOut);

					return false;
				}

			});

			@Override
			public boolean onTouch(View v, MotionEvent event) {

				gestureDetector.onTouchEvent(event);

				return true;
			}
		});

		//get data from intent
		senderUsername = getIntent().getExtras().getString(ParseConstants.KEY_SENDER);
		senderFirstName = getIntent().getExtras().getString(ParseConstants.KEY_SENDER_FIRST_NAME);
		senderObjectId = getIntent().getExtras().getString("senderObjectId");

		// Add a listener to the Capture button
		ImageView captureButton = (ImageView) findViewById(R.id.capture_button);
		captureButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// get an image from the camera
				takeFocusedPicture();

				System.out.println("This works!");
			}
		});

		TextView title_text_reply = (TextView) findViewById(R.id.title_text_reply);
		title_text_reply.setTypeface(montSerrat);
		title_text_reply.setText("send to " + senderFirstName);

		ImageView left_arrow = (ImageView) findViewById(R.id.left_arrow_reply);
		left_arrow.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});

//		ImageView open_from_gallery = (ImageView) findViewById(R.id.open_image_from_gallery);
//		open_from_gallery.setOnClickListener(new View.OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				//Toast.makeText(getBaseContext(), "gallery button click", Toast.LENGTH_SHORT).show();
//				Intent choosePhotoIntent = new Intent(Intent.ACTION_GET_CONTENT);
//				choosePhotoIntent.setType("image/*");
//				startActivityForResult(choosePhotoIntent, SELECT_PICTURE);
//			}
//		});

		String state = Environment.getExternalStorageState();
		if(state.equals(Environment.MEDIA_MOUNTED)){
			mFileTemp = new File(Environment.getExternalStorageDirectory(), TEMP_PHOTO_FILE_NAME);
			externalStorageAvailable = true;
		}
		else{
			mFileTemp = new File(mContext.getFilesDir(), TEMP_PHOTO_FILE_NAME);
			externalStorageAvailable = false;
		}

		final int mWidth = displaySize.x;
		LayoutParams params = img.getLayoutParams();
		params.width = LayoutParams.FILL_PARENT;
		params.height = params.width ;
		img.setLayoutParams(params);
		createImageFromBitmap(img.getDrawingCache(), "img");


		//setting the preview to be square along with a grid added
		cameraFrameView = (LinearLayout) findViewById(R.id.reply_camera_space_holder);
		final RelativeLayout titleBarView    = (RelativeLayout) findViewById(R.id.title_bar_reply);
		final TextView topSpacer       = (TextView) findViewById(R.id.top_spacer);
		final RelativeLayout bottomSpacer    = (RelativeLayout) findViewById(R.id.bottom_spacer);

		final View replyCameraView = findViewById(R.id.reply_camera);
		final ViewTreeObserver mObserver = replyCameraView.getViewTreeObserver();
		mObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
			@Override
			public void onGlobalLayout() {
				//pre adjusting height
				System.out.println("bottom spacer pre view height = " + bottomSpacer.getHeight());
				System.out.println("top spacer pre view height = " + topSpacer.getHeight());

				//entire view height
				int viewHeight = replyCameraView.getHeight();
				System.out.println("entire view height = " + viewHeight);

				//title bar height
				int titleBarHeight = titleBarView.getHeight();
				System.out.println("title bar view height = " + titleBarHeight);

				//camera frame view width
				int cameraViewWidth = cameraFrameView.getWidth();
				System.out.println("camera frame view height = " + cameraViewWidth);

				//do the correct subtraction
				int twiceSpacerHeight = viewHeight - titleBarHeight - cameraViewWidth;
				System.out.println("twice spacer view height = " + twiceSpacerHeight);

				//find actual height in pixels of each spacer view
				int topSpacerHeight = twiceSpacerHeight / 3;
				int bottomSpacerHeight = (2 * twiceSpacerHeight) / 3;
				//System.out.println("spacer view height = " + spacerHeight);			

				//topSpacer setting height
				RelativeLayout.LayoutParams topSpacerParams = (RelativeLayout.LayoutParams)topSpacer.getLayoutParams();
				topSpacerParams.height = topSpacerHeight;
				topSpacerParams.width  = mWidth;
				topSpacer.setLayoutParams(topSpacerParams);
				topSpacer.requestLayout();

				//reference rectangle for moving the camera hole
				Rect mRect = new Rect();
				topSpacer.getLocalVisibleRect(mRect);

				//bottomSpacer setting height
				RelativeLayout.LayoutParams bottomSpacerParams = (RelativeLayout.LayoutParams)bottomSpacer.getLayoutParams();
				bottomSpacerParams.height = bottomSpacerHeight;
				bottomSpacerParams.width  = mWidth;
				bottomSpacer.setLayoutParams(bottomSpacerParams);
				bottomSpacer.requestLayout();

				//setting the size of the camera hole layout
				RelativeLayout.LayoutParams cameraFrameParams = (RelativeLayout.LayoutParams)cameraFrameView.getLayoutParams();
				cameraFrameParams.height = mWidth;
				cameraFrameParams.width  = mWidth;
				cameraFrameView.setLayoutParams(cameraFrameParams);
				int mTrans = ( twiceSpacerHeight / 6 );
				System.out.println("mTrans: " + mTrans);
				cameraFrameView.setTranslationY(-mTrans);
				cameraFrameView.setBackgroundResource(R.drawable.new_grid_overlay_image);
				cameraFrameView.requestLayout();	

				//mView.requestLayout();
				setUpmFrame();

				//check after adjusting spacers
				System.out.println("bottom spacer post view height = " + bottomSpacer.getHeight());
				System.out.println("top spacer post view height = " + topSpacer.getHeight());

				Rect rectf = new Rect();
				cameraFrameView.getGlobalVisibleRect(rectf);
				pixelsTopScreenToCameraCutOut = (int) Math.round(1.75 * rectf.top);

				//remove this observer so it only runs once
				ViewTreeObserver obs = replyCameraView.getViewTreeObserver();
				obs.removeOnGlobalLayoutListener(this);
			}
		});
	}

	public void setUpmFrame(){
		mFrame.removeAllViews();
		mFrame.addView(mPreview);
	}

	private void focusOnTouch(MotionEvent event) {
		if (mCamera != null ) {
			mCamera.cancelAutoFocus();
			Camera.Parameters parameters = mCamera.getParameters();
			if (parameters.getMaxNumMeteringAreas() > 0){
				Log.i(TAG,"fancy !");
				Rect rect = calculateFocusArea(event.getX(), event.getY());

				System.out.println("number of focus areas: " + parameters.getMaxNumFocusAreas());

				parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
				List<Camera.Area> meteringAreas = new ArrayList<Camera.Area>();
				meteringAreas.clear();
				meteringAreas.add(new Camera.Area(rect, 800));
				parameters.setFocusAreas(meteringAreas);

				try{
					mCamera.setParameters(parameters);
				}catch(Exception e){
					e.printStackTrace();
				}

				mCamera.autoFocus(mAutoFocusTakePictureCallback);
			}else {
				mCamera.autoFocus(mAutoFocusTakePictureCallback);
			}
		}
	}

	private Camera.AutoFocusCallback mAutoFocusTakePictureCallback = new Camera.AutoFocusCallback() {
		@Override
		public void onAutoFocus(boolean success, Camera camera) {
			if (success) {
				// do something...
				Log.i("tap_to_focus","success!");
			} else {
				// do something...
				Log.i("tap_to_focus","fail!");
			}
		}
	};

	private Rect calculateFocusArea(float x, float y) {
		int left = clamp(Float.valueOf((x / mPreview.getWidth()) * 2000 - 1000).intValue(), FOCUS_AREA_SIZE);
		int top = clamp(Float.valueOf((y / mPreview.getHeight()) * 2000 - 1000).intValue(), FOCUS_AREA_SIZE);

		return new Rect(left, top, left + FOCUS_AREA_SIZE, top + FOCUS_AREA_SIZE);
	}

	private int clamp(int touchCoordinateInCameraReper, int focusAreaSize) {
		int result;
		if (Math.abs(touchCoordinateInCameraReper)+focusAreaSize/2>1000){
			if (touchCoordinateInCameraReper>0){
				result = 1000 - focusAreaSize/2;
			} else {
				result = -1000 + focusAreaSize/2;
			}
		} else{
			result = touchCoordinateInCameraReper - focusAreaSize/2;
		}
		return result;
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data){
		if(resultCode == RESULT_OK){
			switch(requestCode){
			case SELECT_PICTURE:{
				try{
					InputStream inputStream = mContext.getContentResolver().openInputStream(data.getData());
					FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
					copyStream(inputStream, fileOutputStream);
					fileOutputStream.close();
					inputStream.close();

					crop();

				} catch(Exception e) {
					Log.e(TAG, "Error while create temp file", e);
				}
			}
			case RESULT_CROP:{
				String path = data.getStringExtra(CropImage.IMAGE_PATH);
				Bitmap bmp;
				if (path == null) { 
					return;
				}else{
					bmp = BitmapFactory.decodeFile(mFileTemp.getPath());

					saveToSDCard(bmp);
					img.setImageBitmap(bmp);
					splitImage(img, chunkNumbers);
				}
			}
			}

		}
	}

	/**cropping helper*/
	private void crop() {

		ExifInterface exif = null;
		try {
			exif = new ExifInterface(mFileTemp.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}  
		int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);

		// create explicit intent
		Intent intent = new Intent(mContext, CropImage.class);

		// tell CropImage activity to look for image to crop 
		//	    String filePath = photoUri.toString();
		intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());
		intent.putExtra(CropImage.IMAGE_ORIENTATION, orientation);

		// allow CropImage activity to rescale image
		intent.putExtra(CropImage.SCALE, true);

		// if the aspect ratio is fixed to square
		intent.putExtra(CropImage.ASPECT_X, 2);
		intent.putExtra(CropImage.ASPECT_Y, 2);

		// start activity CropImage with certain request code and listen
		// for result
		startActivityForResult(intent, RESULT_CROP);
		//		resultCropUri = intent.getData();
	}

	public static void copyStream(InputStream input, OutputStream output)
			throws IOException {

		byte[] buffer = new byte[1024];
		int bytesRead;
		while ((bytesRead = input.read(buffer)) != -1) {
			output.write(buffer, 0, bytesRead);
		}
	}

	public void saveToSDCard(Bitmap bitmap) {
		// To be safe check whether external storage is mounted
		//helper method below
		if (externalStorageAvailable){
			File sdCard = Environment.getExternalStorageDirectory();
			File dir = new File(sdCard.getAbsolutePath() + "/Pictures");
			dir.mkdirs();
			File file = new File(dir, "cropped.png");
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
			FileOutputStream f = null;
			try {
				f = new FileOutputStream(file);
				if (f != null) {
					f.write(baos.toByteArray());
					f.flush();
					f.close();
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}


			//Return the image Uri
			resultCropUri = Uri.fromFile(file);
		}

		else{

		}

	}

	private void splitImage(ImageView image, int chunkNumbers) {    
		//For the number of rows and columns of the grid to be displayed
		int rows,cols;
		//For height and width of the small image chunks 
		int chunkHeight,chunkWidth;
		//To store all the small image chunks in bitmap format in this list 
		chunkedImages = new ArrayList<Bitmap>(chunkNumbers);
		//Getting the scaled bitmap of the source image
		BitmapDrawable drawable = (BitmapDrawable) image.getDrawable();
		Bitmap bitmap = drawable.getBitmap();
		Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), true);
		rows = cols = (int) Math.sqrt(chunkNumbers);
		chunkHeight = bitmap.getHeight()/rows;
		chunkWidth = bitmap.getWidth()/cols;
		//xCoord and yCoord are the pixel positions of the image chunks
		int yCoord = 0;
		for(int x=0; x<rows; x++){
			int xCoord = 0;
			for(int y=0; y<cols; y++){
				chunkedImages.add(Bitmap.createBitmap(scaledBitmap, xCoord, yCoord, chunkWidth, chunkHeight));
				xCoord += chunkWidth;
			}
			yCoord += chunkHeight;
		}
		createImageHelper(chunkedImages);
	}

	/**Saves images to sd and starts next activity*/
	public void createImageHelper(ArrayList<Bitmap> bitmap){
		for(int i=0;i<9;i++){
			createImageFromBitmap(bitmap.get(i), "img"+i);
		}
		Intent typeMessageIntent = new Intent(mContext, ReplyTypeMessage.class);
		typeMessageIntent.setData(resultCropUri);
		String fileType = ParseConstants.TYPE_IMAGE;
		typeMessageIntent.putExtra(ParseConstants.KEY_FILE_TYPE, fileType);
		typeMessageIntent.putExtra(ParseConstants.KEY_SENDER, senderUsername);
		typeMessageIntent.putExtra(ParseConstants.KEY_SENDER_FIRST_NAME, senderFirstName);
		typeMessageIntent.putExtra("senderObjectId", senderObjectId);
		startActivity(typeMessageIntent);
		ReplyCamera.this.finish();
	}

	/**Creates images from the bitmaps*/
	public String createImageFromBitmap(Bitmap bitmap, String name) {
		String fileName = name;//no .png or .jpg needed
		try {
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
			FileOutputStream fo = mContext.openFileOutput(fileName, Context.MODE_PRIVATE);
			fo.write(bytes.toByteArray());
			// remember close file output
			fo.close();
		} catch (Exception e) {
			e.printStackTrace();
			fileName = null;
		}
		return fileName;

	}

	/** A safe way to get an instance of the Camera object. */
	public static Camera getCameraInstance(){
		Camera c = null;
		try {
			c = Camera.open(0); // attempt to get a Camera instance
			//c.unlock();
		}
		catch (Exception e){
			// Camera is not available (in use or does not exist)
		}
		return c; // returns null if camera is unavailable
	}

	public void takeFocusedPicture(){
		mCamera.autoFocus(mAutoFocusCallback);
	}

	Camera.AutoFocusCallback mAutoFocusCallback = new Camera.AutoFocusCallback() {

		@Override
		public void onAutoFocus(boolean success, Camera camera) {
			camera.takePicture(null, null, mPicture);			
		}
	};

	PictureCallback mPicture = new PictureCallback(){

		@Override
		public void onPictureTaken(byte[] data, Camera camera) {
			new processCapturedImage().execute(data);
		}
	};

	public class processCapturedImage extends AsyncTask<byte[], Void, Bitmap>{

		@Override
		protected void onPreExecute(){
			mDialog = new ProgressDialog(ReplyCamera.this);
			mDialog.setMessage("Processing image...");
			mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			mDialog.setIndeterminate(true);
			mDialog.show();
		}

		@Override
		protected Bitmap doInBackground(byte[]... params) {
			byte[] data = params[0];

			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;

			BitmapFactory.decodeByteArray(data, 0, data.length, options);

			int bmWidth = options.outWidth;
			int bmHeight = options.outHeight;

			System.out.println("captured image size: " + bmWidth + " x " + bmHeight);

			//scale down the bitmap
			int targetW = 501;
			int targetH = 501;

			int scaleFactor = Math.min(bmWidth/targetW, bmHeight/targetH);

			options.inJustDecodeBounds = false;
			options.inSampleSize = scaleFactor;
			options.inPurgeable = true;

			Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length, options);

			System.out.println("scaled image size: " + bitmap.getWidth() + " x " + bitmap.getHeight());

			Matrix matrix = new Matrix();
			matrix.postRotate(90);

			//apply the rotation to portrait
			Bitmap bitmapTemp = Bitmap.createBitmap(
					bitmap,
					0,
					0,
					bitmap.getWidth(),
					bitmap.getHeight(),
					matrix,
					false
					);

			System.out.println("temp height: " + bitmapTemp.getWidth());
			System.out.println("temp width: " + bitmapTemp.getHeight());

			//crop to the center region of a portrait image
			Bitmap bitmapCropped = Bitmap.createBitmap(
					bitmapTemp,
					0,
					(bitmapTemp.getHeight() / 2) - (bitmapTemp.getWidth() / 2),
					bitmapTemp.getWidth(),
					bitmapTemp.getWidth()
					);

			System.out.println("cropped height: " + bitmapCropped.getWidth());
			System.out.println("cropped width: " + bitmapCropped.getHeight());

			//save the picture
			if(TabActivity.SAVE_PHOTOS){
				File mediaStorageDir = new File(
						Environment.getExternalStoragePublicDirectory(
								Environment.DIRECTORY_PICTURES
								),
								getString(R.string.app_name)
						);

				if (!mediaStorageDir.exists()) {
					if (!mediaStorageDir.mkdirs()) {

						return null;
					}
				}

				String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
				File mediaFile = new File(
						mediaStorageDir.getPath() + File.separator + "fragmentTest_"+ timeStamp + ".jpg"
						);

				try {
					FileOutputStream stream = new FileOutputStream(mediaFile);
					//				stream.write(data);
					bitmapCropped.compress(CompressFormat.JPEG, 100, stream);
					resultCropUri = Uri.fromFile(mediaFile);
				} catch (IOException exception) {


					Log.w(TAG, "IOException during saving bitmap", exception);
					return null;
				}

				MediaScannerConnection.scanFile(
						mContext,
						new String[] { mediaFile.toString() },
						new String[] { "image/jpeg" },
						null
						);
			}else{
				//dont save to gallery
				File privateMediaStorage = new File(mContext.getFilesDir(), "captured_image");

				if (!privateMediaStorage.exists()) {
					if (!privateMediaStorage.mkdirs()) {
						return null;
					}
				}

				String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
				File mediaFile = new File(
						privateMediaStorage.getPath() + File.separator + "fragmentTest_"+ timeStamp + ".jpg"
						);

				try {
					FileOutputStream stream = new FileOutputStream(mediaFile);
					bitmapCropped.compress(CompressFormat.JPEG, 100, stream);
					resultCropUri = Uri.fromFile(mediaFile);
				} catch (IOException exception) {
					Log.w(TAG, "IOException during saving bitmap", exception);
					return null;
				}
			}

			return bitmapCropped;
		}

		@Override
		protected void onPostExecute(Bitmap bitmap){

			if(mDialog.isShowing()){
				mDialog.cancel();
			}

			if(bitmap != null){	
				img.setImageBitmap(bitmap);
				splitImage(img, chunkNumbers);
			}	
		}
	}

	@Override
	public void onResume() {
		super.onResume();

		// Use mCurrentCamera to select the camera desired to safely restore
		// the fragment after the camera has been changed
		System.out.println("onResume called");

		if(mCamera == null){
			newOpenCamera(mCurrentCamera);
		}
	}

	private void oldOpenCamera(int currentCamera){
		try{
			mCamera = Camera.open(currentCamera);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	private void newOpenCamera(int currentCamera){
		if(mThread == null){
			mThread = new CameraHandlerThread();
		}

		synchronized (mThread) {
			mThread.openCamera(currentCamera);
		}
	}

	private CameraHandlerThread mThread = null;
	private class CameraHandlerThread extends HandlerThread {
		Handler mHandler = null;

		CameraHandlerThread(){
			super("CameraHandlerThread");
			start();
			mHandler = new Handler(getLooper());
		}

		synchronized void notifyCameraOpened(){
			notify();
		}

		void openCamera(final int currentCamera){
			mHandler.post(new Runnable() {

				@Override
				public void run() {
					oldOpenCamera(currentCamera);
					notifyCameraOpened();

					ReplyCamera.this.runOnUiThread(new Runnable(){

						@Override
						public void run() {
							mCameraCurrentlyLocked = mCurrentCamera;
							mCamera.setDisplayOrientation(90);
							mPreview.setCamera(mCamera);
							if(mFrame.getChildCount() > 0){
								mFrame.removeAllViews();
							}
							mFrame.addView(mPreview);
							mCamera.startPreview();
						}
					});
				}
			});
			//			try{
			//				wait();
			//			}catch(InterruptedException e){
			//				e.printStackTrace();
			//			}
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		System.out.println("onPause called");
		
		// Because the Camera object is a shared resource, it's very
		// important to release it when the activity is paused.
		if (mCamera != null) {
			mCamera.stopPreview();
			mPreview.setCamera(null);
			mCamera.setPreviewCallback(null);
			mCamera.release();
			mCamera = null;
		}
		mFrame.removeAllViews();

		if (mThread != null && mThread.isAlive()){
			mThread.interrupt();
		}

	}

	/** A basic Camera preview class */
	class ReplyCameraPreview extends ViewGroup implements SurfaceHolder.Callback {

		//	private static final String FOCUS_MODE_CONTINUOUS_PICTURE = "continuous-picture";

		private final String TAG = "Preview";

		SurfaceView mSurfaceView;
		SurfaceHolder mHolder;
		Size mPreviewSize;
		List<Size> mSupportedPreviewSizes;
		Camera mCamera;
		boolean mSurfaceCreated = false;

		int width,height;

		//	public boolean meteringAreaSupported;

		double displayAspectRatio;

		Context mContext;
		ImageView switchCamera;

		Point displaySize = new Point();

		public int getLocalWidth(){
			return width;
		}
		public int getLocalHeight(){
			return height;
		}

		ReplyCameraPreview(Context context) {
			super(context);

			mContext = context;

			mSurfaceView = new SurfaceView(context);

			addView(mSurfaceView);

			// Install a SurfaceHolder.Callback so we get notified when the
			// underlying surface is created and destroyed.
			mHolder = mSurfaceView.getHolder();
			mHolder.addCallback(this);
			mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

			Display display = ((Activity)context).getWindowManager().getDefaultDisplay();
			display.getSize(displaySize);
		}

		public void setCamera(Camera camera) {
			mCamera = camera;
			if (mCamera != null) {
				mSupportedPreviewSizes = mCamera.getParameters().getSupportedPreviewSizes();
				if (mSurfaceCreated){
					requestLayout();
				}
			}
		}

		public Camera getCamera(){
			return mCamera;
		}

		public void switchCamera(Camera camera) {
			setCamera(camera);
			try {
				camera.setPreviewDisplay(mHolder);
			} catch (IOException exception) {
				Log.e(TAG, "IOException caused by setPreviewDisplay()", exception);
			}
		}

		Point screenSize = new Point();

		@Override
		protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
			// We purposely disregard child measurements because act as a
			// wrapper to a SurfaceView that centers the camera preview instead
			// of stretching it.

			width = resolveSize(getSuggestedMinimumWidth(),
					widthMeasureSpec);
			height = resolveSize(getSuggestedMinimumHeight(),
					heightMeasureSpec);

			setMeasuredDimension(width, height);

			if (mSupportedPreviewSizes != null) {
				mPreviewSize = getOptimalPreviewSize(mSupportedPreviewSizes, width, height);
			}

			if (mCamera != null) {
				Camera.Parameters parameters = mCamera.getParameters();
				parameters.setPreviewSize(mPreviewSize.width, mPreviewSize.height);

				System.out.println("mPreviewSize.width: "  + mPreviewSize.width);
				System.out.println("mPreviewSize.height: " + mPreviewSize.height);

				try{
					mCamera.setParameters(parameters);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}

		@Override
		protected void onLayout(boolean changed, int l, int t, int r, int b) {
			if (getChildCount() > 0) {
				final View child = getChildAt(0);

				Display display = ((Activity) mContext).getWindowManager().getDefaultDisplay();
				display.getSize(screenSize);
				int width = screenSize.x;
				int height = screenSize.y;

				int previewWidth = width;
				int previewHeight = height;
				if (mPreviewSize != null) {
					previewWidth = mPreviewSize.height;
					previewHeight = mPreviewSize.width;
					System.out.println("inside onLayout...mPreviewSize.width = " + mPreviewSize.width);
					System.out.println("inside onLayout...mPreviewSize.height = " + mPreviewSize.height);
				}

				// Center the child SurfaceView within the parent.
				if (width * previewHeight > height * previewWidth) {
					final int scaledChildWidth = previewWidth * height / previewHeight;
					child.layout((width - scaledChildWidth) / 2, 0, (width + scaledChildWidth) / 2, height);
				} else {
					final int scaledChildHeight = previewHeight * width / previewWidth;
					child.layout(0, (height - scaledChildHeight) / 2, width, (height + scaledChildHeight) / 2);
				}
			}
		}

		public void surfaceCreated(SurfaceHolder holder) {
			// The Surface has been created, acquire the camera and tell it where
			// to draw.
			try {
				if (mCamera != null) {

					mCamera.setPreviewDisplay(holder);

				}
			} catch (IOException exception) {
				Log.e(TAG, "IOException caused by setPreviewDisplay()", exception);
			}
			if (mPreviewSize == null) requestLayout();
			mSurfaceCreated = true;
		}

		public void surfaceDestroyed(SurfaceHolder holder) {
			// Surface will be destroyed when we return, so stop the preview.
		}

		private Size getOptimalPreviewSize(List<Size> sizes, int w, int h) {
			final double ASPECT_TOLERANCE = 0.05;
			//		double targetRatio = (double) w / h;
			double targetRatio = (double) 3 / 4;
			//		double targetRatio = displayAspectRatio;
			System.out.println("target ratio = " + targetRatio);
			if (sizes == null)
				return null;

			Size optimalSize = null;

			double minDiff = Double.MAX_VALUE;

			int targetHeight = h;

			System.out.println("targetHeight = " + targetHeight);

			// Try to find an size match aspect ratio and size
			for (Size size : sizes) {
				double ratio = (double) size.width / size.height;
				if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE)
					continue;
				if (Math.abs(size.height - targetHeight) < minDiff) {
					optimalSize = size;
					minDiff = Math.abs(size.height - targetHeight);
				}
			}

			// Cannot find the one match the aspect ratio, ignore the requirement
			if (optimalSize == null) {
				minDiff = Double.MAX_VALUE;
				for (Size size : sizes) {
					if (Math.abs(size.height - targetHeight) < minDiff) {
						optimalSize = size;
						minDiff = Math.abs(size.height - targetHeight);
					}
				}
			}
			System.out.println("optimal size: " + optimalSize.width + " x " + optimalSize.height);
			if(optimalSize.width < optimalSize.height){
				int temp = optimalSize.width;
				optimalSize.width = optimalSize.height;
				optimalSize.height = temp;
			}

			//			if((optimalSize.height > displaySize.y) || (optimalSize.width > displaySize.x)){
			//				//optimal size selected is larger than the screen, so recall this function after removeing this size.
			//				sizes.remove(optimalSize);
			//				getOptimalPreviewSize(sizes, w, h);
			//			}

			System.out.println("optimal size selected: " + optimalSize.width + " x " + optimalSize.height);

			return optimalSize;

		}

		public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
			// Now that the size is known, set up the camera parameters and begin
			// the preview.

			System.out.println("surfaceChanged called");

			if(mCamera != null){
				Camera.Parameters parameters = mCamera.getParameters();

				mPreviewSize = getOptimalPreviewSize(parameters.getSupportedPreviewSizes(), width, height);

				List<String> focusModes = parameters.getSupportedFocusModes();
				if (focusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
					parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
					System.out.println("continuous autofocus is supported");
				}else{
					parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
				}

				System.out.println("surfaceChanged preview sizes set as: " + mPreviewSize.width + " x " + mPreviewSize.height);

				parameters.setPreviewSize(mPreviewSize.width, mPreviewSize.height);
				parameters.setWhiteBalance(Camera.Parameters.WHITE_BALANCE_AUTO);

				requestLayout();

				mCamera.setParameters(parameters);

				mCamera.setDisplayOrientation(90);

				mCamera.startPreview();

			}
		}
	}
}

