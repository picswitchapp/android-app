package com.picswitch;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.widget.Toast;

class CropImageView extends ImageViewTouchBase {

	ArrayList<HighlightView> mHighlightViews      = new ArrayList<HighlightView>();
	HighlightView            mMotionHighlightView = null;
	float currX1, currX2, currY1, currY2, oldX1, oldX2, oldY1, oldY2, currDist, oldDist, dXcurr, dYcurr = 0;
	int mMotionEdge;
	//	private float scale = 1f;

//	ScaleGestureDetector mScaleGestureDetector;
//	View.OnTouchListener onTouchListener;

	private Context mContext;

	@Override
	protected void onLayout(boolean changed, int left, int top,
			int right, int bottom) {

		super.onLayout(changed, left, top, right, bottom);
		if (mBitmapDisplayed.getBitmap() != null) {
			for (HighlightView hv : mHighlightViews) {
				hv.mMatrix.set(getImageMatrix());
				hv.invalidate();
				if (hv.mIsFocused) {
					centerBasedOnHighlightView(hv);
				}
			}
		}
	}

	public CropImageView(Context context, AttributeSet attrs) {

		super(context, attrs);
		this.mContext = context;

		//		mScaleGestureDetector = new ScaleGestureDetector(getContext(), new ScaleListener());
		//		onTouchListener = new View.OnTouchListener() {
		//			public boolean onTouch(View v, MotionEvent event) {
		//				return mScaleGestureDetector.onTouchEvent(event);
		//			}
		//		};
		//		for(HighlightView hv : mHighlightViews){
		//			hv.setOnTouchListener(onTouchListener);
		//		}
	}

	//	class MyGestureDetector extends SimpleOnGestureListener {
	//		public MyGestureDetector(Context ctx){
	//
	//		}
	//
	//		public boolean onTouchEvent(MotionEvent event){
	//
	//			switch(event.getAction()){
	//			case MotionEvent.ACTION_DOWN:{
	//				//Toast.makeText(getContext(), "shit works yo", Toast.LENGTH_SHORT).show();
	//				System.out.println("Touch on Highlight View recognized");
	//				break;
	//			}
	//			}
	//
	//			return false;
	//		}
	//	}

	@Override
	protected void zoomTo(float scale, float centerX, float centerY) {

		super.zoomTo(scale, centerX, centerY);
		for (HighlightView hv : mHighlightViews) {
			hv.mMatrix.set(getImageMatrix());
			hv.invalidate();
		}
	}

	@Override
	protected void zoomIn() {

		super.zoomIn();
		for (HighlightView hv : mHighlightViews) {
			hv.mMatrix.set(getImageMatrix());
			hv.invalidate();
		}
	}

	@Override
	protected void zoomOut() {

		super.zoomOut();
		for (HighlightView hv : mHighlightViews) {
			hv.mMatrix.set(getImageMatrix());
			hv.invalidate();
		}
	}

	@Override
	protected void postTranslate(float deltaX, float deltaY) {

		super.postTranslate(deltaX, deltaY);
		for (int i = 0; i < mHighlightViews.size(); i++) {
			HighlightView hv = mHighlightViews.get(i);
			hv.mMatrix.postTranslate(deltaX, deltaY);
			hv.invalidate();
		}
	}

	// According to the event's position, change the focus to the first
	// hitting cropping rectangle.
	private void recomputeFocus(MotionEvent event) {

		for (int i = 0; i < mHighlightViews.size(); i++) {
			HighlightView hv = mHighlightViews.get(i);
			hv.setFocus(false);
			hv.invalidate();
		}

		for (int i = 0; i < mHighlightViews.size(); i++) {
			HighlightView hv = mHighlightViews.get(i);
			int edge = hv.getHit(event.getX(), event.getY());
			if (edge != HighlightView.GROW_NONE) {
				if (!hv.hasFocus()) {
					hv.setFocus(true);
					hv.invalidate();
				}
				break;
			}
		}
		invalidate();
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {

		CropImage cropImage = (CropImage) mContext;
		if (cropImage.mSaving) {
			return false;
		}

		switch (event.getAction()) {

		case MotionEvent.ACTION_DOWN:
			if (cropImage.mWaitingToPick) {
				recomputeFocus(event);
			} else {
				for (int i = 0; i < mHighlightViews.size(); i++) {
					HighlightView hv = mHighlightViews.get(i);
					int edge = hv.getHit(event.getX(), event.getY());
					if (edge != HighlightView.GROW_NONE) {
						mMotionEdge = edge;
						mMotionHighlightView = hv;
						currX1 = event.getX();
						currY1 = event.getY();
						mMotionHighlightView.setMode(
								(edge == HighlightView.MOVE)
								? HighlightView.ModifyMode.Move
										: HighlightView.ModifyMode.Grow);
						break;
					}
				}
			}
			break;

		case MotionEvent.ACTION_UP:
			if (cropImage.mWaitingToPick) {
				for (int i = 0; i < mHighlightViews.size(); i++) {
					HighlightView hv = mHighlightViews.get(i);
					if (hv.hasFocus()) {
						cropImage.mCrop = hv;
						for (int j = 0; j < mHighlightViews.size(); j++) {
							if (j == i) {
								continue;
							}
							mHighlightViews.get(j).setHidden(true);
						}
						centerBasedOnHighlightView(hv);
						((CropImage) mContext).mWaitingToPick = false;
						return true;
					}
				}
			} else if (mMotionHighlightView != null) {
				centerBasedOnHighlightView(mMotionHighlightView);
				mMotionHighlightView.setMode(
						HighlightView.ModifyMode.None);
			}
			mMotionHighlightView = null;
			break;

		case MotionEvent.ACTION_MOVE:
			//int pointerCount = event.getPointerCount();

			//one finger motion
			//if (pointerCount == 1){
				if (cropImage.mWaitingToPick) {
					recomputeFocus(event);
				} else if (mMotionHighlightView != null) {
					mMotionHighlightView.handleMotion(mMotionEdge,
							event.getX() - currX1,
							event.getY() - currY1);
					currX1 = event.getX();
					currY1 = event.getY();

					if (true) {
						// This section of code is optional. It has some user
						// benefit in that moving the crop rectangle against
						// the edge of the screen causes scrolling but it means
						// that the crop rectangle is no longer fixed under
						// the user's finger.
						ensureVisible(mMotionHighlightView);
					}
				}
			//}
			//two finger motion
			//get information on the two fingers
//			else {
//				for(int i = 0; i < event.getPointerCount(); ++i)
//				{
//					if(i == 0){
//						if (cropImage.mWaitingToPick) {
//							recomputeFocus(event);
//						} else if (mMotionHighlightView != null) {
//							//							mMotionHighlightView.handleMotion(mMotionEdge,
//							//									event.getX() - mLastX_1,
//							//									event.getY() - mLastY_1);
//							oldX1 = currX1;
//							oldY1 = currY1;
//							currX1 = event.getX();
//							currY1 = event.getY();
//
//							if (true) {
//								//see comment above
//								ensureVisible(mMotionHighlightView);
//							}
//						}
//					}
//
//					if(i == 1){
//						if (cropImage.mWaitingToPick) {
//							recomputeFocus(event);
//						} else if (mMotionHighlightView != null) {
//							//							mMotionHighlightView.handleMotion(mMotionEdge,
//							//									event.getX() - mLastX_2,
//							//									event.getY() - mLastY_2);
//							oldX2 = currX2;
//							oldY2 = currY2;
//							currX2= event.getX();
//							currY2 = event.getY();
//
//							if (true) {
//								//see comments above
//								ensureVisible(mMotionHighlightView);
//							}
//						}
//					}
//				}
//				//handle the different finger motions
//				
//				oldDist = (float) Math.sqrt((oldX1 - oldX2)*(oldX1 - oldX2) + (oldY1 - oldY2)*(oldY1 - oldY2));
//				currDist = (float) Math.sqrt((currX1 - currX2)*(currX1 - currX2) + (currY1 - currY2)*(currY1 - currY2));
//				
//				dXcurr = Math.abs((oldX1 - oldX2) - (currX1 - currX2));
//				dYcurr = Math.abs((oldY1 - oldY2) - (currY1 - currX2));
//
//				if (mMotionHighlightView != null){
//					if(currDist > oldDist){
//						//the two finger points are farther away, so zoom in, so positive dx, dy
//						mMotionHighlightView.handleMotion(HighlightView.PINCH_IN, dXcurr, dYcurr);
//					}else{
//						//two finger points are moving away - so zoom out, so negative dx, dy
//						mMotionHighlightView.handleMotion(HighlightView.PINCH_OUT, dXcurr, dYcurr);					
//					}
//				}
//			}

			switch (event.getAction()) {
			case MotionEvent.ACTION_UP:
				center(true, true);
				break;
			case MotionEvent.ACTION_MOVE:
				// if we're not zoomed then there's no point in even allowing
				// the user to move the image around.  This call to center puts
				// it back to the normalized location (with false meaning don't
				// animate).
				if (getScale() == 1F) {
					center(true, true);
				}
				break;
			}


		}
		return true;
	}

	// Pan the displayed image to make sure the cropping rectangle is visible.
	private void ensureVisible(HighlightView hv) {

		Rect r = hv.mDrawRect;

		int panDeltaX1 = Math.max(0, mLeft - r.left);
		int panDeltaX2 = Math.min(0, mRight - r.right);

		int panDeltaY1 = Math.max(0, mTop - r.top);
		int panDeltaY2 = Math.min(0, mBottom - r.bottom);

		int panDeltaX = panDeltaX1 != 0 ? panDeltaX1 : panDeltaX2;
		int panDeltaY = panDeltaY1 != 0 ? panDeltaY1 : panDeltaY2;

		if (panDeltaX != 0 || panDeltaY != 0) {
			panBy(panDeltaX, panDeltaY);
		}
	}

	// If the cropping rectangle's size changed significantly, change the
	// view's center and scale according to the cropping rectangle.
	private void centerBasedOnHighlightView(HighlightView hv) {

		Rect drawRect = hv.mDrawRect;

		float width = drawRect.width();
		float height = drawRect.height();

		float thisWidth = getWidth();
		float thisHeight = getHeight();

		float z1 = thisWidth / width * .6F;
		float z2 = thisHeight / height * .6F;

		float zoom = Math.min(z1, z2);
		zoom = zoom * this.getScale();
		zoom = Math.max(1F, zoom);
		if ((Math.abs(zoom - getScale()) / zoom) > .1) {
			float[] coordinates = new float[]{hv.mCropRect.centerX(),
					hv.mCropRect.centerY()};
			getImageMatrix().mapPoints(coordinates);
			zoomTo(zoom, coordinates[0], coordinates[1], 300F);
		}

		ensureVisible(hv);
	}

	@Override
	protected void onDraw(Canvas canvas) {

		super.onDraw(canvas);
		for (int i = 0; i < mHighlightViews.size(); i++) {
			mHighlightViews.get(i).draw(canvas);
		}
	}

	public void add(HighlightView hv) {

		mHighlightViews.add(hv);
		invalidate();
	}

	//	public class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
	//		@Override
	//		public boolean onScale(ScaleGestureDetector detector) {
	//			Matrix matr = new Matrix();
	//			scale *= detector.getScaleFactor();
	//			scale = Math.max(0.1f, Math.min(scale, 5.0f));
	//			matr.setScale(scale, scale);
	//			for(HighlightView hv : mHighlightViews){
	//				hv.setMatrix(matr);
	//			}
	//			return true;
	//		}
	//
	//		public boolean onTouchEvent(MotionEvent event){
	//			Toast.makeText(getContext(), "ScaleListener.onTouchEvent", Toast.LENGTH_SHORT).show();
	//			return false;
	//		}
	//	}
}
