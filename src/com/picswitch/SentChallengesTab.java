package com.picswitch;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;


/**Sent challenges tab
 * Specifies what happens in the Sent picswitch tab of the TabActivity class*/
@SuppressLint("NewApi")
public class SentChallengesTab extends ListFragment {
	protected List<ParseObject> mMessages;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View newTab = inflater.inflate(R.layout.sent_challenges_tab, container, false);
		return newTab;
	}


	@Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
		getActivity().setProgressBarIndeterminate(true);

		ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(ParseConstants.CLASS_MESSAGES);
		query.whereEqualTo(ParseConstants.KEY_SENDER_ID, 
				ParseUser.getCurrentUser().getObjectId());
		query.addDescendingOrder(ParseConstants.KEY_CREATED_AT);
		
		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> messages, ParseException e) {
				getActivity().setProgressBarIndeterminate(false);
				if(e==null){

					//Messages are available
					mMessages = messages;

					String[] usernames = new String[mMessages.size()];
					int i = 0;
					for(ParseObject message : mMessages){
						usernames[i] = message.getString(ParseConstants.KEY_SENDER_NAME);
						i++;
					}
					SentMessageAdapter adapter = new SentMessageAdapter(getListView().getContext(),
							mMessages);
					setListAdapter(adapter);
				}

			}
		});

	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}



	@Override
	public void onListItemClick(ListView list, View v, int position, long id) {
		super.onListItemClick(list, v, position, id);
		ParseObject message = mMessages.get(position);
		String messageType = message.getString(ParseConstants.KEY_FILE_TYPE);
		ParseFile file = message.getParseFile(ParseConstants.KEY_FILE);
		Uri fileUri = Uri.parse(file.getUrl());
		Intent intent = new Intent(getActivity(), SentImageActivity.class);
		intent.setData(fileUri);
		startActivity(intent);


	}


}
