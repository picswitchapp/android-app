package com.picswitch;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
public class AdditionalPuzzlesAdapter extends BaseAdapter{   
	String [] result;
	Context context;
	String puzzleClass = "";
	protected List<ParseObject> additionalPuzzles;
	int [] imageId;
	private static LayoutInflater inflater=null;
	public AdditionalPuzzlesAdapter(Context c, String[] puzzleNames, int[] puzzleImages) {
		TabActivity mainActivity;
		result=puzzleNames;
		context=c;
		imageId=puzzleImages;
		inflater = ( LayoutInflater )context.
				getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	@Override
	public int getCount() {
		return result.length;
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public class Holder
	{
		TextView tv;
		ImageView img;
	}
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Holder holder=new Holder();
		View rowView;       
		rowView = inflater.inflate(R.layout.additional_puzzles_adapter, null);
		holder.tv=(TextView) rowView.findViewById(R.id.puzzle_name);
		holder.img=(ImageView) rowView.findViewById(R.id.imageView1);       
		holder.tv.setText(result[position]);
		holder.img.setImageResource(imageId[position]);         
		rowView.setOnClickListener(new OnClickListener() {            
			@Override
			public void onClick(View v) {
				Toast.makeText(context, "You Clicked " + position+result[position], Toast.LENGTH_LONG).show();
				switch(position){
					case 0:
						AdditionalPuzzlesSelector(ParseConstants.CLASS_ADVENTURE_AND_SPORTS);
						puzzleClass = ParseConstants.CLASS_ADVENTURE_AND_SPORTS;
						break;
					case 1:
						AdditionalPuzzlesSelector(ParseConstants.CLASS_ANIMALS);
						puzzleClass = ParseConstants.CLASS_ANIMALS;
						break;
					case 2:
						AdditionalPuzzlesSelector(ParseConstants.CLASS_ARCHITECTURE);
						puzzleClass = ParseConstants.CLASS_ARCHITECTURE;
						break;
					case 3:
						AdditionalPuzzlesSelector(ParseConstants.CLASS_CITIES);
						puzzleClass = ParseConstants.CLASS_CITIES;
						break;
					case 4:
						AdditionalPuzzlesSelector(ParseConstants.CLASS_JOKES_AND_PUNS);
						puzzleClass = ParseConstants.CLASS_JOKES_AND_PUNS;
						break;
					case 5:
						AdditionalPuzzlesSelector(ParseConstants.CLASS_LANDSCAPES);
						puzzleClass = ParseConstants.CLASS_LANDSCAPES;
						break;
					case 6:
						AdditionalPuzzlesSelector(ParseConstants.CLASS_MODERN_CULTURE);
						puzzleClass = ParseConstants.CLASS_MODERN_CULTURE;
						break;
					case 7:
						AdditionalPuzzlesSelector(ParseConstants.CLASS_NATURE);
						puzzleClass = ParseConstants.CLASS_NATURE;
						break;
					case 8:
						AdditionalPuzzlesSelector(ParseConstants.CLASS_ROMANTIC);
						puzzleClass = ParseConstants.CLASS_ROMANTIC;
						break;
					case 9:
						AdditionalPuzzlesSelector(ParseConstants.CLASS_TECHNOLOGY_AND_TRANSPORTATION);
						puzzleClass = ParseConstants.CLASS_TECHNOLOGY_AND_TRANSPORTATION;
						break;
					case 10:
						AdditionalPuzzlesSelector(ParseConstants.CLASS_VINTAGE);
						puzzleClass = ParseConstants.CLASS_VINTAGE;
						break;
				}

			}
		});   
		
		if(position % 2 == 1){
			rowView.findViewById(R.id.dateLabel).setBackgroundColor(Color.rgb(50, 153, 153));
			rowView.findViewById(R.id.additional_center_spacer).setBackgroundColor(Color.rgb(51, 51, 51));
			rowView.findViewById(R.id.puzzle_name).setBackgroundColor(Color.rgb(51, 51, 51));
			rowView.findViewById(R.id.puzzle_description).setBackgroundColor(Color.rgb(51, 51, 51));
		}
		
		return rowView;
	}

	/***Selects images from the class specified in the parameters
	 * queries to see the images the user hasn't solved yet
	 * starts the next intent based on the query*/
	public void AdditionalPuzzlesSelector(String pClass){
		ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(pClass);
		query.whereNotEqualTo(ParseConstants.KEY_COMPLETED_USERS, 
				ParseUser.getCurrentUser().getObjectId());
		query.addAscendingOrder(ParseConstants.KEY_CREATED_AT);
		query.findInBackground(new FindCallback<ParseObject>() {
			int count =0; 
			@Override
			public void done(List<ParseObject> messages, ParseException e) {
				if(e==null){
					//Messages are available
					additionalPuzzles = messages;
					ParseObject message = additionalPuzzles.get(count);
					String messageID = message.getObjectId();
					String firstCaption = message.getString(ParseConstants.CAPTION_FIRST);
					String secondCaption = message.getString(ParseConstants.CAPTION_SECOND);
					
					ParseFile file = message.getParseFile(ParseConstants.KEY_PHOTO);
					Uri fileUri = Uri.parse(file.getUrl());
					Intent intent = new Intent(context, AdditionalPuzzlesOpenImage.class);
					intent.setData(fileUri);
					intent.putExtra(ParseConstants.KEY_PUZZLE_COUNTER, 0); //tracks which puzzle the user is to solve
					intent.putExtra(ParseConstants.KEY_PUZZLE_CLASS, puzzleClass);
					intent.putExtra(ParseConstants.KEY_MESSAGE_ID, messageID);
					intent.putExtra(ParseConstants.CAPTION_FIRST, firstCaption);
					intent.putExtra(ParseConstants.CAPTION_SECOND, secondCaption);
					context.startActivity(intent);
				}

			}
		});
	}
	public void saveAdditionalPuzzles(List<ParseObject> messages){
		//use SQL database to save message aspects to db
		//use assynctask for speed
		//http://stackoverflow.com/questions/3501516/android-sqlite-database-slow-insertion
		//http://developer.android.com/training/basics/data-storage/databases.html
	}
}
