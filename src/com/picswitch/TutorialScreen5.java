package com.picswitch;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class TutorialScreen5 extends Fragment {
	
	Context mContext;

	public TutorialScreen5() {
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View mView = inflater.inflate(R.layout.tutorial_screen5, container, false);
		
		mContext = container.getContext();
		
		ImageView goOn = (ImageView) mView.findViewById(R.id.tutorial_image_5);
		goOn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				
				setSharedPrefs();
				
				Intent intent = new Intent(mContext, TabActivity.class);
				startActivity(intent);
				((Activity)mContext).finish();
			}
		});
		return mView;
	}
	
	public void setSharedPrefs(){
		SharedPreferences prefs = mContext.getSharedPreferences("com.picswitch", Context.MODE_PRIVATE);
		prefs.edit().putBoolean("tutorialSeen", true).apply();
	}

}
