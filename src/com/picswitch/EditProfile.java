package com.picswitch;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseCrashReporting;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;

public class EditProfile extends Activity {
	
	Typeface montserrat;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_profile);
		getActionBar().hide();
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
			window.setStatusBarColor(Color.rgb(32, 99, 99));
		}

		//back arrow
		ImageView leftArrow = (ImageView) findViewById(R.id.left_arrow_edit_profile);
		leftArrow.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				EditProfile.this.finish();
			}
		});

		//get current user name
		ParseUser mUser = ParseUser.getCurrentUser();
		String firstNameString = mUser.getString(ParseConstants.KEY_FIRST_NAME);
		String lastNameString = mUser.getString(ParseConstants.KEY_LAST_NAME);

		montserrat = Typeface.createFromAsset(getAssets(), "Montserrat-Regular.ttf");
		Typeface varella = Typeface.createFromAsset(getAssets(), "VarelaRound.ttf");

		TextView editProfileTitle = (TextView) findViewById(R.id.title_text_edit_profile);
		editProfileTitle.setTypeface(montserrat);
		TextView editProfileLabel = (TextView) findViewById(R.id.edit_profile_label);
		editProfileLabel.setTypeface(montserrat);
		TextView confirmEditProfile = (TextView) findViewById(R.id.confirm_edit_profile_button);
		confirmEditProfile.setTypeface(varella);
		TextView nameVisInfo = (TextView) findViewById(R.id.name_visibility_info);
		nameVisInfo.setTypeface(varella);
		TextView nameWarning = (TextView) findViewById(R.id.name_warning);
		nameWarning.setTypeface(varella);

		//editTexts
		final EditText firstName = (EditText) findViewById(R.id.first_name);
		firstName.setHint(firstNameString);
		firstName.setTypeface(varella);

		final EditText lastName = (EditText) findViewById(R.id.last_name);
		lastName.setHint(lastNameString);
		lastName.setTypeface(varella);

		RelativeLayout confirmButton = (RelativeLayout) findViewById(R.id.edit_profile_confirm_button);
		confirmButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				final String newFirstName = firstName.getText().toString();
				final String newLastName = lastName.getText().toString();

				if(ParseUser.getCurrentUser().getInt("nameChangeCount") != 1){

					if((newFirstName.length() > 0) && (newLastName.length() > 0)){
						//names are enterred, continue with dialogs
						AlertDialog.Builder mBuilder = new AlertDialog.Builder(EditProfile.this);
						mBuilder.setTitle("Warning");
						mBuilder.setMessage("You are only allowed to change your name once on picswitch."
								+ "  Are you sure you want to make this change?");
						mBuilder.setPositiveButton("Yes, change my name", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								//second confirm dialog					
								dialog.cancel();

								AlertDialog.Builder mmBuilder = new AlertDialog.Builder(EditProfile.this);
								mmBuilder.setTitle("Confirm name change");
								mmBuilder.setMessage("Your name will be chaged to " + newFirstName + " " + newLastName + "." + 
										"  You will not be able to change your name again.  Continue?");
								mmBuilder.setPositiveButton("Yes, change my name", new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										//actually change their name
										ParseUser mUser = ParseUser.getCurrentUser();
										mUser.put(ParseConstants.KEY_FIRST_NAME, newFirstName);
										mUser.put(ParseConstants.KEY_LAST_NAME, newLastName);
										mUser.put("nameChangeCount", 1);
										mUser.saveInBackground(new SaveCallback() {
											@Override
											public void done(ParseException e) {
												if(e == null){
													//saved updated successfully
													Toast.makeText(EditProfile.this, "Name updated successfully", Toast.LENGTH_SHORT).show();
												}else{
													//saving failed
													Toast.makeText(EditProfile.this, "Updating name failed", Toast.LENGTH_SHORT).show();
												}
												EditProfile.this.finish();
											}
										});
										dialog.cancel();
									}
								});
								mmBuilder.setNegativeButton("No, don't change my name", new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										EditProfile.this.finish();
										dialog.cancel();
									}
								});
								AlertDialog mmDialog = mmBuilder.create();
								mmDialog.show();
								
								int titleId = getResources().getIdentifier( "alertTitle", "id", "android" );
								if (titleId > 0) {
								   TextView mTitle = (TextView) mmDialog.findViewById(titleId);
								   if (mTitle != null) {
									   mTitle.setTypeface(montserrat);
								   }
								}
								
								Button button1 = (Button) mmDialog.findViewById(android.R.id.button1);
								Button button2 = (Button) mmDialog.findViewById(android.R.id.button2);
								FontsUtils.TypeFace(button1, getAssets());
								FontsUtils.TypeFace(button2, getAssets());
								
								TextView mMessage = (TextView) mmDialog.findViewById(android.R.id.message);
								mMessage.setTextSize(15);
								FontsUtils.TypeFace(mMessage, getAssets());
							}
						});
						mBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								EditProfile.this.finish();
								dialog.cancel();
							}
						});
						AlertDialog mDialog = mBuilder.create();
						mDialog.show();
						
						int titleId = getResources().getIdentifier( "alertTitle", "id", "android" );
						if (titleId > 0) {
						   TextView mTitle = (TextView) mDialog.findViewById(titleId);
						   if (mTitle != null) {
							   mTitle.setTypeface(montserrat);
						   }
						}
						
						Button button1 = (Button) mDialog.findViewById(android.R.id.button1);
						Button button2 = (Button) mDialog.findViewById(android.R.id.button2);
						FontsUtils.TypeFace(button1, getAssets());
						FontsUtils.TypeFace(button2, getAssets());
						
						TextView mMessage = (TextView) mDialog.findViewById(android.R.id.message);
						mMessage.setTextSize(15);
						FontsUtils.TypeFace(mMessage, getAssets());
						
					}else{
						//entered names are not valid
						AlertDialog.Builder noNamesBuilder = new AlertDialog.Builder(EditProfile.this);
						noNamesBuilder.setTitle("Please enter both a first and last name");
						noNamesBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.cancel();
							}
						});
						noNamesBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.cancel();
								EditProfile.this.finish();
							}
						});
						AlertDialog noNamesDialog = noNamesBuilder.create();
						noNamesDialog.show();
						
						int titleId = getResources().getIdentifier( "alertTitle", "id", "android" );
						if (titleId > 0) {
						   TextView mTitle = (TextView) noNamesDialog.findViewById(titleId);
						   if (mTitle != null) {
							   mTitle.setTypeface(montserrat);
						   }
						}
						
						Button button1 = (Button) noNamesDialog.findViewById(android.R.id.button1);
						Button button2 = (Button) noNamesDialog.findViewById(android.R.id.button2);
						FontsUtils.TypeFace(button1, getAssets());
						FontsUtils.TypeFace(button2, getAssets());
					}
				}else{
					//user has already updated name
					AlertDialog.Builder alreadyChanged = new AlertDialog.Builder(EditProfile.this);
					alreadyChanged.setTitle("You have already changed your name.");
					alreadyChanged.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							EditProfile.this.finish();
							dialog.cancel();
						}
					});
					AlertDialog alreadyDialog = alreadyChanged.create();
					alreadyDialog.show();
					int titleId = getResources().getIdentifier( "alertTitle", "id", "android" );
					if (titleId > 0) {
					   TextView mTitle = (TextView) alreadyDialog.findViewById(titleId);
					   if (mTitle != null) {
						   mTitle.setTypeface(montserrat);
					   }
					}
					Button button1 = (Button) alreadyDialog.findViewById(android.R.id.button1);
					Button button2 = (Button) alreadyDialog.findViewById(android.R.id.button2);
					FontsUtils.TypeFace(button1, getAssets());
					FontsUtils.TypeFace(button2, getAssets());
				}
			}


		});

	}
}
