package com.picswitch;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.Display;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

/**Class that handles user manipulation of the PicSwitch
 * Allows user to move images around 
 * Detects when the user has solved the PicSwitch*/
public class AdditionalPuzzlesOpenImage extends ActionBarActivity{
	private ImageView image1, image2, image3,image4,image5,image6, image7,image8,image9;
	private GestureDetector gestureDetector;
	int animationToRun = R.anim.fadeout; //animation to be used when user touches PicSwitch
	public int count=0, puzzleCounter; //puzzleCounter keeps track of the puzzle the user is to solve
	//if the user clicks next puzzle it increments by 1, if the user solves a puzzle and clicks to
	//solve the next one, it stays at 0 since the list retrieved from parse won't 
	//include the one the user has solved
	//images of the PicSwitch
	Bitmap img, img0, img1, img2,img3,img4,img5,img6, img7, img8; 
	ActionBar actionBar;
	RelativeLayout rLayout;
	ArrayList<Integer> images = new ArrayList<Integer>(Arrays.asList(0,1,2,3,4,5,6,7,8));
	ArrayList<Integer> eightStepTmp = new ArrayList<Integer>(Arrays.asList(-1,-1,-1,-1,-1,-1,-1,-1,-1));
	View.OnTouchListener onTouchListener;
	SurfaceHolder s;
	int chunkNumbers =9, switchCounter=0;
	ImageView placeholder;
	private String firstCaption, secondCaption, solveStatus;
	String puzzleClass = "";
	protected List<ParseObject> additionalPuzzles;
	Uri imageUri;
	private boolean allowSkip = false;

	TextView timerTextView;

	int currentInt;
	String tempSkipped;

	String currentObjectId;
	String currCategoryTitle;

	String[] data;

	String[] titles;
	String[] puzzleClasses;

	ProgressDialog mDialog;

	SoundPool sounds;
	int switchID;
	int nextID;
	int doneID;
	
	long preciseStartTime;
	long preciseEndTime;
	double timeToDisplay;

	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi") @Override
	protected void onCreate(Bundle savedInstanceState) {

		getWindow().requestFeature(Window.FEATURE_ACTION_BAR);

		super.onCreate(savedInstanceState);
		getWindow().setFormat(PixelFormat.RGBA_8888);
		getActionBar().hide();

		rLayout =  (RelativeLayout)findViewById(R.layout.additional_puzzles_open_image);
		setContentView(R.layout.additional_puzzles_open_image);

		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
			window.setStatusBarColor(Color.rgb(32, 99, 99));
		}
		tempSkipped = getIntent().getExtras().getString(ParseConstants.KEY_TEMP_SOLVED);
		currentInt = getIntent().getExtras().getInt(ParseConstants.KEY_CURRENT_INT);

		Bundle b;
		b = getIntent().getExtras();
		data = b.getStringArray(ParseConstants.KEY_ADDITIONAL_DATA);

		int length = data.length;
		currCategoryTitle = data[currentInt];
		puzzleClass = data[(length/2) + currentInt];

		System.out.println("tempSolved received as: " + tempSkipped);
		
		//create new eight step thing
		//first step, shuffle the stock array
		Collections.shuffle(images);
		
		for(int i = 0; i < 9; i++){
			eightStepTmp.set(i, (images.get((images.indexOf(i) + 1) % 9)));
		}

		System.out.println("eight step: " + eightStepTmp.toString());

		images = eightStepTmp;

		System.out.println("images step: " + images.toString());

		//puzzleClass = getIntent().getExtras().getString(ParseConstants.KEY_PUZZLE_CLASS);
		puzzleCounter = getIntent().getExtras().getInt(ParseConstants.KEY_PUZZLE_COUNTER);
		//currCategoryTitle = getIntent().getExtras().getString(ParseConstants.KEY_CATEGORY_TITLE);

		Typeface montSerrat = Typeface.createFromAsset(getAssets(),"Montserrat-Regular.ttf");
		TextView titleText = (TextView) findViewById(R.id.add_open_image_title_text);
		titleText.setText(currCategoryTitle);
		titleText.setTypeface(montSerrat);

		restoreFromBackgroundQuery(puzzleClass);

		//firstCaption = getIntent().getExtras().getString(ParseConstants.CAPTION_FIRST);
		//secondCaption = getIntent().getExtras().getString(ParseConstants.CAPTION_SECOND);

		//placeholder = (ImageView)findViewById(R.id.imageView01);

		actionBar = getActionBar(); //Set up action bar
		//setupActionBar();
		timerTextView = (TextView) findViewById(R.id.timer);
		timerTextView.setText("00");
		buttonsHelper(); //handles buttons
		//titleBarHelper(savedInstanceState); //handles title bar

		makeProgressDialog();

		//set up the sounds
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
			
			AudioAttributes mAttributes = new AudioAttributes.Builder()
			  .setUsage(AudioAttributes.USAGE_GAME)
			  .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
			  .build();
			sounds = new SoundPool.Builder().setAudioAttributes(mAttributes).build();
			
		}else{
			sounds = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
		}
		switchID    = sounds.load(AdditionalPuzzlesOpenImage.this, R.raw.switches, 1);
		doneID      = sounds.load(AdditionalPuzzlesOpenImage.this, R.raw.solved_puzzle, 1);
		nextID      = sounds.load(AdditionalPuzzlesOpenImage.this, R.raw.next_puzzle, 1);

	}

	public void makeProgressDialog(){
		mDialog = new ProgressDialog(AdditionalPuzzlesOpenImage.this);
		mDialog.setMessage("Loading image...");
		mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		mDialog.setIndeterminate(true);
		mDialog.show();
	}

	/** Query the local data store for additional puzzles information */
	public void restoreFromBackgroundQuery(String category){
		//		ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(category);
		//		query.fromLocalDatastore();
		//		ParseQuery<ParseObject> userQuery = new ParseQuery<ParseObject>(ParseConstants.CLASS_PUZZLES_SOLVED);
		//		userQuery.fromLocalDatastore();

		//		makeProgressDialog();

		String t = tempSkipped;
		if(t.contains(" ")){
			t = t.replaceAll(" ", "");
		}

		//		if(!(t.contains("\""))){
		//			t = "\"" + t + "\"";
		//			int i = 0;
		//			while(i < t.length()){
		//				if(t.substring(i).contains(",")){
		//					i = t.indexOf(",", i);
		//					t = t.substring(0,i) + "\"" + ",\"" + t.substring(i+1);
		//					i += 2; 
		//				}else{
		//					break;
		//				}
		//			}
		//		}

		List<String> tmpSkipped = Arrays.asList(t.split(","));

		System.out.println("tmpSkipped: "+ tmpSkipped);

		//		List<String> tmp = new ArrayList<String>();
		//		tmp.addAll(tmpSkipped);
		//		tmp.addAll(tmpSolved);

		System.out.println("after pass: " + tmpSkipped);

		ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(category);
		query.whereNotContainedIn(ParseConstants.KEY_OBJECT_ID, tmpSkipped);
		query.addAscendingOrder(ParseConstants.KEY_CREATED_AT);

		//		query.whereDoesNotMatchKeyInQuery(ParseConstants.KEY_OBJECT_ID,
		//					ParseConstants.KEY_PUZZLES_SOLVED, userQuery);	
		query.getFirstInBackground(new GetCallback<ParseObject>() {

			@Override
			public void done(ParseObject object, ParseException e) {

				if(e == null){
					System.out.println("outside of everything");
					if(object != null){
						ParseFile file = object.getParseFile(ParseConstants.KEY_PHOTO);
						imageUri = Uri.parse(file.getUrl());


						firstCaption = object.getString(ParseConstants.CAPTION_FIRST);
						secondCaption = object.getString(ParseConstants.CAPTION_SECOND);

						Picasso.with(getBaseContext()).load(imageUri.toString()).into(target);
						TextView firstCap = (TextView) findViewById(R.id.firstCaption);
						firstCap.setText(firstCaption);
						currentObjectId = object.getObjectId();
						System.out.println("inside of object != null");

						System.out.println("firstCaption: " + firstCaption);
						System.out.println("secondCaption: " + secondCaption);
						

					}else{
						//						Toast.makeText(getBaseContext(),"done, yo", Toast.LENGTH_SHORT).show();
						nextCategoryCalculator();
					}

				}else{
					System.out.println("parse error!!");
					nextCategoryCalculator();

				}
			}

		});
	}

	public void nextCategoryCalculator(){
		currentInt += 1;
		if(currentInt == (data.length)/2){
			currentInt = 0;
		}
		puzzleClass = data[(data.length / 2) + currentInt];
		currCategoryTitle = data[currentInt];
		((TextView) findViewById(R.id.add_open_image_title_text)).setText(currCategoryTitle);

		restoreFromBackgroundQuery(puzzleClass);
	}

	/**Is a query that updates the easy column when a user completes an easy puzzle*/
	public void statsQuery(){ 
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put(ParseConstants.KEY_STATS_USERNAME, ParseUser.getCurrentUser().getUsername());
		params.put(ParseConstants.KEY_STATS_SWITCHCOUNTER, switchCounter);
		params.put(ParseConstants.KEY_STATS_PUZZLEDIFFICULTY, ParseConstants.ADDITIONAL_STATISTICS);
		params.put(ParseConstants.KEY_STATS_TIME, timeToDisplay);
		params.put(ParseConstants.KEY_STATS_CATEGORY, ParseConstants.KEY_STATS_ADDITIONAL);
		ParseCloud.callFunctionInBackground(ParseConstants.CLOUD_STATISTICS, params, new FunctionCallback<String>() {

			public void done(String success, ParseException e) {
				if (e == null) {
					//Toast.makeText(AdditionalPuzzlesOpenImage.this, "Stats", Toast.LENGTH_LONG).show();
				}
			}
		});
	}

	public void solveQuery(){
		ParseQuery<ParseObject> query = ParseQuery.getQuery(ParseConstants.CLASS_USER);
		query.getInBackground(ParseUser.getCurrentUser().getObjectId(), 
				new GetCallback<ParseObject>() {
			public void done(ParseObject user, ParseException e) {
				if (e == null) {
					//					Toast.makeText(AdditionalPuzzlesOpenImage.this, "Solved", Toast.LENGTH_LONG).show();
					user.add(ParseConstants.KEY_PUZZLES_SOLVED, currentObjectId);
					System.out.println("in solvequery");
					setSolveStatus(user);


				} else {
					e.printStackTrace();
				}
			}
		});
	}

	Target target = new Target() {

		int timerCount = 0;

		@Override
		public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
			if(mDialog.isShowing()){
				mDialog.cancel();
			}
			splitImage(bitmap, chunkNumbers);
			createImageFromBitmap(bitmap, ParseConstants.KEY_ADDITIONAL_PUZZLE_TEMP);
			imageViewsHelper(); //initializes image views
			imageViewsDimensions(); //sets up the dimensions of the imageviews
			assignImageViewPics(); //add images to imageViews
			setImageViewTouch();

			allowSkip = true;
			
			preciseStartTime = System.nanoTime();

			if(timerCount==0){
				startTime = System.currentTimeMillis();
				timerHandler.postDelayed(timerRunnable, 0);
				//TextView timer = (TextView) findViewById(R.id.open_image_timer);
				timerTextView.setBackground(getResources().getDrawable(R.drawable.timerblank));
				timerCount++;
			}

		}

		@Override
		public void onBitmapFailed(Drawable arg0) {

		}

		@Override
		public void onPrepareLoad(Drawable arg0) {


		}
	};


	protected void setSolveStatus(ParseObject user){
		user.saveInBackground(new SaveCallback() {

			@Override
			public void done(ParseException e) {
				if(e==null){
					//success!
					//Toast.makeText(AdditionalPuzzlesOpenImage.this, R.string.success_message, Toast.LENGTH_LONG).show();
					System.out.println("in setSolveStatus");
				}
				else{
					//means there was an error
					AlertDialog.Builder builder = new AlertDialog.Builder(AdditionalPuzzlesOpenImage.this);
					builder.setMessage(R.string.error_sending_message)
					.setTitle(R.string.error_selecting_file_title)
					.setPositiveButton(android.R.string.ok, null);
					AlertDialog dialog = builder.create();
					dialog.show();
				}
			}
		});
	}

	/**Sets up a custom actionbar*/
	@SuppressLint("NewApi")
	private void setupActionBar(){
		final ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(
				R.layout.solve_screen,
				null);
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setCustomView(actionBarLayout);
	}

	/**Creates images from the bitmaps*/
	public String createImageFromBitmap(Bitmap bitmap, String name) {
		String fileName = name;//no .png or .jpg needed
		try {
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
			FileOutputStream fo = openFileOutput(fileName, Context.MODE_PRIVATE);
			fo.write(bytes.toByteArray());
			// remember close file output
			fo.close();
		} catch (Exception e) {
			e.printStackTrace();
			fileName = null;
		}
		return fileName;

	}
	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_open_image,
					container, false);
			return rootView;
		}
	}


	/**Sets touch detection for imageViews*/
	public void setImageViewTouch(){
		//set touch for imageviews
		gestureDetector = new GestureDetector(this, new MyGestureDetector());
		onTouchListener = new View.OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				return gestureDetector.onTouchEvent(event);
			}
		};

		image1.setOnTouchListener(onTouchListener);
		image2.setOnTouchListener(onTouchListener);
		image3.setOnTouchListener(onTouchListener);
		image4.setOnTouchListener(onTouchListener);
		image5.setOnTouchListener(onTouchListener);
		image6.setOnTouchListener(onTouchListener);
		image7.setOnTouchListener(onTouchListener);
		image8.setOnTouchListener(onTouchListener);
		image9.setOnTouchListener(onTouchListener);
	}

	/**Assigns bitmaps from the SD to image Views*/
	public void assignImageViewPics(){
		//shuffle images arraylist 
		//Collections.shuffle(images);
		ImageView previewThumbnail = new ImageView(this);
		int[] ids= {R.id.imageView01,R.id.imageView02,R.id.imageView03,R.id.imageView04,R.id.imageView05,R.id.imageView06,R.id.imageView07,R.id.imageView08,R.id.imageView09};
		for(int i=0; i<9;i++){
			previewThumbnail=(ImageView)findViewById(ids[i]);
			Bitmap bitmap ;
			bitmap = chunkedImages.get(images.get(i)); //get image using images array that has been shuffled
			previewThumbnail.setImageBitmap(bitmap);
		}
	}


	/**Initializes the dimensions of the imageviews that hold the chunked 
	 * images*/
	public void imageViewsDimensions(){
		Display  display = getWindowManager().getDefaultDisplay();
		//width of the imageViews
		//minus 6 to allows for space between imageViews
		int swidth = (int) (display.getWidth()/3-6);
		image1.getLayoutParams().width = swidth;
		image1.getLayoutParams().height = swidth;
		image2.getLayoutParams().width = swidth;
		image2.getLayoutParams().height = swidth;
		image3.getLayoutParams().width = swidth;
		image3.getLayoutParams().height = swidth;
		image4.getLayoutParams().width = swidth;
		image4.getLayoutParams().height = swidth;
		image5.getLayoutParams().width = swidth;
		image5.getLayoutParams().height = swidth;
		image6.getLayoutParams().width = swidth;
		image6.getLayoutParams().height = swidth;
		image7.getLayoutParams().width = swidth;
		image7.getLayoutParams().height = swidth;
		image8.getLayoutParams().width = swidth;
		image8.getLayoutParams().height = swidth;
		image9.getLayoutParams().width = swidth;
		image9.getLayoutParams().height = swidth;
	}


	/**Initialize imageViews to hold chucked images*/
	public void imageViewsHelper(){
		image1 = (ImageView) findViewById(R.id.imageView01);
		image2 = (ImageView) findViewById(R.id.imageView02);
		image3 = (ImageView) findViewById(R.id.imageView03);
		image4 = (ImageView) findViewById(R.id.imageView04);
		image5 = (ImageView) findViewById(R.id.imageView05);
		image6 = (ImageView) findViewById(R.id.imageView06);
		image7 = (ImageView) findViewById(R.id.imageView07);
		image8 = (ImageView) findViewById(R.id.imageView08);
		image9 = (ImageView) findViewById(R.id.imageView09);
	}




	/**Helper for buttons*/
	public void buttonsHelper(){
		
		TextView nextText = (TextView) findViewById(R.id.next_picswitch_text);
		FontsUtils.TypeFace(nextText, getAssets());
		
		RelativeLayout skipThisPicswitchButton = (RelativeLayout) findViewById(R.id.next_picswitch_button);
		skipThisPicswitchButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				//AdditionalPuzzlesSelector(puzzleClass);

				if(allowSkip){
					
					if(TabActivity.SOUND){
						sounds.play(nextID, 1, 1, 0, 0, 1);
					}
					
					timerHandler.removeCallbacks(timerRunnable);

					if(currentObjectId == null){
						System.out.println("click with null objcet id");
					}

					if(tempSkipped == "[]" || tempSkipped.length() < 5){
						tempSkipped = currentObjectId;
					}else{
						tempSkipped = tempSkipped + "," + currentObjectId;
					}
					System.out.println("currentObjectId that was added: " + currentObjectId);
					System.out.println("tempSkipped created: " + tempSkipped);

					Bundle bundle = new Bundle();
					bundle.putStringArray(ParseConstants.KEY_ADDITIONAL_DATA, data);

					Intent intent = new Intent(getBaseContext(), AdditionalPuzzlesOpenImage.class);
					intent.putExtra(ParseConstants.KEY_CURRENT_INT, currentInt);
					intent.putExtra(ParseConstants.KEY_TEMP_SOLVED, tempSkipped);
					//intent.putExtra(ParseConstants.KEY_PUZZLE_CLASS, puzzleClass);
					intent.putExtra(ParseConstants.KEY_PUZZLE_COUNTER, puzzleCounter);
					//intent.putExtra(ParseConstants.KEY_CATEGORY_TITLE, currCategoryTitle);
					intent.putExtras(bundle);

					startActivity(intent);
					finish();
				}
			}
		});
		ImageView backButton = (ImageView) findViewById(R.id.add_open_image_back_button);
		backButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				timerHandler.removeCallbacks(timerRunnable);
				finish();
			}
		});
	}

	/***Selects images from the class specified in the parameters
	 * queries to see the images the user hasn't solved yet
	 * starts the next intent based on the query*/
	public void AdditionalPuzzlesSelector(String pClass){
		ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(pClass);
		query.whereNotEqualTo(ParseConstants.KEY_COMPLETED_USERS, 
				ParseUser.getCurrentUser().getObjectId());
		query.addAscendingOrder(ParseConstants.KEY_CREATED_AT);
		query.findInBackground(new FindCallback<ParseObject>(){
			@Override
			public void done(List<ParseObject> messages, ParseException e) {
				if(e==null){
					//Messages are available
					additionalPuzzles = messages;
					ParseObject message = additionalPuzzles.get(puzzleCounter);
					String messageID = message.getObjectId();
					String firstCaption = message.getString(ParseConstants.CAPTION_FIRST);
					String secondCaption = message.getString(ParseConstants.CAPTION_SECOND);

					ParseFile file = message.getParseFile(ParseConstants.KEY_PHOTO);
					Uri fileUri = Uri.parse(file.getUrl());
					Intent intent = new Intent(getBaseContext(), AdditionalPuzzlesOpenImage.class);
					intent.setData(fileUri);
					intent.putExtra(ParseConstants.KEY_PUZZLE_COUNTER, puzzleCounter);
					intent.putExtra(ParseConstants.KEY_MESSAGE_ID, messageID);
					intent.putExtra(ParseConstants.CAPTION_FIRST, firstCaption);
					intent.putExtra(ParseConstants.CAPTION_SECOND, secondCaption);
					intent.putExtra(ParseConstants.KEY_PUZZLE_CLASS, puzzleClass);
					startActivity(intent);
					finish();
				}

			}
		});
	}

	/**Set up for the timer*/
	//	TextView timerTextView;
	long startTime = 0;
	int seconds;
	int minutes;
	//runs without a timer by reposting this handler at the end of the runnable
	Handler timerHandler = new Handler();
	Runnable timerRunnable = new Runnable() {
		@SuppressLint("NewApi")
		@Override
		public void run() {
			long millis = System.currentTimeMillis() - startTime;
			seconds = (int) (millis / 1000);
			minutes = seconds / 60;
			seconds = seconds % 60;
			timerTextView.setText(String.format("%02d", seconds));
			timerTextView.setTextAlignment(6);;
			timerHandler.postDelayed(this, 500);
		}
	};


	/**Detects touch and starts timer*/
	class MyGestureDetector extends SimpleOnGestureListener {
		int timerCount = 0;
		@SuppressLint("NewApi")
		@Override
		public boolean onDown(MotionEvent e) {
			//			if(timerCount==0){
			//				startTime = System.currentTimeMillis();
			//				timerHandler.postDelayed(timerRunnable, 0);
			//				TextView timer = (TextView) findViewById(R.id.timer);
			//				timer.setBackground(getResources().getDrawable(R.drawable.timerblank));
			//				timerCount++;
			//			}
			count++;
			onTap(e);
			return false;
		}

	}


	/**Helper to swap images on imageViews*/
	private void swapImages(ImageView initial, ImageView current, int init, int finImage){
		Bitmap bitmap;
		int tmp, tmp2;
		try {
			//swap the images in initial and current imageViews
			//bitmap = BitmapFactory.decodeStream(this.openFileInput("img"+images.get(in)));
			initial.setImageBitmap(BitmapFactory.decodeStream(this.openFileInput("img"+fin)));
			current.setImageBitmap(BitmapFactory.decodeStream(this.openFileInput("img"+in)));
			//swap the numbers in arrayList for in and fin
			tmp = init;
			tmp2 = images.indexOf(finImage);
			images.set(images.indexOf(init), finImage);
			images.set(tmp2, tmp);
			checkFinish();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}


	/**Checks whether the PicSwitch is solved*/
	private void checkFinish(){
		if(images.get(0)==0&&
				images.get(1)==1&&
				images.get(2)==2&&
				images.get(3)==3&&
				images.get(4)==4&&
				images.get(5)==5&&
				images.get(6)==6&&
				images.get(7)==7&&
				images.get(8)==8){
			done();

		}
	}

	@SuppressLint("NewApi")
	/** Called when the user finishes a puzzle */
	public void done() {
		
		preciseEndTime = System.nanoTime();
		
		timerHandler.removeCallbacks(timerRunnable);

		if(TabActivity.SOUND){
			sounds.play(doneID, 1, 1, 0, 0, 1);
		}
		
		//calculate elapsed times
		long preciseSolveTime = preciseEndTime - preciseStartTime;
		double preciseSolveTimeSeconds = preciseSolveTime / 1000000000.0;

		double featuredTimeToParse = Math.ceil(preciseSolveTimeSeconds * 100000) / 100000.0;
		System.out.println("precise solve time after trunc = " + featuredTimeToParse);

		timeToDisplay = Math.ceil(preciseSolveTimeSeconds * 100) / 100.0;
		System.out.println("precise time to display = " + timeToDisplay);

		//solveQuery(getIntent()); //adds the current user to the list of users who have solved the puzzle
		Intent intent = new Intent(getBaseContext(), AdditionalPuzzlesViewImage.class);
		int time =  minutes*60 + seconds;
		solveQuery();
		statsQuery();

		if(tempSkipped == "[]" || tempSkipped == "" || tempSkipped == "[\"\"]"){
			tempSkipped = currentObjectId;
		}else{
			tempSkipped = tempSkipped + "," + currentObjectId;
		}

		System.out.println("" + tempSkipped);
		
		System.out.println("call, before + '': " + firstCaption + " ..... " + secondCaption);

//		firstCaption = firstCaption + " "; //incase there are no captions, to avoid null
//		secondCaption = secondCaption + " ";
		
		if(firstCaption == null){
			firstCaption = " ";
		}
		if(secondCaption == null){
			secondCaption = " ";
		}
		
		System.out.println("before call: " + firstCaption + " ..... " + secondCaption);
		
		Bundle bundle=new Bundle();
		bundle.putStringArray(ParseConstants.KEY_ADDITIONAL_DATA, new String[]
				{firstCaption, secondCaption, Double.toString(timeToDisplay), 
				Integer.toString(switchCounter),puzzleClass, Integer.toString(puzzleCounter),tempSkipped, currCategoryTitle});
		intent.putExtras(bundle);
		intent.putExtra(ParseConstants.KEY_TEMP_DATA, data);
		intent.putExtra(ParseConstants.KEY_CURRENT_INT, currentInt);
		startActivity(intent);
		finish();
	}

	ImageView temp = null;
	int in=100, fin=100;
	/**Sets animation when user makes first touch*/
	public void setAnimation(ImageView im, int i){
		Animation animation = AnimationUtils.loadAnimation(this, animationToRun);
		im.startAnimation(animation );
		im.setPadding(8, 8,8,8);
		im.setBackgroundColor(Color.WHITE);
		temp=im;
		in=i;
	}


	/**Resets the animation and updates the switchCounter*/
	public void secondTouch(ImageView imageView, int i){
		count=0;
		if(!(imageView.equals(temp))){
			switchCounter++;
			if(TabActivity.SOUND){
				sounds.play(switchID, 1, 1, 0, 0, 1);
			}
		}
		temp.setPadding(0,0,0,0);
		temp.clearAnimation();
		fin=i;
		swapImages(temp,imageView,in,fin);
	}
	/**Detects touches on imageViews 
	 * Shows the necessary animations
	 * Calls swap images to swap the images*/

	private void onTap(MotionEvent event){
		int firstRow = getLocation(image1)[1]+image1.getHeight(); //lower y coordinate of row 1

		//first column
		if(image1.getLeft()<event.getRawX() && image1.getLeft()+image1.getHeight()>event.getRawX()){
			//image1
			if(image1.getTop()<event.getRawY() && getLocation(image1)[1]+image1.getHeight()>event.getRawY()){
				if(count==1){
					Animation animation = AnimationUtils.loadAnimation(this, animationToRun);
					image1.startAnimation(animation );
					image1.setPadding(8, 8,8,8);
					image1.setBackgroundColor(Color.WHITE);
					temp=image1;
					in=images.get(0);
				}
				else if(count==2){
					secondTouch(image1, images.get(0));
				}
			}
			else if(image4.getTop()<event.getRawY() && getLocation(image4)[1]+image4.getHeight()>event.getRawY()){
				if(count==1){
					Animation animation = AnimationUtils.loadAnimation(this, animationToRun);
					image4.startAnimation(animation );
					image4.setPadding(8, 8,8,8);
					image4.setBackgroundColor(Color.WHITE);
					System.out.println("This is top" + image5.getTop());
					temp=image4;
					in=images.get(3);
					System.out.println("image5");
				}
				else if(count==2){
					secondTouch(image4, images.get(3));
				}
			}
			else if(image7.getTop()<event.getRawY() && getLocation(image7)[1]+image7.getHeight()>event.getRawY()){
				if(count==1){
					Animation animation = AnimationUtils.loadAnimation(this, animationToRun);
					image7.startAnimation(animation );
					image7.setPadding(8, 8,8,8);
					image7.setBackgroundColor(Color.WHITE);
					temp=image7;
					in=images.get(6);
					System.out.println("image7");
				}
				else if(count==2){
					secondTouch(image7, images.get(6));
				}
			}

		}
		//second column
		if(image2.getLeft()<event.getRawX() && image2.getLeft()+image2.getHeight()>event.getRawX()){
			//image1
			if(image2.getTop()<event.getRawY() && firstRow>event.getRawY()){
				if(count==1){
					Animation animation = AnimationUtils.loadAnimation(this, animationToRun);
					image2.startAnimation(animation );
					image2.setPadding(8, 8,8,8);
					image2.setBackgroundColor(Color.WHITE);
					temp=image2;
					in=images.get(1);
					System.out.println("image2");
				}
				else if(count==2){
					secondTouch(image2, images.get(1));
				}
			}
			else if(image5.getTop()<event.getRawY() && getLocation(image5)[1]+image5.getHeight()>event.getRawY()){
				if(count==1){
					Animation animation = AnimationUtils.loadAnimation(this, animationToRun);
					image5.startAnimation(animation );
					image5.setPadding(8, 8,8,8);
					image5.setBackgroundColor(Color.WHITE);
					System.out.println("This is top" + image5.getTop());
					temp=image5;
					in=images.get(4);
					System.out.println("image5");
				}
				else if(count==2){
					secondTouch(image5, images.get(4));
				}
			}
			else if(image8.getTop()<event.getRawY() && getLocation(image8)[1]+image8.getHeight()>event.getRawY()){
				if(count==1){
					Animation animation = AnimationUtils.loadAnimation(this, animationToRun);
					image8.startAnimation(animation );
					image8.setPadding(8, 8,8,8);
					image8.setBackgroundColor(Color.WHITE);
					temp=image8;
					in=images.get(7);
					System.out.println("image8");
				}
				else if(count==2){
					secondTouch(image8, images.get(7));
				}
			}

		}
		//third column
		if(image3.getLeft()<event.getRawX() && image3.getLeft()+image3.getHeight()>event.getRawX()){
			//image1
			if(image3.getTop()<event.getRawY() && firstRow>event.getRawY()){
				if(count==1){
					Animation animation = AnimationUtils.loadAnimation(this, animationToRun);
					image3.startAnimation(animation );
					image3.setPadding(8, 8,8,8);
					image3.setBackgroundColor(Color.WHITE);
					temp=image3;
					in=images.get(2);
					System.out.println("image3");
				}
				else if(count==2){
					secondTouch(image3, images.get(2));
				}
			}
			else if(image6.getTop()<event.getRawY() && getLocation(image6)[1]+image6.getHeight()>event.getRawY()){
				if(count==1){
					Animation animation = AnimationUtils.loadAnimation(this, animationToRun);
					image6.startAnimation(animation );
					image6.setPadding(8, 8,8,8);
					image6.setBackgroundColor(Color.WHITE);
					System.out.println("This is top" + image6.getTop());
					temp=image6;
					in=images.get(5);
					System.out.println("image6");
				}
				else if(count==2){
					secondTouch(image6, images.get(5));
				}
			}
			else if(image9.getTop()<event.getRawY() && getLocation(image9)[1]+image9.getHeight()>event.getRawY()){
				if(count==1){
					Animation animation = AnimationUtils.loadAnimation(this, animationToRun);
					image9.startAnimation(animation );
					image9.setPadding(8, 8,8,8);
					image9.setBackgroundColor(Color.WHITE);
					temp=image9;
					in=images.get(8);
					System.out.println("image7");
				}
				else if(count==2){
					secondTouch(image9, images.get(8));
				}
			}

		}
	}




	private int[] getLocation(View mView) {
		Rect mRect = new Rect();
		int[] location = new int[2];

		mView.getLocationOnScreen(location);

		mRect.left = location[0];
		mRect.top = location[1];
		mRect.right = location[0] + mView.getWidth();
		mRect.bottom = location[1] + mView.getHeight();

		return location;
	}







	ArrayList<Bitmap> chunkedImages; //saves the chunked images

	/**method to split the image in imageView*/
	private void splitImage(Bitmap b, int chunkNumbers) {    
		Bitmap bitmap = b;

		//For the number of rows and columns of the grid to be displayed
		int rows,cols;

		//For height and width of the small image chunks 
		int chunkHeight,chunkWidth;

		//To store all the small image chunks in bitmap format in this list 
		chunkedImages = new ArrayList<Bitmap>(chunkNumbers);

		//Getting the scaled bitmap of the source image
		Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), true);

		rows = cols = (int) Math.sqrt(chunkNumbers);
		chunkHeight = bitmap.getHeight()/rows;
		chunkWidth = bitmap.getWidth()/cols;


		//xCoord and yCoord are the pixel positions of the image chunks
		int yCoord = 0;
		for(int x=0; x<rows; x++){
			int xCoord = 0;
			for(int y=0; y<cols; y++){
				chunkedImages.add(Bitmap.createBitmap(scaledBitmap, xCoord, yCoord, chunkWidth, chunkHeight));

				xCoord += chunkWidth;
			}
			yCoord += chunkHeight;
		}
		createImageHelper(chunkedImages);
	}

	/**Saves images to sd and starts next activity*/
	public void createImageHelper(ArrayList<Bitmap> bitmap){
		for(int i=0;i<9;i++){
			createImageFromBitmap(bitmap.get(i), "img"+i);
		}
		try {
			img0= BitmapFactory.decodeStream(this.openFileInput("img0"));
			img1= BitmapFactory.decodeStream(this.openFileInput("img1"));
			img2= BitmapFactory.decodeStream(this.openFileInput("img2"));
			img3= BitmapFactory.decodeStream(this.openFileInput("img3"));
			img4= BitmapFactory.decodeStream(this.openFileInput("img4"));
			img5= BitmapFactory.decodeStream(this.openFileInput("img5"));
			img6= BitmapFactory.decodeStream(this.openFileInput("img6"));
			img7= BitmapFactory.decodeStream(this.openFileInput("img7"));
			img8= BitmapFactory.decodeStream(this.openFileInput("img8"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}